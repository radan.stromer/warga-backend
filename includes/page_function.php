<?php
// class pagination for branches
class Pagination{
		
	// function for checking the data position
	function searchPosition($limit){
		if(empty($_GET['page'])){
			$position = 0;
			$_GET['page'] = 1;
		}
		else{
			$position = ($_GET['page']-1) * $limit;
		}
		return $position;
	}
		
	// function for count the total page
	function amountPage($amountData, $limit){
		$amountPage = ceil($amountData/$limit);
		return $amountPage;
	}
		
	// function for page link 1,2.3 
	function navPage($activePage, $amountPage){
		$link_page = "";
		
		// link to first page and prev
		if($activePage > 1){
			$prev = $activePage-1;
			$link_page .= "<span class=prevnext><a href=users.php?page=1><< First</a></span> 
		                    <span class=prevnext><a href=users.php?page=$prev>< Prev</a></span> ";
		}
		else{ 
			$link_page .= "<span class=disabled><< First</span> <span class=disabled>< Prev</span>";
		}
		
		// page link 1,2,3, ...
		$numeric = ($activePage > 3 ? " ... " : " "); 
		for ($i=$activePage-2; $i<$activePage; $i++){
		if ($i < 1)
			continue;
			$numeric .= "<a href=users.php?page=$i>$i</a>";
		}
		$numeric .= " <span class=current>$activePage</span> ";
		
		for($i=$activePage+1; $i<($activePage+3); $i++){
			if($i > $amountPage)
				break;
				$numeric .= "<a href=users.php?page=$i>$i</a>";
	    }
	    $numeric .= ($activePage+2<$amountPage ? " ... <a href=users.php?page=$amountPage>$amountPage</a> " : " ");
	    
	    $link_page .= "$numeric";
	    
	    // Link to the next page and the last page
	    if($activePage < $amountPage){
			$next = $activePage+1;
			$link_page .= " <span class=prevnext><a href=users.php?page=$next>Next ></a></span> 
		                     <span class=prevnext><a href=users.php?page=$amountPage>Last >></a></span> ";
		}
		else{
			$link_page .= "<span class=disabled>Next ></span><span class=disabled>Last >></span>";
		}
		
		return $link_page;
	}
}

// class pagination for funeral
class PaginationFuneral{
		
	// function for checking the data position
	function searchPosition($limit){
		if(empty($_GET['page'])){
			$position = 0;
			$_GET['page'] = 1;
		}
		else{
			$position = ($_GET['page']-1) * $limit;
		}
		return $position;
	}
		
	// function for count the total page
	function amountPage($amountData, $limit){
		$amountPage = ceil($amountData/$limit);
		return $amountPage;
	}
		
	// function for page link 1,2.3 
	function navPage($activePage, $amountPage){
		$link_page = "";
		
		// link to first page and prev
		if($activePage > 1){
			$prev = $activePage-1;
			$link_page .= "<span class=prevnext><a href=funeral.php?page=1><< First</a></span> 
		                    <span class=prevnext><a href=funeral.php?page=$prev>< Prev</a></span> ";
		}
		else{ 
			$link_page .= "<span class=disabled><< First</span> <span class=disabled>< Prev</span>";
		}
		
		// page link 1,2,3, ...
		$numeric = ($activePage > 3 ? " ... " : " "); 
		for ($i=$activePage-2; $i<$activePage; $i++){
		if ($i < 1)
			continue;
			$numeric .= "<a href=funeral.php?page=$i>$i</a>";
		}
		$numeric .= " <span class=current>$activePage</span> ";
		
		for($i=$activePage+1; $i<($activePage+3); $i++){
			if($i > $amountPage)
				break;
				$numeric .= "<a href=funeral.php?page=$i>$i</a>";
	    }
	    $numeric .= ($activePage+2<$amountPage ? " ... <a href=funeral.php?page=$amountPage>$amountPage</a> " : " ");
	    
	    $link_page .= "$numeric";
	    
	    // Link to the next page and the last page
	    if($activePage < $amountPage){
			$next = $activePage+1;
			$link_page .= " <span class=prevnext><a href=funeral.php?page=$next>Next ></a></span> 
		                     <span class=prevnext><a href=funeral.php?page=$amountPage>Last >></a></span> ";
		}
		else{
			$link_page .= "<span class=disabled>Next ></span><span class=disabled>Last >></span>";
		}
		
		return $link_page;
	}
}

// class pagination for Babtis
class PaginationBabtis{
		
	// function for checking the data position
	function searchPosition($limit){
		if(empty($_GET['page'])){
			$position = 0;
			$_GET['page'] = 1;
		}
		else{
			$position = ($_GET['page']-1) * $limit;
		}
		return $position;
	}
		
	// function for count the total page
	function amountPage($amountData, $limit){
		$amountPage = ceil($amountData/$limit);
		return $amountPage;
	}
		
	// function for page link 1,2.3 
	function navPage($activePage, $amountPage){
		$link_page = "";
		
		// link to first page and prev
		if($activePage > 1){
			$prev = $activePage-1;
			$link_page .= "<span class=prevnext><a href=babtis.php?page=1><< First</a></span> 
		                    <span class=prevnext><a href=babtis.php?page=$prev>< Prev</a></span> ";
		}
		else{ 
			$link_page .= "<span class=disabled><< First</span> <span class=disabled>< Prev</span>";
		}
		
		// page link 1,2,3, ...
		$numeric = ($activePage > 3 ? " ... " : " "); 
		for ($i=$activePage-2; $i<$activePage; $i++){
		if ($i < 1)
			continue;
			$numeric .= "<a href=babtis.php?page=$i>$i</a>";
		}
		$numeric .= " <span class=current>$activePage</span> ";
		
		for($i=$activePage+1; $i<($activePage+3); $i++){
			if($i > $amountPage)
				break;
				$numeric .= "<a href=babtis.php?page=$i>$i</a>";
	    }
	    $numeric .= ($activePage+2<$amountPage ? " ... <a href=babtis.php?page=$amountPage>$amountPage</a> " : " ");
	    
	    $link_page .= "$numeric";
	    
	    // Link to the next page and the last page
	    if($activePage < $amountPage){
			$next = $activePage+1;
			$link_page .= " <span class=prevnext><a href=babtis.php?page=$next>Next ></a></span> 
		                     <span class=prevnext><a href=babtis.php?page=$amountPage>Last >></a></span> ";
		}
		else{
			$link_page .= "<span class=disabled>Next ></span><span class=disabled>Last >></span>";
		}
		
		return $link_page;
	}
}

// class pagination for Job
class PaginationJob{
		
	// function for checking the data position
	function searchPosition($limit){
		if(empty($_GET['page'])){
			$position = 0;
			$_GET['page'] = 1;
		}
		else{
			$position = ($_GET['page']-1) * $limit;
		}
		return $position;
	}
		
	// function for count the total page
	function amountPage($amountData, $limit){
		$amountPage = ceil($amountData/$limit);
		return $amountPage;
	}
		
	// function for page link 1,2.3 
	function navPage($activePage, $amountPage){
		$link_page = "";
		
		// link to first page and prev
		if($activePage > 1){
			$prev = $activePage-1;
			$link_page .= "<span class=prevnext><a href=jobs.php?page=1><< First</a></span> 
		                    <span class=prevnext><a href=jobs.php?page=$prev>< Prev</a></span> ";
		}
		else{ 
			$link_page .= "<span class=disabled><< First</span> <span class=disabled>< Prev</span>";
		}
		
		// page link 1,2,3, ...
		$numeric = ($activePage > 3 ? " ... " : " "); 
		for ($i=$activePage-2; $i<$activePage; $i++){
		if ($i < 1)
			continue;
			$numeric .= "<a href=jobs.php?page=$i>$i</a>";
		}
		$numeric .= " <span class=current>$activePage</span> ";
		
		for($i=$activePage+1; $i<($activePage+3); $i++){
			if($i > $amountPage)
				break;
				$numeric .= "<a href=jobs.php?page=$i>$i</a>";
	    }
	    $numeric .= ($activePage+2<$amountPage ? " ... <a href=jobs.php?page=$amountPage>$amountPage</a> " : " ");
	    
	    $link_page .= "$numeric";
	    
	    // Link to the next page and the last page
	    if($activePage < $amountPage){
			$next = $activePage+1;
			$link_page .= " <span class=prevnext><a href=jobs.php?page=$next>Next ></a></span> 
		                     <span class=prevnext><a href=jobs.php?page=$amountPage>Last >></a></span> ";
		}
		else{
			$link_page .= "<span class=disabled>Next ></span><span class=disabled>Last >></span>";
		}
		
		return $link_page;
	}
}

// class pagination for Grade
class PaginationGrade{
		
	// function for checking the data position
	function searchPosition($limit){
		if(empty($_GET['page'])){
			$position = 0;
			$_GET['page'] = 1;
		}
		else{
			$position = ($_GET['page']-1) * $limit;
		}
		return $position;
	}
		
	// function for count the total page
	function amountPage($amountData, $limit){
		$amountPage = ceil($amountData/$limit);
		return $amountPage;
	}
		
	// function for page link 1,2.3 
	function navPage($activePage, $amountPage){
		$link_page = "";
		
		// link to first page and prev
		if($activePage > 1){
			$prev = $activePage-1;
			$link_page .= "<span class=prevnext><a href=grade.php?page=1><< First</a></span> 
		                    <span class=prevnext><a href=grade.php?page=$prev>< Prev</a></span> ";
		}
		else{ 
			$link_page .= "<span class=disabled><< First</span> <span class=disabled>< Prev</span>";
		}
		
		// page link 1,2,3, ...
		$numeric = ($activePage > 3 ? " ... " : " "); 
		for ($i=$activePage-2; $i<$activePage; $i++){
		if ($i < 1)
			continue;
			$numeric .= "<a href=grade.php?page=$i>$i</a>";
		}
		$numeric .= " <span class=current>$activePage</span> ";
		
		for($i=$activePage+1; $i<($activePage+3); $i++){
			if($i > $amountPage)
				break;
				$numeric .= "<a href=grade.php?page=$i>$i</a>";
	    }
	    $numeric .= ($activePage+2<$amountPage ? " ... <a href=grade.php?page=$amountPage>$amountPage</a> " : " ");
	    
	    $link_page .= "$numeric";
	    
	    // Link to the next page and the last page
	    if($activePage < $amountPage){
			$next = $activePage+1;
			$link_page .= " <span class=prevnext><a href=grade.php?page=$next>Next ></a></span> 
		                     <span class=prevnext><a href=grade.php?page=$amountPage>Last >></a></span> ";
		}
		else{
			$link_page .= "<span class=disabled>Next ></span><span class=disabled>Last >></span>";
		}
		
		return $link_page;
	}
}

// class pagination for Komisi Periode
class PaginationKomisiPeriode{
		
	// function for checking the data position
	function searchPosition($limit){
		if(empty($_GET['page'])){
			$position = 0;
			$_GET['page'] = 1;
		}
		else{
			$position = ($_GET['page']-1) * $limit;
		}
		return $position;
	}
		
	// function for count the total page
	function amountPage($amountData, $limit){
		$amountPage = ceil($amountData/$limit);
		return $amountPage;
	}
		
	// function for page link 1,2.3 
	function navPage($activePage, $amountPage){
		$link_page = "";
		
		// link to first page and prev
		if($activePage > 1){
			$prev = $activePage-1;
			$link_page .= "<span class=prevnext><a href=periode.php?page=1><< First</a></span> 
		                    <span class=prevnext><a href=periode.php?page=$prev>< Prev</a></span> ";
		}
		else{ 
			$link_page .= "<span class=disabled><< First</span> <span class=disabled>< Prev</span>";
		}
		
		// page link 1,2,3, ...
		$numeric = ($activePage > 3 ? " ... " : " "); 
		for ($i=$activePage-2; $i<$activePage; $i++){
		if ($i < 1)
			continue;
			$numeric .= "<a href=periode.php?page=$i>$i</a>";
		}
		$numeric .= " <span class=current>$activePage</span> ";
		
		for($i=$activePage+1; $i<($activePage+3); $i++){
			if($i > $amountPage)
				break;
				$numeric .= "<a href=periode.php?page=$i>$i</a>";
	    }
	    $numeric .= ($activePage+2<$amountPage ? " ... <a href=periode.php?page=$amountPage>$amountPage</a> " : " ");
	    
	    $link_page .= "$numeric";
	    
	    // Link to the next page and the last page
	    if($activePage < $amountPage){
			$next = $activePage+1;
			$link_page .= " <span class=prevnext><a href=periode.php?page=$next>Next ></a></span> 
		                     <span class=prevnext><a href=periode.php?page=$amountPage>Last >></a></span> ";
		}
		else{
			$link_page .= "<span class=disabled>Next ></span><span class=disabled>Last >></span>";
		}
		
		return $link_page;
	}
}

// class pagination for Komisi
class PaginationKomisi{
		
	// function for checking the data position
	function searchPosition($limit){
		if(empty($_GET['page'])){
			$position = 0;
			$_GET['page'] = 1;
		}
		else{
			$position = ($_GET['page']-1) * $limit;
		}
		return $position;
	}
		
	// function for count the total page
	function amountPage($amountData, $limit){
		$amountPage = ceil($amountData/$limit);
		return $amountPage;
	}
		
	// function for page link 1,2.3 
	function navPage($activePage, $amountPage){
		$link_page = "";
		
		// link to first page and prev
		if($activePage > 1){
			$prev = $activePage-1;
			$link_page .= "<span class=prevnext><a href=komisi.php?page=1><< First</a></span> 
		                    <span class=prevnext><a href=komisi.php?page=$prev>< Prev</a></span> ";
		}
		else{ 
			$link_page .= "<span class=disabled><< First</span> <span class=disabled>< Prev</span>";
		}
		
		// page link 1,2,3, ...
		$numeric = ($activePage > 3 ? " ... " : " "); 
		for ($i=$activePage-2; $i<$activePage; $i++){
		if ($i < 1)
			continue;
			$numeric .= "<a href=komisi.php?page=$i>$i</a>";
		}
		$numeric .= " <span class=current>$activePage</span> ";
		
		for($i=$activePage+1; $i<($activePage+3); $i++){
			if($i > $amountPage)
				break;
				$numeric .= "<a href=komisi.php?page=$i>$i</a>";
	    }
	    $numeric .= ($activePage+2<$amountPage ? " ... <a href=komisi.php?page=$amountPage>$amountPage</a> " : " ");
	    
	    $link_page .= "$numeric";
	    
	    // Link to the next page and the last page
	    if($activePage < $amountPage){
			$next = $activePage+1;
			$link_page .= " <span class=prevnext><a href=komisi.php?page=$next>Next ></a></span> 
		                     <span class=prevnext><a href=komisi.php?page=$amountPage>Last >></a></span> ";
		}
		else{
			$link_page .= "<span class=disabled>Next ></span><span class=disabled>Last >></span>";
		}
		
		return $link_page;
	}
}

// class pagination for Komisi Anggota
class PaginationKomisiAnggota{
		
	// function for checking the data position
	function searchPosition($limit){
		if(empty($_GET['page'])){
			$position = 0;
			$_GET['page'] = 1;
		}
		else{
			$position = ($_GET['page']-1) * $limit;
		}
		return $position;
	}
		
	// function for count the total page
	function amountPage($amountData, $limit){
		$amountPage = ceil($amountData/$limit);
		return $amountPage;
	}
		
	// function for page link 1,2.3 
	function navPage($activePage, $amountPage){
		$link_page = "";
		
		// link to first page and prev
		if($activePage > 1){
			$prev = $activePage-1;
			$link_page .= "<span class=prevnext><a href=anggota_komisi.php?page=1><< First</a></span> 
		                    <span class=prevnext><a href=anggota_komisi.php?page=$prev>< Prev</a></span> ";
		}
		else{ 
			$link_page .= "<span class=disabled><< First</span> <span class=disabled>< Prev</span>";
		}
		
		// page link 1,2,3, ...
		$numeric = ($activePage > 3 ? " ... " : " "); 
		for ($i=$activePage-2; $i<$activePage; $i++){
		if ($i < 1)
			continue;
			$numeric .= "<a href=anggota_komisi.php?page=$i>$i</a>";
		}
		$numeric .= " <span class=current>$activePage</span> ";
		
		for($i=$activePage+1; $i<($activePage+3); $i++){
			if($i > $amountPage)
				break;
				$numeric .= "<a href=anggota_komisi.php?page=$i>$i</a>";
	    }
	    $numeric .= ($activePage+2<$amountPage ? " ... <a href=anggota_komisi.php?page=$amountPage>$amountPage</a> " : " ");
	    
	    $link_page .= "$numeric";
	    
	    // Link to the next page and the last page
	    if($activePage < $amountPage){
			$next = $activePage+1;
			$link_page .= " <span class=prevnext><a href=anggota_komisi.php?page=$next>Next ></a></span> 
		                     <span class=prevnext><a href=anggota_komisi.php?page=$amountPage>Last >></a></span> ";
		}
		else{
			$link_page .= "<span class=disabled>Next ></span><span class=disabled>Last >></span>";
		}
		
		return $link_page;
	}
}

// class pagination for Majelis Anggota
class PaginationPeriodeMajelis{
		
	// function for checking the data position
	function searchPosition($limit){
		if(empty($_GET['page'])){
			$position = 0;
			$_GET['page'] = 1;
		}
		else{
			$position = ($_GET['page']-1) * $limit;
		}
		return $position;
	}
		
	// function for count the total page
	function amountPage($amountData, $limit){
		$amountPage = ceil($amountData/$limit);
		return $amountPage;
	}
		
	// function for page link 1,2.3 
	function navPage($activePage, $amountPage){
		$link_page = "";
		
		// link to first page and prev
		if($activePage > 1){
			$prev = $activePage-1;
			$link_page .= "<span class=prevnext><a href=periode_majelis.php?page=1><< First</a></span> 
		                    <span class=prevnext><a href=periode_majelis.php?page=$prev>< Prev</a></span> ";
		}
		else{ 
			$link_page .= "<span class=disabled><< First</span> <span class=disabled>< Prev</span>";
		}
		
		// page link 1,2,3, ...
		$numeric = ($activePage > 3 ? " ... " : " "); 
		for ($i=$activePage-2; $i<$activePage; $i++){
		if ($i < 1)
			continue;
			$numeric .= "<a href=periode_majelis.php?page=$i>$i</a>";
		}
		$numeric .= " <span class=current>$activePage</span> ";
		
		for($i=$activePage+1; $i<($activePage+3); $i++){
			if($i > $amountPage)
				break;
				$numeric .= "<a href=periode_majelis.php?page=$i>$i</a>";
	    }
	    $numeric .= ($activePage+2<$amountPage ? " ... <a href=periode_majelis.php?page=$amountPage>$amountPage</a> " : " ");
	    
	    $link_page .= "$numeric";
	    
	    // Link to the next page and the last page
	    if($activePage < $amountPage){
			$next = $activePage+1;
			$link_page .= " <span class=prevnext><a href=periode_majelis.php?page=$next>Next ></a></span> 
		                     <span class=prevnext><a href=periode_majelis.php?page=$amountPage>Last >></a></span> ";
		}
		else{
			$link_page .= "<span class=disabled>Next ></span><span class=disabled>Last >></span>";
		}
		
		return $link_page;
	}
}

// class pagination for Majelis
class PaginationMajelis{
		
	// function for checking the data position
	function searchPosition($limit){
		if(empty($_GET['page'])){
			$position = 0;
			$_GET['page'] = 1;
		}
		else{
			$position = ($_GET['page']-1) * $limit;
		}
		return $position;
	}
		
	// function for count the total page
	function amountPage($amountData, $limit){
		$amountPage = ceil($amountData/$limit);
		return $amountPage;
	}
		
	// function for page link 1,2.3 
	function navPage($activePage, $amountPage){
		$link_page = "";
		
		// link to first page and prev
		if($activePage > 1){
			$prev = $activePage-1;
			$link_page .= "<span class=prevnext><a href=majelis.php?page=1><< First</a></span> 
		                    <span class=prevnext><a href=majelis.php?page=$prev>< Prev</a></span> ";
		}
		else{ 
			$link_page .= "<span class=disabled><< First</span> <span class=disabled>< Prev</span>";
		}
		
		// page link 1,2,3, ...
		$numeric = ($activePage > 3 ? " ... " : " "); 
		for ($i=$activePage-2; $i<$activePage; $i++){
		if ($i < 1)
			continue;
			$numeric .= "<a href=majelis.php?page=$i>$i</a>";
		}
		$numeric .= " <span class=current>$activePage</span> ";
		
		for($i=$activePage+1; $i<($activePage+3); $i++){
			if($i > $amountPage)
				break;
				$numeric .= "<a href=majelis.php?page=$i>$i</a>";
	    }
	    $numeric .= ($activePage+2<$amountPage ? " ... <a href=majelis.php?page=$amountPage>$amountPage</a> " : " ");
	    
	    $link_page .= "$numeric";
	    
	    // Link to the next page and the last page
	    if($activePage < $amountPage){
			$next = $activePage+1;
			$link_page .= " <span class=prevnext><a href=majelis.php?page=$next>Next ></a></span> 
		                     <span class=prevnext><a href=majelis.php?page=$amountPage>Last >></a></span> ";
		}
		else{
			$link_page .= "<span class=disabled>Next ></span><span class=disabled>Last >></span>";
		}
		
		return $link_page;
	}
}

// class pagination for Majelis
class PaginationMajelisAnggota{
		
	// function for checking the data position
	function searchPosition($limit){
		if(empty($_GET['page'])){
			$position = 0;
			$_GET['page'] = 1;
		}
		else{
			$position = ($_GET['page']-1) * $limit;
		}
		return $position;
	}
		
	// function for count the total page
	function amountPage($amountData, $limit){
		$amountPage = ceil($amountData/$limit);
		return $amountPage;
	}
		
	// function for page link 1,2.3 
	function navPage($activePage, $amountPage){
		$link_page = "";
		
		// link to first page and prev
		if($activePage > 1){
			$prev = $activePage-1;
			$link_page .= "<span class=prevnext><a href=anggota_majelis.php?page=1><< First</a></span> 
		                    <span class=prevnext><a href=anggota_majelis.php?page=$prev>< Prev</a></span> ";
		}
		else{ 
			$link_page .= "<span class=disabled><< First</span> <span class=disabled>< Prev</span>";
		}
		
		// page link 1,2,3, ...
		$numeric = ($activePage > 3 ? " ... " : " "); 
		for ($i=$activePage-2; $i<$activePage; $i++){
		if ($i < 1)
			continue;
			$numeric .= "<a href=anggota_majelis.php?page=$i>$i</a>";
		}
		$numeric .= " <span class=current>$activePage</span> ";
		
		for($i=$activePage+1; $i<($activePage+3); $i++){
			if($i > $amountPage)
				break;
				$numeric .= "<a href=anggota_majelis.php?page=$i>$i</a>";
	    }
	    $numeric .= ($activePage+2<$amountPage ? " ... <a href=anggota_majelis.php?page=$amountPage>$amountPage</a> " : " ");
	    
	    $link_page .= "$numeric";
	    
	    // Link to the next page and the last page
	    if($activePage < $amountPage){
			$next = $activePage+1;
			$link_page .= " <span class=prevnext><a href=anggota_majelis.php?page=$next>Next ></a></span> 
		                     <span class=prevnext><a href=anggota_majelis.php?page=$amountPage>Last >></a></span> ";
		}
		else{
			$link_page .= "<span class=disabled>Next ></span><span class=disabled>Last >></span>";
		}
		
		return $link_page;
	}
}

// class pagination for Staff
class PaginationStaff{
		
	// function for checking the data position
	function searchPosition($limit){
		if(empty($_GET['page'])){
			$position = 0;
			$_GET['page'] = 1;
		}
		else{
			$position = ($_GET['page']-1) * $limit;
		}
		return $position;
	}
		
	// function for count the total page
	function amountPage($amountData, $limit){
		$amountPage = ceil($amountData/$limit);
		return $amountPage;
	}
		
	// function for page link 1,2.3 
	function navPage($activePage, $amountPage){
		$link_page = "";
		
		// link to first page and prev
		if($activePage > 1){
			$prev = $activePage-1;
			$link_page .= "<span class=prevnext><a href=staff.php?page=1><< First</a></span> 
		                    <span class=prevnext><a href=staff.php?page=$prev>< Prev</a></span> ";
		}
		else{ 
			$link_page .= "<span class=disabled><< First</span> <span class=disabled>< Prev</span>";
		}
		
		// page link 1,2,3, ...
		$numeric = ($activePage > 3 ? " ... " : " "); 
		for ($i=$activePage-2; $i<$activePage; $i++){
		if ($i < 1)
			continue;
			$numeric .= "<a href=staff.php?page=$i>$i</a>";
		}
		$numeric .= " <span class=current>$activePage</span> ";
		
		for($i=$activePage+1; $i<($activePage+3); $i++){
			if($i > $amountPage)
				break;
				$numeric .= "<a href=staff.php?page=$i>$i</a>";
	    }
	    $numeric .= ($activePage+2<$amountPage ? " ... <a href=staff.php?page=$amountPage>$amountPage</a> " : " ");
	    
	    $link_page .= "$numeric";
	    
	    // Link to the next page and the last page
	    if($activePage < $amountPage){
			$next = $activePage+1;
			$link_page .= " <span class=prevnext><a href=staff.php?page=$next>Next ></a></span> 
		                     <span class=prevnext><a href=staff.php?page=$amountPage>Last >></a></span> ";
		}
		else{
			$link_page .= "<span class=disabled>Next ></span><span class=disabled>Last >></span>";
		}
		
		return $link_page;
	}
}

// class pagination for Pendeta
class PaginationPendeta{
		
	// function for checking the data position
	function searchPosition($limit){
		if(empty($_GET['page'])){
			$position = 0;
			$_GET['page'] = 1;
		}
		else{
			$position = ($_GET['page']-1) * $limit;
		}
		return $position;
	}
		
	// function for count the total page
	function amountPage($amountData, $limit){
		$amountPage = ceil($amountData/$limit);
		return $amountPage;
	}
		
	// function for page link 1,2.3 
	function navPage($activePage, $amountPage){
		$link_page = "";
		
		// link to first page and prev
		if($activePage > 1){
			$prev = $activePage-1;
			$link_page .= "<span class=prevnext><a href=pendeta.php?page=1><< First</a></span> 
		                    <span class=prevnext><a href=pendeta.php?page=$prev>< Prev</a></span> ";
		}
		else{ 
			$link_page .= "<span class=disabled><< First</span> <span class=disabled>< Prev</span>";
		}
		
		// page link 1,2,3, ...
		$numeric = ($activePage > 3 ? " ... " : " "); 
		for ($i=$activePage-2; $i<$activePage; $i++){
		if ($i < 1)
			continue;
			$numeric .= "<a href=pendeta.php?page=$i>$i</a>";
		}
		$numeric .= " <span class=current>$activePage</span> ";
		
		for($i=$activePage+1; $i<($activePage+3); $i++){
			if($i > $amountPage)
				break;
				$numeric .= "<a href=pendeta.php?page=$i>$i</a>";
	    }
	    $numeric .= ($activePage+2<$amountPage ? " ... <a href=pendeta.php?page=$amountPage>$amountPage</a> " : " ");
	    
	    $link_page .= "$numeric";
	    
	    // Link to the next page and the last page
	    if($activePage < $amountPage){
			$next = $activePage+1;
			$link_page .= " <span class=prevnext><a href=pendeta.php?page=$next>Next ></a></span> 
		                     <span class=prevnext><a href=pendeta.php?page=$amountPage>Last >></a></span> ";
		}
		else{
			$link_page .= "<span class=disabled>Next ></span><span class=disabled>Last >></span>";
		}
		
		return $link_page;
	}
}
?>