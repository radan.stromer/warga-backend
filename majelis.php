<?php
// include header
include "header.php";
// set the tpl page
$page = "majelis.tpl";

// if session is null, showing up the text and exit
if ($_SESSION['username'] == '' && $_SESSION['password'] == '')
{
	// show up the text and exit
	echo "You have not authorization for access the modules.";
	exit();
}

else 
{
	// get variable
	$module = $_GET['module'];
	$act = $_GET['act'];
	
	// if module is majelis and action is input
	if ($module == 'majelis' && $act == 'input')
	{
		// change each value to variable name
		$createdDate = date('Y-m-d H:i:s');
		$periode_id = $_POST['periode_id'];
		$nama_majelis = $_POST['nama_majelis'];
		$status = $_POST['status'];
		$userID = $_SESSION['userID'];
		
		// save into database
		$queryMajelis = "INSERT INTO as_majelis (majelis_periode_id,nama_majelis,status,created_date,created_userid,modified_date,modified_userid)
		VALUES('$periode_id','$nama_majelis','$status','$created_date','$userID','','')";
		mysqli_query($connect, $queryMajelis);
		
		// redirect to the main majelis page
		header("Location: majelis.php?code=1");
	} // close bracket
	
	// if module is majelis and action is add
	elseif ($module == 'majelis' && $act == 'add')
	{
		$queryPeriode = "SELECT * FROM as_majelis_periode WHERE status = 'Y' ORDER BY nama_periode ASC";
		$sqlPeriode = mysqli_query($connect, $queryPeriode);
		
		// fetch data
		while ($dtPeriode = mysqli_fetch_array($sqlPeriode))
		{
			$dataPeriode[] = array(	'majelis_periode_id' => $dtPeriode['majelis_periode_id'],
									'nama_periode' => $dtPeriode['nama_periode']);
		}
		
		// assign to the tpl
		$smarty->assign("dataPeriode", $dataPeriode);
	} // close bracket
	
	// if module is majelis and action is edit
	elseif ($module == 'majelis' && $act == 'edit')
	{
		$queryPeriode = "SELECT * FROM as_majelis_periode WHERE status = 'Y' ORDER BY nama_periode ASC";
		$sqlPeriode = mysqli_query($connect, $queryPeriode);
		
		// fetch data
		while ($dtPeriode = mysqli_fetch_array($sqlPeriode))
		{
			$dataPeriode[] = array(	'majelis_periode_id' => $dtPeriode['majelis_periode_id'],
									'nama_periode' => $dtPeriode['nama_periode']);
		}
		
		// assign to the tpl
		$smarty->assign("dataPeriode", $dataPeriode);
		
		// get the majelis ID
		$majelis_id = $_GET['majelis_id'];
		
		$queryMajelis = "SELECT * FROM as_majelis WHERE majelis_id = '$majelis_id'";
		$sqlMajelis = mysqli_query($connect, $queryMajelis);
		
		// fetch data
		$dataMajelis = mysqli_fetch_array($sqlMajelis);
		
		// assign data to the tpl
		$smarty->assign("majelis_id", $dataMajelis['majelis_id']);
		$smarty->assign("majelis_periode_id", $dataMajelis['majelis_periode_id']);
		$smarty->assign("nama_majelis", $dataMajelis['nama_majelis']);
		$smarty->assign("status", $dataMajelis['status']);
	} //close bracket
	
	// if module is majelis and action is update
	elseif ($module == 'majelis' && $act == 'update')
	{
		// change each value to variable name
		$modifiedDate = date('Y-m-d H:i:s');
		$periode_id = $_POST['periode_id'];
		$nama_majelis = $_POST['nama_majelis'];
		$status = $_POST['status'];
		$majelis_id = $_POST['majelis_id'];
		$userID = $_SESSION['userID'];
		
		// save into the database
		$queryMajelis = "UPDATE as_majelis SET majelis_periode_id = '$periode_id', nama_majelis = '$nama_majelis', status = '$status', modified_date = '$modifiedDate', modified_userid = '$userID' WHERE majelis_id = '$majelis_id'";
		mysqli_query($connect, $queryMajelis);
		
		// redirect to the main majelis page
		header("Location: majelis.php?code=2");
	} // close bracket
	
	// if module is majelis and action is delete
	elseif ($module == 'majelis' && $act == 'delete')
	{
		// get majelis id
		$majelis_id = $_GET['majelis_id'];
		
		// delete from the table
		$queryMajelis = "DELETE FROM as_majelis WHERE majelis_id = '$majelis_id'";
		mysqli_query($connect, $queryMajelis);
		
		// delete from the table
		$queryAnggota = "DELETE FROM as_majelis_anggota WHERE majelis_id = '$majelis_id'";
		mysqli_query($connect, $queryAnggota);
		
		// redirect to the main majelis page
		header("Location: majelis.php?code=3");
	} // close bracket
	
	// default
	else 
	{
		// create new object pagination
		$p = new PaginationMajelis;
		// limit 10 data for page
		$limit  = 10;
		$position = $p->searchPosition($limit);
		// showing up komisi data
		$queryMajelis = "SELECT A.nama_majelis, A.status, A.majelis_id, B.nama_periode FROM as_majelis A INNER JOIN as_majelis_periode B ON B.majelis_periode_id=A.majelis_periode_id ORDER BY B.nama_periode,A.majelis_id DESC LIMIT $position, $limit";
		$sqlMajelis = mysqli_query($connect, $queryMajelis);
		
		$i = 1 + $position;
		// fetch data
		while ($dtMajelis = mysqli_fetch_array($sqlMajelis))
		{
			// save data into array
			$dataMajelis[] = array(	'majelis_id' => $dtMajelis['majelis_id'],
									'nama_periode' => $dtMajelis['nama_periode'],
									'nama_majelis' => $dtMajelis['nama_majelis'],
									'status' => $dtMajelis['status'],
									'no' => $i
									);
			$i++;
		}
		
		// count data
		$queryCountMajelis = "SELECT A.nama_majelis, A.status, A.majelis_id, B.nama_periode FROM as_majelis A INNER JOIN as_majelis_periode B ON B.majelis_periode_id=A.majelis_periode_id";
		$sqlCountMajelis = mysqli_query($connect, $queryCountMajelis);
		$amountData = mysqli_num_rows($sqlCountMajelis);
		
		$amountPage = $p->amountPage($amountData, $limit);
		$pageLink = $p->navPage($_GET['page'], $amountPage);
		
		$smarty->assign("pageLink", $pageLink);
		// assign to the tpl
		$smarty->assign("dataMajelis", $dataMajelis);
		
	} // close bracket
	
	// assign code to the tpl
	$smarty->assign("code", $_GET['code']);
	$smarty->assign("module", $_GET['module']);
	$smarty->assign("act", $_GET['act']);
	
} // close bracket

// include footer
include "footer.php";
?>