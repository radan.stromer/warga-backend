<?php
// include header
include "header.php";
// set the tpl page
$page = "pendeta.tpl";

// if session is null, showing up the text and exit
if ($_SESSION['username'] == '' && $_SESSION['password'] == '')
{
	// show up the text and exit
	echo "You have not authorization for access the modules.";
	exit();
}

else 
{
	// get variable
	$module = $_GET['module'];
	$act = $_GET['act'];
	
	// if module is pendeta and action is input
	if ($module == 'pendeta' && $act == 'input')
	{
		// change each value to variable name
		$createdDate = date('Y-m-d H:i:s');
		$anggota_id = $_POST['anggota_id'];
		$jabatan = $_POST['jabatan'];
		$tanggal_tasbih = $_POST['tanggal_tasbih'];
		$status = $_POST['status'];
		$biografi = $_POST['biografi'];
		$userID = $_SESSION['userID'];
		
		$queryPendeta = "INSERT INTO as_tokoh_masyarakat (anggota_id,jabatan,tanggal_tasbih,status,biografi,created_date,created_userid,modified_date,modified_userid)
		VALUES('$anggota_id','$jabatan','$tanggal_tasbih','$status','$biografi','$createdDate','$userID','','')";
		mysqli_query($connect, $queryPendeta);
		
		// redirect to the main pendeta page
		header("Location: pendeta.php?code=1");
	} // close bracket
	
	// if module is pendeta and action is add
	elseif ($module == 'pendeta' && $act == 'add')
	{
		$queryAnggota = "SELECT individu_id, full_name FROM as_individu ORDER BY full_name ASC";
		$sqlAnggota = mysqli_query($connect, $queryAnggota);
		
		// fetch data
		while ($dtAnggota = mysqli_fetch_array($sqlAnggota))
		{
			$dataAnggota[] = array(	'anggota_id' => $dtAnggota['individu_id'],
									'full_name' => $dtAnggota['full_name']);
		}
		
		// assign to the tpl
		$smarty->assign("dataAnggota", $dataAnggota);
	} // close bracket
	
	// if module is pendeta and action is edit
	elseif ($module == 'pendeta' && $act == 'edit')
	{
		$queryAnggota = "SELECT individu_id, full_name FROM as_individu ORDER BY full_name ASC";
		$sqlAnggota = mysqli_query($connect, $queryAnggota);
		
		// fetch data
		while ($dtAnggota = mysqli_fetch_array($sqlAnggota))
		{
			$dataAnggota[] = array(	'anggota_id' => $dtAnggota['individu_id'],
									'full_name' => $dtAnggota['full_name']);
		}
		
		// assign to the tpl
		$smarty->assign("dataAnggota", $dataAnggota);
		
		// get the pendeta ID
		$pendeta_id = $_GET['pendeta_id'];
		
		$queryPendeta = "SELECT * FROM as_tokoh_masyarakat WHERE tokoh_id = '$pendeta_id'";
		$sqlPendeta = mysqli_query($connect, $queryPendeta);
		
		// fetch data
		$dataPendeta = mysqli_fetch_array($sqlPendeta);
		
		// assign data to the tpl
		$smarty->assign("tokoh_id", $dataPendeta['tokoh_id']);
		$smarty->assign("anggota_id", $dataPendeta['anggota_id']);
		$smarty->assign("jabatan", $dataPendeta['jabatan']);
		$smarty->assign("tanggal_tasbih", $dataPendeta['tanggal_tasbih']);
		$smarty->assign("status", $dataPendeta['status']);
		$smarty->assign("biografi", $dataPendeta['biografi']);
	} //close bracket
	
	// if module is pendeta and action is update
	elseif ($module == 'pendeta' && $act == 'update')
	{
		// change each value to variable name
		$modifiedDate = date('Y-m-d H:i:s');
		$pendeta_id = $_POST['pendeta_id'];
		$anggota_id = $_POST['anggota_id'];
		$jabatan = $_POST['jabatan'];
		$tanggal_tasbih = $_POST['tanggal_tasbih'];
		$status = $_POST['status'];
		$biografi = $_POST['biografi'];
		$userID = $_SESSION['userID'];
		
		$queryPendeta = "UPDATE as_tokoh_masyarakat SET anggota_id = '$anggota_id', jabatan = '$jabatan', tanggal_tasbih = '$tanggal_tasbih', status = '$status',
		biografi = '$biografi', modified_date = '$modifiedDate', modified_userid = '$userID' WHERE tokoh_id = '$pendeta_id'";
		mysqli_query($connect, $queryPendeta);
		
		// redirect to the main pendeta page
		header("Location: pendeta.php?code=2");
	} // close bracket
	
	// if module is pendeta and action is delete
	elseif ($module == 'pendeta' && $act == 'delete')
	{
		// get pendeta id
		$pendeta_id = $_GET['pendeta_id'];
		
		// delete from the table
		$queryPendeta = "DELETE FROM as_tokoh_masyarakat WHERE tokoh_id = '$pendeta_id'";
		mysqli_query($connect, $queryPendeta);
		
		// redirect to the main pendeta page
		header("Location: pendeta.php?code=3");
	} // close bracket
	
	// default
	else 
	{
		// create new object pagination
		$p = new PaginationPendeta;
		// limit 10 data for page
		$limit  = 10;
		$position = $p->searchPosition($limit);
		// showing up pendeta data
		$queryPendeta = "SELECT A.jabatan, A.tokoh_id, B.gender, A.tanggal_tasbih, A.status, B.no_induk, B.full_name FROM as_tokoh_masyarakat A INNER JOIN as_individu B ON A.anggota_id=B.individu_id ORDER BY A.tokoh_id ASC LIMIT $position, $limit";
		$sqlPendeta = mysqli_query($connect, $queryPendeta);
		
		$i = 1 + $position;
		// fetch data
		while ($dtPendeta = mysqli_fetch_array($sqlPendeta))
		{
			if ($dtPendeta['status'] == 'Y'){
				$status = "Aktif";
			}
			elseif ($dtPendeta['status'] == 'N'){
				$status = "Tidak Aktif";
			}
			
			if ($dtPendeta['tanggal_tasbih'] != '0000-00-00'){
				$tanggal_tasbih = tgl_indo($dtPendeta['tanggal_tasbih']);
			}
			else{
				$tanggal_tasbih = "-";
			}
			// save data into array
			$dataPendeta[] = array(	'pendeta_id' => $dtPendeta['tokoh_id'],
									'jabatan' => $dtPendeta['jabatan'],
									'tanggal_tasbih' => $tanggal_tasbih,
									'status' => $status,
									'gender' => $dtPendeta['gender'],
									'no_induk' => $dtPendeta['no_induk'],
									'full_name' => $dtPendeta['full_name'],
									'no' => $i
									);
			$i++;
		}
		
		// count data
		$queryCountPendeta = "SELECT A.jabatan, A.tokoh_id, A.status, B.no_induk, B.full_name FROM as_tokoh_masyarakat A INNER JOIN as_individu B ON A.anggota_id=B.individu_id";
		$sqlCountPendeta = mysqli_query($connect, $queryCountPendeta);
		$amountData = mysqli_num_rows($sqlCountPendeta);
		
		$amountPage = $p->amountPage($amountData, $limit);
		$pageLink = $p->navPage($_GET['page'], $amountPage);
		
		$smarty->assign("pageLink", $pageLink);
		// assign to the tpl
		$smarty->assign("dataPendeta", $dataPendeta);
		
	} // close bracket
	
	// assign code to the tpl
	$smarty->assign("code", $_GET['code']);
	$smarty->assign("module", $_GET['module']);
	$smarty->assign("act", $_GET['act']);
	
} // close bracket

// include footer
include "footer.php";
?>