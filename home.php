<?php
// include header
include "header.php";
// set the tpl page
$page = "home.tpl";

// assign code to the tpl
$smarty->assign("code", $_GET['code']);

$day = 6;
$now = date('Y-m-d');
$nextN = mktime(0, 0, 0, date('m'), date('d') + $day, date('Y'));
$nextDay = date('Y-m-d', $nextN);

if ($_GET['startDate'] != '' && $_GET['endDate'] != '')
{
	$smarty->assign("hari_ini", tgl_indo($_GET['startDate']));
	$smarty->assign("hari_besok", tgl_indo($_GET['endDate']));
}
else
{
	$smarty->assign("hari_ini", tgl_indo($now));
	$smarty->assign("hari_besok", tgl_indo($nextDay));
}

$i = 1;
if ($_GET['startDate'] != '' && $_GET['endDate'] != '')
{
	$queryBirthday = "SELECT datediff('$now', date_of_birth) as age, no_induk, photo, full_name, date_of_birth, status, gender FROM as_individu WHERE status = 'Y' AND date_format(date_of_birth,'%m-%d') BETWEEN date_format('$_GET[startDate]','%m-%d') AND date_format('$_GET[endDate]','%m-%d') ORDER BY date_of_birth ASC";
}
else
{
	$queryBirthday = "SELECT datediff('$now', date_of_birth) as age, no_induk, photo, full_name, date_of_birth, status, gender FROM as_individu WHERE status = 'Y' AND date_format(date_of_birth,'%m-%d') BETWEEN date_format('$now','%m-%d') AND date_format('$nextDay','%m-%d') ORDER BY date_of_birth ASC";
}

$sqlBirthday = mysqli_query($connect, $queryBirthday);
while ($dtBirthday = mysqli_fetch_array($sqlBirthday))
{
	$age = round($dtBirthday['age'] / 365); 
	
	$dataBirthday[] = array(	'no_induk' => $dtBirthday['no_induk'],
								'full_name' => $dtBirthday['full_name'],
								'photo' => $dtBirthday['photo'],
								'date_of_birth' => tgl_indo($dtBirthday['date_of_birth']),
								'status' => $dtBirthday['status'],
								'gender' => $dtBirthday['gender'],
								'age' => $age,
								'no' => $i);
	$i++;
} // close brakcet

$smarty->assign("dataBirthday", $dataBirthday);

$j = 1;
if ($_GET['startDate'] != '' && $_GET['endDate'] != '')
{
	$queryWedding = "SELECT datediff('$now', A.tanggal_nikah) as age, A.photo, A.tanggal_nikah, A.family_id, A.nik, B.full_name, B.pasangan_id FROM as_family A INNER JOIN as_individu B ON B.individu_id=A.kepala_keluarga WHERE date_format(A.tanggal_nikah,'%m-%d') BETWEEN date_format('$_GET[startDate]','%m-%d') AND date_format('$_GET[endDate]','%m-%d') ORDER BY A.tanggal_nikah ASC";
}
else
{
	$queryWedding = "SELECT datediff('$now', A.tanggal_nikah) as age, A.photo, A.tanggal_nikah, A.family_id, A.nik, B.full_name, B.pasangan_id FROM as_family A INNER JOIN as_individu B ON B.individu_id=A.kepala_keluarga WHERE date_format(A.tanggal_nikah,'%m-%d') BETWEEN date_format('$now','%m-%d') AND date_format('$nextDay','%m-%d') ORDER BY A.tanggal_nikah ASC";
}
	
$sqlWedding = mysqli_query($connect, $queryWedding);
while ($dtWedding = mysqli_fetch_array($sqlWedding))
{
	$age = round($dtWedding['age'] / 365);
	
	$dataWedding[] = array(	'nik' => $dtWedding['nik'],
							'suami' => $dtWedding['full_name'],
							'istri' => $dtWedding['pasangan_id'],
							'photo' => $dtWedding['photo'],
							'age' => $age,
							'tanggal_nikah' => tgl_indo($dtWedding['tanggal_nikah']),
							'no' => $j);
	$j++;
}

$smarty->assign("dataWedding", $dataWedding);
$smarty->assign("start1", $_GET['startDate']);
$smarty->assign("end1", $_GET['endDate']);

// include footer
include "footer.php";
?>
