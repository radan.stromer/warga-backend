<?php
date_default_timezone_set("ASIA/JAKARTA");
error_reporting(0);
session_start();
// include semua file yang dibutuhkan
include "includes/connection.php";
include "includes/debug.php";
include "includes/fungsi_indotgl.php";

// jika session login kosong
if (empty($_SESSION['username']) && empty($_SESSION['password'])){
	// arahkan ke halaman login
	header("Location: index.php?code=3");
}

else{
	ob_start();
	require ("includes/html2pdf/html2pdf.class.php");
	$filename="print_komisi_anggota.pdf";
	$content = ob_get_clean();
	$year = date('Y');
	$month = date('m');
	$date = date('d');
	$now = date('Y-m-d');
	$date_now = tgl_indo($now);
	
	// get the komisi id
	$komisi_id = $_GET['komisi_id'];
	
	$queryKomisi = "SELECT A.nama_periode, B.komisi_id, B.nama_komisi FROM as_komisi_periode A INNER JOIN as_komisi B ON B.komisi_periode_id=A.komisi_periode_id WHERE B.status = 'Y' AND B.komisi_id = '$komisi_id'";
	$sqlKomisi = mysqli_query($connect, $queryKomisi);
	
	// fetch data
	$dataKomisi = mysqli_fetch_array($sqlKomisi);
	
	$content = "<table width='100%' align='center' style='border-bottom: #000; padding-bottom: 10px;'>
					<tr valign='top'>
						<td width='80' align='right' valign='middle'><img src='images/logo.jpg' width='70'></td>
						<td width='610' style='padding-left: 10px;' valign='middle'>
							<div style='font-size: 20px; font-weight: bold; padding-bottom: 5px;'>
								CV. ASFA Solution
							</div>
							Sultan Residence H-9, Jl. Nyimas Gandasari - Kel. Jungjang - Kec. Arjawinangun - Kab. Cirebon <br>
							Telp. (0231) 8830633, Hp. 08562121141 
							Website: http://www.asfasolution.co.id, Email: info@asfasolution.co.id
						</td>
					</tr>
				</table>
				<br>
				<h4><u>ANGGOTA KOMISI</u></h4>
				
				<table>
					<tr>
						<td width='90'>Periode</td>
						<td>:</td>
						<td>$dataKomisi[nama_periode]</td>
					</tr>
					<tr>
						<td>Nama Komisi</td>
						<td>:</td>
						<td>$dataKomisi[nama_komisi]</td>
					</tr>
				</table>
				<br>
				<table border='1' cellpadding='0' cellspacing='0'>
					<tr>
						<th width='15' align='center' style='padding: 5px;'>No.</th>
						<th width='70' align='center' style='padding: 5px;'>No. Induk</th>
						<th width='180' align='center' style='padding: 5px;'>Nama Lengkap</th>
						<th width='30' align='center' style='padding: 5px;'>JK</th>
						<th width='180' align='center' style='padding: 5px;'>Tempat, Tanggal Lahir</th>
						<th width='80' align='center' style='padding: 5px;'>Jabatan</th>
					</tr>";
					
					$queryAnggota = "SELECT A.komisi_id, A.anggota_id, A.jabatan, B.full_name, B.gender, B.no_induk, B.place_of_birth, B.date_of_birth, A.komisi_anggota_id FROM as_komisi_anggota A INNER JOIN as_individu B ON A.anggota_id=B.individu_id WHERE A.komisi_id = '$dataKomisi[komisi_id]'";
					$sqlAnggota = mysqli_query($connect, $queryAnggota);
					$i = 1;
					
					while ($dataAnggota = mysqli_fetch_array($sqlAnggota)){
						$date_of_birth = tgl_indo($dataAnggota['date_of_birth']);
							
						$content .= "<tr>
										<td style='padding: 5px;'>$i</td>
										<td style='padding: 5px;' align='center'>$dataAnggota[no_induk]</td>
										<td style='padding: 5px;'>$dataAnggota[full_name]</td>
										<td style='padding: 5px;' align='center'>$dataAnggota[gender]</td>
										<td style='padding: 5px;'>$dataAnggota[place_of_birth], $date_of_birth</td>
										<td style='padding: 5px;'>$dataAnggota[jabatan]</td>
									</tr>";
						$i++;
					}
		$content .= "</table>
				<p></p>
				<table width='100%'>
					<tr>
						<td width='480'>Ket :</td>
						<td width='200' align='right'>Arjawinangun, $date_now</td>
					</tr>
				</table>
				<p>&nbsp;</p>
				
				<table width='100%'>
					<tr>
						<td width='300' align='center'></td>
						<td width='300' align='center'>CV. ASFA SOLUTION<br>ARJAWINANGUN<br><br><p>&nbsp;</p><br><u>Agus Saputra, A.Md., S.Kom.</u><br>Ketua</td>
					</tr>
				</table>
				";
	ob_end_clean();
	// conversion HTML => PDF
	try
	{
		$html2pdf = new HTML2PDF('P','A4','fr', false, 'ISO-8859-15',array(10, 7, 12, 12)); //setting ukuran kertas dan margin pada dokumen anda
		// $html2pdf->setModeDebug();
		$html2pdf->setDefaultFont('Arial');
		$html2pdf->writeHTML($content, isset($_GET['vuehtml']));
		$html2pdf->Output($filename);
	}
	catch(HTML2PDF_exception $e) { echo $e; }
}
?>