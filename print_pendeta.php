<?php
date_default_timezone_set("ASIA/JAKARTA");
error_reporting(0);
session_start();
// include semua file yang dibutuhkan
include "includes/connection.php";
include "includes/debug.php";
include "includes/fungsi_indotgl.php";

// jika session login kosong
if (empty($_SESSION['username']) && empty($_SESSION['password'])){
	// arahkan ke halaman login
	header("Location: index.php?code=3");
}

else{
	ob_start();
	require ("includes/html2pdf/html2pdf.class.php");
	$filename="print_pendeta.pdf";
	$content = ob_get_clean();
	$year = date('Y');
	$month = date('m');
	$date = date('d');
	$now = date('Y-m-d');
	$date_now = tgl_indo($now);
	
	$content = "<table width='100%' align='center' style='background: #6da4cf;'>
					<tr valign='top'>
						<td width='75' align='center' valign='middle'><img src='images/logo.jpg' width='70'></td>
						<td width='940' align='center'>
							<span style='font-size: 20px; font-weight: bold;'>TOKOH MASYARAKAT<br>
								CV. ASFA SOLUTION
							</span><br>
							Sultan Residence H-9, Jl. Nyimas Gandasari - Kel. jungjang, Kec. Arjawinangun - Kab. Cirebon<br>
							Telp. (0231) 8830633, Hp. 08562121141
							Website: http://www.asfasoolution.co.id, Email: info@asfasolution.co.id
							
						</td>
					</tr>
				</table>
				<br>
				<h4><u>TOKOH MASYARAKAT</u></h4>
				<table border='1' cellpadding='0' cellspacing='0'>
					<tr>
						<th width='15' align='center' style='padding: 5px;'>No.</th>
						<th width='70' align='center' style='padding: 5px;'>No. Induk</th>
						<th width='250' align='center' style='padding: 5px;'>Nama Lengkap</th>
						<th width='50' align='center' style='padding: 5px;'>JK</th>
						<th width='200' align='center' style='padding: 5px;'>Jabatan</th>
						<th width='150' align='center' style='padding: 5px;'>Tanggal Pentahbisan</th>
						<th width='120' align='center' style='padding: 5px;'>Status</th>
					</tr>";
					
					$queryPendeta = "SELECT A.jabatan, A.tokoh_id, B.gender, A.tanggal_tasbih, A.status, B.no_induk, B.full_name FROM as_tokoh_masyarakat A INNER JOIN as_individu B ON A.anggota_id=B.individu_id ORDER BY A.tokoh_id ASC";
					$sqlPendeta = mysqli_query($connect, $queryPendeta);
					$i = 1;
					
					while ($dataPendeta = mysqli_fetch_array($sqlPendeta)){
						if ($dataPendeta['status'] == 'Y'){
							$status = "Aktif";
						}
						elseif ($dataPendeta['status'] == 'Y'){
							$status = "Tidak Aktif";
						}
						if ($dataPendeta['tanggal_tasbih'] != '0000-00-00'){
							$tanggal_tasbih = tgl_indo($dataPendeta['tanggal_tasbih']);
						}
						else{
							$tanggal_tasbih = "-";
						} 
							
						$content .= "<tr>
										<td style='padding: 5px;'>$i</td>
										<td style='padding: 5px;' align='center'>$dataPendeta[no_induk]</td>
										<td style='padding: 5px;'>$dataPendeta[full_name]</td>
										<td style='padding: 5px;' align='center'>$dataPendeta[gender]</td>
										<td style='padding: 5px;'>$dataPendeta[jabatan]</td>
										<td style='padding: 5px;'>$tanggal_tasbih</td>
										<td style='padding: 5px;'>$status</td>
									</tr>";
						$i++;
					}
		$content .= "</table>
				<p></p>
				<table width='100%'>
					<tr>
						<td width='780'>Ket :</td>
						<td width='200' align='right'>Arjawinangun, $date_now</td>
					</tr>
				</table>
				<p>&nbsp;</p>
				
				<table width='100%'>
					<tr>
						<td width='500' align='center'></td>
						<td width='500' align='center'>CV. ASFA SOLUTION<br>ARJAWINANGUN<br><br><p>&nbsp;</p><br><u>Agus Saputra, A.Md., S.Kom.</u><br>Ketua</td>
					</tr>
				</table>
				";
	ob_end_clean();
	// conversion HTML => PDF
	try
	{
		$html2pdf = new HTML2PDF('L','A4','fr', false, 'ISO-8859-15',array(10, 7, 12, 12)); //setting ukuran kertas dan margin pada dokumen anda
		// $html2pdf->setModeDebug();
		$html2pdf->setDefaultFont('Arial');
		$html2pdf->writeHTML($content, isset($_GET['vuehtml']));
		$html2pdf->Output($filename);
	}
	catch(HTML2PDF_exception $e) { echo $e; }
}
?>