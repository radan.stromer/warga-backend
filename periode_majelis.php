<?php
// include header
include "header.php";
// set the tpl page
$page = "periode_majelis.tpl";

// if session is null, showing up the text and exit
if ($_SESSION['username'] == '' && $_SESSION['password'] == '')
{
	// show up the text and exit
	echo "You have not authorization for access the modules.";
	exit();
}

else 
{
	// get variable
	$module = $_GET['module'];
	$act = $_GET['act'];
	
	// if module is periode and action is input
	if ($module == 'periode' && $act == 'input')
	{
		// change each value to variable name
		$createdDate = date('Y-m-d H:i:s');
		$periodeName = $_POST['nama_periode'];
		$status = $_POST['status'];
		$userID = $_SESSION['userID'];
		
		// save into database
		$queryPeriode = "INSERT INTO as_majelis_periode(nama_periode,status,created_date,created_userid,modified_date,modified_userid)
		VALUES('$periodeName','$status','$createdDate','$userID','','')";
		mysqli_query($connect, $queryPeriode);
		
		// redirect to the main periode majelis page
		header("Location: periode_majelis.php?code=1");
	} // close bracket
	
	// if module is periode and action is edit
	elseif ($module == 'periode' && $act == 'edit')
	{
		// get the periode ID
		$periodeID = $_GET['periodeID'];
		
		$queryPeriode = "SELECT * FROM as_majelis_periode WHERE majelis_periode_id = '$periodeID'";
		$sqlPeriode = mysqli_query($connect, $queryPeriode);
		
		// fetch data
		$dataPeriode = mysqli_fetch_array($sqlPeriode);
		
		// assign data to the tpl
		$smarty->assign("periodeID", $dataPeriode['majelis_periode_id']);
		$smarty->assign("nama_periode", $dataPeriode['nama_periode']);
		$smarty->assign("status", $dataPeriode['status']);
	} //close bracket
	
	// if module is periode and action is update
	elseif ($module == 'periode' && $act == 'update')
	{
		// change each value to variable name
		$modifiedDate = date('Y-m-d H:i:s');
		$periodeID = $_POST['periodeID'];
		$periodeName = $_POST['nama_periode'];
		$status = $_POST['status'];
		$userID = $_SESSION['userID'];
		
		// save into the database
		$queryPeriode = "UPDATE as_majelis_periode SET nama_periode = '$periodeName', status = '$status', modified_date = '$modifiedDate', modified_userid = '$userID' WHERE majelis_periode_id = '$periodeID'";
		mysqli_query($connect, $queryPeriode);
		
		// redirect to the main periode page
		header("Location: periode_majelis.php?code=2");
	} // close bracket
	
	// if module is periode and action is delete
	elseif ($module == 'periode' && $act == 'delete')
	{
		// get periode id
		$periodeID = $_GET['periodeID'];
		
		// delete from the table
		$queryPeriode = "DELETE FROM as_majelis_periode WHERE majelis_periode_id = '$periodeID'";
		mysqli_query($connect, $queryPeriode);
		
		// redirect to the main periode page
		header("Location: periode_majelis.php?code=3");
	} // close bracket
	
	// default
	else 
	{
		// create new object pagination
		$p = new PaginationPeriodeMajelis;
		// limit 10 data for page
		$limit  = 10;
		$position = $p->searchPosition($limit);
		// showing up periode data
		$queryPeriode = "SELECT * FROM as_majelis_periode ORDER BY majelis_periode_id DESC LIMIT $position, $limit";
		$sqlPeriode = mysqli_query($connect, $queryPeriode);
		
		$i = 1 + $position;
		// fetch data
		while ($dtPeriode = mysqli_fetch_array($sqlPeriode))
		{
			// save data into array
			$dataPeriode[] = array(	'periodeID' => $dtPeriode['majelis_periode_id'],
									'nama_periode' => $dtPeriode['nama_periode'],
									'status' => $dtPeriode['status'],
									'no' => $i
									);
			$i++;
		}
		
		// count data
		$queryCountPeriode = "SELECT * FROM as_majelis_periode";
		$sqlCountPeriode = mysqli_query($connect, $queryCountPeriode);
		$amountData = mysqli_num_rows($sqlCountPeriode);
		
		$amountPage = $p->amountPage($amountData, $limit);
		$pageLink = $p->navPage($_GET['page'], $amountPage);
		
		$smarty->assign("pageLink", $pageLink);
		// assign to the tpl
		$smarty->assign("dataPeriode", $dataPeriode);
		
	} // close bracket
	
	// assign code to the tpl
	$smarty->assign("code", $_GET['code']);
	$smarty->assign("module", $_GET['module']);
	$smarty->assign("act", $_GET['act']);
	
} // close bracket

// include footer
include "footer.php";
?>