<?php /* Smarty version Smarty-3.1.11, created on 2018-04-13 13:20:16
         compiled from ".\templates\family.tpl" */ ?>
<?php /*%%SmartyHeaderCode:314035ad048d6746341-50236006%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '773c1c65a81698b9d2b4d035dd678bcb4d4b64fe' => 
    array (
      0 => '.\\templates\\family.tpl',
      1 => 1523600414,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '314035ad048d6746341-50236006',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.11',
  'unifunc' => 'content_5ad048d68cbdf5_63464959',
  'variables' => 
  array (
    'code' => 0,
    'act' => 0,
    'module' => 0,
    'nik' => 0,
    'sortno' => 0,
    'dataKepala' => 0,
    'fid' => 0,
    'full_name' => 0,
    'tanggal_nikah' => 0,
    'alamat' => 0,
    'photo' => 0,
    'dataChild' => 0,
    'keyword' => 0,
    'numSearch' => 0,
    'dataSearch' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5ad048d68cbdf5_63464959')) {function content_5ad048d68cbdf5_63464959($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>


<div id="wrapper">
	
	<?php echo $_smarty_tpl->getSubTemplate ("leftMenu.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

	
	<div id="page-wrapper">
		<div class="row">
			<div class="col-lg-12">
				<ol class="breadcrumb">
					<li><a href="home.php"><i class="fa fa-dashboard"></i> Home</a></li>
					<li class="active"><i class="fa fa-caret-square-o-down"></i> Keluarga Jemaat</li>
					<li class="active">Keluarga</li>
				</ol>
				
				<?php if ($_smarty_tpl->tpl_vars['code']->value=='1'){?>
					<div class="alert alert-success alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						Data keluarga berhasil disimpan.
					</div>
				<?php }?>
				<?php if ($_smarty_tpl->tpl_vars['code']->value=='2'){?>
					<div class="alert alert-success alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						Data keluarga berhasil diupdate.
					</div>
				<?php }?>
				<?php if ($_smarty_tpl->tpl_vars['code']->value=='3'){?>
					<div class="alert alert-success alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						Data keluarga berhasil dihapus.
					</div>
				<?php }?>
				<?php if ($_smarty_tpl->tpl_vars['code']->value=='4'){?>
					<div class="alert alert-success alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						Data keluarga berhasil disimpan.
					</div>
				<?php }?>
				<?php if ($_smarty_tpl->tpl_vars['code']->value=='6'){?>
					<div class="alert alert-success alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						Data keluarga berhasil dihapus.
					</div>
				<?php }?>
				<?php if ($_smarty_tpl->tpl_vars['code']->value=='7'){?>
					<div class="alert alert-success alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						Data kepala keluarga berhasil dihapus.
					</div>
				<?php }?>
			</div>
		</div><!-- /.row -->
		
		<script type='text/javascript' src='js/jquery.validate.js'></script>
			
		
			<script type='text/javascript'>
				$(document).ready(function() {
					$('#frm_family').validate({
						rules:{
							kepala_keluarga: true,
							alamat: true,
							tanggal_nikah: true
						},
						messages:{
							kepala_keluarga:{
								required: "This is a required field."
							},
							alamat:{
								required: "This is a required field."
							},
							tanggal_nikah:{
								required: "This is a required field."
							}
						}
					});
					
					$('#frm_child').validate({
						rules:{
							child_id: true,
							status: true
						},
						messages:{
							child_id:{
								required: "This is a required field."
							},
							status:{
								required: "This is a required field."
							}
						}
					});
					
					$( "#datepicker" ).datepicker({
						changeMonth: true,
						changeYear: true,
						dateFormat: "yy-mm-dd",
						yearRange: 'c-100:c-0'
					});
				});
			</script>
			
			
			<?php if ($_smarty_tpl->tpl_vars['act']->value=='add'||$_smarty_tpl->tpl_vars['act']->value=='edit'){?>
				<script type="text/javascript" src="js/ajaxupload.3.5.js" ></script>
				<link rel="stylesheet" type="text/css" href="css/Ajaxfile-upload.css" />
				
					
				<script type='text/javascript'>
					$(function(){
						var btnUpload=$('#me');
						var mestatus=$('#mestatus');
						var files=$('#files');
						new AjaxUpload(btnUpload, {
							action: 'upload_nikah.php',
							name: 'uploadfile',
							onSubmit: function(file, ext){
								 if (! (ext && /^(jpg|jpeg)$/.test(ext))){ 
				                    // extension is not allowed 
									mestatus.text('Only JPG files are allowed');
									return false;
								}
								mestatus.html('<img src="images/ajax-loader.gif" height="16" width="16">');
							},
							onComplete: function(file, response){
								//On completion clear the status
								mestatus.text('');
								//On completion clear the status
								files.html('');
								//Add uploaded file to list
								if(response!=="error"){
									$('<li></li>').appendTo('#files').html('<img src="images/photo_nikah/'+response+'" alt="" width="100" /><br />').addClass('success');
									$('<li></li>').appendTo('#nikah').html('<input type="hidden" name="filename" value="'+response+'">').addClass('nameupload');
									
								} else{
									$('<li></li>').appendTo('#files').text(file).addClass('error');
								}
							}
						});
						
					});
				</script>
				
			<?php }?>
		
		<div class="row">
			<div class="col-lg-12">
				
				<?php if ($_smarty_tpl->tpl_vars['module']->value=='family'&&$_smarty_tpl->tpl_vars['act']->value=='add'){?>
				
					<h3>Tambah Keluarga</h3>
					<form role="form" method="POST" action="family.php?module=family&act=input" id="frm_family">
						<div class="form-group">
							<label>NIK</label>
							<input type="text" value="<?php echo $_smarty_tpl->tpl_vars['nik']->value;?>
" name="nik" class="required" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;" DISABLED>
							<input type="hidden" value="<?php echo $_smarty_tpl->tpl_vars['nik']->value;?>
" name="nik" class="required" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
							<input type="hidden" value="<?php echo $_smarty_tpl->tpl_vars['sortno']->value;?>
" name="sortno" class="required" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
						</div>
						<div class="form-group">
							<label>Nama Kepala Keluarga</label>
							<select name="kepala_keluarga" class="required" style="display: block; width: 270px; height: 34px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
								<option value="">- Pilih Kepala Keluarga -</option>
								<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['dataKepala'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['dataKepala']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataKepala']['name'] = 'dataKepala';
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataKepala']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['dataKepala']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataKepala']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataKepala']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataKepala']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataKepala']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataKepala']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataKepala']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['dataKepala']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataKepala']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['dataKepala']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataKepala']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataKepala']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['dataKepala']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['dataKepala']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataKepala']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataKepala']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataKepala']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['dataKepala']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['dataKepala']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['dataKepala']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['dataKepala']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['dataKepala']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['dataKepala']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataKepala']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataKepala']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataKepala']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataKepala']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['dataKepala']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataKepala']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataKepala']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['dataKepala']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataKepala']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataKepala']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataKepala']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataKepala']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['dataKepala']['total']);
?>
									<option value="<?php echo $_smarty_tpl->tpl_vars['dataKepala']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataKepala']['index']]['kepala_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['dataKepala']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataKepala']['index']]['kepala_name'];?>
</option>
								<?php endfor; endif; ?>
							</select>
						</div>
						<div class="form-group">
							<label>Tanggal Pernikahan</label>
							<input type="text" id="datepicker" name="tanggal_nikah" class="required" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
						</div>
						<div class="form-group">
							<label>Alamat</label>
							<input type="text" name="alamat" class="required" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
						</div>
						<div class="form-group">
							<label>Foto Pernikahan</label>
							<div id="me" class="styleall" style="cursor:pointer;">
								<label>
									<button class="btn btn-primary">Browse</button>
								</label>
							</div>
							<span id="mestatus" ></span>
							<div id="nikah">
								<li class="nameupload"></li>
							</div>
							<div id="files">
								<li class="success"></li>
							</div>
						</div>
						<button type="submit" class="btn btn-primary">Simpan</button>
					</form>
				
				<?php }elseif($_smarty_tpl->tpl_vars['module']->value=='family'&&$_smarty_tpl->tpl_vars['act']->value=='edit'){?>
				
					<h3>Ubah Keluarga</h3>
					<form role="form" method="POST" action="family.php?module=family&act=update" id="frm_family">
						<input type="hidden" value="<?php echo $_smarty_tpl->tpl_vars['fid']->value;?>
" name="fid" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
						<div class="form-group">
							<label>NIK</label>
							<input type="text" value="<?php echo $_smarty_tpl->tpl_vars['nik']->value;?>
" name="nik" class="required" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;" DISABLED>
						</div>
						<div class="form-group">
							<label>Nama Kepala Keluarga</label>
							<input type="text" value="<?php echo $_smarty_tpl->tpl_vars['full_name']->value;?>
" name="tanggal_nikah" class="required" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;" DISABLED>
						</div>
						<div class="form-group">
							<label>Tanggal Pernikahan</label>
							<input type="text" id="datepicker" value="<?php echo $_smarty_tpl->tpl_vars['tanggal_nikah']->value;?>
" name="tanggal_nikah" class="required" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
						</div>
						<div class="form-group">
							<label>Alamat</label>
							<input type="text" value="<?php echo $_smarty_tpl->tpl_vars['alamat']->value;?>
" name="alamat" class="required" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
						</div>
						<div class="form-group">
							<label>Foto Pernikahan</label>
							<div id="me" class="styleall" style="cursor:pointer;">
								<label>
									<button class="btn btn-primary">Browse</button>
								</label>
							</div>
							<span id="mestatus" ></span>
							<div id="nikah">
								<li class="nameupload"></li>
							</div>
							<div id="files">
								<li class="success">
									<?php if ($_smarty_tpl->tpl_vars['photo']->value!=''){?>
										<img src="images/photo_nikah/<?php echo $_smarty_tpl->tpl_vars['photo']->value;?>
" width="100">
									<?php }?>
								</li>
							</div>
						</div>
						<button type="submit" class="btn btn-primary">Simpan</button>
					</form>
				
				<?php }elseif($_smarty_tpl->tpl_vars['module']->value=='family'&&$_smarty_tpl->tpl_vars['act']->value=='step'){?>
					
					<h3>Data Keluarga</h3>
					<div class="form-group">
						<label>NIK :</label> <?php echo $_smarty_tpl->tpl_vars['nik']->value;?>
<br>
						<label>Nama Kepala Keluarga :</label> <?php echo $_smarty_tpl->tpl_vars['full_name']->value;?>
<br>
						<label>Tanggal Pernikahan :</label> <?php echo $_smarty_tpl->tpl_vars['tanggal_nikah']->value;?>
<br>
						<label>Alamat :</label> <?php echo $_smarty_tpl->tpl_vars['alamat']->value;?>
<br>
						<label>Photo :</label> <?php if ($_smarty_tpl->tpl_vars['photo']->value!=''){?> <a href="images/photo_nikah/<?php echo $_smarty_tpl->tpl_vars['photo']->value;?>
" target="_blank"><b>Lihat Foto</b></a> <?php }else{ ?> - <?php }?> <br>
						<a href="family.php?module=family&act=edit&fid=<?php echo $_smarty_tpl->tpl_vars['fid']->value;?>
"><button class="btn btn-success" type="button">Edit Kepala Keluarga</button></a>
					</div>
					<br>
					<a href="family.php?module=family&act=addchild&fid=<?php echo $_smarty_tpl->tpl_vars['fid']->value;?>
"><button class="btn btn-primary" type="button">Tambah Data Keluarga</button></a>
					<a href="print_family.php?module=family&act=addchild&fid=<?php echo $_smarty_tpl->tpl_vars['fid']->value;?>
" target="_blank"><button class="btn btn-primary" type="button">Cetak Kartu Keluarga</button></a>
					<h4>Data Keluarga</h4>
					<div class="table-responsive">
						<table class="table table-bordered table-hover tablesorter">
							<thead>
								<tr>
									<th>No. <i class="fa fa-sort"></i></th>
									<th>Nama Lengkap <i class="fa fa-sort"></i></th>
									<th>JK <i class="fa fa-sort"></i></th>
									<th>No. Induk <i class="fa fa-sort"></i></th>
									<th>Tempat, Tgl. Lahir <i class="fa fa-sort"></i></th>
									<th>Pekerjaan <i class="fa fa-sort"></i></th>
									<th>Gol. Darah <i class="fa fa-sort"></i></th>
									<th>Status <i class="fa fa-sort"></i></th>
									<th>Aksi <i class="fa fa-sort"></i></th>
								</tr>
							</thead>
							<tbody>
								<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['dataChild'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['dataChild']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataChild']['name'] = 'dataChild';
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataChild']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['dataChild']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataChild']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataChild']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataChild']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataChild']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataChild']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataChild']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['dataChild']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataChild']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['dataChild']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataChild']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataChild']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['dataChild']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['dataChild']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataChild']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataChild']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataChild']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['dataChild']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['dataChild']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['dataChild']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['dataChild']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['dataChild']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['dataChild']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataChild']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataChild']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataChild']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataChild']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['dataChild']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataChild']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataChild']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['dataChild']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataChild']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataChild']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataChild']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataChild']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['dataChild']['total']);
?>
								<tr>
									<td><?php echo $_smarty_tpl->tpl_vars['dataChild']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataChild']['index']]['no'];?>
</td>
									<td><?php echo $_smarty_tpl->tpl_vars['dataChild']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataChild']['index']]['full_name'];?>
</td>
									<td><?php echo $_smarty_tpl->tpl_vars['dataChild']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataChild']['index']]['gender'];?>
</td>
									<td><?php echo $_smarty_tpl->tpl_vars['dataChild']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataChild']['index']]['no_induk'];?>
</td>
									<td><?php echo $_smarty_tpl->tpl_vars['dataChild']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataChild']['index']]['place_of_birth'];?>
, <?php echo $_smarty_tpl->tpl_vars['dataChild']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataChild']['index']]['date_of_birth'];?>
</td>
									<td><?php echo $_smarty_tpl->tpl_vars['dataChild']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataChild']['index']]['job_name'];?>
</td>
									<td><?php echo $_smarty_tpl->tpl_vars['dataChild']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataChild']['index']]['blood_type'];?>
</td>
									<td><?php echo $_smarty_tpl->tpl_vars['dataChild']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataChild']['index']]['status'];?>
</td>
									<td>
										<a href="family.php?module=family&act=delete&cid=<?php echo $_smarty_tpl->tpl_vars['dataChild']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataChild']['index']]['cid'];?>
&fid=<?php echo $_smarty_tpl->tpl_vars['dataChild']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataChild']['index']]['fid'];?>
" onclick="return confirm('Anda Yakin ingin menghapus <?php echo $_smarty_tpl->tpl_vars['dataChild']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataChild']['index']]['full_name'];?>
?');"><button type="button" class="btn btn-danger">Hapus</button></a>
									</td>
								</tr>
								<?php endfor; endif; ?>
							</tbody>
						</table>
					</div>
					
				<?php }elseif($_smarty_tpl->tpl_vars['module']->value=='family'&&$_smarty_tpl->tpl_vars['act']->value=='addchild'){?>
					
					<h3>Data Keluarga</h3>
					<div class="form-group">
						<label>NIK :</label> <?php echo $_smarty_tpl->tpl_vars['nik']->value;?>
<br>
						<label>Nama Kepala Keluarga :</label> <?php echo $_smarty_tpl->tpl_vars['full_name']->value;?>
<br>
						<label>Tanggal Pernikahan</label> <?php echo $_smarty_tpl->tpl_vars['tanggal_nikah']->value;?>
<br>
						<label>Alamat :</label> <?php echo $_smarty_tpl->tpl_vars['alamat']->value;?>

					</div>
					<h4>Tambah Data Keluarga</h4>
					<form role="form" method="POST" action="family.php?module=family&act=addchildinput" id="frm_child">
						<input type="hidden" name="fid" value="<?php echo $_smarty_tpl->tpl_vars['fid']->value;?>
">
						<div class="form-group">
							<label>Nama Keluarga</label>
							<select name="child_id" class="required" style="display: block; width: 270px; height: 34px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
								<option value="">- Pilih Kepala Keluarga -</option>
								<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['dataKepala'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['dataKepala']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataKepala']['name'] = 'dataKepala';
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataKepala']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['dataKepala']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataKepala']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataKepala']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataKepala']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataKepala']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataKepala']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataKepala']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['dataKepala']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataKepala']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['dataKepala']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataKepala']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataKepala']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['dataKepala']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['dataKepala']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataKepala']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataKepala']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataKepala']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['dataKepala']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['dataKepala']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['dataKepala']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['dataKepala']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['dataKepala']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['dataKepala']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataKepala']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataKepala']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataKepala']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataKepala']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['dataKepala']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataKepala']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataKepala']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['dataKepala']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataKepala']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataKepala']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataKepala']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataKepala']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['dataKepala']['total']);
?>
									<option value="<?php echo $_smarty_tpl->tpl_vars['dataKepala']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataKepala']['index']]['kepala_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['dataKepala']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataKepala']['index']]['kepala_name'];?>
</option>
								<?php endfor; endif; ?>
							</select>
						</div>
						<div class="form-group">
							<label>Status Keluarga</label>
							<select name="status" class="required" style="display: block; width: 270px; height: 34px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
								<option value="">- Pilih Status Keluarga -</option>
								<option value="1">Suami</option>
								<option value="2">Istri</option>
								<option value="3">Anak</option>
							</select>
						</div>
						<div class="form-group">
							<label>Keterangan</label>
							<input type="text" name="keterangan" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
						</div>
						<button type="submit" class="btn btn-primary">Simpan</button>
					</form>
				
				<?php }elseif($_smarty_tpl->tpl_vars['module']->value=='family'&&$_smarty_tpl->tpl_vars['act']->value=='search'){?>
					<h2>Pencarian Kartu Keluarga</h2>
					<form role="form" method="GET" action="family.php">
						<input type="hidden" name="module" value="family">
						<input type="hidden" name="act" value="search">
						<div class="form-group">
							<label>Masukkan Keyword (Nama Kepala Keluarga)</label>
							<input type="text" value="<?php echo $_smarty_tpl->tpl_vars['keyword']->value;?>
" placeholder="Masukkan Nama Kepala Keluarga" name="name" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
						</div>
						<button type="submit" class="btn btn-primary">Cari</button>
					</form>
					
					<h3>Hasil Pencarian (Nama Kepala Keluarga: <b><?php echo $_smarty_tpl->tpl_vars['keyword']->value;?>
</b>)<br>
					Ditemukan: <?php echo $_smarty_tpl->tpl_vars['numSearch']->value;?>
 data
					</h3>
					<div class="table-responsive">
						<table class="table table-bordered table-hover tablesorter">
							<thead>
								<tr>
									<th width='40'>No. <i class="fa fa-sort"></i></th>
									<th width='150'>NIK <i class="fa fa-sort"></i></th>
									<th width='220'>Nama Kepala Keluarga <i class="fa fa-sort"></i></th>
									<th width="120">Tanggal Pernikahan <i class="fa fa-sort"></i></th>
									<th>Alamat <i class="fa fa-sort"></i></th>
									<th width='200'>Aksi <i class="fa fa-sort"></i></th>
								</tr>
							</thead>
							<tbody>
								<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['dataSearch'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['dataSearch']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataSearch']['name'] = 'dataSearch';
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataSearch']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['dataSearch']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataSearch']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataSearch']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataSearch']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataSearch']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataSearch']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataSearch']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['dataSearch']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataSearch']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['dataSearch']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataSearch']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataSearch']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['dataSearch']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['dataSearch']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataSearch']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataSearch']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataSearch']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['dataSearch']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['dataSearch']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['dataSearch']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['dataSearch']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['dataSearch']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['dataSearch']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataSearch']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataSearch']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataSearch']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataSearch']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['dataSearch']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataSearch']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataSearch']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['dataSearch']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataSearch']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataSearch']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataSearch']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataSearch']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['dataSearch']['total']);
?>
								<tr>
									<td><?php echo $_smarty_tpl->tpl_vars['dataSearch']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataSearch']['index']]['no'];?>
</td>
									<td><?php echo $_smarty_tpl->tpl_vars['dataSearch']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataSearch']['index']]['nik'];?>
</td>
									<td><?php echo $_smarty_tpl->tpl_vars['dataSearch']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataSearch']['index']]['full_name'];?>
</td>
									<td><?php echo $_smarty_tpl->tpl_vars['dataSearch']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataSearch']['index']]['tanggal_nikah'];?>
</td>
									<td><?php echo $_smarty_tpl->tpl_vars['dataSearch']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataSearch']['index']]['alamat'];?>
</td>
									<td>
										<a href="print_family.php?module=family&act=addchild&fid=<?php echo $_smarty_tpl->tpl_vars['dataSearch']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataSearch']['index']]['fid'];?>
" target="_blank"><button type="button" class="btn btn-primary">Cetak</button></a>
										<a href="family.php?module=family&act=step&fid=<?php echo $_smarty_tpl->tpl_vars['dataSearch']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataSearch']['index']]['fid'];?>
&keyword=<?php echo $_smarty_tpl->tpl_vars['keyword']->value;?>
"><button type="button" class="btn btn-success">Edit</button></a>
										<a href="family.php?module=family&act=deletefam&fid=<?php echo $_smarty_tpl->tpl_vars['dataSearch']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataSearch']['index']]['fid'];?>
&keyword=<?php echo $_smarty_tpl->tpl_vars['keyword']->value;?>
" onclick="return confirm('Anda Yakin ingin menghapus kepala keluarga <?php echo $_smarty_tpl->tpl_vars['dataSearch']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataSearch']['index']]['full_name'];?>
?');"><button type="button" class="btn btn-danger">Hapus</button></a>
									</td>
								</tr>
								<?php endfor; endif; ?>
							</tbody>
						</table>
					</div>
				
				<?php }else{ ?>
			
					<a href="family.php?module=family&act=add"><button class="btn btn-primary" type="button">Tambah Keluarga</button></a>
					<h2>Pencarian Kartu Keluarga</h2>
					<form role="form" method="GET" action="family.php">
						<input type="hidden" name="module" value="family">
						<input type="hidden" name="act" value="search">
						<div class="form-group">
							<label>Masukkan Keyword (Nama Kepala Keluarga)</label>
							<input type="text" placeholder="Masukkan Nama Kepala Keluarga" name="name" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
						</div>
						<button type="submit" class="btn btn-primary">Cari</button>
					</form>
				<?php }?>
			</div>
		</div>
	</div>
</div><!-- /#wrapper -->

<?php echo $_smarty_tpl->getSubTemplate ("footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>
<?php }} ?>