<?php /* Smarty version Smarty-3.1.11, created on 2018-04-03 03:06:07
         compiled from ".\templates\grade.tpl" */ ?>
<?php /*%%SmartyHeaderCode:5125ac28d2f2602d5-38672274%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '1e24a4954afc5fcfb5cdb956bfe5a744b6788265' => 
    array (
      0 => '.\\templates\\grade.tpl',
      1 => 1405884600,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '5125ac28d2f2602d5-38672274',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'code' => 0,
    'module' => 0,
    'act' => 0,
    'gradeID' => 0,
    'gradeName' => 0,
    'status' => 0,
    'dataGrade' => 0,
    'pageLink' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.11',
  'unifunc' => 'content_5ac28d2f3088f3_53689100',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5ac28d2f3088f3_53689100')) {function content_5ac28d2f3088f3_53689100($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>


<div id="wrapper">
	
	<?php echo $_smarty_tpl->getSubTemplate ("leftMenu.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

	
	<div id="page-wrapper">
		<div class="row">
			<div class="col-lg-12">
				<ol class="breadcrumb">
					<li><a href="home.php"><i class="fa fa-dashboard"></i> Home</a></li>
					<li class="active"><i class="fa fa-caret-square-o-down"></i> Master Data</li>
					<li class="active">Grade (Penghasilan)</li>
				</ol>
				
				<?php if ($_smarty_tpl->tpl_vars['code']->value=='1'){?>
					<div class="alert alert-success alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						Data grade penghasilan berhasil disimpan.
					</div>
				<?php }?>
				<?php if ($_smarty_tpl->tpl_vars['code']->value=='2'){?>
					<div class="alert alert-success alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						Data grade penghasilan berhasil diupdate.
					</div>
				<?php }?>
				<?php if ($_smarty_tpl->tpl_vars['code']->value=='3'){?>
					<div class="alert alert-success alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						Data grade penghasilan berhasil dihapus.
					</div>
				<?php }?>
			</div>
		</div><!-- /.row -->
		
		<script type='text/javascript' src='js/jquery.validate.js'></script>
			
		
			<script type='text/javascript'>
				$(document).ready(function() {
					$('#frm_grade').validate({
						rules:{
							gradeName: true,
							status: true
						},
						messages:{
							gradeName:{
								required: "This is a required field."
							},
							status:{
								required: "This is a required field."
							}
						}
					});
				});
			</script>
		
		
		<div class="row">
			<div class="col-lg-12">
				
				<?php if ($_smarty_tpl->tpl_vars['module']->value=='grade'&&$_smarty_tpl->tpl_vars['act']->value=='add'){?>
				
					<h3>Tambah Grade Penghasilan</h3>
					<form role="form" method="POST" action="grade.php?module=grade&act=input" id="frm_grade">
						<div class="form-group">
							<label>Grade Penghasilan</label>
							<input type="text" name="gradeName" class="required" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
						</div>
						<div class="form-group">
							<label>Status</label>
							<select name="status" class="required" style="display: block; width: 270px; height: 34px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
								<option value="">- Pilih Status -</option>
								<option value="Y" SELECTED>Aktif</option>
								<option value="N">Tidak Aktif</option>
							</select>
						</div>
						<button type="submit" class="btn btn-primary">Simpan</button>
					</form>
				
				<?php }elseif($_smarty_tpl->tpl_vars['module']->value=='grade'&&$_smarty_tpl->tpl_vars['act']->value=='edit'){?>
				
					<h3>Ubah Grade Penghasilan</h3>
					<form role="form" method="POST" action="grade.php?module=grade&act=update" id="frm_grade">
						<input type="hidden" name="gradeID" value="<?php echo $_smarty_tpl->tpl_vars['gradeID']->value;?>
">
						<div class="form-group">
							<label>Grade Penghasilan</label>
							<input type="text" name="gradeName" value="<?php echo $_smarty_tpl->tpl_vars['gradeName']->value;?>
" class="required" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
						</div>
						<div class="form-group">
							<label>Status</label>
							<select name="status" class="required" style="display: block; width: 270px; height: 34px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
								<option value="">- Pilih Status -</option>
								<option value="Y" <?php if ($_smarty_tpl->tpl_vars['status']->value=='Y'){?> SELECTED <?php }?>>Aktif</option>
								<option value="N" <?php if ($_smarty_tpl->tpl_vars['status']->value=='N'){?> SELECTED <?php }?>>Tidak Aktif</option>
							</select>
						</div>
						<button type="submit" class="btn btn-primary">Simpan</button>
					</form>
					
				<?php }else{ ?>
			
					<a href="grade.php?module=grade&act=add"><button class="btn btn-primary" type="button">Tambah Grade</button></a>
					<h3>Manajemen Grade Penghasilan</h3>
					<div class="table-responsive">
						<table class="table table-bordered table-hover tablesorter">
							<thead>
								<tr>
									<th>No. <i class="fa fa-sort"></i></th>
									<th>Grade <i class="fa fa-sort"></i></th>
									<th>Status <i class="fa fa-sort"></i></th>
									<th>Aksi <i class="fa fa-sort"></i></th>
								</tr>
							</thead>
							<tbody>
								<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['dataGrade'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['dataGrade']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataGrade']['name'] = 'dataGrade';
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataGrade']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['dataGrade']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataGrade']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataGrade']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataGrade']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataGrade']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataGrade']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataGrade']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['dataGrade']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataGrade']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['dataGrade']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataGrade']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataGrade']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['dataGrade']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['dataGrade']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataGrade']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataGrade']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataGrade']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['dataGrade']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['dataGrade']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['dataGrade']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['dataGrade']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['dataGrade']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['dataGrade']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataGrade']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataGrade']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataGrade']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataGrade']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['dataGrade']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataGrade']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataGrade']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['dataGrade']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataGrade']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataGrade']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataGrade']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataGrade']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['dataGrade']['total']);
?>
								<tr>
									<td><?php echo $_smarty_tpl->tpl_vars['dataGrade']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataGrade']['index']]['no'];?>
</td>
									<td><?php echo $_smarty_tpl->tpl_vars['dataGrade']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataGrade']['index']]['gradeName'];?>
</td>
									<td><?php echo $_smarty_tpl->tpl_vars['dataGrade']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataGrade']['index']]['status'];?>
</td>
									<td>
										<a href="grade.php?module=grade&act=edit&gradeID=<?php echo $_smarty_tpl->tpl_vars['dataGrade']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataGrade']['index']]['gradeID'];?>
"><button type="button" class="btn btn-success">Edit</button></a>
										<a href="grade.php?module=grade&act=delete&gradeID=<?php echo $_smarty_tpl->tpl_vars['dataGrade']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataGrade']['index']]['gradeID'];?>
" onclick="return confirm('Anda Yakin ingin menghapus <?php echo $_smarty_tpl->tpl_vars['dataGrade']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataGrade']['index']]['gradeName'];?>
?');"><button type="button" class="btn btn-danger">Hapus</button></a>
									</td>
								</tr>
								<?php endfor; endif; ?>
							</tbody>
						</table>
					</div>
					<div id="paging"><?php echo $_smarty_tpl->tpl_vars['pageLink']->value;?>
</div>
				<?php }?>
			</div>
		</div>
	</div>
</div><!-- /#wrapper -->

<?php echo $_smarty_tpl->getSubTemplate ("footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>
<?php }} ?>