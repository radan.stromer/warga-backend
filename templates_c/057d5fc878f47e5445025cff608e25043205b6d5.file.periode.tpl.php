<?php /* Smarty version Smarty-3.1.11, created on 2018-04-13 14:08:56
         compiled from ".\templates\periode.tpl" */ ?>
<?php /*%%SmartyHeaderCode:290915ad05788c69f31-47282105%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '057d5fc878f47e5445025cff608e25043205b6d5' => 
    array (
      0 => '.\\templates\\periode.tpl',
      1 => 1405884600,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '290915ad05788c69f31-47282105',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'code' => 0,
    'module' => 0,
    'act' => 0,
    'periodeID' => 0,
    'nama_periode' => 0,
    'status' => 0,
    'dataPeriode' => 0,
    'pageLink' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.11',
  'unifunc' => 'content_5ad05788d4ab13_46771419',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5ad05788d4ab13_46771419')) {function content_5ad05788d4ab13_46771419($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>


<div id="wrapper">
	
	<?php echo $_smarty_tpl->getSubTemplate ("leftMenu.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

	
	<div id="page-wrapper">
		<div class="row">
			<div class="col-lg-12">
				<ol class="breadcrumb">
					<li><a href="home.php"><i class="fa fa-dashboard"></i> Home</a></li>
					<li class="active"><i class="fa fa-caret-square-o-down"></i> Kartu Keluarga</li>
					<li class="active">Komisi</li>
				</ol>
				
				<?php if ($_smarty_tpl->tpl_vars['code']->value=='1'){?>
					<div class="alert alert-success alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						Data periode komisi berhasil disimpan.
					</div>
				<?php }?>
				<?php if ($_smarty_tpl->tpl_vars['code']->value=='2'){?>
					<div class="alert alert-success alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						Data periode komisi berhasil diupdate.
					</div>
				<?php }?>
				<?php if ($_smarty_tpl->tpl_vars['code']->value=='3'){?>
					<div class="alert alert-success alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						Data periode komisi berhasil dihapus.
					</div>
				<?php }?>
			</div>
		</div><!-- /.row -->
		
		<script type='text/javascript' src='js/jquery.validate.js'></script>
			
		
			<script type='text/javascript'>
				$(document).ready(function() {
					$('#frm_periode').validate({
						rules:{
							nama_periode: true,
							status: true
						},
						messages:{
							nama_periode:{
								required: "This is a required field."
							},
							status:{
								required: "This is a required field."
							}
						}
					});
				});
			</script>
		
		
		<div class="row">
			<div class="col-lg-12">
				
				<?php if ($_smarty_tpl->tpl_vars['module']->value=='periode'&&$_smarty_tpl->tpl_vars['act']->value=='add'){?>
				
					<h3>Tambah Periode Komisi</h3>
					<form role="form" method="POST" action="periode.php?module=periode&act=input" id="frm_periode">
						<div class="form-group">
							<label>Periode Komisi</label>
							<input type="text" name="nama_periode" placeholder="Ex: 2013-2014" class="required" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
						</div>
						<div class="form-group">
							<label>Status</label>
							<select name="status" class="required" style="display: block; width: 270px; height: 34px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
								<option value="">- Pilih Status -</option>
								<option value="Y" SELECTED>Aktif</option>
								<option value="N">Tidak Aktif</option>
							</select>
						</div>
						<button type="submit" class="btn btn-primary">Simpan</button>
					</form>
				
				<?php }elseif($_smarty_tpl->tpl_vars['module']->value=='periode'&&$_smarty_tpl->tpl_vars['act']->value=='edit'){?>
				
					<h3>Ubah Periode Komisi</h3>
					<form role="form" method="POST" action="periode.php?module=periode&act=update" id="frm_periode">
						<input type="hidden" name="periodeID" value="<?php echo $_smarty_tpl->tpl_vars['periodeID']->value;?>
">
						<div class="form-group">
							<label>Periode Komisi</label>
							<input type="text" name="nama_periode" value="<?php echo $_smarty_tpl->tpl_vars['nama_periode']->value;?>
" placeholder="Ex: 2013-2014" class="required" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
						</div>
						<div class="form-group">
							<label>Status</label>
							<select name="status" class="required" style="display: block; width: 270px; height: 34px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
								<option value="">- Pilih Status -</option>
								<option value="Y" <?php if ($_smarty_tpl->tpl_vars['status']->value=='Y'){?> SELECTED <?php }?>>Aktif</option>
								<option value="N" <?php if ($_smarty_tpl->tpl_vars['status']->value=='N'){?> SELECTED <?php }?>>Tidak Aktif</option>
							</select>
						</div>
						<button type="submit" class="btn btn-primary">Simpan</button>
					</form>
					
				<?php }else{ ?>
			
					<a href="periode.php?module=periode&act=add"><button class="btn btn-primary" type="button">Tambah Periode Komisi</button></a>
					<h3>Manajemen Periode Komisi</h3>
					<div class="table-responsive">
						<table class="table table-bordered table-hover tablesorter">
							<thead>
								<tr>
									<th>No. <i class="fa fa-sort"></i></th>
									<th>Periode Komisi <i class="fa fa-sort"></i></th>
									<th>Status <i class="fa fa-sort"></i></th>
									<th>Aksi <i class="fa fa-sort"></i></th>
								</tr>
							</thead>
							<tbody>
								<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['name'] = 'dataPeriode';
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['dataPeriode']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['total']);
?>
								<tr>
									<td><?php echo $_smarty_tpl->tpl_vars['dataPeriode']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataPeriode']['index']]['no'];?>
</td>
									<td><?php echo $_smarty_tpl->tpl_vars['dataPeriode']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataPeriode']['index']]['nama_periode'];?>
</td>
									<td><?php echo $_smarty_tpl->tpl_vars['dataPeriode']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataPeriode']['index']]['status'];?>
</td>
									<td>
										<a href="periode.php?module=periode&act=edit&periodeID=<?php echo $_smarty_tpl->tpl_vars['dataPeriode']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataPeriode']['index']]['periodeID'];?>
"><button type="button" class="btn btn-success">Edit</button></a>
										<a href="periode.php?module=periode&act=delete&periodeID=<?php echo $_smarty_tpl->tpl_vars['dataPeriode']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataPeriode']['index']]['periodeID'];?>
" onclick="return confirm('Anda Yakin ingin menghapus <?php echo $_smarty_tpl->tpl_vars['dataPeriode']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataPeriode']['index']]['nama_periode'];?>
?');"><button type="button" class="btn btn-danger">Hapus</button></a>
									</td>
								</tr>
								<?php endfor; endif; ?>
							</tbody>
						</table>
					</div>
					<div id="paging"><?php echo $_smarty_tpl->tpl_vars['pageLink']->value;?>
</div>
				<?php }?>
			</div>
		</div>
	</div>
</div><!-- /#wrapper -->

<?php echo $_smarty_tpl->getSubTemplate ("footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>
<?php }} ?>