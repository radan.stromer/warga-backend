<?php /* Smarty version Smarty-3.1.11, created on 2018-03-31 05:43:59
         compiled from ".\templates\index.tpl" */ ?>
<?php /*%%SmartyHeaderCode:156055abebdaf2cd539-64846354%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '749422d4cfc3eb5677cf499730392b6accd4d1c7' => 
    array (
      0 => '.\\templates\\index.tpl',
      1 => 1522274799,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '156055abebdaf2cd539-64846354',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'code' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.11',
  'unifunc' => 'content_5abebdaf63b6d2_81922814',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5abebdaf63b6d2_81922814')) {function content_5abebdaf63b6d2_81922814($_smarty_tpl) {?><!DOCTYPE HTML>
<html>
	<head>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<title>Aplikasi Database Warga</title>
		<link rel="stylesheet" type="text/css" href="css/login.css" media="screen" />
		<script src="js/jquery-1.8.1.min.js" type="text/javascript"></script>
		
		<script type='text/javascript' src='js/jquery.validate.js'></script>
			
		
			<script type='text/javascript'>
				$(document).ready(function() {
					$('#frm_login').validate({
						rules:{
							userName: true,
							userPassword: true
						},
						messages:{
							userName:{
								required: "This is a required field."
							},
							userPassword:{
								required: "This is a required field."
							}
						}
					});
				});
			</script>
		
	</head>
	<body>
		<div class="wrap">
			<div id="content">
				<div id="main">
					<center>
						<img src="images/logo.jpg" height="70"><br><br>
					</center>
					<?php if ($_smarty_tpl->tpl_vars['code']->value=='1'){?>
						<div class='messageerror'><h3>Username dan Password tidak ditemukan.</h3></div>
					<?php }?>
					<?php if ($_smarty_tpl->tpl_vars['code']->value=='2'){?>
						<div class='messagesuccess'><h3>Anda telah keluar [Logout].</h3></div>
					<?php }?>
					
					<div class="full_w">
						<form action="index.php?module=login&act=submit" method="POST" id="frm_login">
							<label for="login">Username:</label>
							<input type="text" name="userName" class="required" size="55" />
							
							<label for="pass">Password:</label>
							<input id="pass" name="userPassword" type="password" size="55" class="required" />
							<div class="sep"></div>
							
							<button type="submit" class="ok">Login</button>
						</form>
					</div>
					<div class="footer">Copyright &copy; <<?php ?>?php echo date('Y'); ?<?php ?>> CV. ASFA Solution - www.asfasolution.co.id - www.agussaputra.com - www.asfamedia.com</div>
				</div>
			</div>
		</div>
	
	</body>
</html><?php }} ?>