<?php /* Smarty version Smarty-3.1.11, created on 2018-03-31 05:44:13
         compiled from ".\templates\funeral.tpl" */ ?>
<?php /*%%SmartyHeaderCode:189865abebdbd4be1b0-57191880%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'fafb60d321e65158093e72e11c63bc709de75380' => 
    array (
      0 => '.\\templates\\funeral.tpl',
      1 => 1405884600,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '189865abebdbd4be1b0-57191880',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'code' => 0,
    'module' => 0,
    'act' => 0,
    'funeralID' => 0,
    'funeralName' => 0,
    'funeralAddress' => 0,
    'status' => 0,
    'dataFuneral' => 0,
    'pageLink' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.11',
  'unifunc' => 'content_5abebdbd583203_42742963',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5abebdbd583203_42742963')) {function content_5abebdbd583203_42742963($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>


<div id="wrapper">
	
	<?php echo $_smarty_tpl->getSubTemplate ("leftMenu.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

	
	<div id="page-wrapper">
		<div class="row">
			<div class="col-lg-12">
				<ol class="breadcrumb">
					<li><a href="home.php"><i class="fa fa-dashboard"></i> Home</a></li>
					<li class="active"><i class="fa fa-caret-square-o-down"></i> Master Data</li>
					<li class="active">Pemakaman</li>
				</ol>
				
				<?php if ($_smarty_tpl->tpl_vars['code']->value=='1'){?>
					<div class="alert alert-success alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						Data pemakaman berhasil disimpan.
					</div>
				<?php }?>
				<?php if ($_smarty_tpl->tpl_vars['code']->value=='2'){?>
					<div class="alert alert-success alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						Data pemakaman berhasil diupdate.
					</div>
				<?php }?>
				<?php if ($_smarty_tpl->tpl_vars['code']->value=='3'){?>
					<div class="alert alert-success alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						Data pemakaman berhasil dihapus.
					</div>
				<?php }?>
			</div>
		</div><!-- /.row -->
		
		<script type='text/javascript' src='js/jquery.validate.js'></script>
			
		
			<script type='text/javascript'>
				$(document).ready(function() {
					$('#frm_funeral').validate({
						rules:{
							funeralName: true,
							funeralAddress: true,
							status: true
						},
						messages:{
							funeralName:{
								required: "This is a required field."
							},
							funeralAddress:{
								required: "This is a required field."
							},
							status:{
								required: "This is a required field."
							}
						}
					});
				});
			</script>
		
		
		<div class="row">
			<div class="col-lg-12">
				
				<?php if ($_smarty_tpl->tpl_vars['module']->value=='funeral'&&$_smarty_tpl->tpl_vars['act']->value=='add'){?>
				
					<h3>Tambah Pemakaman</h3>
					<form role="form" method="POST" action="funeral.php?module=funeral&act=input" id="frm_funeral">
						<div class="form-group">
							<label>Nama Pemakaman</label>
							<input type="text" name="funeralName" class="required" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
						</div>
						<div class="form-group">
							<label>Alamat</label>
							<input type="text" name="funeralAddress" class="required" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
						</div>
						<div class="form-group">
							<label>Status</label>
							<select name="status" class="required" style="display: block; width: 270px; height: 34px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
								<option value="">- Pilih Status -</option>
								<option value="Y" SELECTED>Aktif</option>
								<option value="N">Tidak Aktif</option>
							</select>
						</div>
						<button type="submit" class="btn btn-primary">Simpan</button>
					</form>
				
				<?php }elseif($_smarty_tpl->tpl_vars['module']->value=='funeral'&&$_smarty_tpl->tpl_vars['act']->value=='edit'){?>
				
					<h3>Ubah Pemakaman</h3>
					<form role="form" method="POST" action="funeral.php?module=funeral&act=update" id="frm_funeral">
						<input type="hidden" name="funeralID" value="<?php echo $_smarty_tpl->tpl_vars['funeralID']->value;?>
">
						<div class="form-group">
							<label>Nama Pemakaman</label>
							<input type="text" name="funeralName" value="<?php echo $_smarty_tpl->tpl_vars['funeralName']->value;?>
" class="required" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
						</div>
						<div class="form-group">
							<label>Alamat</label>
							<input type="text" name="funeralAddress" value="<?php echo $_smarty_tpl->tpl_vars['funeralAddress']->value;?>
" class="required" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
						</div>
						<div class="form-group">
							<label>Status</label>
							<select name="status" class="required" style="display: block; width: 270px; height: 34px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
								<option value="">- Pilih Status -</option>
								<option value="Y" <?php if ($_smarty_tpl->tpl_vars['status']->value=='Y'){?> SELECTED <?php }?>>Aktif</option>
								<option value="N" <?php if ($_smarty_tpl->tpl_vars['status']->value=='N'){?> SELECTED <?php }?>>Tidak Aktif</option>
							</select>
						</div>
						<button type="submit" class="btn btn-primary">Simpan</button>
					</form>
					
				<?php }else{ ?>
			
					<a href="funeral.php?module=funeral&act=add"><button class="btn btn-primary" type="button">Tambah Pemakaman</button></a>
					<h3>Manajemen Pemakaman</h3>
					<div class="table-responsive">
						<table class="table table-bordered table-hover tablesorter">
							<thead>
								<tr>
									<th>No. <i class="fa fa-sort"></i></th>
									<th>Nama Pemakaman <i class="fa fa-sort"></i></th>
									<th>Alamat <i class="fa fa-sort"></i></th>
									<th>Status <i class="fa fa-sort"></i></th>
									<th>Aksi <i class="fa fa-sort"></i></th>
								</tr>
							</thead>
							<tbody>
								<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['dataFuneral'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['dataFuneral']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataFuneral']['name'] = 'dataFuneral';
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataFuneral']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['dataFuneral']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataFuneral']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataFuneral']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataFuneral']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataFuneral']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataFuneral']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataFuneral']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['dataFuneral']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataFuneral']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['dataFuneral']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataFuneral']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataFuneral']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['dataFuneral']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['dataFuneral']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataFuneral']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataFuneral']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataFuneral']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['dataFuneral']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['dataFuneral']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['dataFuneral']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['dataFuneral']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['dataFuneral']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['dataFuneral']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataFuneral']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataFuneral']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataFuneral']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataFuneral']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['dataFuneral']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataFuneral']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataFuneral']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['dataFuneral']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataFuneral']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataFuneral']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataFuneral']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataFuneral']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['dataFuneral']['total']);
?>
								<tr>
									<td><?php echo $_smarty_tpl->tpl_vars['dataFuneral']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataFuneral']['index']]['no'];?>
</td>
									<td><?php echo $_smarty_tpl->tpl_vars['dataFuneral']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataFuneral']['index']]['funeralName'];?>
</td>
									<td><?php echo $_smarty_tpl->tpl_vars['dataFuneral']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataFuneral']['index']]['funeralAddress'];?>
</td>
									<td><?php echo $_smarty_tpl->tpl_vars['dataFuneral']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataFuneral']['index']]['status'];?>
</td>
									<td>
										<a href="funeral.php?module=funeral&act=edit&funeralID=<?php echo $_smarty_tpl->tpl_vars['dataFuneral']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataFuneral']['index']]['funeralID'];?>
"><button type="button" class="btn btn-success">Edit</button></a>
										<a href="funeral.php?module=funeral&act=delete&funeralID=<?php echo $_smarty_tpl->tpl_vars['dataFuneral']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataFuneral']['index']]['funeralID'];?>
" onclick="return confirm('Anda Yakin ingin menghapus <?php echo $_smarty_tpl->tpl_vars['dataFuneral']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataFuneral']['index']]['funeralName'];?>
?');"><button type="button" class="btn btn-danger">Hapus</button></a>
									</td>
								</tr>
								<?php endfor; endif; ?>
							</tbody>
						</table>
					</div>
					<div id="paging"><?php echo $_smarty_tpl->tpl_vars['pageLink']->value;?>
</div>
				<?php }?>
			</div>
		</div>
	</div>
</div><!-- /#wrapper -->

<?php echo $_smarty_tpl->getSubTemplate ("footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>
<?php }} ?>