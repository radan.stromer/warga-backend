<?php /* Smarty version Smarty-3.1.11, created on 2018-04-13 21:10:21
         compiled from ".\templates\pendeta.tpl" */ ?>
<?php /*%%SmartyHeaderCode:175695ad0b6e2ea9e41-66197442%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '413465fb65d0779b31679c311dbf0004f129db26' => 
    array (
      0 => '.\\templates\\pendeta.tpl',
      1 => 1523628598,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '175695ad0b6e2ea9e41-66197442',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.11',
  'unifunc' => 'content_5ad0b6e3134490_55295628',
  'variables' => 
  array (
    'code' => 0,
    'module' => 0,
    'act' => 0,
    'dataAnggota' => 0,
    'tokoh_id' => 0,
    'anggota_id' => 0,
    'jabatan' => 0,
    'tanggal_tasbih' => 0,
    'status' => 0,
    'biografi' => 0,
    'dataPendeta' => 0,
    'pageLink' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5ad0b6e3134490_55295628')) {function content_5ad0b6e3134490_55295628($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>


<div id="wrapper">
	
	<?php echo $_smarty_tpl->getSubTemplate ("leftMenu.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

	
	<div id="page-wrapper">
		<div class="row">
			<div class="col-lg-12">
				<ol class="breadcrumb">
					<li><a href="home.php"><i class="fa fa-dashboard"></i> Home</a></li>
					<li class="active"><i class="fa fa-caret-square-o-down"></i> Manajemen Tokoh Masyarakat</li>
					<li class="active">Tokoh Masyarakat</li>
				</ol>
				
				<?php if ($_smarty_tpl->tpl_vars['code']->value=='1'){?>
					<div class="alert alert-success alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						Data tokoh msayarakat berhasil disimpan.
					</div>
				<?php }?>
				<?php if ($_smarty_tpl->tpl_vars['code']->value=='2'){?>
					<div class="alert alert-success alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						Data tokoh masyarakat berhasil diupdate.
					</div>
				<?php }?>
				<?php if ($_smarty_tpl->tpl_vars['code']->value=='3'){?>
					<div class="alert alert-success alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						Data tokoh msayarakat berhasil dihapus.
					</div>
				<?php }?>
			</div>
		</div><!-- /.row -->
		
		<script type='text/javascript' src='js/jquery.validate.js'></script>
			
		
			<script type='text/javascript'>
				$(document).ready(function() {
					$('#frm_pendeta').validate({
						rules:{
							anggota_id: true,
							jabatan: true,
							status: true
						},
						messages:{
							anggota_id:{
								required: "This is a required field."
							},
							jabatan:{
								required: "This is a required field."
							},
							status:{
								required: "This is a required field."
							}
						}
					});
					
					$( "#datepicker1" ).datepicker({
						changeMonth: true,
						changeYear: true,
						dateFormat: "yy-mm-dd",
						yearRange: 'c-100:c-0'
					});
					
					$( "#datepicker2" ).datepicker({
						changeMonth: true,
						changeYear: true,
						dateFormat: "yy-mm-dd",
						yearRange: 'c-100:c-0'
					});
				});
			</script>
		
		
		<div class="row">
			<div class="col-lg-12">
				
				<?php if ($_smarty_tpl->tpl_vars['module']->value=='pendeta'&&$_smarty_tpl->tpl_vars['act']->value=='add'){?>
				
					<h3>Tambah Tokoh Masyarakat</h3>
					<form role="form" method="POST" action="pendeta.php?module=pendeta&act=input" id="frm_pendeta">
						<div class="form-group">
							<label>Nama Tokoh Masyarakat</label>
							<select name="anggota_id" class="required" style="display: block; width: 270px; height: 34px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
								<option value="">- Pilih Tokoh Masyarakat -</option>
								<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['name'] = 'dataAnggota';
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['dataAnggota']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['total']);
?>
									<option value="<?php echo $_smarty_tpl->tpl_vars['dataAnggota']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataAnggota']['index']]['anggota_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['dataAnggota']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataAnggota']['index']]['full_name'];?>
</option>
								<?php endfor; endif; ?>
							</select>
						</div>
						<div class="form-group">
							<label>Jabatan</label>
							<input type="text" name="jabatan" class="required" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
						</div>
						<div class="form-group">
							<label>Tanggal Pengangkatan</label>
							<input type="text" id="datepicker1" name="tanggal_tasbih" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
						</div>
						<div class="form-group">
							<label>Status</label>
							<select name="status" class="required" style="display: block; width: 270px; height: 34px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
								<option value="">- Pilih Status -</option>
								<option value="Y">Aktif</option>
								<option value="N">Tidak Aktif</option>
							</select>
						</div>
						<div class="form-group">
							<label>Biografi</label>
							<textarea name="biografi" style="display: block; width: 400px; height: 100px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;"></textarea>
						</div>
						<button type="submit" class="btn btn-primary">Simpan</button>
					</form>
				
				<?php }elseif($_smarty_tpl->tpl_vars['module']->value=='pendeta'&&$_smarty_tpl->tpl_vars['act']->value=='edit'){?>
				
					<h3>Ubah Tokoh Masyarakat</h3>
					<form role="form" method="POST" action="pendeta.php?module=pendeta&act=update" id="frm_pendeta">
						<input type="hidden" name="pendeta_id" value="<?php echo $_smarty_tpl->tpl_vars['tokoh_id']->value;?>
">
						<div class="form-group">
							<label>Nama Tokoh Masyarakat</label>
							<select name="anggota_id" class="required" style="display: block; width: 270px; height: 34px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
								<option value="">- Pilih Tokoh Masyarakat -</option>
								<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['name'] = 'dataAnggota';
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['dataAnggota']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['total']);
?>
									<?php if ($_smarty_tpl->tpl_vars['dataAnggota']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataAnggota']['index']]['anggota_id']==$_smarty_tpl->tpl_vars['anggota_id']->value){?>
										<option value="<?php echo $_smarty_tpl->tpl_vars['dataAnggota']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataAnggota']['index']]['anggota_id'];?>
" SELECTED><?php echo $_smarty_tpl->tpl_vars['dataAnggota']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataAnggota']['index']]['full_name'];?>
</option>
									<?php }else{ ?>
										<option value="<?php echo $_smarty_tpl->tpl_vars['dataAnggota']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataAnggota']['index']]['anggota_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['dataAnggota']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataAnggota']['index']]['full_name'];?>
</option>
									<?php }?>
								<?php endfor; endif; ?>
							</select>
						</div>
						<div class="form-group">
							<label>Jabatan</label>
							<input type="text" value="<?php echo $_smarty_tpl->tpl_vars['jabatan']->value;?>
" name="jabatan" class="required" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
						</div>
						<div class="form-group">
							<label>Tanggal Pengangkatan</label>
							<input type="text" id="datepicker1" value="<?php echo $_smarty_tpl->tpl_vars['tanggal_tasbih']->value;?>
" name="tanggal_tasbih" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
						</div>
						<div class="form-group">
							<label>Status</label>
							<select name="status" class="required" style="display: block; width: 270px; height: 34px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
								<option value="">- Pilih Status -</option>
								<option value="Y" <?php if ($_smarty_tpl->tpl_vars['status']->value=='Y'){?> SELECTED <?php }?>>Aktif</option>
								<option value="N" <?php if ($_smarty_tpl->tpl_vars['status']->value=='N'){?> SELECTED <?php }?>>Tidak Aktif</option>
							</select>
						</div>
						<div class="form-group">
							<label>Biografi</label>
							<textarea name="biografi" style="display: block; width: 400px; height: 100px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;"><?php echo $_smarty_tpl->tpl_vars['biografi']->value;?>
</textarea>
						</div>
						<button type="submit" class="btn btn-primary">Simpan</button>
					</form>
					
				<?php }else{ ?>
			
					<a href="pendeta.php?module=pendeta&act=add"><button class="btn btn-primary" type="button">Tambah Tokoh Masyarakat</button></a>
					<a href="print_pendeta.php?module=pendeta&act=print" target="_blank"><button class="btn btn-primary" type="button">Cetak</button></a>
					<h3>Manajemen Tokoh Masyarakat</h3>
					<div class="table-responsive">
						<table class="table table-bordered table-hover tablesorter">
							<thead>
								<tr>
									<th>No. <i class="fa fa-sort"></i></th>
									<th>No Induk <i class="fa fa-sort"></i></th>
									<th>Nama Lengkap <i class="fa fa-sort"></i></th>
									<th>JK <i class="fa fa-sort"></i></th>
									<th>Jabatan <i class="fa fa-sort"></i></th>
									<th>Tanggal Pentahbisan <i class="fa fa-sort"></i></th>
									<th>Status <i class="fa fa-sort"></i></th>
									<th>Aksi <i class="fa fa-sort"></i></th>
								</tr>
							</thead>
							<tbody>
								<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['dataPendeta'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['dataPendeta']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataPendeta']['name'] = 'dataPendeta';
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataPendeta']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['dataPendeta']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataPendeta']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataPendeta']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPendeta']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataPendeta']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataPendeta']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPendeta']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPendeta']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataPendeta']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPendeta']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPendeta']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataPendeta']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPendeta']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPendeta']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataPendeta']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataPendeta']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPendeta']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPendeta']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPendeta']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPendeta']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPendeta']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPendeta']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPendeta']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataPendeta']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPendeta']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataPendeta']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPendeta']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPendeta']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataPendeta']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPendeta']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPendeta']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataPendeta']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataPendeta']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataPendeta']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataPendeta']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPendeta']['total']);
?>
								<tr>
									<td><?php echo $_smarty_tpl->tpl_vars['dataPendeta']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataPendeta']['index']]['no'];?>
</td>
									<td><?php echo $_smarty_tpl->tpl_vars['dataPendeta']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataPendeta']['index']]['no_induk'];?>
</td>
									<td><?php echo $_smarty_tpl->tpl_vars['dataPendeta']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataPendeta']['index']]['full_name'];?>
</td>
									<td><?php echo $_smarty_tpl->tpl_vars['dataPendeta']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataPendeta']['index']]['gender'];?>
</td>
									<td><?php echo $_smarty_tpl->tpl_vars['dataPendeta']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataPendeta']['index']]['jabatan'];?>
</td>
									<td><?php echo $_smarty_tpl->tpl_vars['dataPendeta']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataPendeta']['index']]['tanggal_tasbih'];?>
</td>
									<td><?php echo $_smarty_tpl->tpl_vars['dataPendeta']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataPendeta']['index']]['status'];?>
</td>
									<td>
										<a href="pendeta.php?module=pendeta&act=edit&pendeta_id=<?php echo $_smarty_tpl->tpl_vars['dataPendeta']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataPendeta']['index']]['pendeta_id'];?>
"><button type="button" class="btn btn-success">Edit</button></a>
										<a href="pendeta.php?module=pendeta&act=delete&pendeta_id=<?php echo $_smarty_tpl->tpl_vars['dataPendeta']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataPendeta']['index']]['pendeta_id'];?>
" onclick="return confirm('Anda Yakin ingin menghapus <?php echo $_smarty_tpl->tpl_vars['dataPendeta']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataPendeta']['index']]['full_name'];?>
?');"><button type="button" class="btn btn-danger">Hapus</button></a>
									</td>
								</tr>
								<?php endfor; endif; ?>
							</tbody>
						</table>
					</div>
					<div id="paging"><?php echo $_smarty_tpl->tpl_vars['pageLink']->value;?>
</div>
				<?php }?>
			</div>
		</div>
	</div>
</div><!-- /#wrapper -->

<?php echo $_smarty_tpl->getSubTemplate ("footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>
<?php }} ?>