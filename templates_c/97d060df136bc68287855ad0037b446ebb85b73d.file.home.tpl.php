<?php /* Smarty version Smarty-3.1.11, created on 2018-03-31 05:44:07
         compiled from ".\templates\home.tpl" */ ?>
<?php /*%%SmartyHeaderCode:190825abebdb717f4a9-94810353%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '97d060df136bc68287855ad0037b446ebb85b73d' => 
    array (
      0 => '.\\templates\\home.tpl',
      1 => 1407265646,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '190825abebdb717f4a9-94810353',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'code' => 0,
    'start1' => 0,
    'end1' => 0,
    'hari_ini' => 0,
    'hari_besok' => 0,
    'dataBirthday' => 0,
    'dataWedding' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.11',
  'unifunc' => 'content_5abebdb74de129_91617670',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5abebdb74de129_91617670')) {function content_5abebdb74de129_91617670($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>


<div id="wrapper">
	
	<?php echo $_smarty_tpl->getSubTemplate ("leftMenu.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

	
	<div id="page-wrapper">
		<div class="row">
			<div class="col-lg-12">
				<ol class="breadcrumb">
					<li class="active"><i class="fa fa-dashboard"></i> Home</li>
				</ol>
				
				<?php if ($_smarty_tpl->tpl_vars['code']->value=='1'){?>
					<div class="alert alert-success alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						Login Anda berhasil dan saat ini telah memasuki halaman dashboard Administrator.
					</div>
				<?php }?>
				
				
					<script>
						$(document).ready(function() {
							$( "#datepicker1" ).datepicker({
								changeMonth: true,
								changeYear: true,
								dateFormat: "yy-mm-dd",
								yearRange: 'c-100:c-0'
							});
							
							$( "#datepicker2" ).datepicker({
								changeMonth: true,
								changeYear: true,
								dateFormat: "yy-mm-dd",
								yearRange: 'c-100:c-0'
							});
						});
					</script>
				
				
				<a href="print_all.php?module=all&act=print" target="_blank"><button class="btn btn-primary" type="button">Cetak Semua</button></a>
				<a href="print_all_birthday.php?module=birthday&act=print" target="_blank"><button class="btn btn-primary" type="button">Birthday</button></a>
				<a href="print_all_wedding.php?module=wedding&act=print" target="_blank"><button class="btn btn-primary" type="button">Wedding</button></a>
				
				<br><br>
				<form method="GET" action="home.php">
				<input type="hidden" name="act" value="search">
				<table bgcolor="#CCCCCC;">
					<tr>
						<td width="120"><b>Periode Awal : </b></td>
						<td width="320"><input type="text" value="<?php echo $_smarty_tpl->tpl_vars['start1']->value;?>
" name="startDate" id="datepicker1" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;" required></td>
						<td width="120"><b>Periode Akhir : </b></td>
						<td width="280"><input type="text" name="endDate" value="<?php echo $_smarty_tpl->tpl_vars['end1']->value;?>
" id="datepicker2" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;" required></td>
						<td width="100"><button class="btn btn-primary" type="submit">Cari</button></td>
					</tr>
				</table>
				</form>
				<br>
				<h3>Periode : <?php echo $_smarty_tpl->tpl_vars['hari_ini']->value;?>
 s/d <?php echo $_smarty_tpl->tpl_vars['hari_besok']->value;?>
</h3>
				<h4>Jemaat yang Berulang Tahun Minggu Ini</h4>
				<div class="table-responsive">
					<table class="table table-bordered table-hover tablesorter">
						<thead>
							<tr>
								<th>No. <i class="fa fa-sort"></i></th>
								<th>No Induk <i class="fa fa-sort"></i></th>
								<th>Nama Lengkap <i class="fa fa-sort"></i></th>
								<th>Tanggal Lahir <i class="fa fa-sort"></i></th>
								<th>Usia (Tahun) <i class="fa fa-sort"></i></th>
								<th>Gender <i class="fa fa-sort"></i></th>
								<th>Jemaat <i class="fa fa-sort"></i></th>
								<th>Foto <i class="fa fa-sort"></i></th>
							</tr>
						</thead>
						<tbody>
							<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['dataBirthday'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['dataBirthday']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataBirthday']['name'] = 'dataBirthday';
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataBirthday']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['dataBirthday']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataBirthday']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataBirthday']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataBirthday']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataBirthday']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataBirthday']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataBirthday']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['dataBirthday']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataBirthday']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['dataBirthday']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataBirthday']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataBirthday']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['dataBirthday']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['dataBirthday']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataBirthday']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataBirthday']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataBirthday']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['dataBirthday']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['dataBirthday']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['dataBirthday']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['dataBirthday']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['dataBirthday']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['dataBirthday']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataBirthday']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataBirthday']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataBirthday']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataBirthday']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['dataBirthday']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataBirthday']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataBirthday']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['dataBirthday']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataBirthday']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataBirthday']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataBirthday']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataBirthday']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['dataBirthday']['total']);
?>
							<tr>
								<td><?php echo $_smarty_tpl->tpl_vars['dataBirthday']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataBirthday']['index']]['no'];?>
</td>
								<td><?php echo $_smarty_tpl->tpl_vars['dataBirthday']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataBirthday']['index']]['no_induk'];?>
</td>
								<td><?php echo $_smarty_tpl->tpl_vars['dataBirthday']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataBirthday']['index']]['full_name'];?>
</td>
								<td><?php echo $_smarty_tpl->tpl_vars['dataBirthday']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataBirthday']['index']]['date_of_birth'];?>
</td>
								<td><?php echo $_smarty_tpl->tpl_vars['dataBirthday']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataBirthday']['index']]['age'];?>
</td>
								<td><?php echo $_smarty_tpl->tpl_vars['dataBirthday']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataBirthday']['index']]['gender'];?>
</td>
								<td><?php echo $_smarty_tpl->tpl_vars['dataBirthday']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataBirthday']['index']]['status'];?>
</td>
								<td><?php if ($_smarty_tpl->tpl_vars['dataBirthday']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataBirthday']['index']]['photo']!=''){?> <a href="images/photo_individu/<?php echo $_smarty_tpl->tpl_vars['dataBirthday']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataBirthday']['index']]['photo'];?>
" target="_blank"><img src="images/photo_individu/<?php echo $_smarty_tpl->tpl_vars['dataBirthday']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataBirthday']['index']]['photo'];?>
" height="30" width="30"></a> <?php }?></td>
							</tr>
							<?php endfor; endif; ?>
						</tbody>
					</table>
				</div>
				
				<h4>Ulang Tahun Pernikahan</h4>
				<div class="table-responsive">
					<table class="table table-bordered table-hover tablesorter">
						<thead>
							<tr>
								<th>No. <i class="fa fa-sort"></i></th>
								<th>NIK <i class="fa fa-sort"></i></th>
								<th>Nama Suami <i class="fa fa-sort"></i></th>
								<th>Nama Istri <i class="fa fa-sort"></i></th>
								<th>Tanggal Pernikahan <i class="fa fa-sort"></i></th>
								<th>Usia Pernikahan (Tahun) <i class="fa fa-sort"></i></th>
								<th>Foto <i class="fa fa-sort"></i></th>
							</tr>
						</thead>
						<tbody>
							<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['dataWedding'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['dataWedding']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataWedding']['name'] = 'dataWedding';
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataWedding']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['dataWedding']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataWedding']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataWedding']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataWedding']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataWedding']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataWedding']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataWedding']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['dataWedding']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataWedding']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['dataWedding']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataWedding']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataWedding']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['dataWedding']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['dataWedding']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataWedding']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataWedding']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataWedding']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['dataWedding']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['dataWedding']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['dataWedding']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['dataWedding']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['dataWedding']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['dataWedding']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataWedding']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataWedding']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataWedding']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataWedding']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['dataWedding']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataWedding']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataWedding']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['dataWedding']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataWedding']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataWedding']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataWedding']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataWedding']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['dataWedding']['total']);
?>
							<tr>
								<td><?php echo $_smarty_tpl->tpl_vars['dataWedding']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataWedding']['index']]['no'];?>
</td>
								<td><?php echo $_smarty_tpl->tpl_vars['dataWedding']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataWedding']['index']]['nik'];?>
</td>
								<td><?php echo $_smarty_tpl->tpl_vars['dataWedding']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataWedding']['index']]['suami'];?>
</td>
								<td><?php echo $_smarty_tpl->tpl_vars['dataWedding']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataWedding']['index']]['istri'];?>
</td>
								<td><?php echo $_smarty_tpl->tpl_vars['dataWedding']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataWedding']['index']]['tanggal_nikah'];?>
</td>
								<td><?php echo $_smarty_tpl->tpl_vars['dataWedding']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataWedding']['index']]['age'];?>
</td>
								<td><?php if ($_smarty_tpl->tpl_vars['dataWedding']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataWedding']['index']]['photo']!=''){?> <a href="images/photo_nikah/<?php echo $_smarty_tpl->tpl_vars['dataWedding']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataWedding']['index']]['photo'];?>
" target="_blank"><img src="images/photo_nikah/<?php echo $_smarty_tpl->tpl_vars['dataWedding']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataWedding']['index']]['photo'];?>
" height="30" width="30"></a> <?php }?></td>
							</tr>
							<?php endfor; endif; ?>
						</tbody>
					</table>
				</div>
			</div>
		</div><!-- /.row -->
	</div>
</div><!-- /#wrapper -->

<?php echo $_smarty_tpl->getSubTemplate ("footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>
<?php }} ?>