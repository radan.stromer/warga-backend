<?php /* Smarty version Smarty-3.1.11, created on 2018-04-13 20:00:33
         compiled from ".\templates\staff.tpl" */ ?>
<?php /*%%SmartyHeaderCode:12795ad0a9f1d86a93-62699895%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '70b9404c19f013cc072d54e91820c6ca7ed2db26' => 
    array (
      0 => '.\\templates\\staff.tpl',
      1 => 1405884600,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '12795ad0a9f1d86a93-62699895',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'code' => 0,
    'module' => 0,
    'act' => 0,
    'dataAnggota' => 0,
    'staff_id' => 0,
    'anggota_id' => 0,
    'jabatan' => 0,
    'tanggal_mulai' => 0,
    'tanggal_keluar' => 0,
    'status' => 0,
    'dataStaff' => 0,
    'pageLink' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.11',
  'unifunc' => 'content_5ad0a9f1e96341_65865313',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5ad0a9f1e96341_65865313')) {function content_5ad0a9f1e96341_65865313($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>


<div id="wrapper">
	
	<?php echo $_smarty_tpl->getSubTemplate ("leftMenu.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

	
	<div id="page-wrapper">
		<div class="row">
			<div class="col-lg-12">
				<ol class="breadcrumb">
					<li><a href="home.php"><i class="fa fa-dashboard"></i> Home</a></li>
					<li class="active"><i class="fa fa-caret-square-o-down"></i> Manajemen Staff</li>
					<li class="active">Staff</li>
				</ol>
				
				<?php if ($_smarty_tpl->tpl_vars['code']->value=='1'){?>
					<div class="alert alert-success alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						Data staff berhasil disimpan.
					</div>
				<?php }?>
				<?php if ($_smarty_tpl->tpl_vars['code']->value=='2'){?>
					<div class="alert alert-success alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						Data staff berhasil diupdate.
					</div>
				<?php }?>
				<?php if ($_smarty_tpl->tpl_vars['code']->value=='3'){?>
					<div class="alert alert-success alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						Data staff berhasil dihapus.
					</div>
				<?php }?>
			</div>
		</div><!-- /.row -->
		
		<script type='text/javascript' src='js/jquery.validate.js'></script>
			
		
			<script type='text/javascript'>
				$(document).ready(function() {
					$('#frm_staff').validate({
						rules:{
							anggota_id: true,
							jabatan: true,
							status: true
						},
						messages:{
							anggota_id:{
								required: "This is a required field."
							},
							jabatan:{
								required: "This is a required field."
							},
							status:{
								required: "This is a required field."
							}
						}
					});
					
					$( "#datepicker1" ).datepicker({
						changeMonth: true,
						changeYear: true,
						dateFormat: "yy-mm-dd",
						yearRange: 'c-100:c-0'
					});
					
					$( "#datepicker2" ).datepicker({
						changeMonth: true,
						changeYear: true,
						dateFormat: "yy-mm-dd",
						yearRange: 'c-100:c-0'
					});
				});
			</script>
		
		
		<div class="row">
			<div class="col-lg-12">
				
				<?php if ($_smarty_tpl->tpl_vars['module']->value=='staff'&&$_smarty_tpl->tpl_vars['act']->value=='add'){?>
				
					<h3>Tambah Staff</h3>
					<form role="form" method="POST" action="staff.php?module=staff&act=input" id="frm_staff">
						<div class="form-group">
							<label>Anggota</label>
							<select name="anggota_id" class="required" style="display: block; width: 270px; height: 34px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
								<option value="">- Pilih Anggota -</option>
								<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['name'] = 'dataAnggota';
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['dataAnggota']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['total']);
?>
									<option value="<?php echo $_smarty_tpl->tpl_vars['dataAnggota']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataAnggota']['index']]['anggota_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['dataAnggota']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataAnggota']['index']]['full_name'];?>
</option>
								<?php endfor; endif; ?>
							</select>
						</div>
						<div class="form-group">
							<label>Jabatan</label>
							<input type="text" name="jabatan" class="required" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
						</div>
						<div class="form-group">
							<label>Tanggal Mulai</label>
							<input type="text" id="datepicker1" name="tanggal_mulai" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
						</div>
						<div class="form-group">
							<label>Tanggal Keluar</label>
							<input type="text" id="datepicker2" name="tanggal_keluar" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
						</div>
						<div class="form-group">
							<label>Status</label>
							<select name="status" class="required" style="display: block; width: 270px; height: 34px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
								<option value="">- Pilih Status -</option>
								<option value="Y" SELECTED>Aktif</option>
								<option value="N">Tidak Aktif</option>
							</select>
						</div>
						<button type="submit" class="btn btn-primary">Simpan</button>
					</form>
				
				<?php }elseif($_smarty_tpl->tpl_vars['module']->value=='staff'&&$_smarty_tpl->tpl_vars['act']->value=='edit'){?>
				
					<h3>Ubah Staff</h3>
					<form role="form" method="POST" action="staff.php?module=staff&act=update" id="frm_staff">
						<input type="hidden" name="staff_id" value="<?php echo $_smarty_tpl->tpl_vars['staff_id']->value;?>
">
						<div class="form-group">
							<label>Anggota</label>
							<select name="anggota_id" class="required" style="display: block; width: 270px; height: 34px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
								<option value="">- Pilih Anggota -</option>
								<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['name'] = 'dataAnggota';
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['dataAnggota']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['total']);
?>
									<?php if ($_smarty_tpl->tpl_vars['dataAnggota']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataAnggota']['index']]['anggota_id']==$_smarty_tpl->tpl_vars['anggota_id']->value){?>
										<option value="<?php echo $_smarty_tpl->tpl_vars['dataAnggota']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataAnggota']['index']]['anggota_id'];?>
" SELECTED><?php echo $_smarty_tpl->tpl_vars['dataAnggota']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataAnggota']['index']]['full_name'];?>
</option>
									<?php }else{ ?>
										<option value="<?php echo $_smarty_tpl->tpl_vars['dataAnggota']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataAnggota']['index']]['anggota_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['dataAnggota']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataAnggota']['index']]['full_name'];?>
</option>
									<?php }?>
								<?php endfor; endif; ?>
							</select>
						</div>
						<div class="form-group">
							<label>Jabatan</label>
							<input type="text" value="<?php echo $_smarty_tpl->tpl_vars['jabatan']->value;?>
" name="jabatan" class="required" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
						</div>
						<div class="form-group">
							<label>Tanggal Mulai</label>
							<input type="text" value="<?php echo $_smarty_tpl->tpl_vars['tanggal_mulai']->value;?>
" id="datepicker1" name="tanggal_mulai" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
						</div>
						<div class="form-group">
							<label>Tanggal Keluar</label>
							<input type="text" value="<?php echo $_smarty_tpl->tpl_vars['tanggal_keluar']->value;?>
" id="datepicker2" name="tanggal_keluar" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
						</div>
						<div class="form-group">
							<label>Status</label>
							<select name="status" class="required" style="display: block; width: 270px; height: 34px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
								<option value="">- Pilih Status -</option>
								<option value="Y" <?php if ($_smarty_tpl->tpl_vars['status']->value=='Y'){?> SELECTED <?php }?>>Aktif</option>
								<option value="N" <?php if ($_smarty_tpl->tpl_vars['status']->value=='N'){?> SELECTED <?php }?>>Tidak Aktif</option>
							</select>
						</div>
						<button type="submit" class="btn btn-primary">Simpan</button>
					</form>
					
				<?php }else{ ?>
			
					<a href="staff.php?module=staff&act=add"><button class="btn btn-primary" type="button">Tambah Staff</button></a>
					<a href="print_staff.php?module=staff&act=print" target="_blank"><button class="btn btn-primary" type="button">Cetak</button></a>
					<h3>Manajemen Staff</h3>
					<div class="table-responsive">
						<table class="table table-bordered table-hover tablesorter">
							<thead>
								<tr>
									<th>No. <i class="fa fa-sort"></i></th>
									<th>No Induk <i class="fa fa-sort"></i></th>
									<th>Nama Lengkap <i class="fa fa-sort"></i></th>
									<th>Jabatan <i class="fa fa-sort"></i></th>
									<th>Tanggal Mulai <i class="fa fa-sort"></i></th>
									<th>Tanggal Keluar <i class="fa fa-sort"></i></th>
									<th>Status <i class="fa fa-sort"></i></th>
									<th>Aksi <i class="fa fa-sort"></i></th>
								</tr>
							</thead>
							<tbody>
								<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['dataStaff'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['dataStaff']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataStaff']['name'] = 'dataStaff';
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataStaff']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['dataStaff']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataStaff']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataStaff']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataStaff']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataStaff']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataStaff']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataStaff']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['dataStaff']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataStaff']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['dataStaff']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataStaff']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataStaff']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['dataStaff']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['dataStaff']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataStaff']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataStaff']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataStaff']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['dataStaff']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['dataStaff']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['dataStaff']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['dataStaff']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['dataStaff']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['dataStaff']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataStaff']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataStaff']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataStaff']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataStaff']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['dataStaff']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataStaff']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataStaff']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['dataStaff']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataStaff']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataStaff']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataStaff']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataStaff']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['dataStaff']['total']);
?>
								<tr>
									<td><?php echo $_smarty_tpl->tpl_vars['dataStaff']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataStaff']['index']]['no'];?>
</td>
									<td><?php echo $_smarty_tpl->tpl_vars['dataStaff']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataStaff']['index']]['no_induk'];?>
</td>
									<td><?php echo $_smarty_tpl->tpl_vars['dataStaff']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataStaff']['index']]['full_name'];?>
</td>
									<td><?php echo $_smarty_tpl->tpl_vars['dataStaff']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataStaff']['index']]['jabatan'];?>
</td>
									<td><?php echo $_smarty_tpl->tpl_vars['dataStaff']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataStaff']['index']]['tanggal_mulai'];?>
</td>
									<td><?php echo $_smarty_tpl->tpl_vars['dataStaff']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataStaff']['index']]['tanggal_keluar'];?>
</td>
									<td><?php echo $_smarty_tpl->tpl_vars['dataStaff']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataStaff']['index']]['status'];?>
</td>
									<td>
										<a href="staff.php?module=staff&act=edit&staff_id=<?php echo $_smarty_tpl->tpl_vars['dataStaff']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataStaff']['index']]['staff_id'];?>
"><button type="button" class="btn btn-success">Edit</button></a>
										<a href="staff.php?module=staff&act=delete&staff_id=<?php echo $_smarty_tpl->tpl_vars['dataStaff']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataStaff']['index']]['staff_id'];?>
" onclick="return confirm('Anda Yakin ingin menghapus <?php echo $_smarty_tpl->tpl_vars['dataStaff']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataStaff']['index']]['full_name'];?>
?');"><button type="button" class="btn btn-danger">Hapus</button></a>
									</td>
								</tr>
								<?php endfor; endif; ?>
							</tbody>
						</table>
					</div>
					<div id="paging"><?php echo $_smarty_tpl->tpl_vars['pageLink']->value;?>
</div>
				<?php }?>
			</div>
		</div>
	</div>
</div><!-- /#wrapper -->

<?php echo $_smarty_tpl->getSubTemplate ("footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>
<?php }} ?>