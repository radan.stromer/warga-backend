<?php /* Smarty version Smarty-3.1.11, created on 2018-03-29 05:44:10
         compiled from ".\templates\users.tpl" */ ?>
<?php /*%%SmartyHeaderCode:303135abc1abab58f20-13093319%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '9029d314d9148180573fc9a8dab741b06bfa3844' => 
    array (
      0 => '.\\templates\\users.tpl',
      1 => 1405884600,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '303135abc1abab58f20-13093319',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'code' => 0,
    'module' => 0,
    'act' => 0,
    'userID' => 0,
    'fullName' => 0,
    'address' => 0,
    'gender' => 0,
    'position' => 0,
    'handPhone' => 0,
    'status' => 0,
    'blocked' => 0,
    'username' => 0,
    'dataUser' => 0,
    'pageLink' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.11',
  'unifunc' => 'content_5abc1abaea8cd2_69148177',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5abc1abaea8cd2_69148177')) {function content_5abc1abaea8cd2_69148177($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>


<div id="wrapper">
	
	<?php echo $_smarty_tpl->getSubTemplate ("leftMenu.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

	
	<div id="page-wrapper">
		<div class="row">
			<div class="col-lg-12">
				<ol class="breadcrumb">
					<li><a href="home.php"><i class="fa fa-dashboard"></i> Home</a></li>
					<li class="active"><i class="fa fa-caret-square-o-down"></i> Master Data</li>
					<li class="active">Pengguna</li>
				</ol>
				
				<?php if ($_smarty_tpl->tpl_vars['code']->value=='1'){?>
					<div class="alert alert-success alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						Data pengguna berhasil disimpan.
					</div>
				<?php }?>
				<?php if ($_smarty_tpl->tpl_vars['code']->value=='2'){?>
					<div class="alert alert-success alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						Data pengguna berhasil diupdate.
					</div>
				<?php }?>
				<?php if ($_smarty_tpl->tpl_vars['code']->value=='3'){?>
					<div class="alert alert-success alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						Data pengguna berhasil dihapus.
					</div>
				<?php }?>
				<?php if ($_smarty_tpl->tpl_vars['code']->value=='4'){?>
					<div class="alert alert-success alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						Reset password pengguna berhasil disimpan.
					</div>
				<?php }?>
			</div>
		</div><!-- /.row -->
		
		<script type='text/javascript' src='js/jquery.validate.js'></script>
			
		
			<script type='text/javascript'>
				$(document).ready(function() {
					$('#frm_user').validate({
						rules:{
							fullName: true,
							address: true,
							gender: true,
							position: true,
							handPhone: true,
							status: true,
							blocked: true,
							username: true,
							password: true
						},
						messages:{
							fullName:{
								required: "This is a required field."
							},
							address:{
								required: "This is a required field."
							},
							gender:{
								required: "This is a required field."
							},
							position:{
								required: "This is a required field."
							},
							handPhone:{
								required: "This is a required field."
							},
							status:{
								required: "This is a required field."
							},
							blocked:{
								required: "This is a required field."
							},
							username:{
								required: "This is a required field."
							},
							password:{
								required: "This is a required field."
							}
						}
					});
					
					$('#frm_reset').validate({
						rules:{
							username: true,
							password: true
						},
						messages:{
							username:{
								required: "This is a required field."
							},
							password:{
								required: "This is a required field."
							}
						}
					});
				});
			</script>
		
		
		<div class="row">
			<div class="col-lg-12">
				
				<?php if ($_smarty_tpl->tpl_vars['module']->value=='user'&&$_smarty_tpl->tpl_vars['act']->value=='add'){?>
				
					<h3>Tambah Pengguna</h3>
					<form role="form" method="POST" action="users.php?module=user&act=input" id="frm_user">
						<div class="form-group">
							<label>Nama Lengkap</label>
							<input type="text" name="fullName" class="required" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
						</div>
						<div class="form-group">
							<label>Alamat</label>
							<input type="text" name="address" class="required" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
						</div>
						<div class="form-group">
							<label>Jenis Kelamin</label>
							<select name="gender" class="required" style="display: block; width: 270px; height: 34px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
								<option value="">- Pilih Gender -</option>
								<option value="L">Laki-laki</option>
								<option value="P">Perempuan</option>
							</select>
						</div>
						<div class="form-group">
							<label>Jabatan</label>
							<input type="text" name="position" class="required" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
						</div>
						<div class="form-group">
							<label>No. HP</label>
							<input type="text" name="handPhone" class="required" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
						</div>
						<div class="form-group">
							<label>Status</label>
							<select name="status" class="required" style="display: block; width: 270px; height: 34px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
								<option value="">- Pilih Status -</option>
								<option value="Y" SELECTED>Aktif</option>
								<option value="N">Tidak Aktif</option>
							</select>
						</div>
						<div class="form-group">
							<label>Blokir</label>
							<select name="blocked" class="required" style="display: block; width: 270px; height: 34px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
								<option value="">- Pilih Status Blokir -</option>
								<option value="Y">Aktif</option>
								<option value="N" SELECTED>Tidak Aktif</option>
							</select>
						</div>
						<div class="form-group">
							<label>Username</label>
							<input type="text" name="username" class="required" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
						</div>
						<div class="form-group">
							<label>Password</label>
							<input type="text" name="password" value="123456" class="required" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
						</div>
						<button type="submit" class="btn btn-primary">Simpan</button>
					</form>
				
				<?php }elseif($_smarty_tpl->tpl_vars['module']->value=='user'&&$_smarty_tpl->tpl_vars['act']->value=='edit'){?>
				
					<h3>Ubah Pengguna</h3>
					<form role="form" method="POST" action="users.php?module=user&act=update" id="frm_user">
						<input type="hidden" name="userID" value="<?php echo $_smarty_tpl->tpl_vars['userID']->value;?>
">
						<div class="form-group">
							<label>Nama Lengkap</label>
							<input type="text" name="fullName" value="<?php echo $_smarty_tpl->tpl_vars['fullName']->value;?>
" class="required" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
						</div>
						<div class="form-group">
							<label>Alamat</label>
							<input type="text" name="address" value="<?php echo $_smarty_tpl->tpl_vars['address']->value;?>
" class="required" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
						</div>
						<div class="form-group">
							<label>Jenis Kelamin</label>
							<select name="gender" class="required" style="display: block; width: 270px; height: 34px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
								<option value="">- Pilih Gender -</option>
								<option value="L" <?php if ($_smarty_tpl->tpl_vars['gender']->value=='L'){?> SELECTED <?php }?>>Laki-laki</option>
								<option value="P" <?php if ($_smarty_tpl->tpl_vars['gender']->value=='P'){?> SELECTED <?php }?>>Perempuan</option>
							</select>
						</div>
						<div class="form-group">
							<label>Jabatan</label>
							<input type="text" name="position" value="<?php echo $_smarty_tpl->tpl_vars['position']->value;?>
" class="required" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
						</div>
						<div class="form-group">
							<label>No. HP</label>
							<input type="text" name="handPhone" value="<?php echo $_smarty_tpl->tpl_vars['handPhone']->value;?>
" class="required" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
						</div>
						<div class="form-group">
							<label>Status</label>
							<select name="status" class="required" style="display: block; width: 270px; height: 34px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
								<option value="">- Pilih Status -</option>
								<option value="Y" <?php if ($_smarty_tpl->tpl_vars['status']->value=='Y'){?> SELECTED <?php }?>>Aktif</option>
								<option value="N" <?php if ($_smarty_tpl->tpl_vars['status']->value=='N'){?> SELECTED <?php }?>>Tidak Aktif</option>
							</select>
						</div>
						<div class="form-group">
							<label>Blokir</label>
							<select name="blocked" class="required" style="display: block; width: 270px; height: 34px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
								<option value="">- Pilih Status Blokir -</option>
								<option value="Y" <?php if ($_smarty_tpl->tpl_vars['blocked']->value=='Y'){?> SELECTED <?php }?>>Aktif</option>
								<option value="N" <?php if ($_smarty_tpl->tpl_vars['blocked']->value=='N'){?> SELECTED <?php }?>>Tidak Aktif</option>
							</select>
						</div>
						<button type="submit" class="btn btn-primary">Simpan</button>
					</form>
					
				<?php }elseif($_smarty_tpl->tpl_vars['module']->value=='user'&&$_smarty_tpl->tpl_vars['act']->value=='reset'){?>
				
					<h3>Reset Password Pengguna</h3>
					<form role="form" method="POST" action="users.php?module=user&act=inreset" id="frm_reset">
						<input type="hidden" name="userID" value="<?php echo $_smarty_tpl->tpl_vars['userID']->value;?>
">
						<div class="form-group">
							<label>Nama Lengkap</label>
							<input type="text" value="<?php echo $_smarty_tpl->tpl_vars['fullName']->value;?>
" name="fullName" class="required" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;" DISABLED>
						</div>
						<div class="form-group">
							<label>Username</label>
							<input type="text" value="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
" name="username" class="required" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
						</div>
						<div class="form-group">
							<label>Password</label>
							<input type="text" value="123456" name="password" class="required" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
						</div>
						<button type="submit" class="btn btn-primary">Simpan</button>
					</form>
				
				<?php }else{ ?>
			
					<a href="users.php?module=user&act=add"><button class="btn btn-primary" type="button">Tambah Pengguna</button></a>
					<h3>Manajemen Pengguna</h3>
					<div class="table-responsive">
						<table class="table table-bordered table-hover tablesorter">
							<thead>
								<tr>
									<th>No. <i class="fa fa-sort"></i></th>
									<th>Nama Lengkap <i class="fa fa-sort"></i></th>
									<th>Position <i class="fa fa-sort"></i></th>
									<th>Gender <i class="fa fa-sort"></i></th>
									<th>Hp <i class="fa fa-sort"></i></th>
									<th>Status <i class="fa fa-sort"></i></th>
									<th>Blokir <i class="fa fa-sort"></i></th>
									<th>Aksi <i class="fa fa-sort"></i></th>
								</tr>
							</thead>
							<tbody>
								<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['dataUser'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['dataUser']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataUser']['name'] = 'dataUser';
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataUser']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['dataUser']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataUser']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataUser']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataUser']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataUser']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataUser']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataUser']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['dataUser']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataUser']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['dataUser']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataUser']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataUser']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['dataUser']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['dataUser']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataUser']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataUser']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataUser']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['dataUser']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['dataUser']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['dataUser']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['dataUser']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['dataUser']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['dataUser']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataUser']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataUser']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataUser']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataUser']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['dataUser']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataUser']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataUser']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['dataUser']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataUser']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataUser']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataUser']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataUser']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['dataUser']['total']);
?>
								<tr>
									<td><?php echo $_smarty_tpl->tpl_vars['dataUser']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataUser']['index']]['no'];?>
</td>
									<td><?php echo $_smarty_tpl->tpl_vars['dataUser']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataUser']['index']]['fullName'];?>
</td>
									<td><?php echo $_smarty_tpl->tpl_vars['dataUser']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataUser']['index']]['position'];?>
</td>
									<td><?php echo $_smarty_tpl->tpl_vars['dataUser']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataUser']['index']]['gender'];?>
</td>
									<td><?php echo $_smarty_tpl->tpl_vars['dataUser']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataUser']['index']]['handPhone'];?>
</td>
									<td><?php echo $_smarty_tpl->tpl_vars['dataUser']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataUser']['index']]['status'];?>
</td>
									<td><?php echo $_smarty_tpl->tpl_vars['dataUser']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataUser']['index']]['blocked'];?>
</td>
									<td>
										<a href="users.php?module=user&act=edit&userID=<?php echo $_smarty_tpl->tpl_vars['dataUser']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataUser']['index']]['userID'];?>
"><button type="button" class="btn btn-success">Edit</button></a>
										<a href="users.php?module=user&act=reset&userID=<?php echo $_smarty_tpl->tpl_vars['dataUser']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataUser']['index']]['userID'];?>
"><button type="button" class="btn btn-info">Reset</button></a>
										<a href="users.php?module=user&act=delete&userID=<?php echo $_smarty_tpl->tpl_vars['dataUser']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataUser']['index']]['userID'];?>
" onclick="return confirm('Anda Yakin ingin menghapus <?php echo $_smarty_tpl->tpl_vars['dataUser']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataUser']['index']]['fullName'];?>
?');"><button type="button" class="btn btn-danger">Hapus</button></a>
									</td>
								</tr>
								<?php endfor; endif; ?>
							</tbody>
						</table>
					</div>
					<div id="paging"><?php echo $_smarty_tpl->tpl_vars['pageLink']->value;?>
</div>
				<?php }?>
			</div>
		</div>
	</div>
</div><!-- /#wrapper -->

<?php echo $_smarty_tpl->getSubTemplate ("footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>
<?php }} ?>