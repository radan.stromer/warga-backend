<?php /* Smarty version Smarty-3.1.11, created on 2018-04-03 02:55:51
         compiled from ".\templates\jobs.tpl" */ ?>
<?php /*%%SmartyHeaderCode:288035ac28ac7aa6cc9-67784578%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '317dc8a4617ada5ff8a80f65da109b5cea702a3c' => 
    array (
      0 => '.\\templates\\jobs.tpl',
      1 => 1405884600,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '288035ac28ac7aa6cc9-67784578',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'code' => 0,
    'module' => 0,
    'act' => 0,
    'jobID' => 0,
    'jobName' => 0,
    'status' => 0,
    'dataJob' => 0,
    'pageLink' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.11',
  'unifunc' => 'content_5ac28ac80669c9_47623352',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5ac28ac80669c9_47623352')) {function content_5ac28ac80669c9_47623352($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>


<div id="wrapper">
	
	<?php echo $_smarty_tpl->getSubTemplate ("leftMenu.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

	
	<div id="page-wrapper">
		<div class="row">
			<div class="col-lg-12">
				<ol class="breadcrumb">
					<li><a href="home.php"><i class="fa fa-dashboard"></i> Home</a></li>
					<li class="active"><i class="fa fa-caret-square-o-down"></i> Master Data</li>
					<li class="active">Pekerjaan</li>
				</ol>
				
				<?php if ($_smarty_tpl->tpl_vars['code']->value=='1'){?>
					<div class="alert alert-success alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						Data pekerjaan berhasil disimpan.
					</div>
				<?php }?>
				<?php if ($_smarty_tpl->tpl_vars['code']->value=='2'){?>
					<div class="alert alert-success alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						Data pekerjaan berhasil diupdate.
					</div>
				<?php }?>
				<?php if ($_smarty_tpl->tpl_vars['code']->value=='3'){?>
					<div class="alert alert-success alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						Data pekerjaan berhasil dihapus.
					</div>
				<?php }?>
			</div>
		</div><!-- /.row -->
		
		<script type='text/javascript' src='js/jquery.validate.js'></script>
			
		
			<script type='text/javascript'>
				$(document).ready(function() {
					$('#frm_job').validate({
						rules:{
							jobName: true,
							status: true
						},
						messages:{
							jobName:{
								required: "This is a required field."
							},
							status:{
								required: "This is a required field."
							}
						}
					});
				});
			</script>
		
		
		<div class="row">
			<div class="col-lg-12">
				
				<?php if ($_smarty_tpl->tpl_vars['module']->value=='job'&&$_smarty_tpl->tpl_vars['act']->value=='add'){?>
				
					<h3>Tambah Pekerjaan</h3>
					<form role="form" method="POST" action="jobs.php?module=job&act=input" id="frm_job">
						<div class="form-group">
							<label>Nama Pekerjaan</label>
							<input type="text" name="jobName" class="required" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
						</div>
						<div class="form-group">
							<label>Status</label>
							<select name="status" class="required" style="display: block; width: 270px; height: 34px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
								<option value="">- Pilih Status -</option>
								<option value="Y" SELECTED>Aktif</option>
								<option value="N">Tidak Aktif</option>
							</select>
						</div>
						<button type="submit" class="btn btn-primary">Simpan</button>
					</form>
				
				<?php }elseif($_smarty_tpl->tpl_vars['module']->value=='job'&&$_smarty_tpl->tpl_vars['act']->value=='edit'){?>
				
					<h3>Ubah Pekerjaan</h3>
					<form role="form" method="POST" action="jobs.php?module=job&act=update" id="frm_job">
						<input type="hidden" name="jobID" value="<?php echo $_smarty_tpl->tpl_vars['jobID']->value;?>
">
						<div class="form-group">
							<label>Nama Pekerjaan</label>
							<input type="text" name="jobName" value="<?php echo $_smarty_tpl->tpl_vars['jobName']->value;?>
" class="required" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
						</div>
						<div class="form-group">
							<label>Status</label>
							<select name="status" class="required" style="display: block; width: 270px; height: 34px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
								<option value="">- Pilih Status -</option>
								<option value="Y" <?php if ($_smarty_tpl->tpl_vars['status']->value=='Y'){?> SELECTED <?php }?>>Aktif</option>
								<option value="N" <?php if ($_smarty_tpl->tpl_vars['status']->value=='N'){?> SELECTED <?php }?>>Tidak Aktif</option>
							</select>
						</div>
						<button type="submit" class="btn btn-primary">Simpan</button>
					</form>
					
				<?php }else{ ?>
			
					<a href="jobs.php?module=job&act=add"><button class="btn btn-primary" type="button">Tambah Pekerjaan</button></a>
					<h3>Manajemen Pekerjaan</h3>
					<div class="table-responsive">
						<table class="table table-bordered table-hover tablesorter">
							<thead>
								<tr>
									<th>No. <i class="fa fa-sort"></i></th>
									<th>Nama Pekerjaan <i class="fa fa-sort"></i></th>
									<th>Status <i class="fa fa-sort"></i></th>
									<th>Aksi <i class="fa fa-sort"></i></th>
								</tr>
							</thead>
							<tbody>
								<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['dataJob'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['dataJob']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataJob']['name'] = 'dataJob';
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataJob']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['dataJob']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataJob']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataJob']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataJob']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataJob']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataJob']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataJob']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['dataJob']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataJob']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['dataJob']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataJob']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataJob']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['dataJob']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['dataJob']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataJob']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataJob']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataJob']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['dataJob']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['dataJob']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['dataJob']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['dataJob']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['dataJob']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['dataJob']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataJob']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataJob']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataJob']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataJob']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['dataJob']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataJob']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataJob']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['dataJob']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataJob']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataJob']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataJob']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataJob']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['dataJob']['total']);
?>
								<tr>
									<td><?php echo $_smarty_tpl->tpl_vars['dataJob']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataJob']['index']]['no'];?>
</td>
									<td><?php echo $_smarty_tpl->tpl_vars['dataJob']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataJob']['index']]['jobName'];?>
</td>
									<td><?php echo $_smarty_tpl->tpl_vars['dataJob']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataJob']['index']]['status'];?>
</td>
									<td>
										<a href="jobs.php?module=job&act=edit&jobID=<?php echo $_smarty_tpl->tpl_vars['dataJob']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataJob']['index']]['jobID'];?>
"><button type="button" class="btn btn-success">Edit</button></a>
										<a href="jobs.php?module=job&act=delete&jobID=<?php echo $_smarty_tpl->tpl_vars['dataJob']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataJob']['index']]['jobID'];?>
" onclick="return confirm('Anda Yakin ingin menghapus <?php echo $_smarty_tpl->tpl_vars['dataJob']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataJob']['index']]['jobName'];?>
?');"><button type="button" class="btn btn-danger">Hapus</button></a>
									</td>
								</tr>
								<?php endfor; endif; ?>
							</tbody>
						</table>
					</div>
					<div id="paging"><?php echo $_smarty_tpl->tpl_vars['pageLink']->value;?>
</div>
				<?php }?>
			</div>
		</div>
	</div>
</div><!-- /#wrapper -->

<?php echo $_smarty_tpl->getSubTemplate ("footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>
<?php }} ?>