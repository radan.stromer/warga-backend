<?php /* Smarty version Smarty-3.1.11, created on 2018-04-13 21:28:29
         compiled from ".\templates\laporan_jemaat_gender.tpl" */ ?>
<?php /*%%SmartyHeaderCode:270605ad0be8d818961-00685964%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '0375263f133255e0020a8fc70b208ac415e1a583' => 
    array (
      0 => '.\\templates\\laporan_jemaat_gender.tpl',
      1 => 1405884600,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '270605ad0be8d818961-00685964',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'laki' => 0,
    'perempuan' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.11',
  'unifunc' => 'content_5ad0be8d84ccc1_62105413',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5ad0be8d84ccc1_62105413')) {function content_5ad0be8d84ccc1_62105413($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>


<div id="wrapper">
	
	<?php echo $_smarty_tpl->getSubTemplate ("leftMenu.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

	
	<div id="page-wrapper">
		<div class="row">
			<div class="col-lg-12">
				<ol class="breadcrumb">
					<li><a href="home.php"><i class="fa fa-dashboard"></i> Home</a></li>
					<li class="active"><i class="fa fa-caret-square-o-down"></i> Laporan</li>
					<li class="active">Data Jemaat</li>
				</ol>				
			</div>
		</div><!-- /.row -->
		
		<div class="row">
			<div class="col-lg-12">
				<a href="print_jemaat_gender.php?module=laporan&act=print&id=A" target="_blank"><button class="btn btn-primary" type="button">Cetak Semua Data Jemaat</button></a><br><br>
				<div class="table-responsive">
					<table class="table table-bordered table-hover tablesorter">
						<thead>
							<tr>
								<th width='40'>No. <i class="fa fa-sort"></i></th>
								<th width='150'>Gender <i class="fa fa-sort"></i></th>
								<th width='200'>Jumlah Jiwa <i class="fa fa-sort"></i></th>
								<th>Cetak <i class="fa fa-sort"></i></th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>1. </td>
								<td>Laki-laki</td>
								<td><?php echo $_smarty_tpl->tpl_vars['laki']->value;?>
</td>
								<td><a href="print_jemaat_gender.php?module=laporan&act=print&id=L" target="_blank"><button class="btn btn-primary" type="button">Cetak</button></a></td>
							</tr>
							<tr>
								<td>2. </td>
								<td>Perempuan</td>
								<td><?php echo $_smarty_tpl->tpl_vars['perempuan']->value;?>
</td>
								<td><a href="print_jemaat_gender.php?module=laporan&act=print&id=P" target="_blank"><button class="btn btn-primary" type="button">Cetak</button></a></td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div><!-- /#wrapper -->

<?php echo $_smarty_tpl->getSubTemplate ("footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>
<?php }} ?>