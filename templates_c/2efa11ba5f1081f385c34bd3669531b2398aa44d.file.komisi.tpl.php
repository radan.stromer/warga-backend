<?php /* Smarty version Smarty-3.1.11, created on 2018-04-13 14:29:11
         compiled from ".\templates\komisi.tpl" */ ?>
<?php /*%%SmartyHeaderCode:155205ad05c47b93772-46702638%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '2efa11ba5f1081f385c34bd3669531b2398aa44d' => 
    array (
      0 => '.\\templates\\komisi.tpl',
      1 => 1405884600,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '155205ad05c47b93772-46702638',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'code' => 0,
    'module' => 0,
    'act' => 0,
    'dataPeriode' => 0,
    'komisi_id' => 0,
    'komisi_periode_id' => 0,
    'nama_komisi' => 0,
    'status' => 0,
    'dataKomisi' => 0,
    'pageLink' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.11',
  'unifunc' => 'content_5ad05c47c36c92_38199852',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5ad05c47c36c92_38199852')) {function content_5ad05c47c36c92_38199852($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>


<div id="wrapper">
	
	<?php echo $_smarty_tpl->getSubTemplate ("leftMenu.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

	
	<div id="page-wrapper">
		<div class="row">
			<div class="col-lg-12">
				<ol class="breadcrumb">
					<li><a href="home.php"><i class="fa fa-dashboard"></i> Home</a></li>
					<li class="active"><i class="fa fa-caret-square-o-down"></i> Manajemen Komisi</li>
					<li class="active">Komisi</li>
				</ol>
				
				<?php if ($_smarty_tpl->tpl_vars['code']->value=='1'){?>
					<div class="alert alert-success alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						Data komisi berhasil disimpan.
					</div>
				<?php }?>
				<?php if ($_smarty_tpl->tpl_vars['code']->value=='2'){?>
					<div class="alert alert-success alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						Data komisi berhasil diupdate.
					</div>
				<?php }?>
				<?php if ($_smarty_tpl->tpl_vars['code']->value=='3'){?>
					<div class="alert alert-success alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						Data komisi berhasil dihapus.
					</div>
				<?php }?>
			</div>
		</div><!-- /.row -->
		
		<script type='text/javascript' src='js/jquery.validate.js'></script>
			
		
			<script type='text/javascript'>
				$(document).ready(function() {
					$('#frm_komisi').validate({
						rules:{
							periode_id: true,
							nama_komisi: true,
							status: true
						},
						messages:{
							periode_id:{
								required: "This is a required field."
							},
							nama_komisi:{
								required: "This is a required field."
							},
							status:{
								required: "This is a required field."
							}
						}
					});
				});
			</script>
		
		
		<div class="row">
			<div class="col-lg-12">
				
				<?php if ($_smarty_tpl->tpl_vars['module']->value=='komisi'&&$_smarty_tpl->tpl_vars['act']->value=='add'){?>
				
					<h3>Tambah Komisi</h3>
					<form role="form" method="POST" action="komisi.php?module=komisi&act=input" id="frm_komisi">
						<div class="form-group">
							<label>Periode Komisi</label>
							<select name="periode_id" class="required" style="display: block; width: 270px; height: 34px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
								<option value="">- Pilih Periode Komisi -</option>
								<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['name'] = 'dataPeriode';
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['dataPeriode']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['total']);
?>
									<option value="<?php echo $_smarty_tpl->tpl_vars['dataPeriode']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataPeriode']['index']]['komisi_periode_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['dataPeriode']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataPeriode']['index']]['nama_periode'];?>
</option>
								<?php endfor; endif; ?>
							</select>
						</div>
						<div class="form-group">
							<label>Nama Komisi</label>
							<input type="text" name="nama_komisi" placeholder="Ex: Pemuda" class="required" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
						</div>
						<div class="form-group">
							<label>Status</label>
							<select name="status" class="required" style="display: block; width: 270px; height: 34px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
								<option value="">- Pilih Status -</option>
								<option value="Y" SELECTED>Aktif</option>
								<option value="N">Tidak Aktif</option>
							</select>
						</div>
						<button type="submit" class="btn btn-primary">Simpan</button>
					</form>
				
				<?php }elseif($_smarty_tpl->tpl_vars['module']->value=='komisi'&&$_smarty_tpl->tpl_vars['act']->value=='edit'){?>
				
					<h3>Ubah Komisi</h3>
					<form role="form" method="POST" action="komisi.php?module=komisi&act=update" id="frm_komisi">
						<input type="hidden" name="komisi_id" value="<?php echo $_smarty_tpl->tpl_vars['komisi_id']->value;?>
">
						<div class="form-group">
							<label>Periode Komisi</label>
							<select name="periode_id" class="required" style="display: block; width: 270px; height: 34px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
								<option value="">- Pilih Periode Komisi -</option>
								<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['name'] = 'dataPeriode';
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['dataPeriode']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['total']);
?>
									<?php if ($_smarty_tpl->tpl_vars['dataPeriode']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataPeriode']['index']]['komisi_periode_id']==$_smarty_tpl->tpl_vars['komisi_periode_id']->value){?>
										<option value="<?php echo $_smarty_tpl->tpl_vars['dataPeriode']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataPeriode']['index']]['komisi_periode_id'];?>
" SELECTED><?php echo $_smarty_tpl->tpl_vars['dataPeriode']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataPeriode']['index']]['nama_periode'];?>
</option>
									<?php }else{ ?>
										<option value="<?php echo $_smarty_tpl->tpl_vars['dataPeriode']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataPeriode']['index']]['komisi_periode_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['dataPeriode']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataPeriode']['index']]['nama_periode'];?>
</option>
									<?php }?>
								<?php endfor; endif; ?>
							</select>
						</div>
						<div class="form-group">
							<label>Nama Komisi</label>
							<input type="text" name="nama_komisi" value="<?php echo $_smarty_tpl->tpl_vars['nama_komisi']->value;?>
" placeholder="Ex: Pemuda" class="required" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
						</div>
						<div class="form-group">
							<label>Status</label>
							<select name="status" class="required" style="display: block; width: 270px; height: 34px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
								<option value="">- Pilih Status -</option>
								<option value="Y" <?php if ($_smarty_tpl->tpl_vars['status']->value=='Y'){?> SELECTED <?php }?>>Aktif</option>
								<option value="N" <?php if ($_smarty_tpl->tpl_vars['status']->value=='N'){?> SELECTED <?php }?>>Tidak Aktif</option>
							</select>
						</div>
						<button type="submit" class="btn btn-primary">Simpan</button>
					</form>
					
				<?php }else{ ?>
			
					<a href="komisi.php?module=komisi&act=add"><button class="btn btn-primary" type="button">Tambah Komisi</button></a>
					<h3>Manajemen Komisi</h3>
					<div class="table-responsive">
						<table class="table table-bordered table-hover tablesorter">
							<thead>
								<tr>
									<th>No. <i class="fa fa-sort"></i></th>
									<th>Periode Komisi <i class="fa fa-sort"></i></th>
									<th>Nama Komisi <i class="fa fa-sort"></i></th>
									<th>Status <i class="fa fa-sort"></i></th>
									<th>Aksi <i class="fa fa-sort"></i></th>
								</tr>
							</thead>
							<tbody>
								<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['dataKomisi'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['dataKomisi']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataKomisi']['name'] = 'dataKomisi';
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataKomisi']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['dataKomisi']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataKomisi']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataKomisi']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataKomisi']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataKomisi']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataKomisi']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataKomisi']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['dataKomisi']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataKomisi']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['dataKomisi']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataKomisi']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataKomisi']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['dataKomisi']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['dataKomisi']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataKomisi']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataKomisi']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataKomisi']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['dataKomisi']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['dataKomisi']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['dataKomisi']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['dataKomisi']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['dataKomisi']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['dataKomisi']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataKomisi']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataKomisi']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataKomisi']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataKomisi']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['dataKomisi']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataKomisi']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataKomisi']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['dataKomisi']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataKomisi']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataKomisi']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataKomisi']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataKomisi']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['dataKomisi']['total']);
?>
								<tr>
									<td><?php echo $_smarty_tpl->tpl_vars['dataKomisi']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataKomisi']['index']]['no'];?>
</td>
									<td><?php echo $_smarty_tpl->tpl_vars['dataKomisi']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataKomisi']['index']]['nama_periode'];?>
</td>
									<td><?php echo $_smarty_tpl->tpl_vars['dataKomisi']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataKomisi']['index']]['nama_komisi'];?>
</td>
									<td><?php echo $_smarty_tpl->tpl_vars['dataKomisi']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataKomisi']['index']]['status'];?>
</td>
									<td>
										<a href="komisi.php?module=komisi&act=edit&komisi_id=<?php echo $_smarty_tpl->tpl_vars['dataKomisi']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataKomisi']['index']]['komisi_id'];?>
"><button type="button" class="btn btn-success">Edit</button></a>
										<a href="komisi.php?module=komisi&act=delete&komisi_id=<?php echo $_smarty_tpl->tpl_vars['dataKomisi']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataKomisi']['index']]['komisi_id'];?>
" onclick="return confirm('Anda Yakin ingin menghapus <?php echo $_smarty_tpl->tpl_vars['dataKomisi']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataKomisi']['index']]['nama_komisi'];?>
?');"><button type="button" class="btn btn-danger">Hapus</button></a>
									</td>
								</tr>
								<?php endfor; endif; ?>
							</tbody>
						</table>
					</div>
					<div id="paging"><?php echo $_smarty_tpl->tpl_vars['pageLink']->value;?>
</div>
				<?php }?>
			</div>
		</div>
	</div>
</div><!-- /#wrapper -->

<?php echo $_smarty_tpl->getSubTemplate ("footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>
<?php }} ?>