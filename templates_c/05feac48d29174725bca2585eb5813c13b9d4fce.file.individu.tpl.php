<?php /* Smarty version Smarty-3.1.11, created on 2018-04-13 12:25:37
         compiled from ".\templates\individu.tpl" */ ?>
<?php /*%%SmartyHeaderCode:228035ac53e214ae755-97987169%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '05feac48d29174725bca2585eb5813c13b9d4fce' => 
    array (
      0 => '.\\templates\\individu.tpl',
      1 => 1522879218,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '228035ac53e214ae755-97987169',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.11',
  'unifunc' => 'content_5ac53e21c9fa96_88422445',
  'variables' => 
  array (
    'code' => 0,
    'act' => 0,
    'module' => 0,
    'sortno' => 0,
    'dataFuneral' => 0,
    'dataFather' => 0,
    'dataMother' => 0,
    'dataJob' => 0,
    'dataGrade' => 0,
    'dataPasangan' => 0,
    'individu_id' => 0,
    'no_induk' => 0,
    'full_name' => 0,
    'nick_name' => 0,
    'photo' => 0,
    'gender' => 0,
    'blood_type' => 0,
    'place_of_birth' => 0,
    'date_of_birth' => 0,
    'death_status' => 0,
    'death_date' => 0,
    'funeral_name' => 0,
    'religion' => 0,
    'disability' => 0,
    'father_name' => 0,
    'mother_name' => 0,
    'address' => 0,
    'telepon' => 0,
    'hp' => 0,
    'email' => 0,
    'status' => 0,
    'negara' => 0,
    'pendidikan_terakhir' => 0,
    'nama_lembaga' => 0,
    'job_name' => 0,
    'side_job' => 0,
    'grade_name' => 0,
    'hobi' => 0,
    'bakat' => 0,
    'pasangan_name' => 0,
    'tanggal_nikah' => 0,
    'status_nikah' => 0,
    'keyword' => 0,
    'funeral_id' => 0,
    'father_id' => 0,
    'mother_id' => 0,
    'job_id' => 0,
    'grade_id' => 0,
    'pasangan_id' => 0,
    'numSearch' => 0,
    'dataSearch' => 0,
    'pageLink' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5ac53e21c9fa96_88422445')) {function content_5ac53e21c9fa96_88422445($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>


<div id="wrapper">
	
	<?php echo $_smarty_tpl->getSubTemplate ("leftMenu.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

	
	<div id="page-wrapper">
		<div class="row">
			<div class="col-lg-12">
				<ol class="breadcrumb">
					<li><a href="home.php"><i class="fa fa-dashboard"></i> Home</a></li>
					<li class="active"><i class="fa fa-caret-square-o-down"></i> Keluarga Jemaat</li>
					<li class="active">Individu</li>
				</ol>
				
				<?php if ($_smarty_tpl->tpl_vars['code']->value=='1'){?>
					<div class="alert alert-success alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						Data individu berhasil disimpan.
					</div>
				<?php }?>
				<?php if ($_smarty_tpl->tpl_vars['code']->value=='2'){?>
					<div class="alert alert-success alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						Data individu berhasil diupdate.
					</div>
				<?php }?>
				<?php if ($_smarty_tpl->tpl_vars['code']->value=='3'){?>
					<div class="alert alert-success alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						Data individu berhasil dihapus.
					</div>
				<?php }?>
			</div>
		</div><!-- /.row -->
		
		<script type='text/javascript' src='js/jquery.validate.js'></script>
		
		<?php if ($_smarty_tpl->tpl_vars['act']->value=='add'||$_smarty_tpl->tpl_vars['act']->value=='edit'){?>
			<script type="text/javascript" src="js/ajaxupload.3.5.js" ></script>
			<link rel="stylesheet" type="text/css" href="css/Ajaxfile-upload.css" />
					
			
				<script type='text/javascript'>
					$(function(){
						var btnUpload=$('#me');
						var mestatus=$('#mestatus');
						var files=$('#files');
						new AjaxUpload(btnUpload, {
							action: 'upload_photo.php',
							name: 'uploadfile',
							onSubmit: function(file, ext){
								 if (! (ext && /^(jpg|jpeg)$/.test(ext))){ 
				                    // extension is not allowed 
									mestatus.text('Only JPG files are allowed');
									return false;
								}
								mestatus.html('<img src="images/ajax-loader.gif" height="16" width="16">');
							},
							onComplete: function(file, response){
								//On completion clear the status
								mestatus.text('');
								//On completion clear the status
								files.html('');
								//Add uploaded file to list
								if(response!=="error"){
									$('<li></li>').appendTo('#files').html('<img src="images/photo_individu/'+response+'" alt="" width="100" /><br />').addClass('success');
									$('<li></li>').appendTo('#photo').html('<input type="hidden" name="filename" value="'+response+'">').addClass('nameupload');
									
								} else{
									$('<li></li>').appendTo('#files').text(file).addClass('error');
								}
							}
						});
						
					});
				</script>
			
		<?php }?>
		
		
			<script>
				$(document).ready(function() {
					$('#frm_individu').validate({
						rules:{
							full_name: true,
							nick_name: true,
							gender: true,
							blood_type: true,
							place_of_birth: true,
							date_of_birth: true,
							religion: true,
							status: true,
							status_nikah: true
						},
						messages:{
							full_name:{
								required: "This is a required field."
							},
							nick_name:{
								required: "This is a required field."
							},
							gender:{
								required: "This is a required field."
							},
							blood_type:{
								required: "This is a required field."
							},
							place_of_birth:{
								required: "This is a required field."
							},
							date_of_birth:{
								required: "This is a required field."
							},
							religion:{
								required: "This is a required field."
							},
							status:{
								required: "This is a required field."
							},
							status_nikah:{
								required: "This is a required field."
							}
						}
					});
					
					$(".tab_content").hide();
					$(".tab_content:first").show();
					
					$("ul.tabs li").click(function() {
						$("ul.tabs li").removeClass("active");
						$(this).addClass("active");
						$(".tab_content").hide();
						var activeTab = $(this).attr("rel");
						$("#"+activeTab).fadeIn();
					});
					
					$( "#datepicker" ).datepicker({
						changeMonth: true,
						changeYear: true,
						dateFormat: "yy-mm-dd",
						yearRange: 'c-100:c-0'
					});
					
					$( "#datepicker1" ).datepicker({
						changeMonth: true,
						changeYear: true,
						dateFormat: "yy-mm-dd",
						yearRange: 'c-100:c-0'
					});
					
					$( "#datepicker2" ).datepicker({
						changeMonth: true,
						changeYear: true,
						dateFormat: "yy-mm-dd",
						yearRange: 'c-100:c-0'
					});
					
					$( "#datepicker3" ).datepicker({
						changeMonth: true,
						changeYear: true,
						dateFormat: "yy-mm-dd",
						yearRange: 'c-100:c-0'
					});
					
					$( "#datepicker4" ).datepicker({
						changeMonth: true,
						changeYear: true,
						dateFormat: "yy-mm-dd",
						yearRange: 'c-100:c-0'
					});
				});
			</script>
		
		
		<style>
			ul.tabs {
				margin: 0;
				padding: 0;
				float: left;
				list-style: none;
				height: 32px;
				border-bottom: 1px solid #999999;
				border-left: 1px solid #999999;
				width: 100%;
			}
			ul.tabs li {
				float: left;
				margin: 0;
				cursor: pointer;
				padding: 0px 21px ;
				height: 31px;
				line-height: 31px;
				border: 1px solid #999999;
				border-left: none;
				font-weight: bold;
				background: #EEEEEE;
				overflow: hidden;
				position: relative;
			}
			ul.tabs li:hover {
				background: #CCCCCC;
			}	
			ul.tabs li.active{
				background: #FFFFFF;
				border-bottom: 1px solid #FFFFFF;
			}
			.tab_container {
				border: 1px solid #999999;
				border-top: none;
				clear: both;
				float: left; 
				width: 100%;
				background: #FFFFFF;
			}
			.tab_content {
				padding: 20px;
				display: none;
			}
		</style>
		
		<div class="row">
			<div class="col-lg-12">
			
				<?php if ($_smarty_tpl->tpl_vars['module']->value=='individu'&&$_smarty_tpl->tpl_vars['act']->value=='add'){?>
				
					<h3>Tambah Individu</h3>
					<form role="form" method="POST" action="individu.php?module=individu&act=input" id="frm_individu">
					<p><button type="submit" class="btn btn-primary">Simpan</button> <button type="reset" class="btn btn-warning">Reset</button></p>
						<div id="container">
							<ul class="tabs">
								<li class="active" rel="tab1"> Data Pribadi</li>
								<li rel="tab2"> Alamat & Telepon Selular</li>
								<li rel="tab3"> Kewargaan</li>
								<li rel="tab4"> Pendidikan & Pekerjaan</li>
								<li rel="tab5"> Pernikahan</li>
							</ul>
							
							<div class="tab_container">
								<div id="tab1" class="tab_content">
									 <p>
									 	<div class="form-group">
											<label>No Induk</label>
											<input type="hidden" name="no_induk" value="<?php echo $_smarty_tpl->tpl_vars['sortno']->value;?>
" class="required" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
											<input type="text" name="no_induk" value="<?php echo $_smarty_tpl->tpl_vars['sortno']->value;?>
" class="required" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;" DISABLED>
										</div>
									 	<div class="form-group">
											<label>Nama Lengkap (Lengkap dengan Gelar)</label>
											<input type="text" name="full_name" class="required" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
										</div>
										<div class="form-group">
											<label>Nama Panggil</label>
											<input type="text" name="nick_name" class="required" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
										</div>
										<div class="form-group">
											<label>Foto</label>
											<div id="me" class="styleall" style="cursor:pointer;">
												<label>
													<button class="btn btn-primary">Browse</button>
												</label>
											</div>
											<span id="mestatus" ></span>
											<div id="photo">
												<li class="nameupload"></li>
											</div>
											<div id="files">
												<li class="success"></li>
											</div>
										</div>
										<div class="form-group">
											<label>Jenis Kelamin</label>
											<select name="gender" class="required" style="display: block; width: 270px; height: 34px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
												<option value="">- Pilih Jenis Kelamin -</option>
												<option value="L">Laki-laki</option>
												<option value="P">Perempuan</option>
											</select>
										</div>
										<div class="form-group">
											<label>Golongan Darah</label>
											<select name="blood_type" class="required" style="display: block; width: 270px; height: 34px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
												<option value="">- Pilih Golongan Darah -</option>
												<option value="A">A</option>
												<option value="B">B</option>
												<option value="AB">AB</option>
												<option value="O">O</option>
											</select>
										</div>
										<div class="form-group">
											<label>Tempat Lahir</label>
											<input type="text" name="place_of_birth" class="required" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
										</div>
										<div class="form-group">
											<label>Tanggal Lahir</label>
											<input type="text" name="date_of_birth" class="required" id="datepicker" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
										</div>
										<div class="form-group">
											<label>Wafat?</label>
											<select name="death_status" style="display: block; width: 270px; height: 34px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
												<option value="">- Pilih Status Wafat -</option>
												<option value="Y">Ya</option>
												<option value="N" SELECTED>Tidak</option>
											</select>
										</div>
										<div class="form-group">
											<label>Tanggal Wafat</label>
											<input type="text" name="death_date" id="datepicker1" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
										</div>
										<div class="form-group">
											<label>Tempat Pemakaman</label>
											<select name="funeral_id" style="display: block; width: 270px; height: 34px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
												<option value="">- Pilih Tempat Pemakaman -</option>
												<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['dataFuneral'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['dataFuneral']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataFuneral']['name'] = 'dataFuneral';
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataFuneral']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['dataFuneral']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataFuneral']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataFuneral']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataFuneral']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataFuneral']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataFuneral']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataFuneral']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['dataFuneral']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataFuneral']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['dataFuneral']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataFuneral']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataFuneral']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['dataFuneral']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['dataFuneral']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataFuneral']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataFuneral']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataFuneral']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['dataFuneral']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['dataFuneral']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['dataFuneral']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['dataFuneral']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['dataFuneral']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['dataFuneral']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataFuneral']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataFuneral']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataFuneral']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataFuneral']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['dataFuneral']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataFuneral']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataFuneral']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['dataFuneral']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataFuneral']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataFuneral']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataFuneral']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataFuneral']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['dataFuneral']['total']);
?>
													<option value="<?php echo $_smarty_tpl->tpl_vars['dataFuneral']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataFuneral']['index']]['funeral_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['dataFuneral']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataFuneral']['index']]['funeral_name'];?>
</option>
												<?php endfor; endif; ?>
											</select>
										</div>
										<div class="form-group">
											<label>Agama</label>
											<select name="religion" class="required" style="display: block; width: 270px; height: 34px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
												<option value="">- Pilih Agama -</option>
												<option value="1">Islam</option>
												<option value="2">Kristen</option>
												<option value="3">Katolik</option>
												<option value="4">Hindu</option>
												<option value="5">Budha</option>
												<option value="6">Kong Hu Chu</option>
												<option value="7">Lain-Lain</option>
											</select>
										</div>
										<div class="form-group">
											<label>Cacat?</label>
											<select name="disability" style="display: block; width: 270px; height: 34px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
												<option value="">- Pilih Status Cacat -</option>
												<option value="Y">Ya</option>
												<option value="N" SELECTED>Tidak</option>
											</select>
										</div>
										<div class="form-group">
											<label>Ayah</label>
											<input type="text" name="father_id" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
											<!--<select name="father_id" style="display: block; width: 270px; height: 34px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
												<option value="">- Pilih Nama Ayah -</option>
												<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['dataFather'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['dataFather']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataFather']['name'] = 'dataFather';
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataFather']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['dataFather']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataFather']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataFather']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataFather']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataFather']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataFather']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataFather']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['dataFather']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataFather']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['dataFather']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataFather']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataFather']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['dataFather']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['dataFather']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataFather']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataFather']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataFather']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['dataFather']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['dataFather']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['dataFather']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['dataFather']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['dataFather']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['dataFather']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataFather']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataFather']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataFather']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataFather']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['dataFather']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataFather']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataFather']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['dataFather']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataFather']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataFather']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataFather']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataFather']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['dataFather']['total']);
?>
													<option value="<?php echo $_smarty_tpl->tpl_vars['dataFather']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataFather']['index']]['father_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['dataFather']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataFather']['index']]['full_name'];?>
</option>
												<?php endfor; endif; ?>
											</select>-->
										</div>
										<div class="form-group">
											<label>Ibu</label>
											<input type="text" name="mother_id" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
											<!--<select name="mother_id" style="display: block; width: 270px; height: 34px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
												<option value="">- Pilih Nama Ibu -</option>
												<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['dataMother'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['dataMother']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataMother']['name'] = 'dataMother';
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataMother']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['dataMother']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataMother']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataMother']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataMother']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataMother']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataMother']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataMother']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['dataMother']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataMother']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['dataMother']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataMother']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataMother']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['dataMother']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['dataMother']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataMother']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataMother']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataMother']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['dataMother']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['dataMother']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['dataMother']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['dataMother']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['dataMother']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['dataMother']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataMother']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataMother']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataMother']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataMother']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['dataMother']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataMother']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataMother']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['dataMother']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataMother']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataMother']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataMother']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataMother']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['dataMother']['total']);
?>
													<option value="<?php echo $_smarty_tpl->tpl_vars['dataMother']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataMother']['index']]['mother_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['dataMother']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataMother']['index']]['full_name'];?>
</option>
												<?php endfor; endif; ?>
											</select>-->
										</div>
									 </p>
								</div><!-- #tab1 -->
								
								<div id="tab2" class="tab_content">
									<p>
										<div class="form-group">
											<label>Alamat</label>
											<textarea name="address" style="display: block; width: 370px; height: 80px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;"></textarea>
										</div>
										<div class="form-group">
											<label>Telepon</label>
											<input type="text" name="telepon" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
										</div>
										<div class="form-group">
											<label>No. HP</label>
											<input type="text" name="hp" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
										</div>
										<div class="form-group">
											<label>Email</label>
											<input type="text" name="email" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
										</div>
									</p>
								</div><!-- #tab2 -->
								
								<div id="tab3" class="tab_content">
									<p>
										<div class="form-group">
											<label>Kewarganegaraan?</label>
											<select name="status" class="required" style="display: block; width: 270px; height: 34px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
												<option value="">- Pilih Status Warga -</option>
												<option value="Y" SELECTED>WNI</option>
												<option value="N">WNA</option>
											</select>
										</div>
										<div class="form-group">
											<label>Negara Asal</label>
											<input type="text" name="negara" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
										</div>
									</p>
								</div><!-- #tab3 -->
								
								<div id="tab4" class="tab_content">
									<p>
										<div class="form-group">
											<label>Pendidikan Terakhir</label>
											<select name="pendidikan_terakhir" style="display: block; width: 270px; height: 34px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
												<option value="">- Pilih Pendidikan Terakhir -</option>
												<option value="1">S3</option>
												<option value="2">S2</option>
												<option value="3">S1</option>
												<option value="4">D4</option>
												<option value="5">D3</option>
												<option value="6">D2</option>
												<option value="7">D1</option>
												<option value="8">SMA/SMK</option>
												<option value="9">SMP</option>
												<option value="10">SD</option>
												<option value="11">TK</option>
											</select>
										</div>
										<div class="form-group">
											<label>Nama Lembaga / Pendidikan</label>
											<input type="text" name="nama_lembaga" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
										</div>
										<div class="form-group">
											<label>Pekerjaan Utama</label>
											<select name="job_id" style="display: block; width: 270px; height: 34px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
												<option value="">- Pilih Pekerjaan Utama -</option>
												<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['dataJob'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['dataJob']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataJob']['name'] = 'dataJob';
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataJob']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['dataJob']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataJob']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataJob']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataJob']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataJob']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataJob']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataJob']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['dataJob']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataJob']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['dataJob']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataJob']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataJob']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['dataJob']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['dataJob']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataJob']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataJob']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataJob']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['dataJob']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['dataJob']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['dataJob']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['dataJob']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['dataJob']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['dataJob']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataJob']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataJob']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataJob']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataJob']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['dataJob']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataJob']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataJob']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['dataJob']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataJob']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataJob']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataJob']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataJob']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['dataJob']['total']);
?>
													<option value="<?php echo $_smarty_tpl->tpl_vars['dataJob']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataJob']['index']]['job_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['dataJob']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataJob']['index']]['job_name'];?>
</option>
												<?php endfor; endif; ?>
											</select>
										</div>
										<div class="form-group">
											<label>Pekerjaan Sampingan</label>
											<input type="text" name="side_job" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
										</div>
										<div class="form-group">
											<label>Penghasilan Bulanan</label>
											<select name="grade_id" style="display: block; width: 270px; height: 34px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
												<option value="">- Pilih Grade Penghasilan -</option>
												<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['dataGrade'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['dataGrade']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataGrade']['name'] = 'dataGrade';
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataGrade']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['dataGrade']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataGrade']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataGrade']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataGrade']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataGrade']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataGrade']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataGrade']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['dataGrade']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataGrade']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['dataGrade']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataGrade']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataGrade']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['dataGrade']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['dataGrade']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataGrade']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataGrade']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataGrade']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['dataGrade']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['dataGrade']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['dataGrade']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['dataGrade']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['dataGrade']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['dataGrade']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataGrade']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataGrade']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataGrade']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataGrade']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['dataGrade']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataGrade']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataGrade']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['dataGrade']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataGrade']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataGrade']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataGrade']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataGrade']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['dataGrade']['total']);
?>
													<option value="<?php echo $_smarty_tpl->tpl_vars['dataGrade']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataGrade']['index']]['grade_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['dataGrade']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataGrade']['index']]['grade_name'];?>
</option>
												<?php endfor; endif; ?>
											</select>
										</div>
										<div class="form-group">
											<label>Minat / Hobi</label>
											<input type="text" name="hobi" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
										</div>
										<div class="form-group">
											<label>Bakat</label>
											<input type="text" name="bakat" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
										</div>
									</p>
								</div><!-- #tab4 -->
								
								<div id="tab5" class="tab_content">
									<p>
										<div class="form-group">
											<label>Pasangan</label>
											<input type="text" name="pasangan_id" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
											<!--<select name="pasangan_id" style="display: block; width: 270px; height: 34px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
												<option value="">- Pilih Nama Pasangan -</option>
												<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['dataPasangan'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['dataPasangan']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataPasangan']['name'] = 'dataPasangan';
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataPasangan']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['dataPasangan']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataPasangan']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataPasangan']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPasangan']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataPasangan']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataPasangan']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPasangan']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPasangan']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataPasangan']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPasangan']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPasangan']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataPasangan']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPasangan']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPasangan']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataPasangan']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataPasangan']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPasangan']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPasangan']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPasangan']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPasangan']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPasangan']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPasangan']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPasangan']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataPasangan']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPasangan']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataPasangan']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPasangan']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPasangan']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataPasangan']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPasangan']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPasangan']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataPasangan']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataPasangan']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataPasangan']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataPasangan']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPasangan']['total']);
?>
													<option value="<?php echo $_smarty_tpl->tpl_vars['dataPasangan']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataPasangan']['index']]['pasangan_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['dataPasangan']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataPasangan']['index']]['full_name'];?>
</option>
												<?php endfor; endif; ?>
											</select>-->
										</div>
										<div class="form-group">
											<label>Tanggal Pernikahan</label>
											<input type="text" name="tanggal_nikah" id="datepicker4" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
										</div>
										<div class="form-group">
											<label>Status Pernikahan</label>
											<select name="status_nikah" class="required" style="display: block; width: 270px; height: 34px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
												<option value="">- Pilih Status Pernikahan -</option>
												<option value="1">Belum Menikah</option>
												<option value="2">Cerai</option>
												<option value="3">Menikah</option>
												<option value="4">Wafat</option>
											</select>
										</div>
									</p>
								</div>
							</div> <!-- .tab_container -->
						</div> <!-- #container -->
					</form>
				
				<?php }elseif($_smarty_tpl->tpl_vars['module']->value=='individu'&&$_smarty_tpl->tpl_vars['act']->value=='view'){?>
				
					<h3>Detail Individu</h3>
					<p>	<a href="javascript:history.go(-1)"><button type="button" class="btn btn-primary">Kembali</button></a>
						<a href="print_individu.php?module=individu&act=cetak&individuID=<?php echo $_smarty_tpl->tpl_vars['individu_id']->value;?>
" target="_blank"><button type="button" class="btn btn-primary">Cetak</button></a></p>
						<div id="container">
							<ul class="tabs">
								<li class="active" rel="tab1"> Data Pribadi</li>
								<li rel="tab2"> Alamat & Telepon Selular</li>
								<li rel="tab3"> Kewargaan</li>
								<li rel="tab4"> Pendidikan & Pekerjaan</li>
								<li rel="tab5"> Pernikahan</li>
							</ul>
							
							<div class="tab_container">
								<div id="tab1" class="tab_content">
									 <p>
									 	<div class="form-group">
											<label>No Induk</label>
											<p><?php echo $_smarty_tpl->tpl_vars['no_induk']->value;?>
</p>
										</div>
									 	<div class="form-group">
											<label>Nama Lengkap (Lengkap dengan Gelar)</label>
											<p><?php echo $_smarty_tpl->tpl_vars['full_name']->value;?>
</p>
										</div>
										<div class="form-group">
											<label>Nama Panggil</label>
											<p><?php echo $_smarty_tpl->tpl_vars['nick_name']->value;?>
</p>
										</div>
										<div class="form-group">
											<label>Foto</label>
											<p>
												<?php if ($_smarty_tpl->tpl_vars['photo']->value!=''){?>
													<a href="images/photo_individu/<?php echo $_smarty_tpl->tpl_vars['photo']->value;?>
" target="_blank"><img src="images/photo_individu/<?php echo $_smarty_tpl->tpl_vars['photo']->value;?>
" width="100"></a>
												<?php }?>
											</p>
										</div>
										<div class="form-group">
											<label>Jenis Kelamin</label>
											<p><?php echo $_smarty_tpl->tpl_vars['gender']->value;?>
</p>
										</div>
										<div class="form-group">
											<label>Golongan Darah</label>
											<p><?php echo $_smarty_tpl->tpl_vars['blood_type']->value;?>
</p>
										</div>
										<div class="form-group">
											<label>Tempat Lahir</label>
											<p><?php echo $_smarty_tpl->tpl_vars['place_of_birth']->value;?>
</p>
										</div>
										<div class="form-group">
											<label>Tanggal Lahir</label>
											<p><?php echo $_smarty_tpl->tpl_vars['date_of_birth']->value;?>
</p>
										</div>
										<div class="form-group">
											<label>Wafat?</label>
											<p><?php echo $_smarty_tpl->tpl_vars['death_status']->value;?>
</p>
										</div>
										<div class="form-group">
											<label>Tanggal Wafat</label>
											<p><?php echo $_smarty_tpl->tpl_vars['death_date']->value;?>
</p>
										</div>
										<div class="form-group">
											<label>Tempat Pemakaman</label>
											<p><?php echo $_smarty_tpl->tpl_vars['funeral_name']->value;?>
</p>
										</div>
										<div class="form-group">
											<label>Agama</label>
											<p>	<?php if ($_smarty_tpl->tpl_vars['religion']->value=='1'){?> Islam <?php }?>
												<?php if ($_smarty_tpl->tpl_vars['religion']->value=='2'){?> Kristen <?php }?>
												<?php if ($_smarty_tpl->tpl_vars['religion']->value=='3'){?> Katolik <?php }?>
												<?php if ($_smarty_tpl->tpl_vars['religion']->value=='4'){?> Hindu <?php }?>
												<?php if ($_smarty_tpl->tpl_vars['religion']->value=='5'){?> Budha <?php }?>
												<?php if ($_smarty_tpl->tpl_vars['religion']->value=='6'){?> Kong Hu Chu <?php }?>
												<?php if ($_smarty_tpl->tpl_vars['religion']->value=='7'){?> Lain-lain <?php }?>
											</p>
										</div>
										<div class="form-group">
											<label>Cacat?</label>
											<p><?php echo $_smarty_tpl->tpl_vars['disability']->value;?>
</p>
										</div>
										<div class="form-group">
											<label>Ayah</label>
											<p><?php echo $_smarty_tpl->tpl_vars['father_name']->value;?>
</p>
										</div>
										<div class="form-group">
											<label>Ibu</label>
											<p><?php echo $_smarty_tpl->tpl_vars['mother_name']->value;?>
</p>
										</div>
									 </p>
								</div><!-- #tab1 -->
								
								<div id="tab2" class="tab_content">
									<p>
										<div class="form-group">
											<label>Alamat</label>
											<p><?php echo $_smarty_tpl->tpl_vars['address']->value;?>
</p>
										</div>
										<div class="form-group">
											<label>Telepon</label>
											<p><?php echo $_smarty_tpl->tpl_vars['telepon']->value;?>
</p>
										</div>
										<div class="form-group">
											<label>No. HP</label>
											<p><?php echo $_smarty_tpl->tpl_vars['hp']->value;?>
</p>
										</div>
										<div class="form-group">
											<label>Email</label>
											<p><?php echo $_smarty_tpl->tpl_vars['email']->value;?>
</p>
										</div>
									</p>
								</div><!-- #tab2 -->
								
								<div id="tab3" class="tab_content">
									<p>
										<div class="form-group">
											<label>Kewarganegaraan</label>
											<p><?php echo $_smarty_tpl->tpl_vars['status']->value;?>
</p>
										</div>
										<div class="form-group">
											<label>Negara</label>
											<p><?php echo $_smarty_tpl->tpl_vars['negara']->value;?>
</p>
										</div>
									</p>
								</div><!-- #tab3 -->
								
								<div id="tab4" class="tab_content">
									<p>
										<div class="form-group">
											<label>Pendidikan Terakhir</label>
											<p>	<?php if ($_smarty_tpl->tpl_vars['pendidikan_terakhir']->value=='1'){?> S3 <?php }?>
												<?php if ($_smarty_tpl->tpl_vars['pendidikan_terakhir']->value=='2'){?> S2 <?php }?>
												<?php if ($_smarty_tpl->tpl_vars['pendidikan_terakhir']->value=='3'){?> S1 <?php }?>
												<?php if ($_smarty_tpl->tpl_vars['pendidikan_terakhir']->value=='4'){?> D4 <?php }?>
												<?php if ($_smarty_tpl->tpl_vars['pendidikan_terakhir']->value=='5'){?> D3 <?php }?>
												<?php if ($_smarty_tpl->tpl_vars['pendidikan_terakhir']->value=='6'){?> D2 <?php }?>
												<?php if ($_smarty_tpl->tpl_vars['pendidikan_terakhir']->value=='7'){?> D1 <?php }?>
												<?php if ($_smarty_tpl->tpl_vars['pendidikan_terakhir']->value=='8'){?> SMA/SMK <?php }?>
												<?php if ($_smarty_tpl->tpl_vars['pendidikan_terakhir']->value=='9'){?> SMP <?php }?>
												<?php if ($_smarty_tpl->tpl_vars['pendidikan_terakhir']->value=='10'){?> SD <?php }?>
												<?php if ($_smarty_tpl->tpl_vars['pendidikan_terakhir']->value=='11'){?> TK <?php }?>
											</p>
										</div>
										<div class="form-group">
											<label>Nama Lembaga / Pendidikan</label>
											<p><?php echo $_smarty_tpl->tpl_vars['nama_lembaga']->value;?>
</p>
										</div>
										<div class="form-group">
											<label>Pekerjaan Utama</label>
											<p><?php echo $_smarty_tpl->tpl_vars['job_name']->value;?>
</p>
										</div>
										<div class="form-group">
											<label>Pekerjaan Sampingan</label>
											<p><?php echo $_smarty_tpl->tpl_vars['side_job']->value;?>
</p>
										</div>
										<div class="form-group">
											<label>Penghasilan Bulanan</label>
											<p><?php echo $_smarty_tpl->tpl_vars['grade_name']->value;?>
</p>
										</div>
										<div class="form-group">
											<label>Minat / Hobi</label>
											<p><?php echo $_smarty_tpl->tpl_vars['hobi']->value;?>
</p>
										</div>
										<div class="form-group">
											<label>Bakat</label>
											<p><?php echo $_smarty_tpl->tpl_vars['bakat']->value;?>
</p>
										</div>
									</p>
								</div><!-- #tab4 -->
								
								<div id="tab5" class="tab_content">
									<p>
										<div class="form-group">
											<label>Pasangan</label>
											<p><?php echo $_smarty_tpl->tpl_vars['pasangan_name']->value;?>
</p>
										<div class="form-group">
											<label>Tanggal Pernikahan</label>
											<p><?php echo $_smarty_tpl->tpl_vars['tanggal_nikah']->value;?>
</p>
										</div>
										<div class="form-group">
											<label>Status Pernikahan</label>
											<p>
												<?php if ($_smarty_tpl->tpl_vars['status_nikah']->value=='1'){?> Belum Menikah <?php }?>
												<?php if ($_smarty_tpl->tpl_vars['status_nikah']->value=='2'){?> Cerai <?php }?>
												<?php if ($_smarty_tpl->tpl_vars['status_nikah']->value=='3'){?> Menikah <?php }?>
												<?php if ($_smarty_tpl->tpl_vars['status_nikah']->value=='4'){?> Wafat <?php }?>
											</p>
										</div>
									</p>
								</div>
							</div> <!-- .tab_container -->
						</div> <!-- #container -->
					</form>
				
				<?php }elseif($_smarty_tpl->tpl_vars['module']->value=='individu'&&$_smarty_tpl->tpl_vars['act']->value=='edit'){?>
				
					<h3>Ubah Data Individu</h3>
					<form role="form" method="POST" action="individu.php?module=individu&act=update" id="frm_individu">
					<input type="hidden" name="keyword" value="<?php echo $_smarty_tpl->tpl_vars['keyword']->value;?>
">
					<input type="hidden" name="individu_id" value="<?php echo $_smarty_tpl->tpl_vars['individu_id']->value;?>
">
					<p><button type="submit" class="btn btn-primary">Simpan</button> <button type="reset" class="btn btn-warning">Reset</button></p>
						<div id="container">
							<ul class="tabs">
								<li class="active" rel="tab1"> Data Pribadi</li>
								<li rel="tab2"> Alamat & Telepon Selular</li>
								<li rel="tab3"> Kewargaan</li>
								<li rel="tab4"> Pendidikan & Pekerjaan</li>
								<li rel="tab5"> Pernikahan</li>
							</ul>
							
							<div class="tab_container">
								<div id="tab1" class="tab_content">
									 <p>
									 	<div class="form-group">
											<label>No Induk</label>
											<input type="text" value="<?php echo $_smarty_tpl->tpl_vars['no_induk']->value;?>
" name="no_induk" class="required" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;" DISABLED>
										</div>
									 	<div class="form-group">
											<label>Nama Lengkap (Lengkap dengan Gelar)</label>
											<input type="text" value="<?php echo $_smarty_tpl->tpl_vars['full_name']->value;?>
" name="full_name" class="required" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
										</div>
										<div class="form-group">
											<label>Nama Panggil</label>
											<input type="text" value="<?php echo $_smarty_tpl->tpl_vars['nick_name']->value;?>
" name="nick_name" class="required" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
										</div>
										<div class="form-group">
											<label>Foto</label>
											<div id="me" class="styleall" style="cursor:pointer;">
												<label>
													<button class="btn btn-primary">Browse</button>
												</label>
											</div>
											<span id="mestatus" ></span>
											<div id="photo">
												<li class="nameupload"></li>
											</div>
											<div id="files">
												<li class="success">
													<?php if ($_smarty_tpl->tpl_vars['photo']->value!=''){?>
														<img src="images/photo_individu/<?php echo $_smarty_tpl->tpl_vars['photo']->value;?>
" width="100">
													<?php }?>
												</li>
											</div>
										</div>
										<div class="form-group">
											<label>Jenis Kelamin</label>
											<select name="gender" class="required" style="display: block; width: 270px; height: 34px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
												<option value="">- Pilih Jenis Kelamin -</option>
												<option value="L" <?php if ($_smarty_tpl->tpl_vars['gender']->value=='L'){?> SELECTED <?php }?>>Laki-laki</option>
												<option value="P" <?php if ($_smarty_tpl->tpl_vars['gender']->value=='P'){?> SELECTED <?php }?>>Perempuan</option>
											</select>
										</div>
										<div class="form-group">
											<label>Golongan Darah</label>
											<select name="blood_type" class="required" style="display: block; width: 270px; height: 34px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
												<option value="">- Pilih Golongan Darah -</option>
												<option value="A" <?php if ($_smarty_tpl->tpl_vars['blood_type']->value=='A'){?> SELECTED <?php }?>>A</option>
												<option value="B" <?php if ($_smarty_tpl->tpl_vars['blood_type']->value=='B'){?> SELECTED <?php }?>>B</option>
												<option value="AB" <?php if ($_smarty_tpl->tpl_vars['blood_type']->value=='AB'){?> SELECTED <?php }?>>AB</option>
												<option value="O" <?php if ($_smarty_tpl->tpl_vars['blood_type']->value=='O'){?> SELECTED <?php }?>>O</option>
											</select>
										</div>
										<div class="form-group">
											<label>Tempat Lahir</label>
											<input type="text" value="<?php echo $_smarty_tpl->tpl_vars['place_of_birth']->value;?>
" name="place_of_birth" class="required" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
										</div>
										<div class="form-group">
											<label>Tanggal Lahir</label>
											<input type="text" value="<?php echo $_smarty_tpl->tpl_vars['date_of_birth']->value;?>
" name="date_of_birth" class="required" id="datepicker" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
										</div>
										<div class="form-group">
											<label>Wafat?</label>
											<select name="death_status" style="display: block; width: 270px; height: 34px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
												<option value="">- Pilih Status Wafat -</option>
												<option value="Y" <?php if ($_smarty_tpl->tpl_vars['death_status']->value=='Y'){?> SELECTED <?php }?>>Ya</option>
												<option value="N" <?php if ($_smarty_tpl->tpl_vars['death_status']->value=='N'){?> SELECTED <?php }?>>Tidak</option>
											</select>
										</div>
										<div class="form-group">
											<label>Tanggal Wafat</label>
											<input type="text" value="<?php echo $_smarty_tpl->tpl_vars['death_date']->value;?>
" name="death_date" id="datepicker1" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
										</div>
										<div class="form-group">
											<label>Tempat Pemakaman</label>
											<select name="funeral_id" style="display: block; width: 270px; height: 34px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
												<option value="">- Pilih Tempat Pemakaman -</option>
												<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['dataFuneral'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['dataFuneral']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataFuneral']['name'] = 'dataFuneral';
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataFuneral']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['dataFuneral']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataFuneral']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataFuneral']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataFuneral']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataFuneral']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataFuneral']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataFuneral']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['dataFuneral']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataFuneral']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['dataFuneral']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataFuneral']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataFuneral']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['dataFuneral']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['dataFuneral']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataFuneral']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataFuneral']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataFuneral']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['dataFuneral']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['dataFuneral']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['dataFuneral']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['dataFuneral']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['dataFuneral']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['dataFuneral']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataFuneral']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataFuneral']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataFuneral']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataFuneral']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['dataFuneral']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataFuneral']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataFuneral']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['dataFuneral']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataFuneral']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataFuneral']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataFuneral']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataFuneral']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['dataFuneral']['total']);
?>
													<?php if ($_smarty_tpl->tpl_vars['dataFuneral']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataFuneral']['index']]['funeral_id']==$_smarty_tpl->tpl_vars['funeral_id']->value){?>
														<option value="<?php echo $_smarty_tpl->tpl_vars['dataFuneral']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataFuneral']['index']]['funeral_id'];?>
" SELECTED><?php echo $_smarty_tpl->tpl_vars['dataFuneral']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataFuneral']['index']]['funeral_name'];?>
</option>
													<?php }else{ ?>
														<option value="<?php echo $_smarty_tpl->tpl_vars['dataFuneral']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataFuneral']['index']]['funeral_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['dataFuneral']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataFuneral']['index']]['funeral_name'];?>
</option>
													<?php }?>
												<?php endfor; endif; ?>
											</select>
										</div>
										<div class="form-group">
											<label>Agama</label>
											<select name="religion" class="required" style="display: block; width: 270px; height: 34px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
												<option value="">- Pilih Agama -</option>
												<option value="1" <?php if ($_smarty_tpl->tpl_vars['religion']->value=='1'){?> SELECTED <?php }?>>Islam</option>
												<option value="2" <?php if ($_smarty_tpl->tpl_vars['religion']->value=='2'){?> SELECTED <?php }?>>Kristen</option>
												<option value="3" <?php if ($_smarty_tpl->tpl_vars['religion']->value=='3'){?> SELECTED <?php }?>>Katolik</option>
												<option value="4" <?php if ($_smarty_tpl->tpl_vars['religion']->value=='4'){?> SELECTED <?php }?>>Hindu</option>
												<option value="5" <?php if ($_smarty_tpl->tpl_vars['religion']->value=='5'){?> SELECTED <?php }?>>Budha</option>
												<option value="6" <?php if ($_smarty_tpl->tpl_vars['religion']->value=='6'){?> SELECTED <?php }?>>Kong Hu Chu</option>
												<option value="7" <?php if ($_smarty_tpl->tpl_vars['religion']->value=='7'){?> SELECTED <?php }?>>Lain-Lain</option>
											</select>
										</div>
										<div class="form-group">
											<label>Cacat?</label>
											<select name="disability" style="display: block; width: 270px; height: 34px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
												<option value="">- Pilih Status Cacat -</option>
												<option value="Y" <?php if ($_smarty_tpl->tpl_vars['disability']->value=='Y'){?> SELECTED <?php }?>>Ya</option>
												<option value="N" <?php if ($_smarty_tpl->tpl_vars['disability']->value=='N'){?> SELECTED <?php }?>>Tidak</option>
											</select>
										</div>
										<div class="form-group">
											<label>Ayah</label>
											<input type="text" value="<?php echo $_smarty_tpl->tpl_vars['father_id']->value;?>
" name="father_id" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
											<!--<select name="father_id" style="display: block; width: 270px; height: 34px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
												<option value="">- Pilih Nama Ayah -</option>
												<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['dataFather'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['dataFather']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataFather']['name'] = 'dataFather';
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataFather']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['dataFather']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataFather']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataFather']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataFather']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataFather']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataFather']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataFather']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['dataFather']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataFather']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['dataFather']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataFather']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataFather']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['dataFather']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['dataFather']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataFather']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataFather']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataFather']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['dataFather']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['dataFather']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['dataFather']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['dataFather']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['dataFather']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['dataFather']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataFather']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataFather']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataFather']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataFather']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['dataFather']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataFather']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataFather']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['dataFather']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataFather']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataFather']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataFather']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataFather']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['dataFather']['total']);
?>
													<?php if ($_smarty_tpl->tpl_vars['dataFather']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataFather']['index']]['father_id']==$_smarty_tpl->tpl_vars['father_id']->value){?>
														<option value="<?php echo $_smarty_tpl->tpl_vars['dataFather']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataFather']['index']]['father_id'];?>
" SELECTED><?php echo $_smarty_tpl->tpl_vars['dataFather']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataFather']['index']]['full_name'];?>
</option>
													<?php }else{ ?>
														<option value="<?php echo $_smarty_tpl->tpl_vars['dataFather']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataFather']['index']]['father_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['dataFather']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataFather']['index']]['full_name'];?>
</option>
													<?php }?>
												<?php endfor; endif; ?>
											</select>-->
										</div>
										<div class="form-group">
											<label>Ibu</label>
											<input type="text" name="mother_id" value="<?php echo $_smarty_tpl->tpl_vars['mother_id']->value;?>
" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
											<!--<select name="mother_id" style="display: block; width: 270px; height: 34px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
												<option value="">- Pilih Nama Ibu -</option>
												<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['dataMother'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['dataMother']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataMother']['name'] = 'dataMother';
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataMother']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['dataMother']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataMother']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataMother']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataMother']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataMother']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataMother']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataMother']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['dataMother']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataMother']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['dataMother']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataMother']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataMother']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['dataMother']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['dataMother']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataMother']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataMother']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataMother']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['dataMother']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['dataMother']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['dataMother']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['dataMother']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['dataMother']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['dataMother']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataMother']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataMother']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataMother']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataMother']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['dataMother']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataMother']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataMother']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['dataMother']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataMother']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataMother']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataMother']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataMother']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['dataMother']['total']);
?>
													<?php if ($_smarty_tpl->tpl_vars['dataMother']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataMother']['index']]['mother_id']==$_smarty_tpl->tpl_vars['mother_id']->value){?>
														<option value="<?php echo $_smarty_tpl->tpl_vars['dataMother']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataMother']['index']]['mother_id'];?>
" SELECTED><?php echo $_smarty_tpl->tpl_vars['dataMother']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataMother']['index']]['full_name'];?>
</option>
													<?php }else{ ?>
														<option value="<?php echo $_smarty_tpl->tpl_vars['dataMother']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataMother']['index']]['mother_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['dataMother']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataMother']['index']]['full_name'];?>
</option>
													<?php }?>
												<?php endfor; endif; ?>
											</select>-->
										</div>
									 </p>
								</div><!-- #tab1 -->
								
								<div id="tab2" class="tab_content">
									<p>
										<div class="form-group">
											<label>Alamat</label>
											<textarea name="address" style="display: block; width: 370px; height: 80px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;"><?php echo $_smarty_tpl->tpl_vars['address']->value;?>
</textarea>
										</div>
										<div class="form-group">
											<label>Telepon</label>
											<input type="text" value="<?php echo $_smarty_tpl->tpl_vars['telepon']->value;?>
" name="telepon" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
										</div>
										<div class="form-group">
											<label>No. HP</label>
											<input type="text" value="<?php echo $_smarty_tpl->tpl_vars['hp']->value;?>
" name="hp" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
										</div>
										<div class="form-group">
											<label>Email</label>
											<input type="text" value="<?php echo $_smarty_tpl->tpl_vars['email']->value;?>
" name="email" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
										</div>
									</p>
								</div><!-- #tab2 -->
								
								<div id="tab3" class="tab_content">
									<p>
										<div class="form-group">
											<label>Kewarganegaraan</label>
											<select name="status" class="required" style="display: block; width: 270px; height: 34px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
												<option value="">- Pilih Kewarganegaraan -</option>
												<option value="Y" <?php if ($_smarty_tpl->tpl_vars['status']->value=='Y'){?> SELECTED <?php }?>>WNI</option>
												<option value="N" <?php if ($_smarty_tpl->tpl_vars['status']->value=='N'){?> SELECTED <?php }?>>WNA</option>
											</select>
										</div>
										<div class="form-group">
											<label>Negara</label>
											<input type="text" value="<?php echo $_smarty_tpl->tpl_vars['negara']->value;?>
" name="negara" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
										</div>
									</p>
								</div><!-- #tab3 -->
								
								<div id="tab4" class="tab_content">
									<p>
										<div class="form-group">
											<label>Pendidikan Terakhir</label>
											<select name="pendidikan_terakhir" style="display: block; width: 270px; height: 34px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
												<option value="">- Pilih Pendidikan Terakhir -</option>
												<option value="1" <?php if ($_smarty_tpl->tpl_vars['pendidikan_terakhir']->value=='1'){?> SELECTED <?php }?>>S3</option>
												<option value="2" <?php if ($_smarty_tpl->tpl_vars['pendidikan_terakhir']->value=='2'){?> SELECTED <?php }?>>S2</option>
												<option value="3" <?php if ($_smarty_tpl->tpl_vars['pendidikan_terakhir']->value=='3'){?> SELECTED <?php }?>>S1</option>
												<option value="4" <?php if ($_smarty_tpl->tpl_vars['pendidikan_terakhir']->value=='4'){?> SELECTED <?php }?>>D4</option>
												<option value="5" <?php if ($_smarty_tpl->tpl_vars['pendidikan_terakhir']->value=='5'){?> SELECTED <?php }?>>D3</option>
												<option value="6" <?php if ($_smarty_tpl->tpl_vars['pendidikan_terakhir']->value=='6'){?> SELECTED <?php }?>>D2</option>
												<option value="7" <?php if ($_smarty_tpl->tpl_vars['pendidikan_terakhir']->value=='7'){?> SELECTED <?php }?>>D1</option>
												<option value="8" <?php if ($_smarty_tpl->tpl_vars['pendidikan_terakhir']->value=='8'){?> SELECTED <?php }?>>SMA/SMK</option>
												<option value="9" <?php if ($_smarty_tpl->tpl_vars['pendidikan_terakhir']->value=='9'){?> SELECTED <?php }?>>SMP</option>
												<option value="10" <?php if ($_smarty_tpl->tpl_vars['pendidikan_terakhir']->value=='10'){?> SELECTED <?php }?>>SD</option>
												<option value="11" <?php if ($_smarty_tpl->tpl_vars['pendidikan_terakhir']->value=='11'){?> SELECTED <?php }?>>TK</option>
											</select>
										</div>
										<div class="form-group">
											<label>Nama Lembaga / Pendidikan</label>
											<input type="text" value="<?php echo $_smarty_tpl->tpl_vars['nama_lembaga']->value;?>
" name="nama_lembaga" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
										</div>
										<div class="form-group">
											<label>Pekerjaan Utama</label>
											<select name="job_id" style="display: block; width: 270px; height: 34px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
												<option value="">- Pilih Pekerjaan Utama -</option>
												<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['dataJob'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['dataJob']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataJob']['name'] = 'dataJob';
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataJob']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['dataJob']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataJob']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataJob']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataJob']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataJob']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataJob']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataJob']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['dataJob']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataJob']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['dataJob']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataJob']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataJob']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['dataJob']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['dataJob']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataJob']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataJob']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataJob']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['dataJob']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['dataJob']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['dataJob']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['dataJob']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['dataJob']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['dataJob']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataJob']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataJob']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataJob']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataJob']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['dataJob']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataJob']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataJob']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['dataJob']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataJob']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataJob']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataJob']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataJob']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['dataJob']['total']);
?>
													<?php if ($_smarty_tpl->tpl_vars['dataJob']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataJob']['index']]['job_id']==$_smarty_tpl->tpl_vars['job_id']->value){?>
														<option value="<?php echo $_smarty_tpl->tpl_vars['dataJob']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataJob']['index']]['job_id'];?>
" SELECTED><?php echo $_smarty_tpl->tpl_vars['dataJob']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataJob']['index']]['job_name'];?>
</option>
													<?php }else{ ?>
														<option value="<?php echo $_smarty_tpl->tpl_vars['dataJob']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataJob']['index']]['job_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['dataJob']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataJob']['index']]['job_name'];?>
</option>
													<?php }?>
												<?php endfor; endif; ?>
											</select>
										</div>
										<div class="form-group">
											<label>Pekerjaan Sampingan</label>
											<input type="text" value="<?php echo $_smarty_tpl->tpl_vars['side_job']->value;?>
" name="side_job" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
										</div>
										<div class="form-group">
											<label>Penghasilan Bulanan</label>
											<select name="grade_id" style="display: block; width: 270px; height: 34px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
												<option value="">- Pilih Grade Penghasilan -</option>
												<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['dataGrade'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['dataGrade']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataGrade']['name'] = 'dataGrade';
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataGrade']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['dataGrade']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataGrade']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataGrade']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataGrade']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataGrade']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataGrade']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataGrade']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['dataGrade']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataGrade']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['dataGrade']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataGrade']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataGrade']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['dataGrade']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['dataGrade']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataGrade']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataGrade']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataGrade']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['dataGrade']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['dataGrade']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['dataGrade']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['dataGrade']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['dataGrade']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['dataGrade']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataGrade']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataGrade']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataGrade']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataGrade']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['dataGrade']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataGrade']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataGrade']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['dataGrade']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataGrade']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataGrade']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataGrade']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataGrade']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['dataGrade']['total']);
?>
													<?php if ($_smarty_tpl->tpl_vars['dataGrade']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataGrade']['index']]['grade_id']==$_smarty_tpl->tpl_vars['grade_id']->value){?>
														<option value="<?php echo $_smarty_tpl->tpl_vars['dataGrade']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataGrade']['index']]['grade_id'];?>
" SELECTED><?php echo $_smarty_tpl->tpl_vars['dataGrade']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataGrade']['index']]['grade_name'];?>
</option>
													<?php }else{ ?>
														<option value="<?php echo $_smarty_tpl->tpl_vars['dataGrade']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataGrade']['index']]['grade_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['dataGrade']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataGrade']['index']]['grade_name'];?>
</option>
													<?php }?>
												<?php endfor; endif; ?>
											</select>
										</div>
										<div class="form-group">
											<label>Minat / Hobi</label>
											<input type="text" value="<?php echo $_smarty_tpl->tpl_vars['hobi']->value;?>
" name="hobi" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
										</div>
										<div class="form-group">
											<label>Bakat</label>
											<input type="text" value="<?php echo $_smarty_tpl->tpl_vars['bakat']->value;?>
" name="bakat" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
										</div>
									</p>
								</div><!-- #tab4 -->
								
								<div id="tab5" class="tab_content">
									<p>
										<div class="form-group">
											<label>Pasangan</label>
											<input type="text" name="pasangan_id" value="<?php echo $_smarty_tpl->tpl_vars['pasangan_id']->value;?>
" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
											<!--<select name="pasangan_id" style="display: block; width: 270px; height: 34px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
												<option value="">- Pilih Nama Pasangan -</option>
												<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['dataPasangan'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['dataPasangan']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataPasangan']['name'] = 'dataPasangan';
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataPasangan']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['dataPasangan']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataPasangan']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataPasangan']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPasangan']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataPasangan']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataPasangan']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPasangan']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPasangan']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataPasangan']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPasangan']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPasangan']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataPasangan']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPasangan']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPasangan']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataPasangan']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataPasangan']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPasangan']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPasangan']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPasangan']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPasangan']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPasangan']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPasangan']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPasangan']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataPasangan']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPasangan']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataPasangan']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPasangan']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPasangan']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataPasangan']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPasangan']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPasangan']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataPasangan']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataPasangan']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataPasangan']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataPasangan']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPasangan']['total']);
?>
													<?php if ($_smarty_tpl->tpl_vars['dataPasangan']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataPasangan']['index']]['pasangan_id']==$_smarty_tpl->tpl_vars['pasangan_id']->value){?>
														<option value="<?php echo $_smarty_tpl->tpl_vars['dataPasangan']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataPasangan']['index']]['pasangan_id'];?>
" SELECTED><?php echo $_smarty_tpl->tpl_vars['dataPasangan']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataPasangan']['index']]['full_name'];?>
</option>
													<?php }else{ ?>
														<option value="<?php echo $_smarty_tpl->tpl_vars['dataPasangan']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataPasangan']['index']]['pasangan_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['dataPasangan']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataPasangan']['index']]['full_name'];?>
</option>
													<?php }?>
												<?php endfor; endif; ?>
											</select>-->
										</div>
										<div class="form-group">
											<label>Tanggal Pernikahan</label>
											<input type="text" value="<?php echo $_smarty_tpl->tpl_vars['tanggal_nikah']->value;?>
" name="tanggal_nikah" id="datepicker4" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
										</div>
										<div class="form-group">
											<label>Status Pernikahan</label>
											<select name="status_nikah" class="required" style="display: block; width: 270px; height: 34px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
												<option value="">- Pilih Status Pernikahan -</option>
												<option value="1" <?php if ($_smarty_tpl->tpl_vars['status_nikah']->value=='1'){?> SELECTED <?php }?>>Belum Menikah</option>
												<option value="2" <?php if ($_smarty_tpl->tpl_vars['status_nikah']->value=='2'){?> SELECTED <?php }?>>Cerai</option>
												<option value="3" <?php if ($_smarty_tpl->tpl_vars['status_nikah']->value=='3'){?> SELECTED <?php }?>>Menikah</option>
												<option value="4" <?php if ($_smarty_tpl->tpl_vars['status_nikah']->value=='4'){?> SELECTED <?php }?>>Wafat</option>
											</select>
										</div>
									</p>
								</div>
							</div> <!-- .tab_container -->
						</div> <!-- #container -->
					</form>
				
				<?php }elseif($_smarty_tpl->tpl_vars['module']->value=='individu'&&$_smarty_tpl->tpl_vars['act']->value=='search'){?>
				
					<form role="form" method="GET" action="individu.php" id="frm_search">
						<input type="hidden" name="module" value="individu">
						<input type="hidden" name="act" value="search">
						<div class="form-group">
							<label>Masukkan Keyword (Nama)</label>
							<input type="text" value="<?php echo $_smarty_tpl->tpl_vars['keyword']->value;?>
" placeholder="Masukkan Nama Pencarian" name="name" class="required" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
						</div>
						<button type="submit" class="btn btn-primary">Cari</button>
					</form>
					
					<h3>Hasil Pencarian (Nama: <b><?php echo $_smarty_tpl->tpl_vars['keyword']->value;?>
</b>)<br>
					Ditemukan: <?php echo $_smarty_tpl->tpl_vars['numSearch']->value;?>
 data
					</h3>
					<div class="table-responsive">
						<table class="table table-bordered table-hover tablesorter">
							<thead>
								<tr>
									<th>No. <i class="fa fa-sort"></i></th>
									<th>No Induk <i class="fa fa-sort"></i></th>
									<th>Nama Lengkap <i class="fa fa-sort"></i></th>
									<th>Tempat Lahir <i class="fa fa-sort"></i></th>
									<th>Tanggal Lahir <i class="fa fa-sort"></i></th>
									<th>Gender <i class="fa fa-sort"></i></th>
									<th>Jemaat <i class="fa fa-sort"></i></th>
									<th>Aksi <i class="fa fa-sort"></i></th>
								</tr>
							</thead>
							<tbody>
								<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['dataSearch'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['dataSearch']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataSearch']['name'] = 'dataSearch';
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataSearch']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['dataSearch']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataSearch']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataSearch']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataSearch']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataSearch']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataSearch']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataSearch']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['dataSearch']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataSearch']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['dataSearch']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataSearch']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataSearch']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['dataSearch']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['dataSearch']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataSearch']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataSearch']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataSearch']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['dataSearch']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['dataSearch']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['dataSearch']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['dataSearch']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['dataSearch']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['dataSearch']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataSearch']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataSearch']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataSearch']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataSearch']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['dataSearch']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataSearch']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataSearch']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['dataSearch']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataSearch']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataSearch']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataSearch']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataSearch']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['dataSearch']['total']);
?>
								<tr>
									<td><?php echo $_smarty_tpl->tpl_vars['dataSearch']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataSearch']['index']]['no'];?>
</td>
									<td><?php echo $_smarty_tpl->tpl_vars['dataSearch']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataSearch']['index']]['no_induk'];?>
</td>
									<td><?php echo $_smarty_tpl->tpl_vars['dataSearch']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataSearch']['index']]['full_name'];?>
</td>
									<td><?php echo $_smarty_tpl->tpl_vars['dataSearch']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataSearch']['index']]['place_of_birth'];?>
</td>
									<td><?php echo $_smarty_tpl->tpl_vars['dataSearch']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataSearch']['index']]['date_of_birth'];?>
</td>
									<td><?php echo $_smarty_tpl->tpl_vars['dataSearch']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataSearch']['index']]['gender'];?>
</td>
									<td><?php echo $_smarty_tpl->tpl_vars['dataSearch']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataSearch']['index']]['status'];?>
</td>
									<td>
										<a href="individu.php?module=individu&act=view&individuID=<?php echo $_smarty_tpl->tpl_vars['dataSearch']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataSearch']['index']]['individu_id'];?>
&keyword=<?php echo $_smarty_tpl->tpl_vars['keyword']->value;?>
"><button type="button" class="btn btn-primary">View</button></a>
										<a href="individu.php?module=individu&act=edit&individuID=<?php echo $_smarty_tpl->tpl_vars['dataSearch']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataSearch']['index']]['individu_id'];?>
&keyword=<?php echo $_smarty_tpl->tpl_vars['keyword']->value;?>
"><button type="button" class="btn btn-success">Edit</button></a>
										<a href="individu.php?module=individu&act=delete&individuID=<?php echo $_smarty_tpl->tpl_vars['dataSearch']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataSearch']['index']]['individu_id'];?>
&keyword=<?php echo $_smarty_tpl->tpl_vars['keyword']->value;?>
" onclick="return confirm('Anda Yakin ingin menghapus <?php echo $_smarty_tpl->tpl_vars['dataSearch']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataSearch']['index']]['full_name'];?>
?');"><button type="button" class="btn btn-danger">Hapus</button></a>
									</td>
								</tr>
								<?php endfor; endif; ?>
							</tbody>
						</table>
					</div>
					<div id="paging"><?php echo $_smarty_tpl->tpl_vars['pageLink']->value;?>
</div>
				
				<?php }else{ ?>
			
					<a href="individu.php?module=individu&act=add"><button class="btn btn-primary" type="button">Tambah Individu</button></a>
					
					<h2>Pencarian Individu</h2>
					<form role="form" method="GET" action="individu.php" id="frm_search">
						<input type="hidden" name="module" value="individu">
						<input type="hidden" name="act" value="search">
						<div class="form-group">
							<label>Masukkan Keyword (Nama)</label>
							<input type="text" placeholder="Masukkan Nama Pencarian" name="name" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
						</div>
						<button type="submit" class="btn btn-primary">Cari</button>
					</form>
					
				<?php }?>
			</div>
		</div>
	</div>
</div><!-- /#wrapper -->

<?php echo $_smarty_tpl->getSubTemplate ("footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>
<?php }} ?>