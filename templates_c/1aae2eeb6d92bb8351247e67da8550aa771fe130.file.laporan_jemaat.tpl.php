<?php /* Smarty version Smarty-3.1.11, created on 2018-04-13 21:07:38
         compiled from ".\templates\laporan_jemaat.tpl" */ ?>
<?php /*%%SmartyHeaderCode:304065ad0b9aa36eb99-42592236%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '1aae2eeb6d92bb8351247e67da8550aa771fe130' => 
    array (
      0 => '.\\templates\\laporan_jemaat.tpl',
      1 => 1405884600,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '304065ad0b9aa36eb99-42592236',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'batita' => 0,
    'balita' => 0,
    'kanak' => 0,
    'pratama' => 0,
    'madya' => 0,
    'remaja' => 0,
    'pemuda' => 0,
    'dewasa' => 0,
    'lansia' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.11',
  'unifunc' => 'content_5ad0b9aa466d07_31782776',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5ad0b9aa466d07_31782776')) {function content_5ad0b9aa466d07_31782776($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>


<div id="wrapper">
	
	<?php echo $_smarty_tpl->getSubTemplate ("leftMenu.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

	
	<div id="page-wrapper">
		<div class="row">
			<div class="col-lg-12">
				<ol class="breadcrumb">
					<li><a href="home.php"><i class="fa fa-dashboard"></i> Home</a></li>
					<li class="active"><i class="fa fa-caret-square-o-down"></i> Laporan</li>
					<li class="active">Data Jemaat</li>
				</ol>				
			</div>
		</div><!-- /.row -->
		
		<div class="row">
			<div class="col-lg-12">
				<a href="print_jemaat.php?module=laporan&act=print&id=10" target="_blank"><button class="btn btn-primary" type="button">Cetak Semua Data Jemaat</button></a><br><br>
				<div class="table-responsive">
					<table class="table table-bordered table-hover tablesorter">
						<thead>
							<tr>
								<th>No. <i class="fa fa-sort"></i></th>
								<th>Kategori Umur <i class="fa fa-sort"></i></th>
								<th>Rentang Umur <i class="fa fa-sort"></i></th>
								<th>Jumlah Jiwa <i class="fa fa-sort"></i></th>
								<th>Cetak <i class="fa fa-sort"></i></th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>1. </td>
								<td>Batita</td>
								<td>0 - 2</td>
								<td><?php echo $_smarty_tpl->tpl_vars['batita']->value;?>
</td>
								<td><a href="print_jemaat.php?module=laporan&act=print&id=1" target="_blank"><button class="btn btn-primary" type="button">Cetak</button></a></td>
							</tr>
							<tr>
								<td>2. </td>
								<td>Balita</td>
								<td>3 - 4</td>
								<td><?php echo $_smarty_tpl->tpl_vars['balita']->value;?>
</td>
								<td><a href="print_jemaat.php?module=laporan&act=print&id=2" target="_blank"><button class="btn btn-primary" type="button">Cetak</button></a></td>
							</tr>
							<tr>
								<td>3. </td>
								<td>Kanak-Kanak</td>
								<td>5 - 7</td>
								<td><?php echo $_smarty_tpl->tpl_vars['kanak']->value;?>
</td>
								<td><a href="print_jemaat.php?module=laporan&act=print&id=3" target="_blank"><button class="btn btn-primary" type="button">Cetak</button></a></td>
							</tr>
							<tr>
								<td>4. </td>
								<td>Pratama</td>
								<td>8 - 11</td>
								<td><?php echo $_smarty_tpl->tpl_vars['pratama']->value;?>
</td>
								<td><a href="print_jemaat.php?module=laporan&act=print&id=4" target="_blank"><button class="btn btn-primary" type="button">Cetak</button></a></td>
							</tr>
							<tr>
								<td>5. </td>
								<td>Madya</td>
								<td>12 - 14</td>
								<td><?php echo $_smarty_tpl->tpl_vars['madya']->value;?>
</td>
								<td><a href="print_jemaat.php?module=laporan&act=print&id=5" target="_blank"><button class="btn btn-primary" type="button">Cetak</button></a></td>
							</tr>
							<tr>
								<td>6. </td>
								<td>Remaja</td>
								<td>15 - 16</td>
								<td><?php echo $_smarty_tpl->tpl_vars['remaja']->value;?>
</td>
								<td><a href="print_jemaat.php?module=laporan&act=print&id=6" target="_blank"><button class="btn btn-primary" type="button">Cetak</button></a></td>
							</tr>
							<tr>
								<td>7. </td>
								<td>Pemuda</td>
								<td>17 - 24</td>
								<td><?php echo $_smarty_tpl->tpl_vars['pemuda']->value;?>
</td>
								<td><a href="print_jemaat.php?module=laporan&act=print&id=7" target="_blank"><button class="btn btn-primary" type="button">Cetak</button></a></td>
							</tr>
							<tr>
								<td>8. </td>
								<td>Dewasa</td>
								<td>25 - 54</td>
								<td><?php echo $_smarty_tpl->tpl_vars['dewasa']->value;?>
</td>
								<td><a href="print_jemaat.php?module=laporan&act=print&id=8" target="_blank"><button class="btn btn-primary" type="button">Cetak</button></a></td>
							</tr>
							<tr>
								<td>9. </td>
								<td>Lansia</td>
								<td>> 55 </td>
								<td><?php echo $_smarty_tpl->tpl_vars['lansia']->value;?>
</td>
								<td><a href="print_jemaat.php?module=laporan&act=print&id=9" target="_blank"><button class="btn btn-primary" type="button">Cetak</button></a></td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div><!-- /#wrapper -->

<?php echo $_smarty_tpl->getSubTemplate ("footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>
<?php }} ?>