<?php /* Smarty version Smarty-3.1.11, created on 2018-04-13 19:28:52
         compiled from ".\templates\majelis.tpl" */ ?>
<?php /*%%SmartyHeaderCode:164735ad0a284ddc7f9-17237409%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '4c5ecd5fd9eb3acb1a7739d8b36c8452cfa1bede' => 
    array (
      0 => '.\\templates\\majelis.tpl',
      1 => 1405884600,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '164735ad0a284ddc7f9-17237409',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'code' => 0,
    'module' => 0,
    'act' => 0,
    'dataPeriode' => 0,
    'majelis_id' => 0,
    'majelis_periode_id' => 0,
    'nama_majelis' => 0,
    'status' => 0,
    'dataMajelis' => 0,
    'pageLink' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.11',
  'unifunc' => 'content_5ad0a284eee6a7_63353971',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5ad0a284eee6a7_63353971')) {function content_5ad0a284eee6a7_63353971($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>


<div id="wrapper">
	
	<?php echo $_smarty_tpl->getSubTemplate ("leftMenu.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

	
	<div id="page-wrapper">
		<div class="row">
			<div class="col-lg-12">
				<ol class="breadcrumb">
					<li><a href="home.php"><i class="fa fa-dashboard"></i> Home</a></li>
					<li class="active"><i class="fa fa-caret-square-o-down"></i> Manajemen Majelis</li>
					<li class="active">Majelis</li>
				</ol>
				
				<?php if ($_smarty_tpl->tpl_vars['code']->value=='1'){?>
					<div class="alert alert-success alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						Data majelis berhasil disimpan.
					</div>
				<?php }?>
				<?php if ($_smarty_tpl->tpl_vars['code']->value=='2'){?>
					<div class="alert alert-success alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						Data majelis berhasil diupdate.
					</div>
				<?php }?>
				<?php if ($_smarty_tpl->tpl_vars['code']->value=='3'){?>
					<div class="alert alert-success alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						Data majelis berhasil dihapus.
					</div>
				<?php }?>
			</div>
		</div><!-- /.row -->
		
		<script type='text/javascript' src='js/jquery.validate.js'></script>
			
		
			<script type='text/javascript'>
				$(document).ready(function() {
					$('#frm_majelis').validate({
						rules:{
							periode_id: true,
							nama_majelis: true,
							status: true
						},
						messages:{
							periode_id:{
								required: "This is a required field."
							},
							nama_majelis:{
								required: "This is a required field."
							},
							status:{
								required: "This is a required field."
							}
						}
					});
				});
			</script>
		
		
		<div class="row">
			<div class="col-lg-12">
				
				<?php if ($_smarty_tpl->tpl_vars['module']->value=='majelis'&&$_smarty_tpl->tpl_vars['act']->value=='add'){?>
				
					<h3>Tambah Majelis</h3>
					<form role="form" method="POST" action="majelis.php?module=majelis&act=input" id="frm_majelis">
						<div class="form-group">
							<label>Periode Majelis</label>
							<select name="periode_id" class="required" style="display: block; width: 270px; height: 34px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
								<option value="">- Pilih Periode Majelis -</option>
								<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['name'] = 'dataPeriode';
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['dataPeriode']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['total']);
?>
									<option value="<?php echo $_smarty_tpl->tpl_vars['dataPeriode']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataPeriode']['index']]['majelis_periode_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['dataPeriode']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataPeriode']['index']]['nama_periode'];?>
</option>
								<?php endfor; endif; ?>
							</select>
						</div>
						<div class="form-group">
							<label>Nama Majelis</label>
							<input type="text" name="nama_majelis" placeholder="Ex: Diaken" class="required" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
						</div>
						<div class="form-group">
							<label>Status</label>
							<select name="status" class="required" style="display: block; width: 270px; height: 34px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
								<option value="">- Pilih Status -</option>
								<option value="Y" SELECTED>Aktif</option>
								<option value="N">Tidak Aktif</option>
							</select>
						</div>
						<button type="submit" class="btn btn-primary">Simpan</button>
					</form>
				
				<?php }elseif($_smarty_tpl->tpl_vars['module']->value=='majelis'&&$_smarty_tpl->tpl_vars['act']->value=='edit'){?>
				
					<h3>Ubah Majelis</h3>
					<form role="form" method="POST" action="majelis.php?module=majelis&act=update" id="frm_majelis">
						<input type="hidden" name="majelis_id" value="<?php echo $_smarty_tpl->tpl_vars['majelis_id']->value;?>
">
						<div class="form-group">
							<label>Periode Majelis</label>
							<select name="periode_id" class="required" style="display: block; width: 270px; height: 34px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
								<option value="">- Pilih Periode Majelis -</option>
								<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['name'] = 'dataPeriode';
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['dataPeriode']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['dataPeriode']['total']);
?>
									<?php if ($_smarty_tpl->tpl_vars['dataPeriode']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataPeriode']['index']]['majelis_periode_id']==$_smarty_tpl->tpl_vars['majelis_periode_id']->value){?>
										<option value="<?php echo $_smarty_tpl->tpl_vars['dataPeriode']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataPeriode']['index']]['majelis_periode_id'];?>
" SELECTED><?php echo $_smarty_tpl->tpl_vars['dataPeriode']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataPeriode']['index']]['nama_periode'];?>
</option>
									<?php }else{ ?>
										<option value="<?php echo $_smarty_tpl->tpl_vars['dataPeriode']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataPeriode']['index']]['majelis_periode_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['dataPeriode']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataPeriode']['index']]['nama_periode'];?>
</option>
									<?php }?>
								<?php endfor; endif; ?>
							</select>
						</div>
						<div class="form-group">
							<label>Nama Majelis</label>
							<input type="text" name="nama_majelis" value="<?php echo $_smarty_tpl->tpl_vars['nama_majelis']->value;?>
" placeholder="Ex: Diaken" class="required" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
						</div>
						<div class="form-group">
							<label>Status</label>
							<select name="status" class="required" style="display: block; width: 270px; height: 34px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
								<option value="">- Pilih Status -</option>
								<option value="Y" <?php if ($_smarty_tpl->tpl_vars['status']->value=='Y'){?> SELECTED <?php }?>>Aktif</option>
								<option value="N" <?php if ($_smarty_tpl->tpl_vars['status']->value=='N'){?> SELECTED <?php }?>>Tidak Aktif</option>
							</select>
						</div>
						<button type="submit" class="btn btn-primary">Simpan</button>
					</form>
					
				<?php }else{ ?>
			
					<a href="majelis.php?module=majelis&act=add"><button class="btn btn-primary" type="button">Tambah Majelis</button></a>
					<h3>Manajemen Majelis</h3>
					<div class="table-responsive">
						<table class="table table-bordered table-hover tablesorter">
							<thead>
								<tr>
									<th>No. <i class="fa fa-sort"></i></th>
									<th>Periode Majelis <i class="fa fa-sort"></i></th>
									<th>Nama Majelis <i class="fa fa-sort"></i></th>
									<th>Status <i class="fa fa-sort"></i></th>
									<th>Aksi <i class="fa fa-sort"></i></th>
								</tr>
							</thead>
							<tbody>
								<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['dataMajelis'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['dataMajelis']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataMajelis']['name'] = 'dataMajelis';
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataMajelis']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['dataMajelis']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataMajelis']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataMajelis']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataMajelis']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataMajelis']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataMajelis']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataMajelis']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['dataMajelis']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataMajelis']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['dataMajelis']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataMajelis']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataMajelis']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['dataMajelis']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['dataMajelis']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataMajelis']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataMajelis']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataMajelis']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['dataMajelis']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['dataMajelis']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['dataMajelis']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['dataMajelis']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['dataMajelis']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['dataMajelis']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataMajelis']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataMajelis']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataMajelis']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataMajelis']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['dataMajelis']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataMajelis']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataMajelis']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['dataMajelis']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataMajelis']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataMajelis']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataMajelis']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataMajelis']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['dataMajelis']['total']);
?>
								<tr>
									<td><?php echo $_smarty_tpl->tpl_vars['dataMajelis']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataMajelis']['index']]['no'];?>
</td>
									<td><?php echo $_smarty_tpl->tpl_vars['dataMajelis']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataMajelis']['index']]['nama_periode'];?>
</td>
									<td><?php echo $_smarty_tpl->tpl_vars['dataMajelis']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataMajelis']['index']]['nama_majelis'];?>
</td>
									<td><?php echo $_smarty_tpl->tpl_vars['dataMajelis']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataMajelis']['index']]['status'];?>
</td>
									<td>
										<a href="majelis.php?module=majelis&act=edit&majelis_id=<?php echo $_smarty_tpl->tpl_vars['dataMajelis']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataMajelis']['index']]['majelis_id'];?>
"><button type="button" class="btn btn-success">Edit</button></a>
										<a href="majelis.php?module=majelis&act=delete&majelis_id=<?php echo $_smarty_tpl->tpl_vars['dataMajelis']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataMajelis']['index']]['majelis_id'];?>
" onclick="return confirm('Anda Yakin ingin menghapus <?php echo $_smarty_tpl->tpl_vars['dataMajelis']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataMajelis']['index']]['nama_majelis'];?>
?');"><button type="button" class="btn btn-danger">Hapus</button></a>
									</td>
								</tr>
								<?php endfor; endif; ?>
							</tbody>
						</table>
					</div>
					<div id="paging"><?php echo $_smarty_tpl->tpl_vars['pageLink']->value;?>
</div>
				<?php }?>
			</div>
		</div>
	</div>
</div><!-- /#wrapper -->

<?php echo $_smarty_tpl->getSubTemplate ("footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>
<?php }} ?>