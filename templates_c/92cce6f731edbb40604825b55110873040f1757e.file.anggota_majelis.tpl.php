<?php /* Smarty version Smarty-3.1.11, created on 2018-04-13 19:53:34
         compiled from ".\templates\anggota_majelis.tpl" */ ?>
<?php /*%%SmartyHeaderCode:133425ad0a84e86dd88-53209490%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '92cce6f731edbb40604825b55110873040f1757e' => 
    array (
      0 => '.\\templates\\anggota_majelis.tpl',
      1 => 1405884600,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '133425ad0a84e86dd88-53209490',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'code' => 0,
    'module' => 0,
    'act' => 0,
    'nama_periode' => 0,
    'nama_majelis' => 0,
    'majelis_id' => 0,
    'dataAnggota' => 0,
    'majelis_anggota_id' => 0,
    'dataMajelis' => 0,
    'anggota_id' => 0,
    'jabatan' => 0,
    'pageLink' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.11',
  'unifunc' => 'content_5ad0a84e956394_77852174',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5ad0a84e956394_77852174')) {function content_5ad0a84e956394_77852174($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>


<div id="wrapper">
	
	<?php echo $_smarty_tpl->getSubTemplate ("leftMenu.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

	
	<div id="page-wrapper">
		<div class="row">
			<div class="col-lg-12">
				<ol class="breadcrumb">
					<li><a href="home.php"><i class="fa fa-dashboard"></i> Home</a></li>
					<li class="active"><i class="fa fa-caret-square-o-down"></i> Manajemen Majelis</li>
					<li class="active">Anggota Majelis</li>
				</ol>
				
				<?php if ($_smarty_tpl->tpl_vars['code']->value=='1'){?>
					<div class="alert alert-success alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						Data anggota majelis berhasil disimpan.
					</div>
				<?php }?>
				<?php if ($_smarty_tpl->tpl_vars['code']->value=='2'){?>
					<div class="alert alert-success alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						Data anggota majelis berhasil diupdate.
					</div>
				<?php }?>
				<?php if ($_smarty_tpl->tpl_vars['code']->value=='3'){?>
					<div class="alert alert-success alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						Data anggota majelis berhasil dihapus.
					</div>
				<?php }?>
			</div>
		</div><!-- /.row -->
		
		<script type='text/javascript' src='js/jquery.validate.js'></script>
			
		
			<script type='text/javascript'>
				$(document).ready(function() {
					$('#frm_majelis').validate({
						rules:{
							majelis_id: true,
							anggota_id: true,
							jabatan: true
						},
						messages:{
							majelis_id:{
								required: "This is a required field."
							},
							anggota_id:{
								required: "This is a required field."
							},
							jabatan:{
								required: "This is a required field."
							}
						}
					});
				});
			</script>
		
		
		<div class="row">
			<div class="col-lg-12">
				
				<?php if ($_smarty_tpl->tpl_vars['module']->value=='anggota'&&$_smarty_tpl->tpl_vars['act']->value=='add'){?>
				
					<h3>Tambah Majelis Anggota</h3>
					<form role="form" method="POST" action="anggota_majelis.php?module=anggota&act=input" id="frm_majelis">
						<div class="form-group">
							<label>Majelis</label>
							<p><?php echo $_smarty_tpl->tpl_vars['nama_periode']->value;?>
 [<?php echo $_smarty_tpl->tpl_vars['nama_majelis']->value;?>
]	
								<input type="hidden" name="majelis_id" value="<?php echo $_smarty_tpl->tpl_vars['majelis_id']->value;?>
">
							</p>
						</div>
						<div class="form-group">
							<label>Nama Anggota</label>
							<select name="anggota_id" class="required" style="display: block; width: 270px; height: 34px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
								<option value="">- Pilih Majelis Anggota -</option>
								<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['name'] = 'dataAnggota';
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['dataAnggota']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['total']);
?>
									<option value="<?php echo $_smarty_tpl->tpl_vars['dataAnggota']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataAnggota']['index']]['anggota_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['dataAnggota']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataAnggota']['index']]['full_name'];?>
</option>
								<?php endfor; endif; ?>
							</select>
						</div>
						<div class="form-group">
							<label>Jabatan</label>
							<input type="text" name="jabatan" placeholder="Ex: Ketua" class="required" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
						</div>
						<button type="submit" class="btn btn-primary">Simpan</button>
					</form>
				
				<?php }elseif($_smarty_tpl->tpl_vars['module']->value=='anggota'&&$_smarty_tpl->tpl_vars['act']->value=='edit'){?>
				
					<h3>Ubah Majelis Anggota</h3>
					<form role="form" method="POST" action="anggota_majelis.php?module=anggota&act=update" id="frm_majelis">
						<input type="hidden" name="majelis_anggota_id" value="<?php echo $_smarty_tpl->tpl_vars['majelis_anggota_id']->value;?>
">
						<div class="form-group">
							<label>Majelis</label>
							<select name="majelis_id" class="required" style="display: block; width: 270px; height: 34px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
								<option value="">- Pilih Majelis -</option>
								<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['dataMajelis'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['dataMajelis']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataMajelis']['name'] = 'dataMajelis';
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataMajelis']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['dataMajelis']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataMajelis']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataMajelis']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataMajelis']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataMajelis']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataMajelis']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataMajelis']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['dataMajelis']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataMajelis']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['dataMajelis']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataMajelis']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataMajelis']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['dataMajelis']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['dataMajelis']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataMajelis']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataMajelis']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataMajelis']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['dataMajelis']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['dataMajelis']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['dataMajelis']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['dataMajelis']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['dataMajelis']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['dataMajelis']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataMajelis']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataMajelis']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataMajelis']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataMajelis']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['dataMajelis']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataMajelis']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataMajelis']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['dataMajelis']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataMajelis']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataMajelis']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataMajelis']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataMajelis']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['dataMajelis']['total']);
?>
									<?php if ($_smarty_tpl->tpl_vars['dataMajelis']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataMajelis']['index']]['majelis_id']==$_smarty_tpl->tpl_vars['majelis_id']->value){?>
										<option value="<?php echo $_smarty_tpl->tpl_vars['dataMajelis']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataMajelis']['index']]['majelis_id'];?>
" SELECTED><?php echo $_smarty_tpl->tpl_vars['dataMajelis']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataMajelis']['index']]['nama_periode'];?>
 [<?php echo $_smarty_tpl->tpl_vars['dataMajelis']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataMajelis']['index']]['nama_majelis'];?>
]</option>
									<?php }else{ ?>
										<option value="<?php echo $_smarty_tpl->tpl_vars['dataMajelis']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataMajelis']['index']]['majelis_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['dataMajelis']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataMajelis']['index']]['nama_periode'];?>
 [<?php echo $_smarty_tpl->tpl_vars['dataMajelis']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataMajelis']['index']]['nama_majelis'];?>
]</option>
									<?php }?>
								<?php endfor; endif; ?>
							</select>
						</div>
						<div class="form-group">
							<label>Nama Anggota</label>
							<select name="anggota_id" class="required" style="display: block; width: 270px; height: 34px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
								<option value="">- Pilih Komisi Anggota -</option>
								<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['name'] = 'dataAnggota';
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['dataAnggota']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['total']);
?>
									<?php if ($_smarty_tpl->tpl_vars['dataAnggota']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataAnggota']['index']]['anggota_id']==$_smarty_tpl->tpl_vars['anggota_id']->value){?>
										<option value="<?php echo $_smarty_tpl->tpl_vars['dataAnggota']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataAnggota']['index']]['anggota_id'];?>
" SELECTED><?php echo $_smarty_tpl->tpl_vars['dataAnggota']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataAnggota']['index']]['full_name'];?>
</option>
									<?php }else{ ?>
										<option value="<?php echo $_smarty_tpl->tpl_vars['dataAnggota']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataAnggota']['index']]['anggota_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['dataAnggota']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataAnggota']['index']]['full_name'];?>
</option>
									<?php }?>
								<?php endfor; endif; ?>
							</select>
						</div>
						<div class="form-group">
							<label>Jabatan</label>
							<input type="text" value="<?php echo $_smarty_tpl->tpl_vars['jabatan']->value;?>
" name="jabatan" placeholder="Ex: Ketua" class="required" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
						</div>
						<button type="submit" class="btn btn-primary">Simpan</button>
					</form>
				
				<?php }elseif($_smarty_tpl->tpl_vars['module']->value=='anggota'&&$_smarty_tpl->tpl_vars['act']->value=='detail'){?>
					<div class="form-group">
						<label>Periode / Majelis</label>
						<p><?php echo $_smarty_tpl->tpl_vars['nama_periode']->value;?>
 [<?php echo $_smarty_tpl->tpl_vars['nama_majelis']->value;?>
]</p>
					</div>
					
					<a href="anggota_majelis.php"><button class="btn btn-primary" type="button">Kembali</button></a>
					<a href="anggota_majelis.php?module=anggota&act=add&majelis_id=<?php echo $_smarty_tpl->tpl_vars['majelis_id']->value;?>
"><button class="btn btn-primary" type="button">Tambah Majelis Anggota</button></a>
					<a href="print_majelis_anggota.php?module=anggota&act=print&majelis_id=<?php echo $_smarty_tpl->tpl_vars['majelis_id']->value;?>
" target="_blank"><button class="btn btn-primary" type="button">Cetak</button></a>
					<h3>Anggota Komisi</h3>
					<div class="table-responsive">
						<table class="table table-bordered table-hover tablesorter">
							<thead>
								<tr>
									<th>No. <i class="fa fa-sort"></i></th>
									<th>Nama Anggota <i class="fa fa-sort"></i></th>
									<th>Jabatan <i class="fa fa-sort"></i></th>
									<th>Aksi <i class="fa fa-sort"></i></th>
								</tr>
							</thead>
							<tbody>
								<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['name'] = 'dataAnggota';
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['dataAnggota']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['dataAnggota']['total']);
?>
								<tr>
									<td><?php echo $_smarty_tpl->tpl_vars['dataAnggota']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataAnggota']['index']]['no'];?>
</td>
									<td><?php echo $_smarty_tpl->tpl_vars['dataAnggota']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataAnggota']['index']]['full_name'];?>
</td>
									<td><?php echo $_smarty_tpl->tpl_vars['dataAnggota']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataAnggota']['index']]['jabatan'];?>
</td>
									<td>
										<a href="anggota_majelis.php?module=anggota&act=delete&majelis_anggota_id=<?php echo $_smarty_tpl->tpl_vars['dataAnggota']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataAnggota']['index']]['majelis_anggota_id'];?>
&majelis_id=<?php echo $_smarty_tpl->tpl_vars['majelis_id']->value;?>
" onclick="return confirm('Anda Yakin ingin menghapus <?php echo $_smarty_tpl->tpl_vars['dataAnggota']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataAnggota']['index']]['full_name'];?>
?');"><button type="button" class="btn btn-danger">Hapus</button></a>
									</td>
								</tr>
								<?php endfor; endif; ?>
							</tbody>
						</table>
					</div>
									
				<?php }else{ ?>
			
					<h3>Manajemen Majelis</h3>
					<div class="table-responsive">
						<table class="table table-bordered table-hover tablesorter">
							<thead>
								<tr>
									<th>No. <i class="fa fa-sort"></i></th>
									<th>Periode Majelis <i class="fa fa-sort"></i></th>
									<th>Nama Majelis <i class="fa fa-sort"></i></th>
									<th>Total Anggota <i class="fa fa-sort"></i></th>
									<th>Aksi <i class="fa fa-sort"></i></th>
								</tr>
							</thead>
							<tbody>
								<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['dataMajelis'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['dataMajelis']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataMajelis']['name'] = 'dataMajelis';
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataMajelis']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['dataMajelis']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataMajelis']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataMajelis']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataMajelis']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataMajelis']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataMajelis']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataMajelis']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['dataMajelis']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataMajelis']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['dataMajelis']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataMajelis']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataMajelis']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['dataMajelis']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['dataMajelis']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataMajelis']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataMajelis']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataMajelis']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['dataMajelis']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['dataMajelis']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['dataMajelis']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['dataMajelis']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['dataMajelis']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['dataMajelis']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataMajelis']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataMajelis']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataMajelis']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataMajelis']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['dataMajelis']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataMajelis']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['dataMajelis']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['dataMajelis']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataMajelis']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataMajelis']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['dataMajelis']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['dataMajelis']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['dataMajelis']['total']);
?>
								<tr>
									<td><?php echo $_smarty_tpl->tpl_vars['dataMajelis']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataMajelis']['index']]['no'];?>
</td>
									<td><?php echo $_smarty_tpl->tpl_vars['dataMajelis']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataMajelis']['index']]['nama_periode'];?>
</td>
									<td><?php echo $_smarty_tpl->tpl_vars['dataMajelis']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataMajelis']['index']]['nama_majelis'];?>
</td>
									<td><?php echo $_smarty_tpl->tpl_vars['dataMajelis']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataMajelis']['index']]['total'];?>
</td>
									<td>
										<a href="anggota_majelis.php?module=anggota&act=detail&majelis_id=<?php echo $_smarty_tpl->tpl_vars['dataMajelis']->value[$_smarty_tpl->getVariable('smarty')->value['section']['dataMajelis']['index']]['majelis_id'];?>
"><button type="button" class="btn btn-success">Detail</button></a>
									</td>
								</tr>
								<?php endfor; endif; ?>
							</tbody>
						</table>
					</div>
					<div id="paging"><?php echo $_smarty_tpl->tpl_vars['pageLink']->value;?>
</div>
				<?php }?>
			</div>
		</div>
	</div>
</div><!-- /#wrapper -->

<?php echo $_smarty_tpl->getSubTemplate ("footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>
<?php }} ?>