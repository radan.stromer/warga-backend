{include file="header.tpl"}

<div id="wrapper">
	
	{include file="leftMenu.tpl"}
	
	<div id="page-wrapper">
		<div class="row">
			<div class="col-lg-12">
				<ol class="breadcrumb">
					<li><a href="home.php"><i class="fa fa-dashboard"></i> Home</a></li>
					<li class="active"><i class="fa fa-caret-square-o-down"></i> Manajemen Tokoh Masyarakat</li>
					<li class="active">Tokoh Masyarakat</li>
				</ol>
				
				{if $code == '1'}
					<div class="alert alert-success alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						Data tokoh msayarakat berhasil disimpan.
					</div>
				{/if}
				{if $code == '2'}
					<div class="alert alert-success alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						Data tokoh masyarakat berhasil diupdate.
					</div>
				{/if}
				{if $code == '3'}
					<div class="alert alert-success alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						Data tokoh msayarakat berhasil dihapus.
					</div>
				{/if}
			</div>
		</div><!-- /.row -->
		
		<script type='text/javascript' src='js/jquery.validate.js'></script>
			
		{literal}
			<script type='text/javascript'>
				$(document).ready(function() {
					$('#frm_pendeta').validate({
						rules:{
							anggota_id: true,
							jabatan: true,
							status: true
						},
						messages:{
							anggota_id:{
								required: "This is a required field."
							},
							jabatan:{
								required: "This is a required field."
							},
							status:{
								required: "This is a required field."
							}
						}
					});
					
					$( "#datepicker1" ).datepicker({
						changeMonth: true,
						changeYear: true,
						dateFormat: "yy-mm-dd",
						yearRange: 'c-100:c-0'
					});
					
					$( "#datepicker2" ).datepicker({
						changeMonth: true,
						changeYear: true,
						dateFormat: "yy-mm-dd",
						yearRange: 'c-100:c-0'
					});
				});
			</script>
		{/literal}
		
		<div class="row">
			<div class="col-lg-12">
				
				{if $module == 'pendeta' AND $act == 'add'}
				
					<h3>Tambah Tokoh Masyarakat</h3>
					<form role="form" method="POST" action="pendeta.php?module=pendeta&act=input" id="frm_pendeta">
						<div class="form-group">
							<label>Nama Tokoh Masyarakat</label>
							<select name="anggota_id" class="required" style="display: block; width: 270px; height: 34px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
								<option value="">- Pilih Tokoh Masyarakat -</option>
								{section name=dataAnggota loop=$dataAnggota}
									<option value="{$dataAnggota[dataAnggota].anggota_id}">{$dataAnggota[dataAnggota].full_name}</option>
								{/section}
							</select>
						</div>
						<div class="form-group">
							<label>Jabatan</label>
							<input type="text" name="jabatan" class="required" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
						</div>
						<div class="form-group">
							<label>Tanggal Pengangkatan</label>
							<input type="text" id="datepicker1" name="tanggal_tasbih" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
						</div>
						<div class="form-group">
							<label>Status</label>
							<select name="status" class="required" style="display: block; width: 270px; height: 34px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
								<option value="">- Pilih Status -</option>
								<option value="Y">Aktif</option>
								<option value="N">Tidak Aktif</option>
							</select>
						</div>
						<div class="form-group">
							<label>Biografi</label>
							<textarea name="biografi" style="display: block; width: 400px; height: 100px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;"></textarea>
						</div>
						<button type="submit" class="btn btn-primary">Simpan</button>
					</form>
				
				{elseif $module == 'pendeta' AND $act == 'edit'}
				
					<h3>Ubah Tokoh Masyarakat</h3>
					<form role="form" method="POST" action="pendeta.php?module=pendeta&act=update" id="frm_pendeta">
						<input type="hidden" name="pendeta_id" value="{$tokoh_id}">
						<div class="form-group">
							<label>Nama Tokoh Masyarakat</label>
							<select name="anggota_id" class="required" style="display: block; width: 270px; height: 34px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
								<option value="">- Pilih Tokoh Masyarakat -</option>
								{section name=dataAnggota loop=$dataAnggota}
									{if $dataAnggota[dataAnggota].anggota_id == $anggota_id}
										<option value="{$dataAnggota[dataAnggota].anggota_id}" SELECTED>{$dataAnggota[dataAnggota].full_name}</option>
									{else}
										<option value="{$dataAnggota[dataAnggota].anggota_id}">{$dataAnggota[dataAnggota].full_name}</option>
									{/if}
								{/section}
							</select>
						</div>
						<div class="form-group">
							<label>Jabatan</label>
							<input type="text" value="{$jabatan}" name="jabatan" class="required" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
						</div>
						<div class="form-group">
							<label>Tanggal Pengangkatan</label>
							<input type="text" id="datepicker1" value="{$tanggal_tasbih}" name="tanggal_tasbih" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
						</div>
						<div class="form-group">
							<label>Status</label>
							<select name="status" class="required" style="display: block; width: 270px; height: 34px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
								<option value="">- Pilih Status -</option>
								<option value="Y" {if $status == 'Y'} SELECTED {/if}>Aktif</option>
								<option value="N" {if $status == 'N'} SELECTED {/if}>Tidak Aktif</option>
							</select>
						</div>
						<div class="form-group">
							<label>Biografi</label>
							<textarea name="biografi" style="display: block; width: 400px; height: 100px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">{$biografi}</textarea>
						</div>
						<button type="submit" class="btn btn-primary">Simpan</button>
					</form>
					
				{else}
			
					<a href="pendeta.php?module=pendeta&act=add"><button class="btn btn-primary" type="button">Tambah Tokoh Masyarakat</button></a>
					<a href="print_pendeta.php?module=pendeta&act=print" target="_blank"><button class="btn btn-primary" type="button">Cetak</button></a>
					<h3>Manajemen Tokoh Masyarakat</h3>
					<div class="table-responsive">
						<table class="table table-bordered table-hover tablesorter">
							<thead>
								<tr>
									<th>No. <i class="fa fa-sort"></i></th>
									<th>No Induk <i class="fa fa-sort"></i></th>
									<th>Nama Lengkap <i class="fa fa-sort"></i></th>
									<th>JK <i class="fa fa-sort"></i></th>
									<th>Jabatan <i class="fa fa-sort"></i></th>
									<th>Tanggal Pentahbisan <i class="fa fa-sort"></i></th>
									<th>Status <i class="fa fa-sort"></i></th>
									<th>Aksi <i class="fa fa-sort"></i></th>
								</tr>
							</thead>
							<tbody>
								{section name=dataPendeta loop=$dataPendeta}
								<tr>
									<td>{$dataPendeta[dataPendeta].no}</td>
									<td>{$dataPendeta[dataPendeta].no_induk}</td>
									<td>{$dataPendeta[dataPendeta].full_name}</td>
									<td>{$dataPendeta[dataPendeta].gender}</td>
									<td>{$dataPendeta[dataPendeta].jabatan}</td>
									<td>{$dataPendeta[dataPendeta].tanggal_tasbih}</td>
									<td>{$dataPendeta[dataPendeta].status}</td>
									<td>
										<a href="pendeta.php?module=pendeta&act=edit&pendeta_id={$dataPendeta[dataPendeta].pendeta_id}"><button type="button" class="btn btn-success">Edit</button></a>
										<a href="pendeta.php?module=pendeta&act=delete&pendeta_id={$dataPendeta[dataPendeta].pendeta_id}" onclick="return confirm('Anda Yakin ingin menghapus {$dataPendeta[dataPendeta].full_name}?');"><button type="button" class="btn btn-danger">Hapus</button></a>
									</td>
								</tr>
								{/section}
							</tbody>
						</table>
					</div>
					<div id="paging">{$pageLink}</div>
				{/if}
			</div>
		</div>
	</div>
</div><!-- /#wrapper -->

{include file="footer.tpl"}