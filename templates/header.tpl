<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="www.gbiawn.org">
		<meta name="author" content="CV. ASFA Solution - www.asfasolution.co.id - agus.saputra@asfasolution.co.id">
		
		<title>Aplikasi Database Jemaat</title>
		
		<!-- Bootstrap core CSS -->
		<link href="css/bootstrap.css" rel="stylesheet">
		
		<!-- Add custom CSS here -->
		<link href="css/sb-admin.css" rel="stylesheet">
		<link rel="stylesheet" href="font-awesome/css/font-awesome.min.css">
		<!-- Page Specific CSS -->
		<script src="js/jquery-1.8.1.min.js" type="text/javascript"></script>
		<!--<script src="js/jquery-1.10.2.js" type="text/javascript"></script>-->
		
		<!-- JavaScript -->
		<script src="js/bootstrap.js"></script>
		
		<!-- Page Specific Plugins -->
		<script src="js/tablesorter/jquery.tablesorter.js"></script>
		<script src="js/tablesorter/tables.js"></script>
		
		<link rel="stylesheet" href="js/development-bundle/themes/base/jquery.ui.all.css" type="text/css">
		<script type="text/javascript" src="js/development-bundle/ui/jquery.ui.core.js"></script>
		<script type="text/javascript" src="js/development-bundle/ui/jquery.ui.datepicker.js"></script>
		<script type="text/javascript" src="js/development-bundle/ui/jquery.ui.widget.js"></script>
		<style>
			.error {
				-webkit-border-radius: 4px;
				-moz-border-radius: 4px;
				border-radius: 4px;
		  		border-color: #eed3d7;
		  		color: #b94a48; 
			}
			#paging {
				padding: 2px;
				margin: 2px;
				text-align: left;
				font-family: tahoma;
				font-size: 12px;
			}

			#paging a {
				padding: 2px 5px 2px 5px;
				margin-right: 2px;
				border: 1px solid #DEDFDE;
				text-decoration: none;
				color: #3E3C3C;
			}
			
			#paging a:hover {
				border: 1px solid orange;
				color: #FFFFFF;
				background-color: orange;
			}
			
			#paging span.current {
				padding: 2px 5px 2px 5px;
				margin-right: 2px;
				border: 1px solid orange;
				font-weight: bold;
				background-color: orange;
				color: #FFFFFF;
			}
			
			#paging span.disabled {
				padding: 2px 5px 2px 5px;
				margin-right: 2px;
				border: 1px solid #999999;
				color: #999999;
			}
			
			#paging span.prevnext {
				
			}
		</style>
	</head>
	
	<body>