{include file="header.tpl"}

<div id="wrapper">
	
	{include file="leftMenu.tpl"}
	
	<div id="page-wrapper">
		<div class="row">
			<div class="col-lg-12">
				<ol class="breadcrumb">
					<li class="active"><i class="fa fa-dashboard"></i> Home</li>
				</ol>
				
				{if $code == '1'}
					<div class="alert alert-success alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						Login Anda berhasil dan saat ini telah memasuki halaman dashboard Administrator.
					</div>
				{/if}
				
				{literal}
					<script>
						$(document).ready(function() {
							$( "#datepicker1" ).datepicker({
								changeMonth: true,
								changeYear: true,
								dateFormat: "yy-mm-dd",
								yearRange: 'c-100:c-0'
							});
							
							$( "#datepicker2" ).datepicker({
								changeMonth: true,
								changeYear: true,
								dateFormat: "yy-mm-dd",
								yearRange: 'c-100:c-0'
							});
						});
					</script>
				{/literal}
				
				<a href="print_all.php?module=all&act=print" target="_blank"><button class="btn btn-primary" type="button">Cetak Semua</button></a>
				<a href="print_all_birthday.php?module=birthday&act=print" target="_blank"><button class="btn btn-primary" type="button">Birthday</button></a>
				<a href="print_all_wedding.php?module=wedding&act=print" target="_blank"><button class="btn btn-primary" type="button">Wedding</button></a>
				
				<br><br>
				<form method="GET" action="home.php">
				<input type="hidden" name="act" value="search">
				<table bgcolor="#CCCCCC;">
					<tr>
						<td width="120"><b>Periode Awal : </b></td>
						<td width="320"><input type="text" value="{$start1}" name="startDate" id="datepicker1" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;" required></td>
						<td width="120"><b>Periode Akhir : </b></td>
						<td width="280"><input type="text" name="endDate" value="{$end1}" id="datepicker2" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;" required></td>
						<td width="100"><button class="btn btn-primary" type="submit">Cari</button></td>
					</tr>
				</table>
				</form>
				<br>
				<h3>Periode : {$hari_ini} s/d {$hari_besok}</h3>
				<h4>Jemaat yang Berulang Tahun Minggu Ini</h4>
				<div class="table-responsive">
					<table class="table table-bordered table-hover tablesorter">
						<thead>
							<tr>
								<th>No. <i class="fa fa-sort"></i></th>
								<th>No Induk <i class="fa fa-sort"></i></th>
								<th>Nama Lengkap <i class="fa fa-sort"></i></th>
								<th>Tanggal Lahir <i class="fa fa-sort"></i></th>
								<th>Usia (Tahun) <i class="fa fa-sort"></i></th>
								<th>Gender <i class="fa fa-sort"></i></th>
								<th>Jemaat <i class="fa fa-sort"></i></th>
								<th>Foto <i class="fa fa-sort"></i></th>
							</tr>
						</thead>
						<tbody>
							{section name=dataBirthday loop=$dataBirthday}
							<tr>
								<td>{$dataBirthday[dataBirthday].no}</td>
								<td>{$dataBirthday[dataBirthday].no_induk}</td>
								<td>{$dataBirthday[dataBirthday].full_name}</td>
								<td>{$dataBirthday[dataBirthday].date_of_birth}</td>
								<td>{$dataBirthday[dataBirthday].age}</td>
								<td>{$dataBirthday[dataBirthday].gender}</td>
								<td>{$dataBirthday[dataBirthday].status}</td>
								<td>{if $dataBirthday[dataBirthday].photo != ''} <a href="images/photo_individu/{$dataBirthday[dataBirthday].photo}" target="_blank"><img src="images/photo_individu/{$dataBirthday[dataBirthday].photo}" height="30" width="30"></a> {/if}</td>
							</tr>
							{/section}
						</tbody>
					</table>
				</div>
				
				<h4>Ulang Tahun Pernikahan</h4>
				<div class="table-responsive">
					<table class="table table-bordered table-hover tablesorter">
						<thead>
							<tr>
								<th>No. <i class="fa fa-sort"></i></th>
								<th>NIK <i class="fa fa-sort"></i></th>
								<th>Nama Suami <i class="fa fa-sort"></i></th>
								<th>Nama Istri <i class="fa fa-sort"></i></th>
								<th>Tanggal Pernikahan <i class="fa fa-sort"></i></th>
								<th>Usia Pernikahan (Tahun) <i class="fa fa-sort"></i></th>
								<th>Foto <i class="fa fa-sort"></i></th>
							</tr>
						</thead>
						<tbody>
							{section name=dataWedding loop=$dataWedding}
							<tr>
								<td>{$dataWedding[dataWedding].no}</td>
								<td>{$dataWedding[dataWedding].nik}</td>
								<td>{$dataWedding[dataWedding].suami}</td>
								<td>{$dataWedding[dataWedding].istri}</td>
								<td>{$dataWedding[dataWedding].tanggal_nikah}</td>
								<td>{$dataWedding[dataWedding].age}</td>
								<td>{if $dataWedding[dataWedding].photo != ''} <a href="images/photo_nikah/{$dataWedding[dataWedding].photo}" target="_blank"><img src="images/photo_nikah/{$dataWedding[dataWedding].photo}" height="30" width="30"></a> {/if}</td>
							</tr>
							{/section}
						</tbody>
					</table>
				</div>
			</div>
		</div><!-- /.row -->
	</div>
</div><!-- /#wrapper -->

{include file="footer.tpl"}