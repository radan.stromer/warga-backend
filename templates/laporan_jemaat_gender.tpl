{include file="header.tpl"}

<div id="wrapper">
	
	{include file="leftMenu.tpl"}
	
	<div id="page-wrapper">
		<div class="row">
			<div class="col-lg-12">
				<ol class="breadcrumb">
					<li><a href="home.php"><i class="fa fa-dashboard"></i> Home</a></li>
					<li class="active"><i class="fa fa-caret-square-o-down"></i> Laporan</li>
					<li class="active">Data Jemaat</li>
				</ol>				
			</div>
		</div><!-- /.row -->
		
		<div class="row">
			<div class="col-lg-12">
				<a href="print_jemaat_gender.php?module=laporan&act=print&id=A" target="_blank"><button class="btn btn-primary" type="button">Cetak Semua Data Jemaat</button></a><br><br>
				<div class="table-responsive">
					<table class="table table-bordered table-hover tablesorter">
						<thead>
							<tr>
								<th width='40'>No. <i class="fa fa-sort"></i></th>
								<th width='150'>Gender <i class="fa fa-sort"></i></th>
								<th width='200'>Jumlah Jiwa <i class="fa fa-sort"></i></th>
								<th>Cetak <i class="fa fa-sort"></i></th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>1. </td>
								<td>Laki-laki</td>
								<td>{$laki}</td>
								<td><a href="print_jemaat_gender.php?module=laporan&act=print&id=L" target="_blank"><button class="btn btn-primary" type="button">Cetak</button></a></td>
							</tr>
							<tr>
								<td>2. </td>
								<td>Perempuan</td>
								<td>{$perempuan}</td>
								<td><a href="print_jemaat_gender.php?module=laporan&act=print&id=P" target="_blank"><button class="btn btn-primary" type="button">Cetak</button></a></td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div><!-- /#wrapper -->

{include file="footer.tpl"}