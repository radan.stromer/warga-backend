<!-- Sidebar -->
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
	<!-- Brand and toggle get grouped for better mobile display -->
	<div class="navbar-header">
		<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
			<span class="sr-only">Toggle navigation</span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		</button>
		<a class="navbar-brand" href="index.html"><img src="images/logo.jpg" height="30" style='border-radius: 8px;'> - www.agussaputra.com - www.asfamedia.com - www.asfasolution.co.id</a><br><br><br>
		<span style='color: #FFF; padding-left: 10px;'>Sultan Residence H-9, Jl. Nyimas Gandasari - Kel. Jungjang - Kec. Arjawinangun - Kab. Cirebon 45162</span><br>
		<span style='color: #FFF; padding-left: 10px;'>Telp. (0231) 8830633, Website: http://www.asfasolution.co.id, Email: info@asfasolution.com</span>
	</div>
	
	<!-- Collect the nav links, forms, and other content for toggling -->
	<div class="collapse navbar-collapse navbar-ex1-collapse">
		<ul class="nav navbar-nav side-nav">
			<li class="active"><a href="home.php"><i class="fa fa-dashboard"></i> Home</a></li>
			<li class="dropdown">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-caret-square-o-down"></i> Master Data <b class="caret"></b></a>
				<ul class="dropdown-menu">
					<li><a href="users.php">Data Pengguna</a></li>
					<li><a href="funeral.php">Data Pemakaman</a></li>
					<li><a href="jobs.php">Data Pekerjaan</a></li>
					<li><a href="grade.php">Data Penghasilan</a></li>
				</ul>
			</li>
			<li class="dropdown">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-caret-square-o-down"></i> Kartu Keluarga<b class="caret"></b></a>
				<ul class="dropdown-menu">
					<li><a href="individu.php">Individu</a></li>
					<li><a href="family.php">Keluarga</a></li>
				</ul>
			</li>
			<li class="dropdown">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-caret-square-o-down"></i> Manajemen Komisi <b class="caret"></b></a>
				<ul class="dropdown-menu">
					<li><a href="periode.php">Periode Komisi</a></li>
					<li><a href="komisi.php">Komisi</a></li>
					<li><a href="anggota_komisi.php">Anggota Komisi</a></li>
				</ul>
			</li>
			<li class="dropdown">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-caret-square-o-down"></i> Manajemen Majelis <b class="caret"></b></a>
				<ul class="dropdown-menu">
					<li><a href="periode_majelis.php">Periode Majelis</a></li>
					<li><a href="majelis.php">Majelis</a></li>
					<li><a href="anggota_majelis.php">Anggota Majelis</a></li>
				</ul>
			</li>
			<li class="dropdown">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-caret-square-o-down"></i> Manajemen Staff <b class="caret"></b></a>
				<ul class="dropdown-menu">
					<li><a href="staff.php">Staff</a></li>
				</ul>
			</li>
			<li class="dropdown">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-caret-square-o-down"></i> Tokoh Masyarakat <b class="caret"></b></a>
				<ul class="dropdown-menu">
					<li><a href="pendeta.php">Tokoh Masyarakat</a></li>
				</ul>
			</li>
			<li class="dropdown">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-caret-square-o-down"></i> Laporan Data Warga<b class="caret"></b></a>
				<ul class="dropdown-menu">
					<li><a href="laporan_jemaat.php">Berdasarkan Kelompok Umur</a></li>
					<li><a href="laporan_jemaat_gender.php">Berdasarkan Gender</a></li>
				</ul>
			</li>
		</ul>
		
		<ul class="nav navbar-nav navbar-right navbar-user">
			<li><a href="logout.php"><i class="fa fa-power-off"></i> Log Out</a></li>
		</ul>
	</div><!-- /.navbar-collapse -->
</nav>