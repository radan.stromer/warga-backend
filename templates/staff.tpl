{include file="header.tpl"}

<div id="wrapper">
	
	{include file="leftMenu.tpl"}
	
	<div id="page-wrapper">
		<div class="row">
			<div class="col-lg-12">
				<ol class="breadcrumb">
					<li><a href="home.php"><i class="fa fa-dashboard"></i> Home</a></li>
					<li class="active"><i class="fa fa-caret-square-o-down"></i> Manajemen Staff</li>
					<li class="active">Staff</li>
				</ol>
				
				{if $code == '1'}
					<div class="alert alert-success alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						Data staff berhasil disimpan.
					</div>
				{/if}
				{if $code == '2'}
					<div class="alert alert-success alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						Data staff berhasil diupdate.
					</div>
				{/if}
				{if $code == '3'}
					<div class="alert alert-success alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						Data staff berhasil dihapus.
					</div>
				{/if}
			</div>
		</div><!-- /.row -->
		
		<script type='text/javascript' src='js/jquery.validate.js'></script>
			
		{literal}
			<script type='text/javascript'>
				$(document).ready(function() {
					$('#frm_staff').validate({
						rules:{
							anggota_id: true,
							jabatan: true,
							status: true
						},
						messages:{
							anggota_id:{
								required: "This is a required field."
							},
							jabatan:{
								required: "This is a required field."
							},
							status:{
								required: "This is a required field."
							}
						}
					});
					
					$( "#datepicker1" ).datepicker({
						changeMonth: true,
						changeYear: true,
						dateFormat: "yy-mm-dd",
						yearRange: 'c-100:c-0'
					});
					
					$( "#datepicker2" ).datepicker({
						changeMonth: true,
						changeYear: true,
						dateFormat: "yy-mm-dd",
						yearRange: 'c-100:c-0'
					});
				});
			</script>
		{/literal}
		
		<div class="row">
			<div class="col-lg-12">
				
				{if $module == 'staff' AND $act == 'add'}
				
					<h3>Tambah Staff</h3>
					<form role="form" method="POST" action="staff.php?module=staff&act=input" id="frm_staff">
						<div class="form-group">
							<label>Anggota</label>
							<select name="anggota_id" class="required" style="display: block; width: 270px; height: 34px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
								<option value="">- Pilih Anggota -</option>
								{section name=dataAnggota loop=$dataAnggota}
									<option value="{$dataAnggota[dataAnggota].anggota_id}">{$dataAnggota[dataAnggota].full_name}</option>
								{/section}
							</select>
						</div>
						<div class="form-group">
							<label>Jabatan</label>
							<input type="text" name="jabatan" class="required" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
						</div>
						<div class="form-group">
							<label>Tanggal Mulai</label>
							<input type="text" id="datepicker1" name="tanggal_mulai" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
						</div>
						<div class="form-group">
							<label>Tanggal Keluar</label>
							<input type="text" id="datepicker2" name="tanggal_keluar" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
						</div>
						<div class="form-group">
							<label>Status</label>
							<select name="status" class="required" style="display: block; width: 270px; height: 34px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
								<option value="">- Pilih Status -</option>
								<option value="Y" SELECTED>Aktif</option>
								<option value="N">Tidak Aktif</option>
							</select>
						</div>
						<button type="submit" class="btn btn-primary">Simpan</button>
					</form>
				
				{elseif $module == 'staff' AND $act == 'edit'}
				
					<h3>Ubah Staff</h3>
					<form role="form" method="POST" action="staff.php?module=staff&act=update" id="frm_staff">
						<input type="hidden" name="staff_id" value="{$staff_id}">
						<div class="form-group">
							<label>Anggota</label>
							<select name="anggota_id" class="required" style="display: block; width: 270px; height: 34px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
								<option value="">- Pilih Anggota -</option>
								{section name=dataAnggota loop=$dataAnggota}
									{if $dataAnggota[dataAnggota].anggota_id == $anggota_id}
										<option value="{$dataAnggota[dataAnggota].anggota_id}" SELECTED>{$dataAnggota[dataAnggota].full_name}</option>
									{else}
										<option value="{$dataAnggota[dataAnggota].anggota_id}">{$dataAnggota[dataAnggota].full_name}</option>
									{/if}
								{/section}
							</select>
						</div>
						<div class="form-group">
							<label>Jabatan</label>
							<input type="text" value="{$jabatan}" name="jabatan" class="required" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
						</div>
						<div class="form-group">
							<label>Tanggal Mulai</label>
							<input type="text" value="{$tanggal_mulai}" id="datepicker1" name="tanggal_mulai" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
						</div>
						<div class="form-group">
							<label>Tanggal Keluar</label>
							<input type="text" value="{$tanggal_keluar}" id="datepicker2" name="tanggal_keluar" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
						</div>
						<div class="form-group">
							<label>Status</label>
							<select name="status" class="required" style="display: block; width: 270px; height: 34px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
								<option value="">- Pilih Status -</option>
								<option value="Y" {if $status == 'Y'} SELECTED {/if}>Aktif</option>
								<option value="N" {if $status == 'N'} SELECTED {/if}>Tidak Aktif</option>
							</select>
						</div>
						<button type="submit" class="btn btn-primary">Simpan</button>
					</form>
					
				{else}
			
					<a href="staff.php?module=staff&act=add"><button class="btn btn-primary" type="button">Tambah Staff</button></a>
					<a href="print_staff.php?module=staff&act=print" target="_blank"><button class="btn btn-primary" type="button">Cetak</button></a>
					<h3>Manajemen Staff</h3>
					<div class="table-responsive">
						<table class="table table-bordered table-hover tablesorter">
							<thead>
								<tr>
									<th>No. <i class="fa fa-sort"></i></th>
									<th>No Induk <i class="fa fa-sort"></i></th>
									<th>Nama Lengkap <i class="fa fa-sort"></i></th>
									<th>Jabatan <i class="fa fa-sort"></i></th>
									<th>Tanggal Mulai <i class="fa fa-sort"></i></th>
									<th>Tanggal Keluar <i class="fa fa-sort"></i></th>
									<th>Status <i class="fa fa-sort"></i></th>
									<th>Aksi <i class="fa fa-sort"></i></th>
								</tr>
							</thead>
							<tbody>
								{section name=dataStaff loop=$dataStaff}
								<tr>
									<td>{$dataStaff[dataStaff].no}</td>
									<td>{$dataStaff[dataStaff].no_induk}</td>
									<td>{$dataStaff[dataStaff].full_name}</td>
									<td>{$dataStaff[dataStaff].jabatan}</td>
									<td>{$dataStaff[dataStaff].tanggal_mulai}</td>
									<td>{$dataStaff[dataStaff].tanggal_keluar}</td>
									<td>{$dataStaff[dataStaff].status}</td>
									<td>
										<a href="staff.php?module=staff&act=edit&staff_id={$dataStaff[dataStaff].staff_id}"><button type="button" class="btn btn-success">Edit</button></a>
										<a href="staff.php?module=staff&act=delete&staff_id={$dataStaff[dataStaff].staff_id}" onclick="return confirm('Anda Yakin ingin menghapus {$dataStaff[dataStaff].full_name}?');"><button type="button" class="btn btn-danger">Hapus</button></a>
									</td>
								</tr>
								{/section}
							</tbody>
						</table>
					</div>
					<div id="paging">{$pageLink}</div>
				{/if}
			</div>
		</div>
	</div>
</div><!-- /#wrapper -->

{include file="footer.tpl"}