{include file="header.tpl"}

<div id="wrapper">
	
	{include file="leftMenu.tpl"}
	
	<div id="page-wrapper">
		<div class="row">
			<div class="col-lg-12">
				<ol class="breadcrumb">
					<li><a href="home.php"><i class="fa fa-dashboard"></i> Home</a></li>
					<li class="active"><i class="fa fa-caret-square-o-down"></i> Keluarga Jemaat</li>
					<li class="active">Keluarga</li>
				</ol>
				
				{if $code == '1'}
					<div class="alert alert-success alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						Data keluarga berhasil disimpan.
					</div>
				{/if}
				{if $code == '2'}
					<div class="alert alert-success alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						Data keluarga berhasil diupdate.
					</div>
				{/if}
				{if $code == '3'}
					<div class="alert alert-success alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						Data keluarga berhasil dihapus.
					</div>
				{/if}
				{if $code == '4'}
					<div class="alert alert-success alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						Data keluarga berhasil disimpan.
					</div>
				{/if}
				{if $code == '6'}
					<div class="alert alert-success alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						Data keluarga berhasil dihapus.
					</div>
				{/if}
				{if $code == '7'}
					<div class="alert alert-success alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						Data kepala keluarga berhasil dihapus.
					</div>
				{/if}
			</div>
		</div><!-- /.row -->
		
		<script type='text/javascript' src='js/jquery.validate.js'></script>
			
		{literal}
			<script type='text/javascript'>
				$(document).ready(function() {
					$('#frm_family').validate({
						rules:{
							kepala_keluarga: true,
							alamat: true,
							tanggal_nikah: true
						},
						messages:{
							kepala_keluarga:{
								required: "This is a required field."
							},
							alamat:{
								required: "This is a required field."
							},
							tanggal_nikah:{
								required: "This is a required field."
							}
						}
					});
					
					$('#frm_child').validate({
						rules:{
							child_id: true,
							status: true
						},
						messages:{
							child_id:{
								required: "This is a required field."
							},
							status:{
								required: "This is a required field."
							}
						}
					});
					
					$( "#datepicker" ).datepicker({
						changeMonth: true,
						changeYear: true,
						dateFormat: "yy-mm-dd",
						yearRange: 'c-100:c-0'
					});
				});
			</script>
			{/literal}
			
			{if $act eq 'add' || $act eq 'edit'}
				<script type="text/javascript" src="js/ajaxupload.3.5.js" ></script>
				<link rel="stylesheet" type="text/css" href="css/Ajaxfile-upload.css" />
				
				{literal}	
				<script type='text/javascript'>
					$(function(){
						var btnUpload=$('#me');
						var mestatus=$('#mestatus');
						var files=$('#files');
						new AjaxUpload(btnUpload, {
							action: 'upload_nikah.php',
							name: 'uploadfile',
							onSubmit: function(file, ext){
								 if (! (ext && /^(jpg|jpeg)$/.test(ext))){ 
				                    // extension is not allowed 
									mestatus.text('Only JPG files are allowed');
									return false;
								}
								mestatus.html('<img src="images/ajax-loader.gif" height="16" width="16">');
							},
							onComplete: function(file, response){
								//On completion clear the status
								mestatus.text('');
								//On completion clear the status
								files.html('');
								//Add uploaded file to list
								if(response!=="error"){
									$('<li></li>').appendTo('#files').html('<img src="images/photo_nikah/'+response+'" alt="" width="100" /><br />').addClass('success');
									$('<li></li>').appendTo('#nikah').html('<input type="hidden" name="filename" value="'+response+'">').addClass('nameupload');
									
								} else{
									$('<li></li>').appendTo('#files').text(file).addClass('error');
								}
							}
						});
						
					});
				</script>
				{/literal}
			{/if}
		
		<div class="row">
			<div class="col-lg-12">
				
				{if $module == 'family' AND $act == 'add'}
				
					<h3>Tambah Keluarga</h3>
					<form role="form" method="POST" action="family.php?module=family&act=input" id="frm_family">
						<div class="form-group">
							<label>NIK</label>
							<input type="text" value="{$nik}" name="nik" class="required" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;" DISABLED>
							<input type="hidden" value="{$nik}" name="nik" class="required" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
							<input type="hidden" value="{$sortno}" name="sortno" class="required" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
						</div>
						<div class="form-group">
							<label>Nama Kepala Keluarga</label>
							<select name="kepala_keluarga" class="required" style="display: block; width: 270px; height: 34px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
								<option value="">- Pilih Kepala Keluarga -</option>
								{section name=dataKepala loop=$dataKepala}
									<option value="{$dataKepala[dataKepala].kepala_id}">{$dataKepala[dataKepala].kepala_name}</option>
								{/section}
							</select>
						</div>
						<div class="form-group">
							<label>Tanggal Pernikahan</label>
							<input type="text" id="datepicker" name="tanggal_nikah" class="required" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
						</div>
						<div class="form-group">
							<label>Alamat</label>
							<input type="text" name="alamat" class="required" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
						</div>
						<div class="form-group">
							<label>Foto Pernikahan</label>
							<div id="me" class="styleall" style="cursor:pointer;">
								<label>
									<button class="btn btn-primary">Browse</button>
								</label>
							</div>
							<span id="mestatus" ></span>
							<div id="nikah">
								<li class="nameupload"></li>
							</div>
							<div id="files">
								<li class="success"></li>
							</div>
						</div>
						<button type="submit" class="btn btn-primary">Simpan</button>
					</form>
				
				{elseif $module == 'family' AND $act == 'edit'}
				
					<h3>Ubah Keluarga</h3>
					<form role="form" method="POST" action="family.php?module=family&act=update" id="frm_family">
						<input type="hidden" value="{$fid}" name="fid" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
						<div class="form-group">
							<label>NIK</label>
							<input type="text" value="{$nik}" name="nik" class="required" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;" DISABLED>
						</div>
						<div class="form-group">
							<label>Nama Kepala Keluarga</label>
							<input type="text" value="{$full_name}" name="tanggal_nikah" class="required" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;" DISABLED>
						</div>
						<div class="form-group">
							<label>Tanggal Pernikahan</label>
							<input type="text" id="datepicker" value="{$tanggal_nikah}" name="tanggal_nikah" class="required" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
						</div>
						<div class="form-group">
							<label>Alamat</label>
							<input type="text" value="{$alamat}" name="alamat" class="required" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
						</div>
						<div class="form-group">
							<label>Foto Pernikahan</label>
							<div id="me" class="styleall" style="cursor:pointer;">
								<label>
									<button class="btn btn-primary">Browse</button>
								</label>
							</div>
							<span id="mestatus" ></span>
							<div id="nikah">
								<li class="nameupload"></li>
							</div>
							<div id="files">
								<li class="success">
									{if $photo != ''}
										<img src="images/photo_nikah/{$photo}" width="100">
									{/if}
								</li>
							</div>
						</div>
						<button type="submit" class="btn btn-primary">Simpan</button>
					</form>
				
				{elseif $module == 'family' AND $act == 'step'}
					
					<h3>Data Keluarga</h3>
					<div class="form-group">
						<label>NIK :</label> {$nik}<br>
						<label>Nama Kepala Keluarga :</label> {$full_name}<br>
						<label>Tanggal Pernikahan :</label> {$tanggal_nikah}<br>
						<label>Alamat :</label> {$alamat}<br>
						<label>Photo :</label> {if $photo != ''} <a href="images/photo_nikah/{$photo}" target="_blank"><b>Lihat Foto</b></a> {else} - {/if} <br>
						<a href="family.php?module=family&act=edit&fid={$fid}"><button class="btn btn-success" type="button">Edit Kepala Keluarga</button></a>
					</div>
					<br>
					<a href="family.php?module=family&act=addchild&fid={$fid}"><button class="btn btn-primary" type="button">Tambah Data Keluarga</button></a>
					<a href="print_family.php?module=family&act=addchild&fid={$fid}" target="_blank"><button class="btn btn-primary" type="button">Cetak Kartu Keluarga</button></a>
					<h4>Data Keluarga</h4>
					<div class="table-responsive">
						<table class="table table-bordered table-hover tablesorter">
							<thead>
								<tr>
									<th>No. <i class="fa fa-sort"></i></th>
									<th>Nama Lengkap <i class="fa fa-sort"></i></th>
									<th>JK <i class="fa fa-sort"></i></th>
									<th>No. Induk <i class="fa fa-sort"></i></th>
									<th>Tempat, Tgl. Lahir <i class="fa fa-sort"></i></th>
									<th>Pekerjaan <i class="fa fa-sort"></i></th>
									<th>Gol. Darah <i class="fa fa-sort"></i></th>
									<th>Status <i class="fa fa-sort"></i></th>
									<th>Aksi <i class="fa fa-sort"></i></th>
								</tr>
							</thead>
							<tbody>
								{section name=dataChild loop=$dataChild}
								<tr>
									<td>{$dataChild[dataChild].no}</td>
									<td>{$dataChild[dataChild].full_name}</td>
									<td>{$dataChild[dataChild].gender}</td>
									<td>{$dataChild[dataChild].no_induk}</td>
									<td>{$dataChild[dataChild].place_of_birth}, {$dataChild[dataChild].date_of_birth}</td>
									<td>{$dataChild[dataChild].job_name}</td>
									<td>{$dataChild[dataChild].blood_type}</td>
									<td>{$dataChild[dataChild].status}</td>
									<td>
										<a href="family.php?module=family&act=delete&cid={$dataChild[dataChild].cid}&fid={$dataChild[dataChild].fid}" onclick="return confirm('Anda Yakin ingin menghapus {$dataChild[dataChild].full_name}?');"><button type="button" class="btn btn-danger">Hapus</button></a>
									</td>
								</tr>
								{/section}
							</tbody>
						</table>
					</div>
					
				{elseif $module == 'family' AND $act == 'addchild'}
					
					<h3>Data Keluarga</h3>
					<div class="form-group">
						<label>NIK :</label> {$nik}<br>
						<label>Nama Kepala Keluarga :</label> {$full_name}<br>
						<label>Tanggal Pernikahan</label> {$tanggal_nikah}<br>
						<label>Alamat :</label> {$alamat}
					</div>
					<h4>Tambah Data Keluarga</h4>
					<form role="form" method="POST" action="family.php?module=family&act=addchildinput" id="frm_child">
						<input type="hidden" name="fid" value="{$fid}">
						<div class="form-group">
							<label>Nama Keluarga</label>
							<select name="child_id" class="required" style="display: block; width: 270px; height: 34px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
								<option value="">- Pilih Kepala Keluarga -</option>
								{section name=dataKepala loop=$dataKepala}
									<option value="{$dataKepala[dataKepala].kepala_id}">{$dataKepala[dataKepala].kepala_name}</option>
								{/section}
							</select>
						</div>
						<div class="form-group">
							<label>Status Keluarga</label>
							<select name="status" class="required" style="display: block; width: 270px; height: 34px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
								<option value="">- Pilih Status Keluarga -</option>
								<option value="1">Suami</option>
								<option value="2">Istri</option>
								<option value="3">Anak</option>
							</select>
						</div>
						<div class="form-group">
							<label>Keterangan</label>
							<input type="text" name="keterangan" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
						</div>
						<button type="submit" class="btn btn-primary">Simpan</button>
					</form>
				
				{elseif $module == 'family' AND $act == 'search'}
					<h2>Pencarian Kartu Keluarga</h2>
					<form role="form" method="GET" action="family.php">
						<input type="hidden" name="module" value="family">
						<input type="hidden" name="act" value="search">
						<div class="form-group">
							<label>Masukkan Keyword (Nama Kepala Keluarga)</label>
							<input type="text" value="{$keyword}" placeholder="Masukkan Nama Kepala Keluarga" name="name" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
						</div>
						<button type="submit" class="btn btn-primary">Cari</button>
					</form>
					
					<h3>Hasil Pencarian (Nama Kepala Keluarga: <b>{$keyword}</b>)<br>
					Ditemukan: {$numSearch} data
					</h3>
					<div class="table-responsive">
						<table class="table table-bordered table-hover tablesorter">
							<thead>
								<tr>
									<th width='40'>No. <i class="fa fa-sort"></i></th>
									<th width='150'>NIK <i class="fa fa-sort"></i></th>
									<th width='220'>Nama Kepala Keluarga <i class="fa fa-sort"></i></th>
									<th width="120">Tanggal Pernikahan <i class="fa fa-sort"></i></th>
									<th>Alamat <i class="fa fa-sort"></i></th>
									<th width='200'>Aksi <i class="fa fa-sort"></i></th>
								</tr>
							</thead>
							<tbody>
								{section name=dataSearch loop=$dataSearch}
								<tr>
									<td>{$dataSearch[dataSearch].no}</td>
									<td>{$dataSearch[dataSearch].nik}</td>
									<td>{$dataSearch[dataSearch].full_name}</td>
									<td>{$dataSearch[dataSearch].tanggal_nikah}</td>
									<td>{$dataSearch[dataSearch].alamat}</td>
									<td>
										<a href="print_family.php?module=family&act=addchild&fid={$dataSearch[dataSearch].fid}" target="_blank"><button type="button" class="btn btn-primary">Cetak</button></a>
										<a href="family.php?module=family&act=step&fid={$dataSearch[dataSearch].fid}&keyword={$keyword}"><button type="button" class="btn btn-success">Edit</button></a>
										<a href="family.php?module=family&act=deletefam&fid={$dataSearch[dataSearch].fid}&keyword={$keyword}" onclick="return confirm('Anda Yakin ingin menghapus kepala keluarga {$dataSearch[dataSearch].full_name}?');"><button type="button" class="btn btn-danger">Hapus</button></a>
									</td>
								</tr>
								{/section}
							</tbody>
						</table>
					</div>
				
				{else}
			
					<a href="family.php?module=family&act=add"><button class="btn btn-primary" type="button">Tambah Keluarga</button></a>
					<h2>Pencarian Kartu Keluarga</h2>
					<form role="form" method="GET" action="family.php">
						<input type="hidden" name="module" value="family">
						<input type="hidden" name="act" value="search">
						<div class="form-group">
							<label>Masukkan Keyword (Nama Kepala Keluarga)</label>
							<input type="text" placeholder="Masukkan Nama Kepala Keluarga" name="name" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
						</div>
						<button type="submit" class="btn btn-primary">Cari</button>
					</form>
				{/if}
			</div>
		</div>
	</div>
</div><!-- /#wrapper -->

{include file="footer.tpl"}