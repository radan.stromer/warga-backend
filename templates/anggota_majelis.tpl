{include file="header.tpl"}

<div id="wrapper">
	
	{include file="leftMenu.tpl"}
	
	<div id="page-wrapper">
		<div class="row">
			<div class="col-lg-12">
				<ol class="breadcrumb">
					<li><a href="home.php"><i class="fa fa-dashboard"></i> Home</a></li>
					<li class="active"><i class="fa fa-caret-square-o-down"></i> Manajemen Majelis</li>
					<li class="active">Anggota Majelis</li>
				</ol>
				
				{if $code == '1'}
					<div class="alert alert-success alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						Data anggota majelis berhasil disimpan.
					</div>
				{/if}
				{if $code == '2'}
					<div class="alert alert-success alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						Data anggota majelis berhasil diupdate.
					</div>
				{/if}
				{if $code == '3'}
					<div class="alert alert-success alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						Data anggota majelis berhasil dihapus.
					</div>
				{/if}
			</div>
		</div><!-- /.row -->
		
		<script type='text/javascript' src='js/jquery.validate.js'></script>
			
		{literal}
			<script type='text/javascript'>
				$(document).ready(function() {
					$('#frm_majelis').validate({
						rules:{
							majelis_id: true,
							anggota_id: true,
							jabatan: true
						},
						messages:{
							majelis_id:{
								required: "This is a required field."
							},
							anggota_id:{
								required: "This is a required field."
							},
							jabatan:{
								required: "This is a required field."
							}
						}
					});
				});
			</script>
		{/literal}
		
		<div class="row">
			<div class="col-lg-12">
				
				{if $module == 'anggota' AND $act == 'add'}
				
					<h3>Tambah Majelis Anggota</h3>
					<form role="form" method="POST" action="anggota_majelis.php?module=anggota&act=input" id="frm_majelis">
						<div class="form-group">
							<label>Majelis</label>
							<p>{$nama_periode} [{$nama_majelis}]	
								<input type="hidden" name="majelis_id" value="{$majelis_id}">
							</p>
						</div>
						<div class="form-group">
							<label>Nama Anggota</label>
							<select name="anggota_id" class="required" style="display: block; width: 270px; height: 34px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
								<option value="">- Pilih Majelis Anggota -</option>
								{section name=dataAnggota loop=$dataAnggota}
									<option value="{$dataAnggota[dataAnggota].anggota_id}">{$dataAnggota[dataAnggota].full_name}</option>
								{/section}
							</select>
						</div>
						<div class="form-group">
							<label>Jabatan</label>
							<input type="text" name="jabatan" placeholder="Ex: Ketua" class="required" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
						</div>
						<button type="submit" class="btn btn-primary">Simpan</button>
					</form>
				
				{elseif $module == 'anggota' AND $act == 'edit'}
				
					<h3>Ubah Majelis Anggota</h3>
					<form role="form" method="POST" action="anggota_majelis.php?module=anggota&act=update" id="frm_majelis">
						<input type="hidden" name="majelis_anggota_id" value="{$majelis_anggota_id}">
						<div class="form-group">
							<label>Majelis</label>
							<select name="majelis_id" class="required" style="display: block; width: 270px; height: 34px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
								<option value="">- Pilih Majelis -</option>
								{section name=dataMajelis loop=$dataMajelis}
									{if $dataMajelis[dataMajelis].majelis_id == $majelis_id}
										<option value="{$dataMajelis[dataMajelis].majelis_id}" SELECTED>{$dataMajelis[dataMajelis].nama_periode} [{$dataMajelis[dataMajelis].nama_majelis}]</option>
									{else}
										<option value="{$dataMajelis[dataMajelis].majelis_id}">{$dataMajelis[dataMajelis].nama_periode} [{$dataMajelis[dataMajelis].nama_majelis}]</option>
									{/if}
								{/section}
							</select>
						</div>
						<div class="form-group">
							<label>Nama Anggota</label>
							<select name="anggota_id" class="required" style="display: block; width: 270px; height: 34px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
								<option value="">- Pilih Komisi Anggota -</option>
								{section name=dataAnggota loop=$dataAnggota}
									{if $dataAnggota[dataAnggota].anggota_id == $anggota_id}
										<option value="{$dataAnggota[dataAnggota].anggota_id}" SELECTED>{$dataAnggota[dataAnggota].full_name}</option>
									{else}
										<option value="{$dataAnggota[dataAnggota].anggota_id}">{$dataAnggota[dataAnggota].full_name}</option>
									{/if}
								{/section}
							</select>
						</div>
						<div class="form-group">
							<label>Jabatan</label>
							<input type="text" value="{$jabatan}" name="jabatan" placeholder="Ex: Ketua" class="required" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
						</div>
						<button type="submit" class="btn btn-primary">Simpan</button>
					</form>
				
				{elseif $module == 'anggota' AND $act == 'detail'}
					<div class="form-group">
						<label>Periode / Majelis</label>
						<p>{$nama_periode} [{$nama_majelis}]</p>
					</div>
					
					<a href="anggota_majelis.php"><button class="btn btn-primary" type="button">Kembali</button></a>
					<a href="anggota_majelis.php?module=anggota&act=add&majelis_id={$majelis_id}"><button class="btn btn-primary" type="button">Tambah Majelis Anggota</button></a>
					<a href="print_majelis_anggota.php?module=anggota&act=print&majelis_id={$majelis_id}" target="_blank"><button class="btn btn-primary" type="button">Cetak</button></a>
					<h3>Anggota Komisi</h3>
					<div class="table-responsive">
						<table class="table table-bordered table-hover tablesorter">
							<thead>
								<tr>
									<th>No. <i class="fa fa-sort"></i></th>
									<th>Nama Anggota <i class="fa fa-sort"></i></th>
									<th>Jabatan <i class="fa fa-sort"></i></th>
									<th>Aksi <i class="fa fa-sort"></i></th>
								</tr>
							</thead>
							<tbody>
								{section name=dataAnggota loop=$dataAnggota}
								<tr>
									<td>{$dataAnggota[dataAnggota].no}</td>
									<td>{$dataAnggota[dataAnggota].full_name}</td>
									<td>{$dataAnggota[dataAnggota].jabatan}</td>
									<td>
										<a href="anggota_majelis.php?module=anggota&act=delete&majelis_anggota_id={$dataAnggota[dataAnggota].majelis_anggota_id}&majelis_id={$majelis_id}" onclick="return confirm('Anda Yakin ingin menghapus {$dataAnggota[dataAnggota].full_name}?');"><button type="button" class="btn btn-danger">Hapus</button></a>
									</td>
								</tr>
								{/section}
							</tbody>
						</table>
					</div>
									
				{else}
			
					<h3>Manajemen Majelis</h3>
					<div class="table-responsive">
						<table class="table table-bordered table-hover tablesorter">
							<thead>
								<tr>
									<th>No. <i class="fa fa-sort"></i></th>
									<th>Periode Majelis <i class="fa fa-sort"></i></th>
									<th>Nama Majelis <i class="fa fa-sort"></i></th>
									<th>Total Anggota <i class="fa fa-sort"></i></th>
									<th>Aksi <i class="fa fa-sort"></i></th>
								</tr>
							</thead>
							<tbody>
								{section name=dataMajelis loop=$dataMajelis}
								<tr>
									<td>{$dataMajelis[dataMajelis].no}</td>
									<td>{$dataMajelis[dataMajelis].nama_periode}</td>
									<td>{$dataMajelis[dataMajelis].nama_majelis}</td>
									<td>{$dataMajelis[dataMajelis].total}</td>
									<td>
										<a href="anggota_majelis.php?module=anggota&act=detail&majelis_id={$dataMajelis[dataMajelis].majelis_id}"><button type="button" class="btn btn-success">Detail</button></a>
									</td>
								</tr>
								{/section}
							</tbody>
						</table>
					</div>
					<div id="paging">{$pageLink}</div>
				{/if}
			</div>
		</div>
	</div>
</div><!-- /#wrapper -->

{include file="footer.tpl"}