{include file="header.tpl"}

<div id="wrapper">
	
	{include file="leftMenu.tpl"}
	
	<div id="page-wrapper">
		<div class="row">
			<div class="col-lg-12">
				<ol class="breadcrumb">
					<li><a href="home.php"><i class="fa fa-dashboard"></i> Home</a></li>
					<li class="active"><i class="fa fa-caret-square-o-down"></i> Master Data</li>
					<li class="active">Babtis</li>
				</ol>
				
				{if $code == '1'}
					<div class="alert alert-success alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						Data babtis berhasil disimpan.
					</div>
				{/if}
				{if $code == '2'}
					<div class="alert alert-success alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						Data babtis berhasil diupdate.
					</div>
				{/if}
				{if $code == '3'}
					<div class="alert alert-success alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						Data babtis berhasil dihapus.
					</div>
				{/if}
			</div>
		</div><!-- /.row -->
		
		<script type='text/javascript' src='js/jquery.validate.js'></script>
			
		{literal}
			<script type='text/javascript'>
				$(document).ready(function() {
					$('#frm_babtis').validate({
						rules:{
							babtisName: true,
							status: true
						},
						messages:{
							babtisName:{
								required: "This is a required field."
							},
							status:{
								required: "This is a required field."
							}
						}
					});
				});
			</script>
		{/literal}
		
		<div class="row">
			<div class="col-lg-12">
				
				{if $module == 'babtis' AND $act == 'add'}
				
					<h3>Tambah Jenis Babtis</h3>
					<form role="form" method="POST" action="babtis.php?module=babtis&act=input" id="frm_babtis">
						<div class="form-group">
							<label>Nama (Jenis) Babtis</label>
							<input type="text" name="babtisName" class="required" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
						</div>
						<div class="form-group">
							<label>Status</label>
							<select name="status" class="required" style="display: block; width: 270px; height: 34px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
								<option value="">- Pilih Status -</option>
								<option value="Y" SELECTED>Aktif</option>
								<option value="N">Tidak Aktif</option>
							</select>
						</div>
						<button type="submit" class="btn btn-primary">Simpan</button>
					</form>
				
				{elseif $module == 'babtis' AND $act == 'edit'}
				
					<h3>Ubah Jenis Babtis</h3>
					<form role="form" method="POST" action="babtis.php?module=babtis&act=update" id="frm_babtis">
						<input type="hidden" name="babtisID" value="{$babtisID}">
						<div class="form-group">
							<label>Nama (Jenis) Babtis</label>
							<input type="text" name="babtisName" value="{$babtisName}" class="required" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
						</div>
						<div class="form-group">
							<label>Status</label>
							<select name="status" class="required" style="display: block; width: 270px; height: 34px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
								<option value="">- Pilih Status -</option>
								<option value="Y" {if $status == 'Y'} SELECTED {/if}>Aktif</option>
								<option value="N" {if $status == 'N'} SELECTED {/if}>Tidak Aktif</option>
							</select>
						</div>
						<button type="submit" class="btn btn-primary">Simpan</button>
					</form>
					
				{else}
			
					<a href="babtis.php?module=babtis&act=add"><button class="btn btn-primary" type="button">Tambah Babtis</button></a>
					<h3>Manajemen Jenis Babtis</h3>
					<div class="table-responsive">
						<table class="table table-bordered table-hover tablesorter">
							<thead>
								<tr>
									<th>No. <i class="fa fa-sort"></i></th>
									<th>Nama (Jenis) Babtis <i class="fa fa-sort"></i></th>
									<th>Status <i class="fa fa-sort"></i></th>
									<th>Aksi <i class="fa fa-sort"></i></th>
								</tr>
							</thead>
							<tbody>
								{section name=dataBabtis loop=$dataBabtis}
								<tr>
									<td>{$dataBabtis[dataBabtis].no}</td>
									<td>{$dataBabtis[dataBabtis].babtisName}</td>
									<td>{$dataBabtis[dataBabtis].status}</td>
									<td>
										<a href="babtis.php?module=babtis&act=edit&babtisID={$dataBabtis[dataBabtis].babtisID}"><button type="button" class="btn btn-success">Edit</button></a>
										<a href="babtis.php?module=babtis&act=delete&babtisID={$dataBabtis[dataBabtis].babtisID}" onclick="return confirm('Anda Yakin ingin menghapus {$dataBabtis[dataBabtis].babtisName}?');"><button type="button" class="btn btn-danger">Hapus</button></a>
									</td>
								</tr>
								{/section}
							</tbody>
						</table>
					</div>
					<div id="paging">{$pageLink}</div>
				{/if}
			</div>
		</div>
	</div>
</div><!-- /#wrapper -->

{include file="footer.tpl"}