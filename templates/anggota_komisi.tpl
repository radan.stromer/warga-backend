{include file="header.tpl"}

<div id="wrapper">
	
	{include file="leftMenu.tpl"}
	
	<div id="page-wrapper">
		<div class="row">
			<div class="col-lg-12">
				<ol class="breadcrumb">
					<li><a href="home.php"><i class="fa fa-dashboard"></i> Home</a></li>
					<li class="active"><i class="fa fa-caret-square-o-down"></i> Manajemen Komisi</li>
					<li class="active">Anggota Komisi</li>
				</ol>
				
				{if $code == '1'}
					<div class="alert alert-success alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						Data anggota komisi berhasil disimpan.
					</div>
				{/if}
				{if $code == '2'}
					<div class="alert alert-success alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						Data anggota komisi berhasil diupdate.
					</div>
				{/if}
				{if $code == '3'}
					<div class="alert alert-success alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						Data anggota komisi berhasil dihapus.
					</div>
				{/if}
			</div>
		</div><!-- /.row -->
		
		<script type='text/javascript' src='js/jquery.validate.js'></script>
			
		{literal}
			<script type='text/javascript'>
				$(document).ready(function() {
					$('#frm_komisi').validate({
						rules:{
							komisi_id: true,
							anggota_id: true,
							jabatan: true
						},
						messages:{
							komisi_id:{
								required: "This is a required field."
							},
							anggota_id:{
								required: "This is a required field."
							},
							jabatan:{
								required: "This is a required field."
							}
						}
					});
				});
			</script>
		{/literal}
		
		<div class="row">
			<div class="col-lg-12">
				
				{if $module == 'anggota' AND $act == 'add'}
				
					<h3>Tambah Komisi Anggota</h3>
					<form role="form" method="POST" action="anggota_komisi.php?module=anggota&act=input" id="frm_komisi">
						<div class="form-group">
							<label>Komisi</label>
							<p>{$nama_periode} [{$nama_komisi}]	
								<input type="hidden" name="komisi_id" value="{$komisi_id}">
							</p>
						</div>
						<div class="form-group">
							<label>Nama Anggota</label>
							<select name="anggota_id" class="required" style="display: block; width: 270px; height: 34px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
								<option value="">- Pilih Komisi Anggota -</option>
								{section name=dataAnggota loop=$dataAnggota}
									<option value="{$dataAnggota[dataAnggota].anggota_id}">{$dataAnggota[dataAnggota].full_name}</option>
								{/section}
							</select>
						</div>
						<div class="form-group">
							<label>Jabatan</label>
							<input type="text" name="jabatan" placeholder="Ex: Ketua" class="required" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
						</div>
						<button type="submit" class="btn btn-primary">Simpan</button>
					</form>
				
				{elseif $module == 'anggota' AND $act == 'edit'}
				
					<h3>Ubah Komisi Anggota</h3>
					<form role="form" method="POST" action="anggota_komisi.php?module=anggota&act=update" id="frm_komisi">
						<input type="hidden" name="komisi_anggota_id" value="{$komisi_anggota_id}">
						<div class="form-group">
							<label>Komisi</label>
							<select name="komisi_id" class="required" style="display: block; width: 270px; height: 34px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
								<option value="">- Pilih Komisi -</option>
								{section name=dataKomisi loop=$dataKomisi}
									{if $dataKomisi[dataKomisi].komisi_id == $komisi_id}
										<option value="{$dataKomisi[dataKomisi].komisi_id}" SELECTED>{$dataKomisi[dataKomisi].nama_periode} [{$dataKomisi[dataKomisi].nama_komisi}]</option>
									{else}
										<option value="{$dataKomisi[dataKomisi].komisi_id}">{$dataKomisi[dataKomisi].nama_periode} [{$dataKomisi[dataKomisi].nama_komisi}]</option>
									{/if}
								{/section}
							</select>
						</div>
						<div class="form-group">
							<label>Nama Anggota</label>
							<select name="anggota_id" class="required" style="display: block; width: 270px; height: 34px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
								<option value="">- Pilih Komisi Anggota -</option>
								{section name=dataAnggota loop=$dataAnggota}
									{if $dataAnggota[dataAnggota].anggota_id == $anggota_id}
										<option value="{$dataAnggota[dataAnggota].anggota_id}" SELECTED>{$dataAnggota[dataAnggota].full_name}</option>
									{else}
										<option value="{$dataAnggota[dataAnggota].anggota_id}">{$dataAnggota[dataAnggota].full_name}</option>
									{/if}
								{/section}
							</select>
						</div>
						<div class="form-group">
							<label>Jabatan</label>
							<input type="text" value="{$jabatan}" name="jabatan" placeholder="Ex: Ketua" class="required" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
						</div>
						<button type="submit" class="btn btn-primary">Simpan</button>
					</form>
				
				{elseif $module == 'anggota' AND $act == 'detail'}
					<div class="form-group">
						<label>Periode / Komisi</label>
						<p>{$nama_periode} [{$nama_komisi}]</p>
					</div>
					
					<a href="anggota_komisi.php"><button class="btn btn-primary" type="button">Kembali</button></a>
					<a href="anggota_komisi.php?module=anggota&act=add&komisi_id={$komisi_id}"><button class="btn btn-primary" type="button">Tambah Komisi Anggota</button></a>
					<a href="print_komisi_anggota.php?module=anggota&act=print&komisi_id={$komisi_id}" target="_blank"><button class="btn btn-primary" type="button">Cetak</button></a>
					<h3>Anggota Komisi</h3>
					<div class="table-responsive">
						<table class="table table-bordered table-hover tablesorter">
							<thead>
								<tr>
									<th>No. <i class="fa fa-sort"></i></th>
									<th>Nama Anggota <i class="fa fa-sort"></i></th>
									<th>Jabatan <i class="fa fa-sort"></i></th>
									<th>Aksi <i class="fa fa-sort"></i></th>
								</tr>
							</thead>
							<tbody>
								{section name=dataAnggota loop=$dataAnggota}
								<tr>
									<td>{$dataAnggota[dataAnggota].no}</td>
									<td>{$dataAnggota[dataAnggota].full_name}</td>
									<td>{$dataAnggota[dataAnggota].jabatan}</td>
									<td>
										<a href="anggota_komisi.php?module=anggota&act=delete&komisi_anggota_id={$dataAnggota[dataAnggota].komisi_anggota_id}&komisi_id={$komisi_id}" onclick="return confirm('Anda Yakin ingin menghapus {$dataAnggota[dataAnggota].full_name}?');"><button type="button" class="btn btn-danger">Hapus</button></a>
									</td>
								</tr>
								{/section}
							</tbody>
						</table>
					</div>
									
				{else}
			
					<h3>Manajemen Komisi</h3>
					<div class="table-responsive">
						<table class="table table-bordered table-hover tablesorter">
							<thead>
								<tr>
									<th>No. <i class="fa fa-sort"></i></th>
									<th>Periode Komisi <i class="fa fa-sort"></i></th>
									<th>Nama Komisi <i class="fa fa-sort"></i></th>
									<th>Total Anggota <i class="fa fa-sort"></i></th>
									<th>Aksi <i class="fa fa-sort"></i></th>
								</tr>
							</thead>
							<tbody>
								{section name=dataKomisi loop=$dataKomisi}
								<tr>
									<td>{$dataKomisi[dataKomisi].no}</td>
									<td>{$dataKomisi[dataKomisi].nama_periode}</td>
									<td>{$dataKomisi[dataKomisi].nama_komisi}</td>
									<td>{$dataKomisi[dataKomisi].total}</td>
									<td>
										<a href="anggota_komisi.php?module=anggota&act=detail&komisi_id={$dataKomisi[dataKomisi].komisi_id}"><button type="button" class="btn btn-success">Detail</button></a>
									</td>
								</tr>
								{/section}
							</tbody>
						</table>
					</div>
					<div id="paging">{$pageLink}</div>
				{/if}
			</div>
		</div>
	</div>
</div><!-- /#wrapper -->

{include file="footer.tpl"}