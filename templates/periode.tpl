{include file="header.tpl"}

<div id="wrapper">
	
	{include file="leftMenu.tpl"}
	
	<div id="page-wrapper">
		<div class="row">
			<div class="col-lg-12">
				<ol class="breadcrumb">
					<li><a href="home.php"><i class="fa fa-dashboard"></i> Home</a></li>
					<li class="active"><i class="fa fa-caret-square-o-down"></i> Kartu Keluarga</li>
					<li class="active">Komisi</li>
				</ol>
				
				{if $code == '1'}
					<div class="alert alert-success alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						Data periode komisi berhasil disimpan.
					</div>
				{/if}
				{if $code == '2'}
					<div class="alert alert-success alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						Data periode komisi berhasil diupdate.
					</div>
				{/if}
				{if $code == '3'}
					<div class="alert alert-success alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						Data periode komisi berhasil dihapus.
					</div>
				{/if}
			</div>
		</div><!-- /.row -->
		
		<script type='text/javascript' src='js/jquery.validate.js'></script>
			
		{literal}
			<script type='text/javascript'>
				$(document).ready(function() {
					$('#frm_periode').validate({
						rules:{
							nama_periode: true,
							status: true
						},
						messages:{
							nama_periode:{
								required: "This is a required field."
							},
							status:{
								required: "This is a required field."
							}
						}
					});
				});
			</script>
		{/literal}
		
		<div class="row">
			<div class="col-lg-12">
				
				{if $module == 'periode' AND $act == 'add'}
				
					<h3>Tambah Periode Komisi</h3>
					<form role="form" method="POST" action="periode.php?module=periode&act=input" id="frm_periode">
						<div class="form-group">
							<label>Periode Komisi</label>
							<input type="text" name="nama_periode" placeholder="Ex: 2013-2014" class="required" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
						</div>
						<div class="form-group">
							<label>Status</label>
							<select name="status" class="required" style="display: block; width: 270px; height: 34px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
								<option value="">- Pilih Status -</option>
								<option value="Y" SELECTED>Aktif</option>
								<option value="N">Tidak Aktif</option>
							</select>
						</div>
						<button type="submit" class="btn btn-primary">Simpan</button>
					</form>
				
				{elseif $module == 'periode' AND $act == 'edit'}
				
					<h3>Ubah Periode Komisi</h3>
					<form role="form" method="POST" action="periode.php?module=periode&act=update" id="frm_periode">
						<input type="hidden" name="periodeID" value="{$periodeID}">
						<div class="form-group">
							<label>Periode Komisi</label>
							<input type="text" name="nama_periode" value="{$nama_periode}" placeholder="Ex: 2013-2014" class="required" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
						</div>
						<div class="form-group">
							<label>Status</label>
							<select name="status" class="required" style="display: block; width: 270px; height: 34px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
								<option value="">- Pilih Status -</option>
								<option value="Y" {if $status == 'Y'} SELECTED {/if}>Aktif</option>
								<option value="N" {if $status == 'N'} SELECTED {/if}>Tidak Aktif</option>
							</select>
						</div>
						<button type="submit" class="btn btn-primary">Simpan</button>
					</form>
					
				{else}
			
					<a href="periode.php?module=periode&act=add"><button class="btn btn-primary" type="button">Tambah Periode Komisi</button></a>
					<h3>Manajemen Periode Komisi</h3>
					<div class="table-responsive">
						<table class="table table-bordered table-hover tablesorter">
							<thead>
								<tr>
									<th>No. <i class="fa fa-sort"></i></th>
									<th>Periode Komisi <i class="fa fa-sort"></i></th>
									<th>Status <i class="fa fa-sort"></i></th>
									<th>Aksi <i class="fa fa-sort"></i></th>
								</tr>
							</thead>
							<tbody>
								{section name=dataPeriode loop=$dataPeriode}
								<tr>
									<td>{$dataPeriode[dataPeriode].no}</td>
									<td>{$dataPeriode[dataPeriode].nama_periode}</td>
									<td>{$dataPeriode[dataPeriode].status}</td>
									<td>
										<a href="periode.php?module=periode&act=edit&periodeID={$dataPeriode[dataPeriode].periodeID}"><button type="button" class="btn btn-success">Edit</button></a>
										<a href="periode.php?module=periode&act=delete&periodeID={$dataPeriode[dataPeriode].periodeID}" onclick="return confirm('Anda Yakin ingin menghapus {$dataPeriode[dataPeriode].nama_periode}?');"><button type="button" class="btn btn-danger">Hapus</button></a>
									</td>
								</tr>
								{/section}
							</tbody>
						</table>
					</div>
					<div id="paging">{$pageLink}</div>
				{/if}
			</div>
		</div>
	</div>
</div><!-- /#wrapper -->

{include file="footer.tpl"}