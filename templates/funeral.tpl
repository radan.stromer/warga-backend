{include file="header.tpl"}

<div id="wrapper">
	
	{include file="leftMenu.tpl"}
	
	<div id="page-wrapper">
		<div class="row">
			<div class="col-lg-12">
				<ol class="breadcrumb">
					<li><a href="home.php"><i class="fa fa-dashboard"></i> Home</a></li>
					<li class="active"><i class="fa fa-caret-square-o-down"></i> Master Data</li>
					<li class="active">Pemakaman</li>
				</ol>
				
				{if $code == '1'}
					<div class="alert alert-success alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						Data pemakaman berhasil disimpan.
					</div>
				{/if}
				{if $code == '2'}
					<div class="alert alert-success alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						Data pemakaman berhasil diupdate.
					</div>
				{/if}
				{if $code == '3'}
					<div class="alert alert-success alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						Data pemakaman berhasil dihapus.
					</div>
				{/if}
			</div>
		</div><!-- /.row -->
		
		<script type='text/javascript' src='js/jquery.validate.js'></script>
			
		{literal}
			<script type='text/javascript'>
				$(document).ready(function() {
					$('#frm_funeral').validate({
						rules:{
							funeralName: true,
							funeralAddress: true,
							status: true
						},
						messages:{
							funeralName:{
								required: "This is a required field."
							},
							funeralAddress:{
								required: "This is a required field."
							},
							status:{
								required: "This is a required field."
							}
						}
					});
				});
			</script>
		{/literal}
		
		<div class="row">
			<div class="col-lg-12">
				
				{if $module == 'funeral' AND $act == 'add'}
				
					<h3>Tambah Pemakaman</h3>
					<form role="form" method="POST" action="funeral.php?module=funeral&act=input" id="frm_funeral">
						<div class="form-group">
							<label>Nama Pemakaman</label>
							<input type="text" name="funeralName" class="required" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
						</div>
						<div class="form-group">
							<label>Alamat</label>
							<input type="text" name="funeralAddress" class="required" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
						</div>
						<div class="form-group">
							<label>Status</label>
							<select name="status" class="required" style="display: block; width: 270px; height: 34px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
								<option value="">- Pilih Status -</option>
								<option value="Y" SELECTED>Aktif</option>
								<option value="N">Tidak Aktif</option>
							</select>
						</div>
						<button type="submit" class="btn btn-primary">Simpan</button>
					</form>
				
				{elseif $module == 'funeral' AND $act == 'edit'}
				
					<h3>Ubah Pemakaman</h3>
					<form role="form" method="POST" action="funeral.php?module=funeral&act=update" id="frm_funeral">
						<input type="hidden" name="funeralID" value="{$funeralID}">
						<div class="form-group">
							<label>Nama Pemakaman</label>
							<input type="text" name="funeralName" value="{$funeralName}" class="required" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
						</div>
						<div class="form-group">
							<label>Alamat</label>
							<input type="text" name="funeralAddress" value="{$funeralAddress}" class="required" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
						</div>
						<div class="form-group">
							<label>Status</label>
							<select name="status" class="required" style="display: block; width: 270px; height: 34px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
								<option value="">- Pilih Status -</option>
								<option value="Y" {if $status == 'Y'} SELECTED {/if}>Aktif</option>
								<option value="N" {if $status == 'N'} SELECTED {/if}>Tidak Aktif</option>
							</select>
						</div>
						<button type="submit" class="btn btn-primary">Simpan</button>
					</form>
					
				{else}
			
					<a href="funeral.php?module=funeral&act=add"><button class="btn btn-primary" type="button">Tambah Pemakaman</button></a>
					<h3>Manajemen Pemakaman</h3>
					<div class="table-responsive">
						<table class="table table-bordered table-hover tablesorter">
							<thead>
								<tr>
									<th>No. <i class="fa fa-sort"></i></th>
									<th>Nama Pemakaman <i class="fa fa-sort"></i></th>
									<th>Alamat <i class="fa fa-sort"></i></th>
									<th>Status <i class="fa fa-sort"></i></th>
									<th>Aksi <i class="fa fa-sort"></i></th>
								</tr>
							</thead>
							<tbody>
								{section name=dataFuneral loop=$dataFuneral}
								<tr>
									<td>{$dataFuneral[dataFuneral].no}</td>
									<td>{$dataFuneral[dataFuneral].funeralName}</td>
									<td>{$dataFuneral[dataFuneral].funeralAddress}</td>
									<td>{$dataFuneral[dataFuneral].status}</td>
									<td>
										<a href="funeral.php?module=funeral&act=edit&funeralID={$dataFuneral[dataFuneral].funeralID}"><button type="button" class="btn btn-success">Edit</button></a>
										<a href="funeral.php?module=funeral&act=delete&funeralID={$dataFuneral[dataFuneral].funeralID}" onclick="return confirm('Anda Yakin ingin menghapus {$dataFuneral[dataFuneral].funeralName}?');"><button type="button" class="btn btn-danger">Hapus</button></a>
									</td>
								</tr>
								{/section}
							</tbody>
						</table>
					</div>
					<div id="paging">{$pageLink}</div>
				{/if}
			</div>
		</div>
	</div>
</div><!-- /#wrapper -->

{include file="footer.tpl"}