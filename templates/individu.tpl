{include file="header.tpl"}

<div id="wrapper">
	
	{include file="leftMenu.tpl"}
	
	<div id="page-wrapper">
		<div class="row">
			<div class="col-lg-12">
				<ol class="breadcrumb">
					<li><a href="home.php"><i class="fa fa-dashboard"></i> Home</a></li>
					<li class="active"><i class="fa fa-caret-square-o-down"></i> Keluarga Jemaat</li>
					<li class="active">Individu</li>
				</ol>
				
				{if $code == '1'}
					<div class="alert alert-success alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						Data individu berhasil disimpan.
					</div>
				{/if}
				{if $code == '2'}
					<div class="alert alert-success alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						Data individu berhasil diupdate.
					</div>
				{/if}
				{if $code == '3'}
					<div class="alert alert-success alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						Data individu berhasil dihapus.
					</div>
				{/if}
			</div>
		</div><!-- /.row -->
		
		<script type='text/javascript' src='js/jquery.validate.js'></script>
		
		{if $act eq 'add' || $act eq 'edit'}
			<script type="text/javascript" src="js/ajaxupload.3.5.js" ></script>
			<link rel="stylesheet" type="text/css" href="css/Ajaxfile-upload.css" />
					
			{literal}
				<script type='text/javascript'>
					$(function(){
						var btnUpload=$('#me');
						var mestatus=$('#mestatus');
						var files=$('#files');
						new AjaxUpload(btnUpload, {
							action: 'upload_photo.php',
							name: 'uploadfile',
							onSubmit: function(file, ext){
								 if (! (ext && /^(jpg|jpeg)$/.test(ext))){ 
				                    // extension is not allowed 
									mestatus.text('Only JPG files are allowed');
									return false;
								}
								mestatus.html('<img src="images/ajax-loader.gif" height="16" width="16">');
							},
							onComplete: function(file, response){
								//On completion clear the status
								mestatus.text('');
								//On completion clear the status
								files.html('');
								//Add uploaded file to list
								if(response!=="error"){
									$('<li></li>').appendTo('#files').html('<img src="images/photo_individu/'+response+'" alt="" width="100" /><br />').addClass('success');
									$('<li></li>').appendTo('#photo').html('<input type="hidden" name="filename" value="'+response+'">').addClass('nameupload');
									
								} else{
									$('<li></li>').appendTo('#files').text(file).addClass('error');
								}
							}
						});
						
					});
				</script>
			{/literal}
		{/if}
		
		{literal}
			<script>
				$(document).ready(function() {
					$('#frm_individu').validate({
						rules:{
							full_name: true,
							nick_name: true,
							gender: true,
							blood_type: true,
							place_of_birth: true,
							date_of_birth: true,
							religion: true,
							status: true,
							status_nikah: true
						},
						messages:{
							full_name:{
								required: "This is a required field."
							},
							nick_name:{
								required: "This is a required field."
							},
							gender:{
								required: "This is a required field."
							},
							blood_type:{
								required: "This is a required field."
							},
							place_of_birth:{
								required: "This is a required field."
							},
							date_of_birth:{
								required: "This is a required field."
							},
							religion:{
								required: "This is a required field."
							},
							status:{
								required: "This is a required field."
							},
							status_nikah:{
								required: "This is a required field."
							}
						}
					});
					
					$(".tab_content").hide();
					$(".tab_content:first").show();
					
					$("ul.tabs li").click(function() {
						$("ul.tabs li").removeClass("active");
						$(this).addClass("active");
						$(".tab_content").hide();
						var activeTab = $(this).attr("rel");
						$("#"+activeTab).fadeIn();
					});
					
					$( "#datepicker" ).datepicker({
						changeMonth: true,
						changeYear: true,
						dateFormat: "yy-mm-dd",
						yearRange: 'c-100:c-0'
					});
					
					$( "#datepicker1" ).datepicker({
						changeMonth: true,
						changeYear: true,
						dateFormat: "yy-mm-dd",
						yearRange: 'c-100:c-0'
					});
					
					$( "#datepicker2" ).datepicker({
						changeMonth: true,
						changeYear: true,
						dateFormat: "yy-mm-dd",
						yearRange: 'c-100:c-0'
					});
					
					$( "#datepicker3" ).datepicker({
						changeMonth: true,
						changeYear: true,
						dateFormat: "yy-mm-dd",
						yearRange: 'c-100:c-0'
					});
					
					$( "#datepicker4" ).datepicker({
						changeMonth: true,
						changeYear: true,
						dateFormat: "yy-mm-dd",
						yearRange: 'c-100:c-0'
					});
				});
			</script>
		{/literal}
		
		<style>
			ul.tabs {
				margin: 0;
				padding: 0;
				float: left;
				list-style: none;
				height: 32px;
				border-bottom: 1px solid #999999;
				border-left: 1px solid #999999;
				width: 100%;
			}
			ul.tabs li {
				float: left;
				margin: 0;
				cursor: pointer;
				padding: 0px 21px ;
				height: 31px;
				line-height: 31px;
				border: 1px solid #999999;
				border-left: none;
				font-weight: bold;
				background: #EEEEEE;
				overflow: hidden;
				position: relative;
			}
			ul.tabs li:hover {
				background: #CCCCCC;
			}	
			ul.tabs li.active{
				background: #FFFFFF;
				border-bottom: 1px solid #FFFFFF;
			}
			.tab_container {
				border: 1px solid #999999;
				border-top: none;
				clear: both;
				float: left; 
				width: 100%;
				background: #FFFFFF;
			}
			.tab_content {
				padding: 20px;
				display: none;
			}
		</style>
		
		<div class="row">
			<div class="col-lg-12">
			
				{if $module == 'individu' AND $act == 'add'}
				
					<h3>Tambah Individu</h3>
					<form role="form" method="POST" action="individu.php?module=individu&act=input" id="frm_individu">
					<p><button type="submit" class="btn btn-primary">Simpan</button> <button type="reset" class="btn btn-warning">Reset</button></p>
						<div id="container">
							<ul class="tabs">
								<li class="active" rel="tab1"> Data Pribadi</li>
								<li rel="tab2"> Alamat & Telepon Selular</li>
								<li rel="tab3"> Kewargaan</li>
								<li rel="tab4"> Pendidikan & Pekerjaan</li>
								<li rel="tab5"> Pernikahan</li>
							</ul>
							
							<div class="tab_container">
								<div id="tab1" class="tab_content">
									 <p>
									 	<div class="form-group">
											<label>No Induk</label>
											<input type="hidden" name="no_induk" value="{$sortno}" class="required" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
											<input type="text" name="no_induk" value="{$sortno}" class="required" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;" DISABLED>
										</div>
									 	<div class="form-group">
											<label>Nama Lengkap (Lengkap dengan Gelar)</label>
											<input type="text" name="full_name" class="required" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
										</div>
										<div class="form-group">
											<label>Nama Panggil</label>
											<input type="text" name="nick_name" class="required" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
										</div>
										<div class="form-group">
											<label>Foto</label>
											<div id="me" class="styleall" style="cursor:pointer;">
												<label>
													<button class="btn btn-primary">Browse</button>
												</label>
											</div>
											<span id="mestatus" ></span>
											<div id="photo">
												<li class="nameupload"></li>
											</div>
											<div id="files">
												<li class="success"></li>
											</div>
										</div>
										<div class="form-group">
											<label>Jenis Kelamin</label>
											<select name="gender" class="required" style="display: block; width: 270px; height: 34px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
												<option value="">- Pilih Jenis Kelamin -</option>
												<option value="L">Laki-laki</option>
												<option value="P">Perempuan</option>
											</select>
										</div>
										<div class="form-group">
											<label>Golongan Darah</label>
											<select name="blood_type" class="required" style="display: block; width: 270px; height: 34px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
												<option value="">- Pilih Golongan Darah -</option>
												<option value="A">A</option>
												<option value="B">B</option>
												<option value="AB">AB</option>
												<option value="O">O</option>
											</select>
										</div>
										<div class="form-group">
											<label>Tempat Lahir</label>
											<input type="text" name="place_of_birth" class="required" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
										</div>
										<div class="form-group">
											<label>Tanggal Lahir</label>
											<input type="text" name="date_of_birth" class="required" id="datepicker" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
										</div>
										<div class="form-group">
											<label>Wafat?</label>
											<select name="death_status" style="display: block; width: 270px; height: 34px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
												<option value="">- Pilih Status Wafat -</option>
												<option value="Y">Ya</option>
												<option value="N" SELECTED>Tidak</option>
											</select>
										</div>
										<div class="form-group">
											<label>Tanggal Wafat</label>
											<input type="text" name="death_date" id="datepicker1" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
										</div>
										<div class="form-group">
											<label>Tempat Pemakaman</label>
											<select name="funeral_id" style="display: block; width: 270px; height: 34px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
												<option value="">- Pilih Tempat Pemakaman -</option>
												{section name=dataFuneral loop=$dataFuneral}
													<option value="{$dataFuneral[dataFuneral].funeral_id}">{$dataFuneral[dataFuneral].funeral_name}</option>
												{/section}
											</select>
										</div>
										<div class="form-group">
											<label>Agama</label>
											<select name="religion" class="required" style="display: block; width: 270px; height: 34px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
												<option value="">- Pilih Agama -</option>
												<option value="1">Islam</option>
												<option value="2">Kristen</option>
												<option value="3">Katolik</option>
												<option value="4">Hindu</option>
												<option value="5">Budha</option>
												<option value="6">Kong Hu Chu</option>
												<option value="7">Lain-Lain</option>
											</select>
										</div>
										<div class="form-group">
											<label>Cacat?</label>
											<select name="disability" style="display: block; width: 270px; height: 34px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
												<option value="">- Pilih Status Cacat -</option>
												<option value="Y">Ya</option>
												<option value="N" SELECTED>Tidak</option>
											</select>
										</div>
										<div class="form-group">
											<label>Ayah</label>
											<input type="text" name="father_id" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
											<!--<select name="father_id" style="display: block; width: 270px; height: 34px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
												<option value="">- Pilih Nama Ayah -</option>
												{section name=dataFather loop=$dataFather}
													<option value="{$dataFather[dataFather].father_id}">{$dataFather[dataFather].full_name}</option>
												{/section}
											</select>-->
										</div>
										<div class="form-group">
											<label>Ibu</label>
											<input type="text" name="mother_id" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
											<!--<select name="mother_id" style="display: block; width: 270px; height: 34px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
												<option value="">- Pilih Nama Ibu -</option>
												{section name=dataMother loop=$dataMother}
													<option value="{$dataMother[dataMother].mother_id}">{$dataMother[dataMother].full_name}</option>
												{/section}
											</select>-->
										</div>
									 </p>
								</div><!-- #tab1 -->
								
								<div id="tab2" class="tab_content">
									<p>
										<div class="form-group">
											<label>Alamat</label>
											<textarea name="address" style="display: block; width: 370px; height: 80px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;"></textarea>
										</div>
										<div class="form-group">
											<label>Telepon</label>
											<input type="text" name="telepon" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
										</div>
										<div class="form-group">
											<label>No. HP</label>
											<input type="text" name="hp" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
										</div>
										<div class="form-group">
											<label>Email</label>
											<input type="text" name="email" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
										</div>
									</p>
								</div><!-- #tab2 -->
								
								<div id="tab3" class="tab_content">
									<p>
										<div class="form-group">
											<label>Kewarganegaraan?</label>
											<select name="status" class="required" style="display: block; width: 270px; height: 34px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
												<option value="">- Pilih Status Warga -</option>
												<option value="Y" SELECTED>WNI</option>
												<option value="N">WNA</option>
											</select>
										</div>
										<div class="form-group">
											<label>Negara Asal</label>
											<input type="text" name="negara" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
										</div>
									</p>
								</div><!-- #tab3 -->
								
								<div id="tab4" class="tab_content">
									<p>
										<div class="form-group">
											<label>Pendidikan Terakhir</label>
											<select name="pendidikan_terakhir" style="display: block; width: 270px; height: 34px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
												<option value="">- Pilih Pendidikan Terakhir -</option>
												<option value="1">S3</option>
												<option value="2">S2</option>
												<option value="3">S1</option>
												<option value="4">D4</option>
												<option value="5">D3</option>
												<option value="6">D2</option>
												<option value="7">D1</option>
												<option value="8">SMA/SMK</option>
												<option value="9">SMP</option>
												<option value="10">SD</option>
												<option value="11">TK</option>
											</select>
										</div>
										<div class="form-group">
											<label>Nama Lembaga / Pendidikan</label>
											<input type="text" name="nama_lembaga" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
										</div>
										<div class="form-group">
											<label>Pekerjaan Utama</label>
											<select name="job_id" style="display: block; width: 270px; height: 34px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
												<option value="">- Pilih Pekerjaan Utama -</option>
												{section name=dataJob loop=$dataJob}
													<option value="{$dataJob[dataJob].job_id}">{$dataJob[dataJob].job_name}</option>
												{/section}
											</select>
										</div>
										<div class="form-group">
											<label>Pekerjaan Sampingan</label>
											<input type="text" name="side_job" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
										</div>
										<div class="form-group">
											<label>Penghasilan Bulanan</label>
											<select name="grade_id" style="display: block; width: 270px; height: 34px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
												<option value="">- Pilih Grade Penghasilan -</option>
												{section name=dataGrade loop=$dataGrade}
													<option value="{$dataGrade[dataGrade].grade_id}">{$dataGrade[dataGrade].grade_name}</option>
												{/section}
											</select>
										</div>
										<div class="form-group">
											<label>Minat / Hobi</label>
											<input type="text" name="hobi" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
										</div>
										<div class="form-group">
											<label>Bakat</label>
											<input type="text" name="bakat" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
										</div>
									</p>
								</div><!-- #tab4 -->
								
								<div id="tab5" class="tab_content">
									<p>
										<div class="form-group">
											<label>Pasangan</label>
											<input type="text" name="pasangan_id" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
											<!--<select name="pasangan_id" style="display: block; width: 270px; height: 34px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
												<option value="">- Pilih Nama Pasangan -</option>
												{section name=dataPasangan loop=$dataPasangan}
													<option value="{$dataPasangan[dataPasangan].pasangan_id}">{$dataPasangan[dataPasangan].full_name}</option>
												{/section}
											</select>-->
										</div>
										<div class="form-group">
											<label>Tanggal Pernikahan</label>
											<input type="text" name="tanggal_nikah" id="datepicker4" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
										</div>
										<div class="form-group">
											<label>Status Pernikahan</label>
											<select name="status_nikah" class="required" style="display: block; width: 270px; height: 34px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
												<option value="">- Pilih Status Pernikahan -</option>
												<option value="1">Belum Menikah</option>
												<option value="2">Cerai</option>
												<option value="3">Menikah</option>
												<option value="4">Wafat</option>
											</select>
										</div>
									</p>
								</div>
							</div> <!-- .tab_container -->
						</div> <!-- #container -->
					</form>
				
				{elseif $module == 'individu' AND $act == 'view'}
				
					<h3>Detail Individu</h3>
					<p>	<a href="javascript:history.go(-1)"><button type="button" class="btn btn-primary">Kembali</button></a>
						<a href="print_individu.php?module=individu&act=cetak&individuID={$individu_id}" target="_blank"><button type="button" class="btn btn-primary">Cetak</button></a></p>
						<div id="container">
							<ul class="tabs">
								<li class="active" rel="tab1"> Data Pribadi</li>
								<li rel="tab2"> Alamat & Telepon Selular</li>
								<li rel="tab3"> Kewargaan</li>
								<li rel="tab4"> Pendidikan & Pekerjaan</li>
								<li rel="tab5"> Pernikahan</li>
							</ul>
							
							<div class="tab_container">
								<div id="tab1" class="tab_content">
									 <p>
									 	<div class="form-group">
											<label>No Induk</label>
											<p>{$no_induk}</p>
										</div>
									 	<div class="form-group">
											<label>Nama Lengkap (Lengkap dengan Gelar)</label>
											<p>{$full_name}</p>
										</div>
										<div class="form-group">
											<label>Nama Panggil</label>
											<p>{$nick_name}</p>
										</div>
										<div class="form-group">
											<label>Foto</label>
											<p>
												{if $photo != ''}
													<a href="images/photo_individu/{$photo}" target="_blank"><img src="images/photo_individu/{$photo}" width="100"></a>
												{/if}
											</p>
										</div>
										<div class="form-group">
											<label>Jenis Kelamin</label>
											<p>{$gender}</p>
										</div>
										<div class="form-group">
											<label>Golongan Darah</label>
											<p>{$blood_type}</p>
										</div>
										<div class="form-group">
											<label>Tempat Lahir</label>
											<p>{$place_of_birth}</p>
										</div>
										<div class="form-group">
											<label>Tanggal Lahir</label>
											<p>{$date_of_birth}</p>
										</div>
										<div class="form-group">
											<label>Wafat?</label>
											<p>{$death_status}</p>
										</div>
										<div class="form-group">
											<label>Tanggal Wafat</label>
											<p>{$death_date}</p>
										</div>
										<div class="form-group">
											<label>Tempat Pemakaman</label>
											<p>{$funeral_name}</p>
										</div>
										<div class="form-group">
											<label>Agama</label>
											<p>	{if $religion == '1'} Islam {/if}
												{if $religion == '2'} Kristen {/if}
												{if $religion == '3'} Katolik {/if}
												{if $religion == '4'} Hindu {/if}
												{if $religion == '5'} Budha {/if}
												{if $religion == '6'} Kong Hu Chu {/if}
												{if $religion == '7'} Lain-lain {/if}
											</p>
										</div>
										<div class="form-group">
											<label>Cacat?</label>
											<p>{$disability}</p>
										</div>
										<div class="form-group">
											<label>Ayah</label>
											<p>{$father_name}</p>
										</div>
										<div class="form-group">
											<label>Ibu</label>
											<p>{$mother_name}</p>
										</div>
									 </p>
								</div><!-- #tab1 -->
								
								<div id="tab2" class="tab_content">
									<p>
										<div class="form-group">
											<label>Alamat</label>
											<p>{$address}</p>
										</div>
										<div class="form-group">
											<label>Telepon</label>
											<p>{$telepon}</p>
										</div>
										<div class="form-group">
											<label>No. HP</label>
											<p>{$hp}</p>
										</div>
										<div class="form-group">
											<label>Email</label>
											<p>{$email}</p>
										</div>
									</p>
								</div><!-- #tab2 -->
								
								<div id="tab3" class="tab_content">
									<p>
										<div class="form-group">
											<label>Kewarganegaraan</label>
											<p>{$status}</p>
										</div>
										<div class="form-group">
											<label>Negara</label>
											<p>{$negara}</p>
										</div>
									</p>
								</div><!-- #tab3 -->
								
								<div id="tab4" class="tab_content">
									<p>
										<div class="form-group">
											<label>Pendidikan Terakhir</label>
											<p>	{if $pendidikan_terakhir == '1'} S3 {/if}
												{if $pendidikan_terakhir == '2'} S2 {/if}
												{if $pendidikan_terakhir == '3'} S1 {/if}
												{if $pendidikan_terakhir == '4'} D4 {/if}
												{if $pendidikan_terakhir == '5'} D3 {/if}
												{if $pendidikan_terakhir == '6'} D2 {/if}
												{if $pendidikan_terakhir == '7'} D1 {/if}
												{if $pendidikan_terakhir == '8'} SMA/SMK {/if}
												{if $pendidikan_terakhir == '9'} SMP {/if}
												{if $pendidikan_terakhir == '10'} SD {/if}
												{if $pendidikan_terakhir == '11'} TK {/if}
											</p>
										</div>
										<div class="form-group">
											<label>Nama Lembaga / Pendidikan</label>
											<p>{$nama_lembaga}</p>
										</div>
										<div class="form-group">
											<label>Pekerjaan Utama</label>
											<p>{$job_name}</p>
										</div>
										<div class="form-group">
											<label>Pekerjaan Sampingan</label>
											<p>{$side_job}</p>
										</div>
										<div class="form-group">
											<label>Penghasilan Bulanan</label>
											<p>{$grade_name}</p>
										</div>
										<div class="form-group">
											<label>Minat / Hobi</label>
											<p>{$hobi}</p>
										</div>
										<div class="form-group">
											<label>Bakat</label>
											<p>{$bakat}</p>
										</div>
									</p>
								</div><!-- #tab4 -->
								
								<div id="tab5" class="tab_content">
									<p>
										<div class="form-group">
											<label>Pasangan</label>
											<p>{$pasangan_name}</p>
										<div class="form-group">
											<label>Tanggal Pernikahan</label>
											<p>{$tanggal_nikah}</p>
										</div>
										<div class="form-group">
											<label>Status Pernikahan</label>
											<p>
												{if $status_nikah == '1'} Belum Menikah {/if}
												{if $status_nikah == '2'} Cerai {/if}
												{if $status_nikah == '3'} Menikah {/if}
												{if $status_nikah == '4'} Wafat {/if}
											</p>
										</div>
									</p>
								</div>
							</div> <!-- .tab_container -->
						</div> <!-- #container -->
					</form>
				
				{elseif $module == 'individu' AND $act == 'edit'}
				
					<h3>Ubah Data Individu</h3>
					<form role="form" method="POST" action="individu.php?module=individu&act=update" id="frm_individu">
					<input type="hidden" name="keyword" value="{$keyword}">
					<input type="hidden" name="individu_id" value="{$individu_id}">
					<p><button type="submit" class="btn btn-primary">Simpan</button> <button type="reset" class="btn btn-warning">Reset</button></p>
						<div id="container">
							<ul class="tabs">
								<li class="active" rel="tab1"> Data Pribadi</li>
								<li rel="tab2"> Alamat & Telepon Selular</li>
								<li rel="tab3"> Kewargaan</li>
								<li rel="tab4"> Pendidikan & Pekerjaan</li>
								<li rel="tab5"> Pernikahan</li>
							</ul>
							
							<div class="tab_container">
								<div id="tab1" class="tab_content">
									 <p>
									 	<div class="form-group">
											<label>No Induk</label>
											<input type="text" value="{$no_induk}" name="no_induk" class="required" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;" DISABLED>
										</div>
									 	<div class="form-group">
											<label>Nama Lengkap (Lengkap dengan Gelar)</label>
											<input type="text" value="{$full_name}" name="full_name" class="required" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
										</div>
										<div class="form-group">
											<label>Nama Panggil</label>
											<input type="text" value="{$nick_name}" name="nick_name" class="required" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
										</div>
										<div class="form-group">
											<label>Foto</label>
											<div id="me" class="styleall" style="cursor:pointer;">
												<label>
													<button class="btn btn-primary">Browse</button>
												</label>
											</div>
											<span id="mestatus" ></span>
											<div id="photo">
												<li class="nameupload"></li>
											</div>
											<div id="files">
												<li class="success">
													{if $photo != ''}
														<img src="images/photo_individu/{$photo}" width="100">
													{/if}
												</li>
											</div>
										</div>
										<div class="form-group">
											<label>Jenis Kelamin</label>
											<select name="gender" class="required" style="display: block; width: 270px; height: 34px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
												<option value="">- Pilih Jenis Kelamin -</option>
												<option value="L" {if $gender == 'L'} SELECTED {/if}>Laki-laki</option>
												<option value="P" {if $gender == 'P'} SELECTED {/if}>Perempuan</option>
											</select>
										</div>
										<div class="form-group">
											<label>Golongan Darah</label>
											<select name="blood_type" class="required" style="display: block; width: 270px; height: 34px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
												<option value="">- Pilih Golongan Darah -</option>
												<option value="A" {if $blood_type == 'A'} SELECTED {/if}>A</option>
												<option value="B" {if $blood_type == 'B'} SELECTED {/if}>B</option>
												<option value="AB" {if $blood_type == 'AB'} SELECTED {/if}>AB</option>
												<option value="O" {if $blood_type == 'O'} SELECTED {/if}>O</option>
											</select>
										</div>
										<div class="form-group">
											<label>Tempat Lahir</label>
											<input type="text" value="{$place_of_birth}" name="place_of_birth" class="required" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
										</div>
										<div class="form-group">
											<label>Tanggal Lahir</label>
											<input type="text" value="{$date_of_birth}" name="date_of_birth" class="required" id="datepicker" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
										</div>
										<div class="form-group">
											<label>Wafat?</label>
											<select name="death_status" style="display: block; width: 270px; height: 34px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
												<option value="">- Pilih Status Wafat -</option>
												<option value="Y" {if $death_status == 'Y'} SELECTED {/if}>Ya</option>
												<option value="N" {if $death_status == 'N'} SELECTED {/if}>Tidak</option>
											</select>
										</div>
										<div class="form-group">
											<label>Tanggal Wafat</label>
											<input type="text" value="{$death_date}" name="death_date" id="datepicker1" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
										</div>
										<div class="form-group">
											<label>Tempat Pemakaman</label>
											<select name="funeral_id" style="display: block; width: 270px; height: 34px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
												<option value="">- Pilih Tempat Pemakaman -</option>
												{section name=dataFuneral loop=$dataFuneral}
													{if $dataFuneral[dataFuneral].funeral_id == $funeral_id}
														<option value="{$dataFuneral[dataFuneral].funeral_id}" SELECTED>{$dataFuneral[dataFuneral].funeral_name}</option>
													{else}
														<option value="{$dataFuneral[dataFuneral].funeral_id}">{$dataFuneral[dataFuneral].funeral_name}</option>
													{/if}
												{/section}
											</select>
										</div>
										<div class="form-group">
											<label>Agama</label>
											<select name="religion" class="required" style="display: block; width: 270px; height: 34px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
												<option value="">- Pilih Agama -</option>
												<option value="1" {if $religion == '1'} SELECTED {/if}>Islam</option>
												<option value="2" {if $religion == '2'} SELECTED {/if}>Kristen</option>
												<option value="3" {if $religion == '3'} SELECTED {/if}>Katolik</option>
												<option value="4" {if $religion == '4'} SELECTED {/if}>Hindu</option>
												<option value="5" {if $religion == '5'} SELECTED {/if}>Budha</option>
												<option value="6" {if $religion == '6'} SELECTED {/if}>Kong Hu Chu</option>
												<option value="7" {if $religion == '7'} SELECTED {/if}>Lain-Lain</option>
											</select>
										</div>
										<div class="form-group">
											<label>Cacat?</label>
											<select name="disability" style="display: block; width: 270px; height: 34px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
												<option value="">- Pilih Status Cacat -</option>
												<option value="Y" {if $disability == 'Y'} SELECTED {/if}>Ya</option>
												<option value="N" {if $disability == 'N'} SELECTED {/if}>Tidak</option>
											</select>
										</div>
										<div class="form-group">
											<label>Ayah</label>
											<input type="text" value="{$father_id}" name="father_id" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
											<!--<select name="father_id" style="display: block; width: 270px; height: 34px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
												<option value="">- Pilih Nama Ayah -</option>
												{section name=dataFather loop=$dataFather}
													{if $dataFather[dataFather].father_id == $father_id}
														<option value="{$dataFather[dataFather].father_id}" SELECTED>{$dataFather[dataFather].full_name}</option>
													{else}
														<option value="{$dataFather[dataFather].father_id}">{$dataFather[dataFather].full_name}</option>
													{/if}
												{/section}
											</select>-->
										</div>
										<div class="form-group">
											<label>Ibu</label>
											<input type="text" name="mother_id" value="{$mother_id}" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
											<!--<select name="mother_id" style="display: block; width: 270px; height: 34px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
												<option value="">- Pilih Nama Ibu -</option>
												{section name=dataMother loop=$dataMother}
													{if $dataMother[dataMother].mother_id == $mother_id}
														<option value="{$dataMother[dataMother].mother_id}" SELECTED>{$dataMother[dataMother].full_name}</option>
													{else}
														<option value="{$dataMother[dataMother].mother_id}">{$dataMother[dataMother].full_name}</option>
													{/if}
												{/section}
											</select>-->
										</div>
									 </p>
								</div><!-- #tab1 -->
								
								<div id="tab2" class="tab_content">
									<p>
										<div class="form-group">
											<label>Alamat</label>
											<textarea name="address" style="display: block; width: 370px; height: 80px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">{$address}</textarea>
										</div>
										<div class="form-group">
											<label>Telepon</label>
											<input type="text" value="{$telepon}" name="telepon" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
										</div>
										<div class="form-group">
											<label>No. HP</label>
											<input type="text" value="{$hp}" name="hp" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
										</div>
										<div class="form-group">
											<label>Email</label>
											<input type="text" value="{$email}" name="email" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
										</div>
									</p>
								</div><!-- #tab2 -->
								
								<div id="tab3" class="tab_content">
									<p>
										<div class="form-group">
											<label>Kewarganegaraan</label>
											<select name="status" class="required" style="display: block; width: 270px; height: 34px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
												<option value="">- Pilih Kewarganegaraan -</option>
												<option value="Y" {if $status == 'Y'} SELECTED {/if}>WNI</option>
												<option value="N" {if $status == 'N'} SELECTED {/if}>WNA</option>
											</select>
										</div>
										<div class="form-group">
											<label>Negara</label>
											<input type="text" value="{$negara}" name="negara" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
										</div>
									</p>
								</div><!-- #tab3 -->
								
								<div id="tab4" class="tab_content">
									<p>
										<div class="form-group">
											<label>Pendidikan Terakhir</label>
											<select name="pendidikan_terakhir" style="display: block; width: 270px; height: 34px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
												<option value="">- Pilih Pendidikan Terakhir -</option>
												<option value="1" {if $pendidikan_terakhir == '1'} SELECTED {/if}>S3</option>
												<option value="2" {if $pendidikan_terakhir == '2'} SELECTED {/if}>S2</option>
												<option value="3" {if $pendidikan_terakhir == '3'} SELECTED {/if}>S1</option>
												<option value="4" {if $pendidikan_terakhir == '4'} SELECTED {/if}>D4</option>
												<option value="5" {if $pendidikan_terakhir == '5'} SELECTED {/if}>D3</option>
												<option value="6" {if $pendidikan_terakhir == '6'} SELECTED {/if}>D2</option>
												<option value="7" {if $pendidikan_terakhir == '7'} SELECTED {/if}>D1</option>
												<option value="8" {if $pendidikan_terakhir == '8'} SELECTED {/if}>SMA/SMK</option>
												<option value="9" {if $pendidikan_terakhir == '9'} SELECTED {/if}>SMP</option>
												<option value="10" {if $pendidikan_terakhir == '10'} SELECTED {/if}>SD</option>
												<option value="11" {if $pendidikan_terakhir == '11'} SELECTED {/if}>TK</option>
											</select>
										</div>
										<div class="form-group">
											<label>Nama Lembaga / Pendidikan</label>
											<input type="text" value="{$nama_lembaga}" name="nama_lembaga" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
										</div>
										<div class="form-group">
											<label>Pekerjaan Utama</label>
											<select name="job_id" style="display: block; width: 270px; height: 34px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
												<option value="">- Pilih Pekerjaan Utama -</option>
												{section name=dataJob loop=$dataJob}
													{if $dataJob[dataJob].job_id == $job_id}
														<option value="{$dataJob[dataJob].job_id}" SELECTED>{$dataJob[dataJob].job_name}</option>
													{else}
														<option value="{$dataJob[dataJob].job_id}">{$dataJob[dataJob].job_name}</option>
													{/if}
												{/section}
											</select>
										</div>
										<div class="form-group">
											<label>Pekerjaan Sampingan</label>
											<input type="text" value="{$side_job}" name="side_job" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
										</div>
										<div class="form-group">
											<label>Penghasilan Bulanan</label>
											<select name="grade_id" style="display: block; width: 270px; height: 34px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
												<option value="">- Pilih Grade Penghasilan -</option>
												{section name=dataGrade loop=$dataGrade}
													{if $dataGrade[dataGrade].grade_id == $grade_id}
														<option value="{$dataGrade[dataGrade].grade_id}" SELECTED>{$dataGrade[dataGrade].grade_name}</option>
													{else}
														<option value="{$dataGrade[dataGrade].grade_id}">{$dataGrade[dataGrade].grade_name}</option>
													{/if}
												{/section}
											</select>
										</div>
										<div class="form-group">
											<label>Minat / Hobi</label>
											<input type="text" value="{$hobi}" name="hobi" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
										</div>
										<div class="form-group">
											<label>Bakat</label>
											<input type="text" value="{$bakat}" name="bakat" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
										</div>
									</p>
								</div><!-- #tab4 -->
								
								<div id="tab5" class="tab_content">
									<p>
										<div class="form-group">
											<label>Pasangan</label>
											<input type="text" name="pasangan_id" value="{$pasangan_id}" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
											<!--<select name="pasangan_id" style="display: block; width: 270px; height: 34px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
												<option value="">- Pilih Nama Pasangan -</option>
												{section name=dataPasangan loop=$dataPasangan}
													{if $dataPasangan[dataPasangan].pasangan_id == $pasangan_id}
														<option value="{$dataPasangan[dataPasangan].pasangan_id}" SELECTED>{$dataPasangan[dataPasangan].full_name}</option>
													{else}
														<option value="{$dataPasangan[dataPasangan].pasangan_id}">{$dataPasangan[dataPasangan].full_name}</option>
													{/if}
												{/section}
											</select>-->
										</div>
										<div class="form-group">
											<label>Tanggal Pernikahan</label>
											<input type="text" value="{$tanggal_nikah}" name="tanggal_nikah" id="datepicker4" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
										</div>
										<div class="form-group">
											<label>Status Pernikahan</label>
											<select name="status_nikah" class="required" style="display: block; width: 270px; height: 34px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
												<option value="">- Pilih Status Pernikahan -</option>
												<option value="1" {if $status_nikah == '1'} SELECTED {/if}>Belum Menikah</option>
												<option value="2" {if $status_nikah == '2'} SELECTED {/if}>Cerai</option>
												<option value="3" {if $status_nikah == '3'} SELECTED {/if}>Menikah</option>
												<option value="4" {if $status_nikah == '4'} SELECTED {/if}>Wafat</option>
											</select>
										</div>
									</p>
								</div>
							</div> <!-- .tab_container -->
						</div> <!-- #container -->
					</form>
				
				{elseif $module == 'individu' AND $act == 'search'}
				
					<form role="form" method="GET" action="individu.php" id="frm_search">
						<input type="hidden" name="module" value="individu">
						<input type="hidden" name="act" value="search">
						<div class="form-group">
							<label>Masukkan Keyword (Nama)</label>
							<input type="text" value="{$keyword}" placeholder="Masukkan Nama Pencarian" name="name" class="required" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
						</div>
						<button type="submit" class="btn btn-primary">Cari</button>
					</form>
					
					<h3>Hasil Pencarian (Nama: <b>{$keyword}</b>)<br>
					Ditemukan: {$numSearch} data
					</h3>
					<div class="table-responsive">
						<table class="table table-bordered table-hover tablesorter">
							<thead>
								<tr>
									<th>No. <i class="fa fa-sort"></i></th>
									<th>No Induk <i class="fa fa-sort"></i></th>
									<th>Nama Lengkap <i class="fa fa-sort"></i></th>
									<th>Tempat Lahir <i class="fa fa-sort"></i></th>
									<th>Tanggal Lahir <i class="fa fa-sort"></i></th>
									<th>Gender <i class="fa fa-sort"></i></th>
									<th>Jemaat <i class="fa fa-sort"></i></th>
									<th>Aksi <i class="fa fa-sort"></i></th>
								</tr>
							</thead>
							<tbody>
								{section name=dataSearch loop=$dataSearch}
								<tr>
									<td>{$dataSearch[dataSearch].no}</td>
									<td>{$dataSearch[dataSearch].no_induk}</td>
									<td>{$dataSearch[dataSearch].full_name}</td>
									<td>{$dataSearch[dataSearch].place_of_birth}</td>
									<td>{$dataSearch[dataSearch].date_of_birth}</td>
									<td>{$dataSearch[dataSearch].gender}</td>
									<td>{$dataSearch[dataSearch].status}</td>
									<td>
										<a href="individu.php?module=individu&act=view&individuID={$dataSearch[dataSearch].individu_id}&keyword={$keyword}"><button type="button" class="btn btn-primary">View</button></a>
										<a href="individu.php?module=individu&act=edit&individuID={$dataSearch[dataSearch].individu_id}&keyword={$keyword}"><button type="button" class="btn btn-success">Edit</button></a>
										<a href="individu.php?module=individu&act=delete&individuID={$dataSearch[dataSearch].individu_id}&keyword={$keyword}" onclick="return confirm('Anda Yakin ingin menghapus {$dataSearch[dataSearch].full_name}?');"><button type="button" class="btn btn-danger">Hapus</button></a>
									</td>
								</tr>
								{/section}
							</tbody>
						</table>
					</div>
					<div id="paging">{$pageLink}</div>
				
				{else}
			
					<a href="individu.php?module=individu&act=add"><button class="btn btn-primary" type="button">Tambah Individu</button></a>
					
					<h2>Pencarian Individu</h2>
					<form role="form" method="GET" action="individu.php" id="frm_search">
						<input type="hidden" name="module" value="individu">
						<input type="hidden" name="act" value="search">
						<div class="form-group">
							<label>Masukkan Keyword (Nama)</label>
							<input type="text" placeholder="Masukkan Nama Pencarian" name="name" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
						</div>
						<button type="submit" class="btn btn-primary">Cari</button>
					</form>
					
				{/if}
			</div>
		</div>
	</div>
</div><!-- /#wrapper -->

{include file="footer.tpl"}