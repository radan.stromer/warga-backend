{include file="header.tpl"}

<div id="wrapper">
	
	{include file="leftMenu.tpl"}
	
	<div id="page-wrapper">
		<div class="row">
			<div class="col-lg-12">
				<ol class="breadcrumb">
					<li><a href="home.php"><i class="fa fa-dashboard"></i> Home</a></li>
					<li class="active"><i class="fa fa-caret-square-o-down"></i> Laporan</li>
					<li class="active">Data Jemaat</li>
				</ol>				
			</div>
		</div><!-- /.row -->
		
		<div class="row">
			<div class="col-lg-12">
				<a href="print_jemaat.php?module=laporan&act=print&id=10" target="_blank"><button class="btn btn-primary" type="button">Cetak Semua Data Jemaat</button></a><br><br>
				<div class="table-responsive">
					<table class="table table-bordered table-hover tablesorter">
						<thead>
							<tr>
								<th>No. <i class="fa fa-sort"></i></th>
								<th>Kategori Umur <i class="fa fa-sort"></i></th>
								<th>Rentang Umur <i class="fa fa-sort"></i></th>
								<th>Jumlah Jiwa <i class="fa fa-sort"></i></th>
								<th>Cetak <i class="fa fa-sort"></i></th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>1. </td>
								<td>Batita</td>
								<td>0 - 2</td>
								<td>{$batita}</td>
								<td><a href="print_jemaat.php?module=laporan&act=print&id=1" target="_blank"><button class="btn btn-primary" type="button">Cetak</button></a></td>
							</tr>
							<tr>
								<td>2. </td>
								<td>Balita</td>
								<td>3 - 4</td>
								<td>{$balita}</td>
								<td><a href="print_jemaat.php?module=laporan&act=print&id=2" target="_blank"><button class="btn btn-primary" type="button">Cetak</button></a></td>
							</tr>
							<tr>
								<td>3. </td>
								<td>Kanak-Kanak</td>
								<td>5 - 7</td>
								<td>{$kanak}</td>
								<td><a href="print_jemaat.php?module=laporan&act=print&id=3" target="_blank"><button class="btn btn-primary" type="button">Cetak</button></a></td>
							</tr>
							<tr>
								<td>4. </td>
								<td>Pratama</td>
								<td>8 - 11</td>
								<td>{$pratama}</td>
								<td><a href="print_jemaat.php?module=laporan&act=print&id=4" target="_blank"><button class="btn btn-primary" type="button">Cetak</button></a></td>
							</tr>
							<tr>
								<td>5. </td>
								<td>Madya</td>
								<td>12 - 14</td>
								<td>{$madya}</td>
								<td><a href="print_jemaat.php?module=laporan&act=print&id=5" target="_blank"><button class="btn btn-primary" type="button">Cetak</button></a></td>
							</tr>
							<tr>
								<td>6. </td>
								<td>Remaja</td>
								<td>15 - 16</td>
								<td>{$remaja}</td>
								<td><a href="print_jemaat.php?module=laporan&act=print&id=6" target="_blank"><button class="btn btn-primary" type="button">Cetak</button></a></td>
							</tr>
							<tr>
								<td>7. </td>
								<td>Pemuda</td>
								<td>17 - 24</td>
								<td>{$pemuda}</td>
								<td><a href="print_jemaat.php?module=laporan&act=print&id=7" target="_blank"><button class="btn btn-primary" type="button">Cetak</button></a></td>
							</tr>
							<tr>
								<td>8. </td>
								<td>Dewasa</td>
								<td>25 - 54</td>
								<td>{$dewasa}</td>
								<td><a href="print_jemaat.php?module=laporan&act=print&id=8" target="_blank"><button class="btn btn-primary" type="button">Cetak</button></a></td>
							</tr>
							<tr>
								<td>9. </td>
								<td>Lansia</td>
								<td>> 55 </td>
								<td>{$lansia}</td>
								<td><a href="print_jemaat.php?module=laporan&act=print&id=9" target="_blank"><button class="btn btn-primary" type="button">Cetak</button></a></td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div><!-- /#wrapper -->

{include file="footer.tpl"}