{include file="header.tpl"}

<div id="wrapper">
	
	{include file="leftMenu.tpl"}
	
	<div id="page-wrapper">
		<div class="row">
			<div class="col-lg-12">
				<ol class="breadcrumb">
					<li><a href="home.php"><i class="fa fa-dashboard"></i> Home</a></li>
					<li class="active"><i class="fa fa-caret-square-o-down"></i> Master Data</li>
					<li class="active">Pengguna</li>
				</ol>
				
				{if $code == '1'}
					<div class="alert alert-success alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						Data pengguna berhasil disimpan.
					</div>
				{/if}
				{if $code == '2'}
					<div class="alert alert-success alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						Data pengguna berhasil diupdate.
					</div>
				{/if}
				{if $code == '3'}
					<div class="alert alert-success alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						Data pengguna berhasil dihapus.
					</div>
				{/if}
				{if $code == '4'}
					<div class="alert alert-success alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						Reset password pengguna berhasil disimpan.
					</div>
				{/if}
			</div>
		</div><!-- /.row -->
		
		<script type='text/javascript' src='js/jquery.validate.js'></script>
			
		{literal}
			<script type='text/javascript'>
				$(document).ready(function() {
					$('#frm_user').validate({
						rules:{
							fullName: true,
							address: true,
							gender: true,
							position: true,
							handPhone: true,
							status: true,
							blocked: true,
							username: true,
							password: true
						},
						messages:{
							fullName:{
								required: "This is a required field."
							},
							address:{
								required: "This is a required field."
							},
							gender:{
								required: "This is a required field."
							},
							position:{
								required: "This is a required field."
							},
							handPhone:{
								required: "This is a required field."
							},
							status:{
								required: "This is a required field."
							},
							blocked:{
								required: "This is a required field."
							},
							username:{
								required: "This is a required field."
							},
							password:{
								required: "This is a required field."
							}
						}
					});
					
					$('#frm_reset').validate({
						rules:{
							username: true,
							password: true
						},
						messages:{
							username:{
								required: "This is a required field."
							},
							password:{
								required: "This is a required field."
							}
						}
					});
				});
			</script>
		{/literal}
		
		<div class="row">
			<div class="col-lg-12">
				
				{if $module == 'user' AND $act == 'add'}
				
					<h3>Tambah Pengguna</h3>
					<form role="form" method="POST" action="users.php?module=user&act=input" id="frm_user">
						<div class="form-group">
							<label>Nama Lengkap</label>
							<input type="text" name="fullName" class="required" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
						</div>
						<div class="form-group">
							<label>Alamat</label>
							<input type="text" name="address" class="required" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
						</div>
						<div class="form-group">
							<label>Jenis Kelamin</label>
							<select name="gender" class="required" style="display: block; width: 270px; height: 34px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
								<option value="">- Pilih Gender -</option>
								<option value="L">Laki-laki</option>
								<option value="P">Perempuan</option>
							</select>
						</div>
						<div class="form-group">
							<label>Jabatan</label>
							<input type="text" name="position" class="required" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
						</div>
						<div class="form-group">
							<label>No. HP</label>
							<input type="text" name="handPhone" class="required" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
						</div>
						<div class="form-group">
							<label>Status</label>
							<select name="status" class="required" style="display: block; width: 270px; height: 34px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
								<option value="">- Pilih Status -</option>
								<option value="Y" SELECTED>Aktif</option>
								<option value="N">Tidak Aktif</option>
							</select>
						</div>
						<div class="form-group">
							<label>Blokir</label>
							<select name="blocked" class="required" style="display: block; width: 270px; height: 34px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
								<option value="">- Pilih Status Blokir -</option>
								<option value="Y">Aktif</option>
								<option value="N" SELECTED>Tidak Aktif</option>
							</select>
						</div>
						<div class="form-group">
							<label>Username</label>
							<input type="text" name="username" class="required" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
						</div>
						<div class="form-group">
							<label>Password</label>
							<input type="text" name="password" value="123456" class="required" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
						</div>
						<button type="submit" class="btn btn-primary">Simpan</button>
					</form>
				
				{elseif $module == 'user' AND $act == 'edit'}
				
					<h3>Ubah Pengguna</h3>
					<form role="form" method="POST" action="users.php?module=user&act=update" id="frm_user">
						<input type="hidden" name="userID" value="{$userID}">
						<div class="form-group">
							<label>Nama Lengkap</label>
							<input type="text" name="fullName" value="{$fullName}" class="required" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
						</div>
						<div class="form-group">
							<label>Alamat</label>
							<input type="text" name="address" value="{$address}" class="required" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
						</div>
						<div class="form-group">
							<label>Jenis Kelamin</label>
							<select name="gender" class="required" style="display: block; width: 270px; height: 34px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
								<option value="">- Pilih Gender -</option>
								<option value="L" {if $gender == 'L'} SELECTED {/if}>Laki-laki</option>
								<option value="P" {if $gender == 'P'} SELECTED {/if}>Perempuan</option>
							</select>
						</div>
						<div class="form-group">
							<label>Jabatan</label>
							<input type="text" name="position" value="{$position}" class="required" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
						</div>
						<div class="form-group">
							<label>No. HP</label>
							<input type="text" name="handPhone" value="{$handPhone}" class="required" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
						</div>
						<div class="form-group">
							<label>Status</label>
							<select name="status" class="required" style="display: block; width: 270px; height: 34px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
								<option value="">- Pilih Status -</option>
								<option value="Y" {if $status == 'Y'} SELECTED {/if}>Aktif</option>
								<option value="N" {if $status == 'N'} SELECTED {/if}>Tidak Aktif</option>
							</select>
						</div>
						<div class="form-group">
							<label>Blokir</label>
							<select name="blocked" class="required" style="display: block; width: 270px; height: 34px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
								<option value="">- Pilih Status Blokir -</option>
								<option value="Y" {if $blocked == 'Y'} SELECTED {/if}>Aktif</option>
								<option value="N" {if $blocked == 'N'} SELECTED {/if}>Tidak Aktif</option>
							</select>
						</div>
						<button type="submit" class="btn btn-primary">Simpan</button>
					</form>
					
				{elseif $module == 'user' AND $act == 'reset'}
				
					<h3>Reset Password Pengguna</h3>
					<form role="form" method="POST" action="users.php?module=user&act=inreset" id="frm_reset">
						<input type="hidden" name="userID" value="{$userID}">
						<div class="form-group">
							<label>Nama Lengkap</label>
							<input type="text" value="{$fullName}" name="fullName" class="required" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;" DISABLED>
						</div>
						<div class="form-group">
							<label>Username</label>
							<input type="text" value="{$username}" name="username" class="required" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
						</div>
						<div class="form-group">
							<label>Password</label>
							<input type="text" value="123456" name="password" class="required" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
						</div>
						<button type="submit" class="btn btn-primary">Simpan</button>
					</form>
				
				{else}
			
					<a href="users.php?module=user&act=add"><button class="btn btn-primary" type="button">Tambah Pengguna</button></a>
					<h3>Manajemen Pengguna</h3>
					<div class="table-responsive">
						<table class="table table-bordered table-hover tablesorter">
							<thead>
								<tr>
									<th>No. <i class="fa fa-sort"></i></th>
									<th>Nama Lengkap <i class="fa fa-sort"></i></th>
									<th>Position <i class="fa fa-sort"></i></th>
									<th>Gender <i class="fa fa-sort"></i></th>
									<th>Hp <i class="fa fa-sort"></i></th>
									<th>Status <i class="fa fa-sort"></i></th>
									<th>Blokir <i class="fa fa-sort"></i></th>
									<th>Aksi <i class="fa fa-sort"></i></th>
								</tr>
							</thead>
							<tbody>
								{section name=dataUser loop=$dataUser}
								<tr>
									<td>{$dataUser[dataUser].no}</td>
									<td>{$dataUser[dataUser].fullName}</td>
									<td>{$dataUser[dataUser].position}</td>
									<td>{$dataUser[dataUser].gender}</td>
									<td>{$dataUser[dataUser].handPhone}</td>
									<td>{$dataUser[dataUser].status}</td>
									<td>{$dataUser[dataUser].blocked}</td>
									<td>
										<a href="users.php?module=user&act=edit&userID={$dataUser[dataUser].userID}"><button type="button" class="btn btn-success">Edit</button></a>
										<a href="users.php?module=user&act=reset&userID={$dataUser[dataUser].userID}"><button type="button" class="btn btn-info">Reset</button></a>
										<a href="users.php?module=user&act=delete&userID={$dataUser[dataUser].userID}" onclick="return confirm('Anda Yakin ingin menghapus {$dataUser[dataUser].fullName}?');"><button type="button" class="btn btn-danger">Hapus</button></a>
									</td>
								</tr>
								{/section}
							</tbody>
						</table>
					</div>
					<div id="paging">{$pageLink}</div>
				{/if}
			</div>
		</div>
	</div>
</div><!-- /#wrapper -->

{include file="footer.tpl"}