{include file="header.tpl"}

<div id="wrapper">
	
	{include file="leftMenu.tpl"}
	
	<div id="page-wrapper">
		<div class="row">
			<div class="col-lg-12">
				<ol class="breadcrumb">
					<li><a href="home.php"><i class="fa fa-dashboard"></i> Home</a></li>
					<li class="active"><i class="fa fa-caret-square-o-down"></i> Master Data</li>
					<li class="active">Pekerjaan</li>
				</ol>
				
				{if $code == '1'}
					<div class="alert alert-success alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						Data pekerjaan berhasil disimpan.
					</div>
				{/if}
				{if $code == '2'}
					<div class="alert alert-success alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						Data pekerjaan berhasil diupdate.
					</div>
				{/if}
				{if $code == '3'}
					<div class="alert alert-success alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						Data pekerjaan berhasil dihapus.
					</div>
				{/if}
			</div>
		</div><!-- /.row -->
		
		<script type='text/javascript' src='js/jquery.validate.js'></script>
			
		{literal}
			<script type='text/javascript'>
				$(document).ready(function() {
					$('#frm_job').validate({
						rules:{
							jobName: true,
							status: true
						},
						messages:{
							jobName:{
								required: "This is a required field."
							},
							status:{
								required: "This is a required field."
							}
						}
					});
				});
			</script>
		{/literal}
		
		<div class="row">
			<div class="col-lg-12">
				
				{if $module == 'job' AND $act == 'add'}
				
					<h3>Tambah Pekerjaan</h3>
					<form role="form" method="POST" action="jobs.php?module=job&act=input" id="frm_job">
						<div class="form-group">
							<label>Nama Pekerjaan</label>
							<input type="text" name="jobName" class="required" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
						</div>
						<div class="form-group">
							<label>Status</label>
							<select name="status" class="required" style="display: block; width: 270px; height: 34px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
								<option value="">- Pilih Status -</option>
								<option value="Y" SELECTED>Aktif</option>
								<option value="N">Tidak Aktif</option>
							</select>
						</div>
						<button type="submit" class="btn btn-primary">Simpan</button>
					</form>
				
				{elseif $module == 'job' AND $act == 'edit'}
				
					<h3>Ubah Pekerjaan</h3>
					<form role="form" method="POST" action="jobs.php?module=job&act=update" id="frm_job">
						<input type="hidden" name="jobID" value="{$jobID}">
						<div class="form-group">
							<label>Nama Pekerjaan</label>
							<input type="text" name="jobName" value="{$jobName}" class="required" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
						</div>
						<div class="form-group">
							<label>Status</label>
							<select name="status" class="required" style="display: block; width: 270px; height: 34px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
								<option value="">- Pilih Status -</option>
								<option value="Y" {if $status == 'Y'} SELECTED {/if}>Aktif</option>
								<option value="N" {if $status == 'N'} SELECTED {/if}>Tidak Aktif</option>
							</select>
						</div>
						<button type="submit" class="btn btn-primary">Simpan</button>
					</form>
					
				{else}
			
					<a href="jobs.php?module=job&act=add"><button class="btn btn-primary" type="button">Tambah Pekerjaan</button></a>
					<h3>Manajemen Pekerjaan</h3>
					<div class="table-responsive">
						<table class="table table-bordered table-hover tablesorter">
							<thead>
								<tr>
									<th>No. <i class="fa fa-sort"></i></th>
									<th>Nama Pekerjaan <i class="fa fa-sort"></i></th>
									<th>Status <i class="fa fa-sort"></i></th>
									<th>Aksi <i class="fa fa-sort"></i></th>
								</tr>
							</thead>
							<tbody>
								{section name=dataJob loop=$dataJob}
								<tr>
									<td>{$dataJob[dataJob].no}</td>
									<td>{$dataJob[dataJob].jobName}</td>
									<td>{$dataJob[dataJob].status}</td>
									<td>
										<a href="jobs.php?module=job&act=edit&jobID={$dataJob[dataJob].jobID}"><button type="button" class="btn btn-success">Edit</button></a>
										<a href="jobs.php?module=job&act=delete&jobID={$dataJob[dataJob].jobID}" onclick="return confirm('Anda Yakin ingin menghapus {$dataJob[dataJob].jobName}?');"><button type="button" class="btn btn-danger">Hapus</button></a>
									</td>
								</tr>
								{/section}
							</tbody>
						</table>
					</div>
					<div id="paging">{$pageLink}</div>
				{/if}
			</div>
		</div>
	</div>
</div><!-- /#wrapper -->

{include file="footer.tpl"}