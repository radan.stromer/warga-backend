{include file="header.tpl"}

<div id="wrapper">
	
	{include file="leftMenu.tpl"}
	
	<div id="page-wrapper">
		<div class="row">
			<div class="col-lg-12">
				<ol class="breadcrumb">
					<li><a href="home.php"><i class="fa fa-dashboard"></i> Home</a></li>
					<li class="active"><i class="fa fa-caret-square-o-down"></i> Manajemen Komisi</li>
					<li class="active">Komisi</li>
				</ol>
				
				{if $code == '1'}
					<div class="alert alert-success alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						Data komisi berhasil disimpan.
					</div>
				{/if}
				{if $code == '2'}
					<div class="alert alert-success alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						Data komisi berhasil diupdate.
					</div>
				{/if}
				{if $code == '3'}
					<div class="alert alert-success alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						Data komisi berhasil dihapus.
					</div>
				{/if}
			</div>
		</div><!-- /.row -->
		
		<script type='text/javascript' src='js/jquery.validate.js'></script>
			
		{literal}
			<script type='text/javascript'>
				$(document).ready(function() {
					$('#frm_komisi').validate({
						rules:{
							periode_id: true,
							nama_komisi: true,
							status: true
						},
						messages:{
							periode_id:{
								required: "This is a required field."
							},
							nama_komisi:{
								required: "This is a required field."
							},
							status:{
								required: "This is a required field."
							}
						}
					});
				});
			</script>
		{/literal}
		
		<div class="row">
			<div class="col-lg-12">
				
				{if $module == 'komisi' AND $act == 'add'}
				
					<h3>Tambah Komisi</h3>
					<form role="form" method="POST" action="komisi.php?module=komisi&act=input" id="frm_komisi">
						<div class="form-group">
							<label>Periode Komisi</label>
							<select name="periode_id" class="required" style="display: block; width: 270px; height: 34px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
								<option value="">- Pilih Periode Komisi -</option>
								{section name=dataPeriode loop=$dataPeriode}
									<option value="{$dataPeriode[dataPeriode].komisi_periode_id}">{$dataPeriode[dataPeriode].nama_periode}</option>
								{/section}
							</select>
						</div>
						<div class="form-group">
							<label>Nama Komisi</label>
							<input type="text" name="nama_komisi" placeholder="Ex: Pemuda" class="required" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
						</div>
						<div class="form-group">
							<label>Status</label>
							<select name="status" class="required" style="display: block; width: 270px; height: 34px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
								<option value="">- Pilih Status -</option>
								<option value="Y" SELECTED>Aktif</option>
								<option value="N">Tidak Aktif</option>
							</select>
						</div>
						<button type="submit" class="btn btn-primary">Simpan</button>
					</form>
				
				{elseif $module == 'komisi' AND $act == 'edit'}
				
					<h3>Ubah Komisi</h3>
					<form role="form" method="POST" action="komisi.php?module=komisi&act=update" id="frm_komisi">
						<input type="hidden" name="komisi_id" value="{$komisi_id}">
						<div class="form-group">
							<label>Periode Komisi</label>
							<select name="periode_id" class="required" style="display: block; width: 270px; height: 34px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
								<option value="">- Pilih Periode Komisi -</option>
								{section name=dataPeriode loop=$dataPeriode}
									{if $dataPeriode[dataPeriode].komisi_periode_id == $komisi_periode_id}
										<option value="{$dataPeriode[dataPeriode].komisi_periode_id}" SELECTED>{$dataPeriode[dataPeriode].nama_periode}</option>
									{else}
										<option value="{$dataPeriode[dataPeriode].komisi_periode_id}">{$dataPeriode[dataPeriode].nama_periode}</option>
									{/if}
								{/section}
							</select>
						</div>
						<div class="form-group">
							<label>Nama Komisi</label>
							<input type="text" name="nama_komisi" value="{$nama_komisi}" placeholder="Ex: Pemuda" class="required" style="display: block; width: 270px; height: 20px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
						</div>
						<div class="form-group">
							<label>Status</label>
							<select name="status" class="required" style="display: block; width: 270px; height: 34px; padding: 6px 12px; font-size: 14px; line-height: 1.428571429; color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px;">
								<option value="">- Pilih Status -</option>
								<option value="Y" {if $status == 'Y'} SELECTED {/if}>Aktif</option>
								<option value="N" {if $status == 'N'} SELECTED {/if}>Tidak Aktif</option>
							</select>
						</div>
						<button type="submit" class="btn btn-primary">Simpan</button>
					</form>
					
				{else}
			
					<a href="komisi.php?module=komisi&act=add"><button class="btn btn-primary" type="button">Tambah Komisi</button></a>
					<h3>Manajemen Komisi</h3>
					<div class="table-responsive">
						<table class="table table-bordered table-hover tablesorter">
							<thead>
								<tr>
									<th>No. <i class="fa fa-sort"></i></th>
									<th>Periode Komisi <i class="fa fa-sort"></i></th>
									<th>Nama Komisi <i class="fa fa-sort"></i></th>
									<th>Status <i class="fa fa-sort"></i></th>
									<th>Aksi <i class="fa fa-sort"></i></th>
								</tr>
							</thead>
							<tbody>
								{section name=dataKomisi loop=$dataKomisi}
								<tr>
									<td>{$dataKomisi[dataKomisi].no}</td>
									<td>{$dataKomisi[dataKomisi].nama_periode}</td>
									<td>{$dataKomisi[dataKomisi].nama_komisi}</td>
									<td>{$dataKomisi[dataKomisi].status}</td>
									<td>
										<a href="komisi.php?module=komisi&act=edit&komisi_id={$dataKomisi[dataKomisi].komisi_id}"><button type="button" class="btn btn-success">Edit</button></a>
										<a href="komisi.php?module=komisi&act=delete&komisi_id={$dataKomisi[dataKomisi].komisi_id}" onclick="return confirm('Anda Yakin ingin menghapus {$dataKomisi[dataKomisi].nama_komisi}?');"><button type="button" class="btn btn-danger">Hapus</button></a>
									</td>
								</tr>
								{/section}
							</tbody>
						</table>
					</div>
					<div id="paging">{$pageLink}</div>
				{/if}
			</div>
		</div>
	</div>
</div><!-- /#wrapper -->

{include file="footer.tpl"}