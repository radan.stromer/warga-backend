<!DOCTYPE HTML>
<html>
	<head>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<title>Aplikasi Database Warga Blok L & K Triraksa Vilage 2</title>
		<link rel="stylesheet" type="text/css" href="css/login.css" media="screen" />
		<script src="js/jquery-1.8.1.min.js" type="text/javascript"></script>
		
		<script type='text/javascript' src='js/jquery.validate.js'></script>
			
		{literal}
			<script type='text/javascript'>
				$(document).ready(function() {
					$('#frm_login').validate({
						rules:{
							userName: true,
							userPassword: true
						},
						messages:{
							userName:{
								required: "This is a required field."
							},
							userPassword:{
								required: "This is a required field."
							}
						}
					});
				});
			</script>
		{/literal}
	</head>
	<body>
		<div class="wrap">
			<div id="content">
				<div id="main">
					<center>
						<img src="images/logo-triraksa-2.jpeg" height="70"><br><br>
					</center>
					{if $code == '1'}
						<div class='messageerror'><h3>Username dan Password tidak ditemukan.</h3></div>
					{/if}
					{if $code == '2'}
						<div class='messagesuccess'><h3>Anda telah keluar [Logout].</h3></div>
					{/if}
					
					<div class="full_w">
						<form action="index.php?module=login&act=submit" method="POST" id="frm_login">
							<label for="login">Username:</label>
							<input type="text" name="userName" class="required" size="55" />
							
							<label for="pass">Password:</label>
							<input id="pass" name="userPassword" type="password" size="55" class="required" />
							<div class="sep"></div>
							
							<button type="submit" class="ok">Login</button>
						</form>
					</div>
					<div class="footer">Copyright &copy; <?php echo date('Y'); ?> RT. 09 Triraksa Vilage 2</div>
				</div>
			</div>
		</div>
	
	</body>
</html>