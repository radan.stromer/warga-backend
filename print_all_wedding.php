<?php
date_default_timezone_set("ASIA/JAKARTA");
error_reporting(0);
session_start();
// include semua file yang dibutuhkan
include "includes/connection.php";
include "includes/debug.php";
include "includes/fungsi_indotgl.php";

// jika session login kosong
if (empty($_SESSION['username']) && empty($_SESSION['password'])){
	// arahkan ke halaman login
	header("Location: index.php?code=3");
}

else{
	ob_start();
	require ("includes/html2pdf/html2pdf.class.php");
	$filename="print_all_wedding.pdf";
	$content = ob_get_clean();
	$year = date('Y');
	$month = date('m');
	$date = date('d');
	$now = date('Y-m-d');
	$date_now = tgl_indo($now);
	$day = 7;
	$nextN = mktime(0, 0, 0, date('m'), date('d') + $day, date('Y'));
	$nextDay = date('Y-m-d', $nextN);
	$noww = tgl_indo($now);
	$nextnow = tgl_indo($nextDay);
	
	
	$content = "<table width='100%' align='center' style='background: #6da4cf;'>
					<tr valign='top'>
						<td width='90' align='center' valign='middle'><img src='images/logo.jpg' width='70'></td>
						<td width='910' align='center'>
							<span style='font-size: 20px; font-weight: bold;'>W E D D I N G A N N I V E R S A R Y<br>
								Gereja Bethel Indonesia Arjawinangun
							</span><br>
							Jl. Kantor Pos No. 191 Arjawinangun - Cirebon 45162, Jawa Barat - Indonesia <br>
							Telp. (0231) 357216, Fax. (0231) 357216,
							Website: http://www.gbiawn.org, Email: info@gbiawn.org
							
						</td>
					</tr>
				</table>
				<br>
				<h4>Periode : $noww s/d $nextnow <br><br>Ulang Tahun Pernikahan</h4>
				<table border='1' cellpadding='0' cellspacing='0'>
					<tr>
						<th width='15' align='center' style='padding: 5px;'>No.</th>
						<th width='148' align='center' style='padding: 5px;'>NIK</th>
						<th width='210' align='center' style='padding: 5px;'>Nama Suami</th>
						<th width='210' align='center' style='padding: 5px;'>Nama Istri</th>
						<th width='170' align='center' style='padding: 5px;'>Tanggal Pernikahan</th>
						<th width='130' align='center' style='padding: 5px;'>Usia Nikah (Tahun)</th>
					</tr>";
					
					$day = 7;
					$now = date('Y-m-d');
					$nextN = mktime(0, 0, 0, date('m'), date('d') + $day, date('Y'));
					$nextDay = date('Y-m-d', $nextN);
					
					$j = 1;
					$queryWedding = "SELECT datediff('$now', A.tanggal_nikah) as age, A.tanggal_nikah, A.family_id, A.nik, B.full_name FROM as_family A INNER JOIN as_individu B ON B.individu_id=A.kepala_keluarga WHERE date_format(A.tanggal_nikah,'%m-%d') BETWEEN date_format('$now','%m-%d') AND date_format('$nextDay','%m-%d') ORDER BY A.tanggal_nikah ASC";
					$sqlWedding = mysqli_query($connect, $queryWedding);
					while ($dataWedding = mysqli_fetch_array($sqlWedding))
					{
						$dataChildAyah = mysqli_fetch_array(mysqli_query($connect, "SELECT B.full_name FROM as_family_child A INNER JOIN as_individu B ON B.individu_id=A.child_id WHERE A.family_id = '$dataWedding[family_id]' AND A.status = '1'"));
						$dataChildIbu = mysqli_fetch_array(mysqli_query($connect, "SELECT B.full_name FROM as_family_child A INNER JOIN as_individu B ON B.individu_id=A.child_id WHERE A.family_id = '$dataWedding[family_id]' AND A.status = '2'"));
						$age = ceil($dataWedding['age'] / 365);
						$tanggal_nikah = tgl_indo($dataWedding['tanggal_nikah']);
						
						$content .= "<tr>
										<td style='padding: 5px;'>$j</td>
										<td style='padding: 5px;' align='center'>$dataWedding[nik]</td>
										<td style='padding: 5px;'>$dataChildAyah[full_name]</td>
										<td style='padding: 5px;'>$dataChildIbu[full_name]</td>
										<td style='padding: 5px;'>$tanggal_nikah</td>
										<td style='padding: 5px;' align='center'>$age</td>
									</tr>";
						$i++;
					}
		$content .= "</table>
				<p></p>
				<table width='100%'>
					<tr>
						<td width='500'>Ket :</td>
						<td width='500' align='right'>Arjawinangun, $date_now</td>
					</tr>
				</table>
				<p>&nbsp;</p>
				
				<table width='100%'>
					<tr>
						<td width='500' align='center'></td>
						<td width='500' align='center'>GEREJA BETHEL INDONESIA<br>ARJAWINANGUN<br><br><p>&nbsp;</p><br><u>Pdt. Steve Mardianto, M.Th.</u><br>Gembala Jemaat</td>
					</tr>
				</table>
				";
	ob_end_clean();
	// conversion HTML => PDF
	try
	{
		$html2pdf = new HTML2PDF('L','A4','fr', false, 'ISO-8859-15',array(12, 12, 12, 12)); //setting ukuran kertas dan margin pada dokumen anda
		// $html2pdf->setModeDebug();
		$html2pdf->setDefaultFont('Arial');
		$html2pdf->writeHTML($content, isset($_GET['vuehtml']));
		$html2pdf->Output($filename);
	}
	catch(HTML2PDF_exception $e) { echo $e; }
}
?>