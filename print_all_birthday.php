<?php
date_default_timezone_set("ASIA/JAKARTA");
error_reporting(0);
session_start();
// include semua file yang dibutuhkan
include "includes/connection.php";
include "includes/debug.php";
include "includes/fungsi_indotgl.php";

// jika session login kosong
if (empty($_SESSION['username']) && empty($_SESSION['password'])){
	// arahkan ke halaman login
	header("Location: index.php?code=3");
}

else{
	ob_start();
	require ("includes/html2pdf/html2pdf.class.php");
	$filename="print_all_birthday.pdf";
	$content = ob_get_clean();
	$year = date('Y');
	$month = date('m');
	$date = date('d');
	$now = date('Y-m-d');
	$date_now = tgl_indo($now);
	$day = 7;
	$nextN = mktime(0, 0, 0, date('m'), date('d') + $day, date('Y'));
	$nextDay = date('Y-m-d', $nextN);
	$noww = tgl_indo($now);
	$nextnow = tgl_indo($nextDay);
	
	
	$content = "<table width='100%' align='center' style='background: #6da4cf;'>
					<tr valign='top'>
						<td width='90' align='center' valign='middle'><img src='images/logo.jpg' width='70'></td>
						<td width='910' align='center'>
							<span style='font-size: 20px; font-weight: bold;'>JEMAAT YANG BERULANG TAHUN<br>
								Gereja Bethel Indonesia Arjawinangun
							</span><br>
							Jl. Kantor Pos No. 191 Arjawinangun - Cirebon 45162, Jawa Barat - Indonesia <br>
							Telp. (0231) 357216, Fax. (0231) 357216,
							Website: http://www.gbiawn.org, Email: info@gbiawn.org
							
						</td>
					</tr>
				</table>
				<br>
				<h4>Periode : $noww s/d $nextnow <br><br>Jemaat yang Berulang Tahun</h4>
				<table border='1' cellpadding='0' cellspacing='0'>
					<tr>
						<th width='15' align='center' style='padding: 5px;'>No.</th>
						<th width='70' align='center' style='padding: 5px;'>No Induk</th>
						<th width='230' align='center' style='padding: 5px;'>Nama Lengkap</th>
						<th width='30' align='center' style='padding: 5px;'>JK</th>
						<th width='240' align='center' style='padding: 5px;'>Tempat, Tanggal Lahir</th>
						<th width='90' align='center' style='padding: 5px;'>Usia (Tahun)</th>
						<th width='60' align='center' style='padding: 5px;'>Jemaat</th>
						<th width='100' align='center' style='padding: 5px;'>Status Babtis</th>
					</tr>";
					
					$i = 1;
					$queryBirthday = "SELECT datediff('$now', date_of_birth) as age, status_babtis, place_of_birth, no_induk, full_name, date_of_birth, status, gender FROM as_individu WHERE date_format(date_of_birth,'%m-%d') BETWEEN date_format('$now','%m-%d') AND date_format('$nextDay','%m-%d') ORDER BY date_of_birth ASC";
					$sqlBirthday = mysqli_query($connect, $queryBirthday);
					while ($dataBirthday = mysqli_fetch_array($sqlBirthday))
					{
						$age = ceil($dataBirthday['age'] / 365);
						$date_of_birth = tgl_indo($dataBirthday['date_of_birth']);
						
						if ($dataBirthday['status_babtis'] == 'B'){
							$status_babtis = "Belum Dibabtis";
						}
						elseif ($dataBirthday['status_babtis'] == 'S'){
							$status_babtis = "Sudah Dibabtis";
						}
						
						$content .= "<tr>
										<td style='padding: 5px;'>$i</td>
										<td style='padding: 5px;' align='center'>$dataBirthday[no_induk]</td>
										<td style='padding: 5px;'>$dataBirthday[full_name]</td>
										<td style='padding: 5px;' align='center'>$dataBirthday[gender]</td>
										<td style='padding: 5px;'>$dataBirthday[place_of_birth], $date_of_birth</td>
										<td style='padding: 5px;' align='center'>$age</td>
										<td style='padding: 5px;' align='center'>$dataBirthday[status]</td>
										<td style='padding: 5px;' align='center'>$status_babtis</td>
									</tr>";
						$i++;
					}
		$content .= "</table>
				<br>
				<table width='100%'>
					<tr>
						<td width='500'>Ket :</td>
						<td width='500' align='right'>Arjawinangun, $date_now</td>
					</tr>
				</table>
				<p>&nbsp;</p>
				
				<table width='100%'>
					<tr>
						<td width='500' align='center'></td>
						<td width='500' align='center'>GEREJA BETHEL INDONESIA<br>ARJAWINANGUN<br><br><p>&nbsp;</p><br><u>Pdt. Steve Mardianto, M.Th.</u><br>Gembala Jemaat</td>
					</tr>
				</table>
				";
	ob_end_clean();
	// conversion HTML => PDF
	try
	{
		$html2pdf = new HTML2PDF('L','A4','fr', false, 'ISO-8859-15',array(12, 12, 12, 12)); //setting ukuran kertas dan margin pada dokumen anda
		// $html2pdf->setModeDebug();
		$html2pdf->setDefaultFont('Arial');
		$html2pdf->writeHTML($content, isset($_GET['vuehtml']));
		$html2pdf->Output($filename);
	}
	catch(HTML2PDF_exception $e) { echo $e; }
}
?>