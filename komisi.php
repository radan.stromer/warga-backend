<?php
// include header
include "header.php";
// set the tpl page
$page = "komisi.tpl";

// if session is null, showing up the text and exit
if ($_SESSION['username'] == '' && $_SESSION['password'] == '')
{
	// show up the text and exit
	echo "You have not authorization for access the modules.";
	exit();
}

else 
{
	// get variable
	$module = $_GET['module'];
	$act = $_GET['act'];
	
	// if module is komisi and action is input
	if ($module == 'komisi' && $act == 'input')
	{
		// change each value to variable name
		$createdDate = date('Y-m-d H:i:s');
		$periode_id = $_POST['periode_id'];
		$nama_komisi = $_POST['nama_komisi'];
		$status = $_POST['status'];
		$userID = $_SESSION['userID'];
		
		// save into database
		$queryKomisi = "INSERT INTO as_komisi (komisi_periode_id,nama_komisi,status,created_date,created_userid,modified_date,modified_userid)
		VALUES('$periode_id','$nama_komisi','$status','$created_date','$userID','','')";
		mysqli_query($connect, $queryKomisi);
		
		// redirect to the main komisi page
		header("Location: komisi.php?code=1");
	} // close bracket
	
	// if module is komisi and action is add
	elseif ($module == 'komisi' && $act == 'add')
	{
		$queryPeriode = "SELECT * FROM as_komisi_periode WHERE status = 'Y' ORDER BY nama_periode ASC";
		$sqlPeriode = mysqli_query($connect, $queryPeriode);
		
		// fetch data
		while ($dtPeriode = mysqli_fetch_array($sqlPeriode))
		{
			$dataPeriode[] = array(	'komisi_periode_id' => $dtPeriode['komisi_periode_id'],
									'nama_periode' => $dtPeriode['nama_periode']);
		}
		
		// assign to the tpl
		$smarty->assign("dataPeriode", $dataPeriode);
	} // close bracket
	
	// if module is komisi and action is edit
	elseif ($module == 'komisi' && $act == 'edit')
	{
		$queryPeriode = "SELECT * FROM as_komisi_periode WHERE status = 'Y' ORDER BY nama_periode ASC";
		$sqlPeriode = mysqli_query($connect, $queryPeriode);
		
		// fetch data
		while ($dtPeriode = mysqli_fetch_array($sqlPeriode))
		{
			$dataPeriode[] = array(	'komisi_periode_id' => $dtPeriode['komisi_periode_id'],
									'nama_periode' => $dtPeriode['nama_periode']);
		}
		
		// assign to the tpl
		$smarty->assign("dataPeriode", $dataPeriode);
		
		// get the komisi ID
		$komisi_id = $_GET['komisi_id'];
		
		$queryKomisi = "SELECT * FROM as_komisi WHERE komisi_id = '$komisi_id'";
		$sqlKomisi = mysqli_query($connect, $queryKomisi);
		
		// fetch data
		$dataKomisi = mysqli_fetch_array($sqlKomisi);
		
		// assign data to the tpl
		$smarty->assign("komisi_id", $dataKomisi['komisi_id']);
		$smarty->assign("komisi_periode_id", $dataKomisi['komisi_periode_id']);
		$smarty->assign("nama_komisi", $dataKomisi['nama_komisi']);
		$smarty->assign("status", $dataKomisi['status']);
	} //close bracket
	
	// if module is komisi and action is update
	elseif ($module == 'komisi' && $act == 'update')
	{
		// change each value to variable name
		$modifiedDate = date('Y-m-d H:i:s');
		$periode_id = $_POST['periode_id'];
		$nama_komisi = $_POST['nama_komisi'];
		$status = $_POST['status'];
		$komisi_id = $_POST['komisi_id'];
		$userID = $_SESSION['userID'];
		
		// save into the database
		$queryKomisi = "UPDATE as_komisi SET komisi_periode_id = '$periode_id', nama_komisi = '$nama_komisi', status = '$status', modified_date = '$modifiedDate', modified_userid = '$userID' WHERE komisi_id = '$komisi_id'";
		mysqli_query($connect, $queryKomisi);
		
		// redirect to the main komisi page
		header("Location: komisi.php?code=2");
	} // close bracket
	
	// if module is komisi and action is delete
	elseif ($module == 'komisi' && $act == 'delete')
	{
		// get komisi id
		$komisi_id = $_GET['komisi_id'];
		
		// delete from the table
		$queryKomisi = "DELETE FROM as_komisi WHERE komisi_id = '$komisi_id'";
		mysqli_query($connect, $queryKomisi);
		
		// delete from the table
		$queryAnggota = "DELETE FROM as_komisi_anggota WHERE komisi_id = '$komisi_id'";
		mysqli_query($connect, $queryAnggota);
		
		// redirect to the main komisi page
		header("Location: komisi.php?code=3");
	} // close bracket
	
	// default
	else 
	{
		// create new object pagination
		$p = new PaginationKomisi;
		// limit 10 data for page
		$limit  = 10;
		$position = $p->searchPosition($limit);
		// showing up komisi data
		$queryKomisi = "SELECT A.nama_komisi, A.status, A.komisi_id, B.nama_periode FROM as_komisi A INNER JOIN as_komisi_periode B ON B.komisi_periode_id=A.komisi_periode_id ORDER BY B.nama_periode,A.komisi_id DESC LIMIT $position, $limit";
		$sqlKomisi = mysqli_query($connect, $queryKomisi);
		
		$i = 1 + $position;
		// fetch data
		while ($dtKomisi = mysqli_fetch_array($sqlKomisi))
		{
			// save data into array
			$dataKomisi[] = array(	'komisi_id' => $dtKomisi['komisi_id'],
									'nama_periode' => $dtKomisi['nama_periode'],
									'nama_komisi' => $dtKomisi['nama_komisi'],
									'status' => $dtKomisi['status'],
									'no' => $i
									);
			$i++;
		}
		
		// count data
		$queryCountKomisi = "SELECT A.nama_komisi, A.status, A.komisi_id, B.nama_periode FROM as_komisi A INNER JOIN as_komisi_periode B ON B.komisi_periode_id=A.komisi_periode_id";
		$sqlCountKomisi = mysqli_query($connect, $queryCountKomisi);
		$amountData = mysqli_num_rows($sqlCountKomisi);
		
		$amountPage = $p->amountPage($amountData, $limit);
		$pageLink = $p->navPage($_GET['page'], $amountPage);
		
		$smarty->assign("pageLink", $pageLink);
		// assign to the tpl
		$smarty->assign("dataKomisi", $dataKomisi);
		
	} // close bracket
	
	// assign code to the tpl
	$smarty->assign("code", $_GET['code']);
	$smarty->assign("module", $_GET['module']);
	$smarty->assign("act", $_GET['act']);
	
} // close bracket

// include footer
include "footer.php";
?>